/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.lucsim.stat;

import java.util.Iterator;
import java.util.List;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author gvuidel
 */
public abstract class DataExtractor implements Iterator<Instance>{
    
    public abstract Instances getFullDataset();
    
    public abstract Instances getEmptyDataset();
    
    public abstract double getWeight();
    
    public abstract StateLayer getFormerLayer();

    public abstract List<Layer> getLayers();

    public abstract State getTargetState();

    public abstract String getFunction();

    public abstract void reset();
}
