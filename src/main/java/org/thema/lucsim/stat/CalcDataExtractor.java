/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.stat;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import org.thema.lucsim.engine.Cell;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.engine.States;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Util class gathering the different methods used to extract data from two 
 * different layers.
 */
public final class CalcDataExtractor extends DataExtractor {

    private StateLayer formerLayer, modernLayer;
    private List<Layer> layers;
    private String function;
    private int radiusMin, radiusMax, step;
    private State targetState;
    
    private double weight;
    
    private Instances dataset;
    private List<Attribute> attributes;
    private NeighborhoodFunction neighborFunction;
    private int[] stateValue;
    
    private int index = 0;
    
    /**
     * Creates a new dataextractor with the given paramters for 
     * the future dataset.
     * @param formerLayer former Layer chosen by the user
     * @param modernLayer newer Layer chosen by the user
     * @param layers list, eventually empty, of the additional layers
     * @param function String representing the function chosen by the user
     * @param ring whether to use ring or plain neighborhoods
     * @param radiusMin minimal distance between the current cell under
     * calculation and the cells taken into consideration
     * @param radiusMax maximal distance between the current cell under
     * calculation and the cells taken into consideration
     * @param step step to use to grow the neighborhood size
     * @param targetState the target state or null
     * @param weight weight to apply to the minority data class
     */
    public CalcDataExtractor(StateLayer formerLayer, StateLayer modernLayer, List<Layer> layers, 
            String function, boolean ring, int radiusMin, int radiusMax, int step, State targetState, double weight) {
        this.formerLayer = formerLayer;
        this.modernLayer = modernLayer;
        this.layers = layers;
        this.function = function;
        this.radiusMin = radiusMin;
        this.radiusMax = radiusMax;
        this.step = step;
        this.targetState = targetState;
        this.weight = weight;
        
        neighborFunction = functionStringToLogic(function, ring);
        
        stateValue = fillStateValuesTab(formerLayer.getStates(), formerLayer.listStateString());
        
        buildAttributes();
        initDataset();
    }

    @Override
    public StateLayer getFormerLayer() {
        return formerLayer;
    }

    @Override
    public List<Layer> getLayers() {
        return layers;
    }

    @Override
    public State getTargetState() {
        return targetState;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    @Override
    public String getFunction() {
        return function;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    /**
     * Builds the list of attributes for 
     * the future dataset.
     */ 
    private void buildAttributes() {
        attributes = new ArrayList<>();
        ArrayList<String> stateNames = formerLayer.listStateString();
        // adding the first attribute: the state in the former layer
        // discretization is needed to avoid splits like <layer> < 3.14
        attributes.add(new Attribute(formerLayer.getName(), formerLayer.listStateString()));

        // if there is additional layers, we add its names as an attribute
        layers.stream().forEach((layer) -> {
            attributes.add(new Attribute(layer.getName()));
        });

        // adding every attribute to the attribute list
        // attributes will look like stateX_<radiusMin>px, 
        // stateY_<radiusMin>px... stateY_<radiusMax>px
        stateNames.stream().forEach((stateName) -> {
            for (int i = radiusMin; i <= radiusMax; i += step) {
                attributes.add(new Attribute(function + "_" + stateName + "_" + i + "px"));
            }
        });

        // adding the last attribute, which is by convention
        // the class attribute
        List<String> transValues = new ArrayList<>();
        if(targetState == null) {
            transValues.addAll(modernLayer.listStateString());
        } else {
            transValues.add("false");
            transValues.add("true");
        }
        attributes.add(new Attribute(targetState.getName() + "(" + weight + ")", transValues));
    }

    /**
     * Fills a tab of integers with the different values of the states.
     * @param states list of the states
     */
    private static int[] fillStateValuesTab(States states, List<String> stateNames) {
        int [] values = new int[stateNames.size()];

        int i = 0;
        for (String s : stateNames) {
            values[i++] = states.getState(s).getValue();
        }
        
        return values;
    }

    @Override
    public Instances getFullDataset() {
        if(dataset.isEmpty()) {
            buildFullDataset();
        }
        return dataset;
    }
    
    @Override
    public Instances getEmptyDataset() {
        if(dataset.isEmpty()) {
            return dataset;
        } else {
            return new Instances(dataset, 0, 0);
        }
    }
    
    public void buildFullDataset() {
        final int width = formerLayer.getWidth();
        final int height = formerLayer.getHeight();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(formerLayer.getElement(j, i) != States.NODATA_VAL) {
                    Instance instance = buildInstance(j, i);
                    dataset.add(instance);
                }
            }

        }
    }

    /**
     * Builds an empty new dataset from a given list of attributes.
     * @param attributes list of attributes
     * @return empty new dataset
     */
    private void initDataset() {
        dataset = new Instances(
                "trainingData",
                (ArrayList<Attribute>)getAttributes(),
                0);

        // setting the class index, which is by convention 
        // the last attribute
        dataset.setClassIndex(dataset.numAttributes() - 1);
    }

    /**
     * Builds a lambda-expression in function of the given parameters to 
     * retrieve a cell's neighborhood data, following the parameters chosen by 
     * the user.
     * @param function String representing the function chosen by the user
     * @param ring whether to use ring or plain neighborhoods
     * @return logic to retrieve neighborhood data
     */
    private static NeighborhoodFunction functionStringToLogic(String function, boolean ring) {
        NeighborhoodFunction neighborhoodFunction;
        // setting the right lambda function to use depending of the neighboring 
        // function chosen by the user
        if (ring) {
            switch (function) {
                case "nbCellSq":
                    neighborhoodFunction = (cell, layer, state, size, previous) -> {
                        return (cell.nbCellSq(layer, state, size) - previous);
                    };
                    break;
                case "nbCellCir":
                    neighborhoodFunction = (cell, layer, state, size, previous) -> {
                        return (cell.nbCellCir(layer, state, size) - previous);
                    };
                    break;
                default:
                    throw new IllegalArgumentException("Invalid function name: " + function);
            }
        } 
        else {
            switch (function) {
                case "nbCellSq":
                    neighborhoodFunction = (cell, layer, state, size, previous) -> {
                        return cell.nbCellSq(layer, state, size);
                    };
                    break;
                case "nbCellCir":
                    neighborhoodFunction = (cell, layer, state, size, previous) -> {
                        return cell.nbCellCir(layer, state, size);
                    };
                    break;
                default:
                    throw new IllegalArgumentException("Invalid function name: " + function);
            }
        }

        return neighborhoodFunction;
    }

    /**
     * Builds the instance of data corresponding to the (j, i) cell of the 
     * former layer, taking different parameters chosen by the user into 
     * account.
     * Built instance is supposed to be treated, and thus only the values 
     * different from 0 for the classic attributes and "false" for the class 
     * attribute will be displayed, along with their index in the attribute 
     * list.
     * @param x horizontal position of the cell
     * @param y vertical position of the cell
     * @return instance for the (x, y) cell of the former layer
     */
    private Instance buildInstance(int x, int y) {
        int currentAttribute = 0;

        Instance instance = new DenseInstance(attributes.size());
        instance.setDataset(dataset);

        Cell cell = new Cell(formerLayer, new Point(x, y));

        // setting the former layer cell's state value
        instance.setValue(
                currentAttribute++,
                formerLayer.getStates()
                .getState(
                        (int) formerLayer.getElement(x, y))
                .getName());
        

        for (Layer layer : layers) {
            instance.setValue(currentAttribute++,
                    layer.getElement(x, y));
        }

        // the previous result is affected to a variable to avoid a double
        // calculation each time
        int previous, actual;
        
        // setting every attribute value
        for (int state : stateValue) {
            previous = 0;
            for (int r = radiusMin; r <= radiusMax; r += step) {
                actual = neighborFunction.fetchData(cell, formerLayer, state, r, previous);
                instance.setValue(currentAttribute++, actual);
                
                // the void part of the current ring is equal to the sum of the
                // previous rings (a plain square)
                previous += actual;
            }
        }
        
        if(targetState == null) {
            instance.setValue(
                currentAttribute,
                modernLayer.getStates()
                .getState(
                        (int) modernLayer.getElement(x, y))
                .getName());
            instance.setWeight(1.0);
        } else {
            // setting the last attribute value (true or false):
            // - true if the cell had a transition towards the target state
            // - false otherwise
            if ((formerLayer.getElement(x, y) != targetState.getValue()) && (modernLayer.getElement(x, y) == targetState.getValue())) {
                instance.setValue(currentAttribute, "true");
                instance.setWeight(weight);
            } else {
                instance.setValue(currentAttribute, "false");
                instance.setWeight(1.0);
            }
        }
        
        return instance;
    }

    @Override
    public void reset() {
        index = 0;
    }

    @Override
    public boolean hasNext() {
        if(!dataset.isEmpty()) {
            return index < dataset.size();
        }
        
        final int w = formerLayer.getWidth();
        final int h = formerLayer.getHeight();
        while(index < w*h && formerLayer.getElement(index%w, index/w) == States.NODATA_VAL) {
            index++;
        }
        return index < w*h;
    }

    @Override
    public Instance next() {
        if(!dataset.isEmpty()) {
            return dataset.instance(index++);
        }
        
        final int w = formerLayer.getWidth();
        Instance instance = buildInstance(index%w, index/w);
        index++;
        return instance;
    }


    /**
     * Interface used in the lambda-expression used to retrieve neighborhood
     * data.
     */
    private interface NeighborhoodFunction {

        /**
         * Signature of the method to use to retrieve neighborhood data.
         * @param cell cell to retrieve neighborhood data from
         * @param layer layer containing the cell
         * @param state state targeted for this neighborhood data calculation
         * @param size size of the neighborhood 
         * @param previous value of the previous result; substracted if the user
         * chose to use a ring neighborhood, ignored otherwise
         * @return number returned by the lambda-expression with these given
         * parameters
         * @see Cell#nbCellSq(org.thema.lucsim.engine.Layer, double, int) 
         * @see Cell#nbCellCir(org.thema.lucsim.engine.Layer, double, int) 
         */
        public int fetchData(Cell cell, StateLayer layer, int state, int size, int previous);
    }
    
    /**
     * Estimate the weight of cell changing in the state target for having the same sum between these cells and cells that do not change
     * @param layer1 the start layer
     * @param layer2 the final layer
     * @param target the target state
     * @return the weight
     */
    public static double estimWeight(StateLayer layer1, StateLayer layer2, State target) {
        double targetStateValue = target.getValue();
        double transU = 0;
        double total = 0;

        int width = layer1.getWidth();
        int height = layer1.getHeight();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((layer2.getElement(j, height - (i + 1)) == targetStateValue)
                    && (layer1.getElement(j, height - (i + 1)) != targetStateValue)) {
                    transU++;
                }
                total++;
            }
        }
        
        return transU == 0 ? 1 : ((total - transU) / transU);
    }
}
