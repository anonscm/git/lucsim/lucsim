/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.lucsim.stat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

/**
 *
 * @author gvuidel
 */
public class FileDataExtractor extends DataExtractor {
    private File file;
    private Project project;
    
    private StateLayer formerLayer;
    private List<Layer> layers;
    private String function;
    private State targetState;
    private double weight = 1.0;
    
    private Instances dataset;
    
    private int index = 0;
    
    public FileDataExtractor(File file, Project project) {
        this.file = file;
        this.project = project;
    }
    
    public void loadDataset() throws IOException {
        CSVLoader csvLoader = new CSVLoader();
        csvLoader.setSource(file);
        dataset = csvLoader.getDataSet();
        
        formerLayer = project.getStateLayer(dataset.attribute(0).name());
        layers = new ArrayList<>();
        int indAttr = 1;
        while(indAttr < dataset.numAttributes()-1 && !dataset.attribute(indAttr).name().endsWith("px")) {
            layers.add(project.getLayer(dataset.attribute(indAttr).name()));
            indAttr++;
        }
        String attr = dataset.attribute(indAttr).name();
        if(attr.endsWith("px")) {
            function = attr.split("_")[0];
        }
        
        dataset.setClassIndex(dataset.numAttributes() - 1);
        attr = dataset.classAttribute().name();
        if(attr.contains("(")) {
            weight = Double.parseDouble(attr.split("\\(")[1].replace(")", ""));
        }
        targetState = project.getStates().getState(attr.split("\\(")[0]);
        
        for (int i = 0; i < dataset.numInstances(); i++) {
            if (dataset.instance(i).classValue() == 1) {
                dataset.instance(i).setWeight(weight);
            }
        }
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public Instances getFullDataset() {
        if(dataset == null) {
            try {
                loadDataset();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return dataset;
    }
    
    @Override
    public Instances getEmptyDataset() {
        return new Instances(getFullDataset(), 0, 0);
    }

    @Override
    public boolean hasNext() {
        return index < dataset.size();
    }


    @Override
    public Instance next() {
        return dataset.instance(index++);
    }

    @Override
    public void reset() {
        index = 0;
    }
    
    @Override
    public StateLayer getFormerLayer() {
        return formerLayer;
    }

    @Override
    public List<Layer> getLayers() {
        return layers;
    }

    @Override
    public State getTargetState() {
        return targetState;
    }

    @Override
    public String getFunction() {
        return function;
    }

}
