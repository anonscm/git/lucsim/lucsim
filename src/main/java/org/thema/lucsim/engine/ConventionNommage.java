/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class gathering the methods to assert that naming conventions are respected.
 * @author alexis
 */
public class ConventionNommage {

    public static class BadNameException extends Exception {

        public BadNameException(String message) {
            super(message);
        }
        
    }
    
    /**
     * Checks if a given name is suitable to save a given current project.
     * @param name name to check
     * @param project current project
     * @throws BadNameException
     */
    public static void checkName(String name, Project project) throws BadNameException {

        // is not a layer name
        if(project.isLayerExists(name)) {
            throw new BadNameException("The layer name already exist");
        }
        
        // is not a state name
        if(getStateNames(project).contains(name)) {
            throw new BadNameException("The state name already exist");
        }

        // is not a reserved keyword (function, operator...)
        List<String> keywords = Arrays.asList("nbCellNeu", "nbCellMoo", "nbCellSq", "nbCellCir", "pCellSq", "pCellCir", "nbCellAng", "cellDuration", "North", "South", "East", "West", "NorthEast", "NorthWest", "SouthEast", "SouthWest", "nbCell", "pCell", "step", "nbSameStep", "nbcellneu", "nbcellmoo", "nbcellsq", "nbcellcir", "pcellsq", "pcellcir", "nbcellang", "cellduration", "north", "south", "east", "west", "northeast", "northwest", "southeast", "southwest", "nbcell", "pcell", "nbsamestep", "not", "NOT", "or", "OR", "xor", "XOR", "and", "AND", "true", "false", "TRUE", "FALSE", "mod", "MOD", "random", "int", "round", "repeat", "while", "if");
        if(keywords.contains(name)) {
            throw new BadNameException("The name is a reserved keyword");
        }

        // is longer than 2 characters
        if (name.length() < 3) {
            throw new BadNameException("The name is too short");
        }

        // does not have a number as a first character
        if (name.charAt(0) >= 48 && name.charAt(0) <= 57) {
            throw new BadNameException("The name begins with a number");
        }


        // is an alphanumeric character
        if(!name.matches("(\\w|_)+")) {
            throw new BadNameException("The name contains special characters");
        }
    }

    
    /**
     * Returns a list of String containing the different states' names of a
     * project.
     * @param project project to query
     * @return list of String containing the different states' names of a 
     * project
     */
    public static List<String> getStateNames(Project project) {
        List<String> names = new ArrayList<>();
        names.add("all");
        for(State s : project.getStates().getStates()) {
            names.add(s.getName());
        }
        
        return names;
    }
}
