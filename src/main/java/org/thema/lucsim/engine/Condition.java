/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import org.thema.lucsim.parser.AnalyzerConstants;
import org.thema.lucsim.parser.Token;

/**
 * Represents a condition of a transition rule.
 * @see Rule
 * @author gvuidel
 */
public class Condition {

    /**
     * Cette liste de tokens représente la condition. Chaque token de la pile
     * représente un 'mot' ou symbole. Cette pile sera évaluer lors de la
     * simulation pour chaque cellules pour en déduire si la cellule change
     * détat. Cette liste de token est plus pratique que la condition en chaine
     * de caractere car un token peut représenter plusieurs symbole et les
     * probleme d'espace sont gerés.
     */
    private Stack<Token> tokenListPostfix;

    /**
     * Creates new condition with a list of tokens infixed mode
     *
     * @param tokens list of tokens to create the condition from
     */
    public Condition(List<Token> tokens) {
        this(tokens, false);
    }

    /**
     * Constructs a copy a given condition.
     * @param condition condition to copy
     */
    public Condition(Condition condition) {
        this.tokenListPostfix = new Stack<>();
        condition.getTokenPost().stream().forEach(t -> tokenListPostfix.push(t));
    }
    
    /**
     * Creates a new condition with a list of tokens in postfixed notation.
     * @param tokens list of tokens to create the condition from
     * @param isListPostfixed whether the list of tokens is in postfixed 
     * notation
     */
    public Condition(List<Token> tokens, boolean isListPostfixed) {
        if (isListPostfixed) {
            Stack<Token> stack = new Stack<>();
            stack.addAll(tokens);
            this.tokenListPostfix = stack;
        } else {
            this.tokenListPostfix = convertInfixToPostfix(tokens);
        }
    }
    
    /**
     * Adds an and condition, represented as a list of tokens in infixed 
     * notation, to the condition.
     * @param tokens list of tokens to add to the condition
     */
    public void addAndCondition(List<Token> tokens) {
        addAndCondition(tokens, false);
    }
    
    /**
     * Adds an and condition, represented as a list of tokens in postfixed 
     * notation, to the condition.
     * @param tokens list of tokens to add to the condition
     * @param isListPostFixed whether the list of tokens is in postfixed 
     * notation
     */
    public void addAndCondition(List<Token> tokens, boolean isListPostFixed) {
        if (isListPostFixed) {
            tokenListPostfix.addAll(tokens);
        } else {
            tokenListPostfix.addAll(convertInfixToPostfix(tokens));
        }
        tokenListPostfix.add(new Token(AnalyzerConstants.AND, "and"));
    }
    
    /**
     * Adds a given condition as an and condition to the condition.
     * @param condition condition to add 
     */
    public void addAndCondition(Condition condition) {
        condition.getTokenPost().stream().forEach(t -> tokenListPostfix.push(t));
        tokenListPostfix.push(new Token(AnalyzerConstants.AND, "and"));
    }
    
    /**
     * Adds an or condition, represented as a list of tokens in infixed 
     * notation, to the condition.
     * @param tokens list of tokens to add to the condition
     */
    public void addOrCondition(List<Token> tokens) {
        addOrCondition(tokens, false);
    }
    
    /**
     * Adds an or condition, represented as a list of tokens in postfixed 
     * notation, to the condition.
     * @param tokens list of tokens to add to the condition
     * @param isListPostFixed whether the list of tokens is in postfixed 
     * notation
     */
    public void addOrCondition(List<Token> tokens, boolean isListPostFixed) {
        if (isListPostFixed) {
            tokenListPostfix.addAll(tokens);
        } else {
            tokenListPostfix.addAll(convertInfixToPostfix(tokens));
        }
        tokenListPostfix.add(new Token(AnalyzerConstants.OR, "or"));
    }
    
    /**
     * Adds a given condition as an or condition to the condition.
     * @param condition condition to add 
     */
    public void addOrCondition(Condition condition) {
        condition.getTokenPost().stream().forEach(t -> tokenListPostfix.push(t));
        tokenListPostfix.add(new Token(AnalyzerConstants.OR, "or"));
    }

    /**
     * Returns the list of tokens representing the condition as a stack in 
     * postfixed notation.
     * This returned list is a copy, and thus can be modified without any impact
     * on the condition.
     * @return list of tokens representing the condition
     */
    public Deque<Token> getTokenPost() {
        return new LinkedList<>(tokenListPostfix);
    }

    @Override
    public String toString() {
        return genString(getTokenPost());
    }

    /**
     * Generate a string representation of the condition from a given postfixed 
     * list of tokens.
     * @param postfix postfixed list of tokens
     * @return string representation of the condition
     */
    private static String genString(Deque<Token> postfix) {
        String a, b, c, d;
        Token token = postfix.pollLast();

        switch (token.kind) { // the token is removed

            /**
             * *************** Constants *************
             */
            case AnalyzerConstants.INTEGER:
            case AnalyzerConstants.FLOAT:
            case AnalyzerConstants.TRUE:
            case AnalyzerConstants.FALSE:
                return token.image;

            /**
             * *************** Units *************
             */
            case AnalyzerConstants.METER:
            case AnalyzerConstants.DEGREE:
            case AnalyzerConstants.PERCENT:
                return genString(postfix) + token.image;

            /**
             * ************** Unary logical operators **********
             */
            case AnalyzerConstants.NOT:
                return "not(" + genString(postfix) + ")";

            /**
             * ************* Binary operators ****************
             */
            case AnalyzerConstants.OR:
            case AnalyzerConstants.AND:
            case AnalyzerConstants.XOR:
            case AnalyzerConstants.EQUAL:
            case AnalyzerConstants.GEQ:
            case AnalyzerConstants.LEQ:
            case AnalyzerConstants.NEQ:
            case AnalyzerConstants.GREAT:
            case AnalyzerConstants.LESS:
            case AnalyzerConstants.PLUS:
            case AnalyzerConstants.MINUS:
            case AnalyzerConstants.TIMES:
            case AnalyzerConstants.DIVIDE:
            case AnalyzerConstants.MODULO:
                boolean parB = postfix.getLast().getPriority() < token.getPriority();
                b = genString(postfix);
                if(parB) {
                    b = "(" + b + ")";
                }
                boolean parA = postfix.getLast().getPriority() < token.getPriority();
                a = genString(postfix);
                if(parA) {
                    a = "(" + a + ")";
                }
                return a + " " + token.image + " " + b;

            /**
             * ************** Functions  **************
             */
            case AnalyzerConstants.CELLDURATION:
            case AnalyzerConstants.STEP:
            case AnalyzerConstants.NBSAMESTEP:
            case AnalyzerConstants.RANDOM:
                return token.image + "()";

            case AnalyzerConstants.NBCELLDURATION:
            case AnalyzerConstants.INT:
            case AnalyzerConstants.ROUND:
            case AnalyzerConstants.NBCELLNEU:
            case AnalyzerConstants.NBCELLMOO:
            case AnalyzerConstants.NBCELL:
            case AnalyzerConstants.PCELL:
                return token.image + "(" + genString(postfix) + ")";

            case AnalyzerConstants.NBCELLSQ:
            case AnalyzerConstants.NBCELLCIR:
            case AnalyzerConstants.PCELLSQ:
            case AnalyzerConstants.PCELLCIR:
                b = genString(postfix);
                a = genString(postfix);
                return token.image + "(" + a + "," + b + ")";

            case AnalyzerConstants.NBCELLANG:
            case AnalyzerConstants.PCELLANG:
                d = genString(postfix);
                c = genString(postfix);
                b = genString(postfix);
                a = genString(postfix);
                return token.image + "(" + a + ", " + b + ", " + c + ", " + d + ")";

            case AnalyzerConstants.NORTH:
            case AnalyzerConstants.SOUTH:
            case AnalyzerConstants.EAST:
            case AnalyzerConstants.WEST:
            case AnalyzerConstants.NORTH_EAST:
            case AnalyzerConstants.NORTH_WEST:
            case AnalyzerConstants.SOUTH_EAST:
            case AnalyzerConstants.SOUTH_WEST:
                return token.image + "()";

            /**
             * ***************** Layers ***************
             */
            case AnalyzerConstants.INTERRO:
                Token layer = postfix.pollLast();
                return layer.image + "?" + genString(postfix);

            case AnalyzerConstants.NAME:
                return token.image;

            case AnalyzerConstants.DOT:
                layer = postfix.pollLast();
                return layer + "." + genString(postfix);

            default:
                throw new IllegalArgumentException("Unknown token " + token.toString());
        }

    }

    /**
     * Converts a given list of tokens in infixed notation to the postfixed 
     * notation.
     * @param infix list of tokens to convert
     * @return postfixed version of the given infixed list of tokens
     */
    public static Stack<Token> convertInfixToPostfix(List<Token> infix) {

        Stack<Token> stack = new Stack<>();
        Stack<Token> post = new Stack<>();

        for(Token token : infix) {
            try {
                Double.parseDouble(token.toString());
                post.push(token);
            } catch (NumberFormatException nfe) {
                // OPENING BRACKET
                if ("(".equals(token.toString())) {
                    stack.push(token);
                } // NAME (layer name)
                else if (token.kind == AnalyzerConstants.NAME) {
                    post.push(token);
                } // OPERATOR, COMPARATOR, LOGICAL OPERATOR
                else if ((token.kind >= AnalyzerConstants.PLUS && token.kind <= AnalyzerConstants.MODULO) || (token.kind >= AnalyzerConstants.EQUAL && token.kind <= AnalyzerConstants.LESS) || (token.kind >= AnalyzerConstants.OR && token.kind <= AnalyzerConstants.NOT)) {
                    while ((stack.size() > 0) && (!"(".equals(stack.peek().toString()))) {
                        if (comparePrecedence(stack.peek(), token)) {
                            post.push(stack.pop());
                        } else {
                            break;
                        }
                    }
                    stack.push(token);
                } // CLOSING BRACKET
                else if (")".equals(token.toString())) {
                    while ((stack.size() > 0) && (!"(".equals(stack.peek().toString()))) {
                        post.push(stack.pop());
                    }

                    if (stack.size() > 0) {
                        stack.pop(); // popping out the left bracket '('
                    }
                } // FUNCTION
                else if ((token.kind >= AnalyzerConstants.NBCELLNEU && token.kind <= AnalyzerConstants.ROUND) || token.kind >= AnalyzerConstants.INTERRO || token.kind >= AnalyzerConstants.DOT) {
                    stack.push(token);
                    if (token.kind >= AnalyzerConstants.DOT) {
                        stack.push(post.pop());
                    }
                } // COMMA
                else if (token.kind >= AnalyzerConstants.COMMA) {
                } // TRUE FALSE METER DEGREE
                else {
                    post.push(token);
                }
            }

        }
        
        while(!stack.isEmpty()) {
            post.push(stack.pop());
        }

        return post;
    }

    /**
     * Compares the precedence of two given tokens.
     * @param top first token 
     * @param p_2 second token
     * @return whether the first token has an higher precedence than the second
     */
    private static boolean comparePrecedence(Token top, Token p_2) {
        return top.kind >= p_2.kind;
    }
}
