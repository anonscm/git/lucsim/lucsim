/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.geotools.geometry.Envelope2D;

/**
 * Represents a numeric layer.
 * @author xinouch
 * @see Layer
 */
public class NumLayer extends Layer implements Serializable {

    /**
     * Constructs a numeric layer with a given name, an image representing the 
     * layer and an envelope for the grid coverage.
     * @param name name of the layer
     * @param img image representing the layer
     * @param envelope envelope to use for the grid coverage
     */
    public NumLayer(String name, RenderedImage img, Envelope2D envelope) {
        super(name, img, envelope);
    }
    
    /**
     * Constructs a numeric layer with a given name, an image representing the 
     * layer and an envelope for the grid coverage.
     * @param name name of the layer
     * @param img raster representing the layer
     * @param envelope envelope to use for the grid coverage
     */
    public NumLayer(String name, Raster img, Envelope2D envelope) {
        super(name, img, envelope);
    }

    /**
     * Returns a table containing the list of every state's value for each cell
     * composing the image.
     * @return table containing the list of every state's value for each cell
     * composing the image
     */
    public double[] tabPoint() {
        double[] histo = new double[getHeight() * getWidth()];

        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                histo[i * getWidth() + j] = getElement(j, i);
            }
        }

        return histo;
    }
    
    /**
     * Returns the min and the max values of this layer.
     * @return min and the max values of this layer
     */
    public double[] getExtrema() {
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                final double val = getElement(j, i);
                if (val > max) {
                    max = val;
                }
                if (val < min) {
                    min = val;
                }
            }
        }

        return new double[] {min, max};
    }
    
    
    @Override
    public boolean isStateLayer() {
        return false;
    }

    /**
     * Returns a list of the states' values as strings of this layer.
     * @return list of the states' values as strings of this layer
     */
    @Override
    public ArrayList<String> listStateString() {
        ArrayList<Integer> arr = new ArrayList<>();
        ArrayList<String> arr2 = new ArrayList<>();
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                int val = (int)getElement(i, j);
                if (!arr.contains(val)) {
                    arr.add(val);
                }
            }
        }
        Collections.sort(arr);
        for(Integer i : arr){
            arr2.add(i+"");
        }
        return arr2;
    }
}