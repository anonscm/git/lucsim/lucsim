/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.net.MalformedURLException;
import java.util.Collection;
import org.thema.lucsim.parser.Token;
import org.thema.lucsim.parser.AnalyzerConstants;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import org.apache.commons.math3.util.MathArrays;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.AbstractParallelFTask;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.lucsim.engine.compiler.Compiler;
import org.thema.lucsim.engine.compiler.BlockExec;
import org.thema.lucsim.gui.LucsimView;

/**
 * Represents the object computing the new steps of the simulation on the 
 * simulation layer, according to the transition rules.
 * @author rgrillot
 * @author alexis
 * @see Project
 * @see Rule
 * @see SimLayer
 */
public class Simulation {

    /**
     * Project to work with (layers and rules)
     */
    private Project project;

    private SimLayer simLayer;

    /**
     * ID of the ongoing simulation step
     */
    private int step;
    
    /**
     * ID of the block the simulation stopped on, to handle stops/resumptions of 
     * the simulation step by step
     */
    private int blockStop;
    
    /**
     * Step counter in a block
     */
    private int compteurStepBlock;
    
    /**
     * Wether the simulation is finished 
     */
    private boolean finish;

    private boolean compute;

    /**
     * Whether the rules have been compiled (requires JDK)
     */
    private boolean compiled;
    
    private String lastRulesCompiled;
    
    private File dirCompiledCode;

    /**
     * Whether the simulation runs in synchronous mode (true = synchronous, 
     * false = asynchronous)
     */
    private boolean synchronous;

    /**
     * Default constructor.
     * @param project project the simulation has to work with
     * @param layer layer to use as a simulation layer
     */
    public Simulation(Project project, StateLayer layer) {
        step = 0;
        this.project = project;
        this.simLayer = new SimLayer(layer);
        blockStop = 0;
        compteurStepBlock = 0;
        compute = false;
        finish = false;
        compiled = false;
        synchronous = true;
    }

    /**
     * Returns the simulation layer used.
     * @return simulation layer used
     */
    public SimLayer getSimLayer() {
        return simLayer;
    }

    /**
     * Returns the current simulation step.
     * @return current simulation step
     */
    public int getStep() {
        return step;
    }

    /**
     * Returns whether the simulation is finished.
     * @return whether the simulation is finished
     */
    public boolean isFinish() {
        return finish;
    }

    /**
     * Returns whether the simulation runs in synchronous mode.
     * @return whether the simulation runs in synchronous mode
     */
    public boolean isSynchronous() {
        return synchronous;
    }

    /**
     * Sets the simulation mode to a given one.
     * @param synchronous simulation mode to set
     */
    public void setSynchronous(boolean synchronous) {
        this.synchronous = synchronous;
    }

    /**
     * Returns whether a simulation step is being computed.
     * @return whether a simulation step is being computed
     */
    public boolean isCompute() {
        return compute;
    }

    /**
     * Stops the computation of the simulation.
     */
    public void stopSimulation() {
        compute = false;
    }

    /*
     * Returns the block step counter.
     */
    private void incBlockStop() {
        blockStop++;
        compteurStepBlock = 0;
    }

    /**
     * Returns the project the simulation is working with.
     * @return project the simulation is working with
     */
    public Project getProject() {
        return project;
    }

    /**
     * compile rules to bytecode only if the rules have changed
     * @throws java.io.IOException
     */
    public void compileRules() throws IOException {
        compiled = false;
        if(!project.getStringRules().equals(lastRulesCompiled)) {
            dirCompiledCode = new Compiler(project).compile(project.getRulesBlocks());
            lastRulesCompiled = project.getStringRules();
        }
        compiled = true;
    }

    /** 
     *
     */
    public void interpretRules() {
        compiled = false;
    }

    private boolean executeOneStep(final int block, ProgressBar monitor) {
        final int width = simLayer.getWidth();
        final int height = simLayer.getHeight();

        final BlockExec compBlock;
        if (compiled) {
            try {
                URL url = dirCompiledCode.toURI().toURL();
                URL[] urls = new URL[]{url};
                ClassLoader cl = new URLClassLoader(urls);
                Class cls = cl.loadClass("org.thema.lucsim.engine.compiler.Block" + (block));
                compBlock = (BlockExec) cls.getDeclaredConstructor(Project.class).newInstance(project);
            } catch (MalformedURLException | ClassNotFoundException | InstantiationException | IllegalAccessException | 
                    NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException("Unable to load compiled code", e);
            } 
        } else {
            compBlock = null;
        }

        final int[] table;
        final byte[] result;
        if (synchronous) {
            result = new byte[width * height];
            Arrays.fill(result, (byte) -1);
            table = null;
        } else {
            result = null;
            table = new int[height * width];
            for (int i = 0; i < table.length; i++) {
                table[i] = i;
            }
            MathArrays.shuffle(table);
        }
        AbstractParallelFTask<Boolean, Boolean> task = new AbstractParallelFTask<Boolean, Boolean>(monitor) {
            private boolean resChange;

            @Override
            protected Boolean execute(int start, int end) {
                boolean change = false;
                Cell cell = new Cell(simLayer);
                for (int j = start; j < end; j++) {
                    for (int i = 0; i < width; i++) {
                        final int ind = synchronous ? (j * width + i) : table[(j * width + i)];
                        final int x = ind % width;
                        final int y = ind / width;
                        cell.setPosition(x, y);
                        int state;
                        if (compBlock != null) {
                            state = compBlock.execute(cell);
                        } else {
                            try {
                                state = execInterpretedRule(block, cell);
                            } catch (UnknownStateException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                        if (state != -1) {
                            double etatIni = simLayer.getElement(x, y);
                            if (synchronous) {
                                if(state != etatIni) {
                                    result[ind] = (byte) state;
                                }
                            } else {
                                if(state != etatIni) {
                                    simLayer.setElement(x, y, state);
                                } else {
                                    simLayer.incDuration(x, y);
                                }
                            }

                            if (!change && state != etatIni) {//Continuer si le changement change d'état la cellule
                                change = true;
                            }
                        } else if(!synchronous) {
                            simLayer.incDuration(x, y);
                        }
                    }
                    incProgress(1);
                }
                return change;
            }

            @Override
            protected int getSplitRange() {
                return height;
            }

            @Override
            public void finish(Collection<Boolean> clctn) {
                resChange = false;
                for (Boolean b : clctn) {
                    if (b) {
                        resChange = true;
                        break;
                    }
                }
            }

            @Override
            public Boolean getResult() {
                return resChange;
            }
        };
        try {
            new ParallelFExecutor(task).executeAndWait();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        if (synchronous) {
            for (int i = 0; i < result.length; i++) {
                final int x = i % width;
                final int y = i / width;
                if (result[i] != -1) {
                    simLayer.setElement(x, y, result[i] & 0xFF);
                } else {
                    simLayer.incDuration(x, y);
                }
            }
        }
        boolean change = task.getResult();
        if (change) {
            step++;
        }

        project.getStates().getRasterStyle().update();
        
        return change;
    }

    private int execInterpretedRule(int block, Cell cell) throws UnknownStateException {
        double bestEval = 0;
        State state = project.getStates().getState((int) cell.getStateImage(simLayer));
        List<Rule> rules = new ArrayList<>();
        Rule bestRule = null;

        for(Rule rule : project.getRulesBlocks().get(block).getRules(state, rules)) {
            Deque<Token> stack = rule.getCondition().getTokenPost();
            double eval = evaluatePostFix(stack, cell, simLayer);
            if (eval > bestEval) {
                bestEval = eval;
                bestRule = rule;
            }
        }

        // if there is a rule having the good initial state, with a true 
        // condition (and which gives the best result)
        if (bestRule != null) {
            return bestRule.getFinalState().getValue();
        } else {
            return -1;
        }
    }

    /**
     * Resets the simulation layer to its initial state.
     */
    public void reset() {
        stopSimulation();
        simLayer.reset();
        step = 0;
        //on remet les compteur de block de simulation à 0
        blockStop = 0;
        compteurStepBlock = 0;
        finish = false;
    }

    /**
     * ****************************
     */
    
    /*      E X E C U T I O N      */
    
    /**
     * ****************************
     */
    
    /**
     * Evaluates a condition represented as a postfixed list of tokens.
     * @param postfix postfixed list of tokens to evaluate
     * @param cell cell for which the neighborhood data has to be retrieved
     * @param layer layer containing the cell
     * @return greater than 0 if the condition is true, less than 0 otherwise
     */
    public double evaluatePostFix(Deque<Token> postfix, Cell cell, Layer layer) {

        double a, b, c, d;

        Token token = postfix.pollLast();

        switch (token.kind) { // the token is removed

            /**
             * *************** Numbes *************
             */
            case AnalyzerConstants.INTEGER:
                return Integer.parseInt(token.toString());

            case AnalyzerConstants.FLOAT:
                return Double.parseDouble(token.toString());

            /**
             * *************** Boolean *************
             */
            case AnalyzerConstants.TRUE:
                return 1;

            case AnalyzerConstants.FALSE:
                return 0;

            /**
             * *************** Units *************
             */
            case AnalyzerConstants.METER:
                return layer.getNbCellForMeter(evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.DEGREE:
                return evaluatePostFix(postfix, cell, simLayer);

            case AnalyzerConstants.PERCENT:
                return evaluatePostFix(postfix, cell, simLayer) / 100.0;

            /**
             * ************** Logical operators **********
             */
            case AnalyzerConstants.OR:
                return evaluatePostFix(postfix, cell, simLayer) + evaluatePostFix(postfix, cell, simLayer);

            case AnalyzerConstants.AND:
                return evaluatePostFix(postfix, cell, simLayer) * evaluatePostFix(postfix, cell, simLayer);

            case AnalyzerConstants.XOR:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if ((a == 0 && b > 0) || (a > 0 && b == 0)) {
                    return a + b;
                } else {
                    return 0;
                }

            case AnalyzerConstants.NOT:
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a > 0) {
                    return 0;
                } else {
                    return 1;
                }

            /**
             * ************* Comparators ****************
             */
            case AnalyzerConstants.EQUAL:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a == b) {
                    return 1;
                } else {
                    return 0;
                }

            case AnalyzerConstants.GEQ:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a >= b) {
                    return 1;
                } else {
                    return 0;
                }

            case AnalyzerConstants.LEQ:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a <= b) {
                    return 1;
                } else {
                    return 0;
                }

            case AnalyzerConstants.NEQ:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a != b) {
                    return 1;
                } else {
                    return 0;
                }

            case AnalyzerConstants.GREAT:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a > b) {
                    return 1;
                } else {
                    return 0;
                }

            case AnalyzerConstants.LESS:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                if (a < b) {
                    return 1;
                } else {
                    return 0;
                }

            /**
             * ************** Operators **************
             */
            case AnalyzerConstants.PLUS:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return a + b;

            case AnalyzerConstants.MINUS:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return a - b;

            case AnalyzerConstants.TIMES:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return a * b;

            case AnalyzerConstants.DIVIDE:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return a / b;

            case AnalyzerConstants.MODULO:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return a % b;

            /**
             * ************** Functions **************
             */
            case AnalyzerConstants.CELLDURATION:
                int r = ((SimLayer) layer).getDuration(cell.getX(), cell.getY());
                return r;

            case AnalyzerConstants.STEP:
                return this.getStep();

            case AnalyzerConstants.NBSAMESTEP:
                return ((SimLayer) layer).nbSameStep();

            case AnalyzerConstants.NBCELLDURATION:
                return ((SimLayer) layer).nbCellDuration((int) evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.RANDOM:
                return (double) Math.random();

            case AnalyzerConstants.INT:
                return (int) (evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.ROUND:
                return Math.round(evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.NBCELLNEU:
                return cell.nbCellNeu(layer, evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.NBCELLMOO:
                return cell.nbCellMoo(layer, evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.NBCELLSQ:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return cell.nbCellSq(layer, a, (int) b);

            case AnalyzerConstants.NBCELLCIR:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return cell.nbCellCir(layer, a, (int) b);

            case AnalyzerConstants.PCELLSQ:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return cell.pCellSq(layer, a, (int) b);

            case AnalyzerConstants.PCELLCIR:
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                return cell.pCellCir(layer, a, (int) b);

            case AnalyzerConstants.NBCELLANG:
                d = evaluatePostFix(postfix, cell, simLayer);
                c = evaluatePostFix(postfix, cell, simLayer);
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                double g = cell.nbCellAng(layer, a, b, c, (int)d);
                return g;

            case AnalyzerConstants.PCELLANG:
                d = evaluatePostFix(postfix, cell, simLayer);
                c = evaluatePostFix(postfix, cell, simLayer);
                b = evaluatePostFix(postfix, cell, simLayer);
                a = evaluatePostFix(postfix, cell, simLayer);
                double f = cell.pCellAng(layer, a, b, c, (int)d);
                return f;

            case AnalyzerConstants.NORTH:
                return cell.north(layer);

            case AnalyzerConstants.SOUTH:
                return cell.south(layer);

            case AnalyzerConstants.EAST:
                return cell.east(layer);

            case AnalyzerConstants.WEST:
                return cell.west(layer);

            case AnalyzerConstants.NORTH_EAST:
                return cell.northEast(layer);

            case AnalyzerConstants.NORTH_WEST:
                return cell.northWest(layer);

            case AnalyzerConstants.SOUTH_EAST:
                return cell.southEast(layer);

            case AnalyzerConstants.SOUTH_WEST:
                return cell.southWest(layer);

            case AnalyzerConstants.NBCELL:
                return layer.nbCell(evaluatePostFix(postfix, cell, simLayer));

            case AnalyzerConstants.PCELL:
                return layer.pCell(evaluatePostFix(postfix, cell, simLayer));

            /**
             * ***************** Layer ***************
             */
            case AnalyzerConstants.INTERRO:

                Token layerName = postfix.pollLast();

                double state = evaluatePostFix(postfix, cell, simLayer);

                if (!(layer.getName().equals(layerName.toString()))) {
                    if (cell.getStateImage(project.getLayer(layerName.toString())) == state) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else if (cell.getStateImage(layer) == state) {
                    return 1;
                } else {
                    return 0;
                }

            case AnalyzerConstants.NAME:
                if (!(layer.getName().equals(token.image))) {
                    try { // other layer
                        return cell.getStateImage(project.getLayer(token.image));
                    } catch (UnknownLayerException ex) {
                        // it is not a layer but a state
                        return project.getStates().getState(token.image).getValue();
                    }
                } else { // current layer
                    return cell.getStateImage(layer);
                }

            case AnalyzerConstants.DOT:
                Token tok = postfix.pollLast();
                if (!(layer.getName().equals(tok.toString()))) {
                    return evaluatePostFix(postfix, cell, project.getLayer(tok.toString()));
                } else {
                    return evaluatePostFix(postfix, cell, layer);
                }

            default:
                throw new IllegalArgumentException("Unknown token " + token.toString());
        }

    }

    /**
     * Runs the simulation.
     * @param view application's main frame (to color the executing block and 
     * update the simulation layer image)
     * @param monitor progress bar to update during the computation
     * @param one whether only one step has to be executed
     */
    public void run(LucsimView view, ProgressBar monitor, boolean one) {

        compute = true;

        // allows to stop the simulation once the step to execute is found 
        // during a step by step simulation
        boolean boolContinue = true;

        // for each block
        while (blockStop < project.getRulesBlocks().size()) {

            if (!compute) {
                return;
            }

            RulesBlock rb = project.getRulesBlocks().get(blockStop);
            if (view != null) {
                view.colorBlockExecution(blockStop);
            }

            switch (rb.getType()) {

                case IF:
                    // tests the step counter by block is 0, in order to not 
                    // evaluate the condition at each resumption of the step by 
                    // step simulation
                    if (compteurStepBlock == 0) {
                        if (evaluatePostFix(rb.getCondition().getTokenPost(), null, simLayer) == 0) {
                            boolContinue = false;
                            incBlockStop();
                        }
                    }
                    break;

                case WHILE:
                    if (evaluatePostFix(rb.getCondition().getTokenPost(), null, simLayer) == 0) {
                        boolContinue = false;
                        incBlockStop();
                    }
                    break;

                case REPEAT:
                    if (rb.getNbRepeat() < 1) {
                        boolContinue = false;
                        incBlockStop();
                    }
                    break;

            }

            while (boolContinue && compute) {

                boolContinue = executeOneStep(blockStop, monitor);

                compteurStepBlock++;
                if (view != null) {
                    view.updateImageSimulation();
                }

                // tests if the block has to be executed at least one more time
                switch (rb.getType()) { 
                    case WHILE:
                        if (evaluatePostFix(rb.getCondition().getTokenPost(), null, simLayer) == 0) {
                            boolContinue = false;
                        }
                        break;

                    case REPEAT:
                        boolContinue = compteurStepBlock < rb.getNbRepeat();
                        break;

                    case NOBLOCK:
                        boolContinue = false;
                        break;
                }

                // if boolContinue is false, it means that a stable state was 
                // reached or that the condition of the block was no longer true
                if (!boolContinue) {
                    incBlockStop();
                }

                // the step by step simulation is stopped too
                if (one) {
                    boolContinue = false;
                    compute = false;
                }
            }

            // if it is the end of the simulation
            if (blockStop == project.getRulesBlocks().size() && !boolContinue) {
                finish = true;
            }

            boolContinue = true;
        }

        // end of the simulation
        compute = false;
    }
}
