/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.lucsim.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.thema.common.ConsoleProgress;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.swing.TaskMonitor;
import org.thema.lucsim.generator.dt.BatchClassifierRunnable;
import org.thema.lucsim.generator.dt.IncrementalClassifierRunnable;
import org.thema.lucsim.generator.dt.RulesGeneratorDT;
import org.thema.lucsim.generator.dt.TreeBinaryClassifier;
import org.thema.lucsim.gui.generator.dt.PostProcessDialog;
import org.thema.lucsim.stat.CalcDataExtractor;
import org.thema.lucsim.stat.DataExtractor;
import org.thema.lucsim.stat.FileDataExtractor;
import weka.core.converters.CSVSaver;

/**
 *
 * @author Gilles Vuidel
 */
public class CLI {
    
    private static Project project;
    private static File prjDir;
    
    private static DataExtractor dataExtractor;
    private static File dataFile;
    
    public static void execute(String[] argArr) throws Exception {
        if(argArr[0].equals("--help")) {    
            System.out.println("Usage :\njava -jar lucsim.jar [-proc n]\n"  +
                    "--project project_file.xml COMMAND\n\n" +
                    "Command list :\n" +
                    "--extract layer1=layer_name layer2=layer_name function=nbCellSq|nbCellCirc radius=min:inc:max target=state_name [other=layer_name,..] [weight=1|2|..|n] [output=file.csv]\n" +
                    "--rulexec simlayer=layer_name [rules=file.txt] [nstep=val] [async] [output=filename] [detail=dir]\n" +
                    "--rulegen [data=file.csv] training=percent [seed=num] [output=dir] algo=J48|Hoeffding [algo parameters]\n");
            return;
        }
        
        
        TaskMonitor.setHeadlessStream(new PrintStream(File.createTempFile("java", "monitor")));

        List<String> args = new ArrayList<>(Arrays.asList(argArr));
        
        // parallel options
        while(!args.isEmpty() && !args.get(0).startsWith("--project")) {
            String p = args.remove(0);
            switch (p) {
                case "-proc":
                    int n = Integer.parseInt(args.remove(0));
                    ParallelFExecutor.setNbProc(n);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown option " + p);
            }
        }
        if(args.isEmpty()) {
            throw new IllegalArgumentException("Need --project");
        }
        
        args.remove(0);
        File f = new File(args.remove(0));
        project = Project.load(f);
        prjDir = f.getAbsoluteFile().getParentFile();
        
        while(!args.isEmpty()) {
            String arg = args.remove(0);
            switch(arg) {
                case "--extract":
                    dataExtract(args);
                    break;
                case "--rulegen":
                    ruleGen(args);
                    break;
                case "--rulexec":
                    ruleExec(args);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown command : " + arg);
            }
        }
    }

    private static void dataExtract(List<String> args) throws IOException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("layer1", "layer2", "function", "radius", "target"), 
                Arrays.asList("other", "weight", "output"));
        
        StateLayer layer1 = project.getStateLayer(params.get("layer1"));
        StateLayer layer2 = project.getStateLayer(params.get("layer2"));
        String function = params.get("function");
        Range radius = Range.parse(params.get("radius"));
        State target = project.getStates().getState(params.get("target"));
        
        List<Layer> layers = new ArrayList<>();
        if(params.containsKey("other")) {
            for(String l : params.get("other").split(",")) {
                layers.add(project.getLayer(l));
            }
        }
        double weight = 1;
        if(params.containsKey("weight")) {
            weight = CalcDataExtractor.estimWeight(layer1, layer2, target) / Double.parseDouble(params.get("weight"));
        }
        
        if(params.containsKey("output")) {
            dataFile = new File(params.get("output"));
        } else {
            dataFile = new File(prjDir, String.format("data_%s_%s_%s_%s_%s_%s_%g.csv", 
                    layer1.getName(), layer2.getName(), function, radius.toString(), target.getName(), layers.toString(), weight));
        }
        
        dataExtractor = new CalcDataExtractor(layer1, layer2, layers, function, false, (int)radius.getMin(), (int)radius.getMax(), (int)radius.getInc(), target, weight);

        CSVSaver csvSaver = new CSVSaver();
        csvSaver.setFile(dataFile);
        csvSaver.setInstances(dataExtractor.getFullDataset());
        csvSaver.writeBatch();
        
    }
    
    private static void ruleExec(List<String> args) throws Exception {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("simlayer"), Arrays.asList("rules", "nstep", "async", "output", "detail"));
        File fRule = null;
        if(params.containsKey("rules")) {
            String rules = "";
            fRule = new File(params.get("rules"));
            BufferedReader br = new BufferedReader(new FileReader(fRule));
 
            String line;
            while ((line = br.readLine()) != null) {
                rules += line + "\n";
            }
            project.setStringRules(rules);
        }
        project.validateRules();
        
        Simulation sim = project.setSimuLayer(params.get("simlayer"));
        sim.setSynchronous(!params.containsKey("async"));
        try {
            sim.compileRules();
        } catch(Exception ex) {
            Logger.getLogger("").log(Level.WARNING, "Rules cannot be compiled", ex);
        }
        
        if(params.containsKey("nstep")) {
            int nStep = Integer.parseInt(params.get("nstep"));
            File dirDetail = null;
            if(params.containsKey("detail")) {
                dirDetail = new File(params.get("detail"));
                dirDetail.mkdir();
            }
            for(int i = 0; i < nStep; i++) {
                sim.run(null, new ConsoleProgress("Execute step " + (i+1), 1), true);
                if(dirDetail != null) {
                    sim.getSimLayer().saveLayer(dirDetail, params.get("simlayer") + "_step" + sim.getStep());
                }
            }
        } else {
            sim.run(null, new ConsoleProgress("Execute all steps", 1), false);
        }
        String output = params.get("simlayer") + (fRule != null ? "_" + fRule.getName() : "") + "_step" + sim.getStep();
        if(params.containsKey("output")) {
            output = params.get("output");
        }
        sim.getSimLayer().saveLayer(prjDir, output);
    }
    
    private static void ruleGen(List<String> args) throws Exception {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("training", "algo"), null);
        
        if(params.containsKey("data")) {
            dataFile = new File(params.get("data"));
            dataExtractor = new FileDataExtractor(dataFile, project);
        } else {
            if(dataFile == null) {
                throw new IllegalArgumentException("data parameter must be set or use --extract command before this command");
            }
        }
        
        double trainingRatio = Double.parseDouble(params.get("training")) / 100;
        int seed = (int) System.currentTimeMillis();
        if(params.containsKey("seed")) {
            seed = Integer.parseInt(params.get("seed"));
        }
        
        File dir;
        if(params.containsKey("output")) {
            dir = new File(params.get("output"));
        } else {
            dir = new File(dataFile.getAbsoluteFile().getParentFile(), dataFile.getName().substring(0, dataFile.getName().length()-4));
        }
        
        HashSet<String> set = new HashSet<>(params.keySet());
        set.removeAll(Arrays.asList("training", "algo", "data", "seed", "output"));
        String paramAlgo = "";
        for(String p : set) {
            paramAlgo += " " + p;
        }
        
        TreeBinaryClassifier classifier;
        if(params.get("algo").equals("J48")) {
            classifier = new BatchClassifierRunnable(dataExtractor, trainingRatio, paramAlgo, new TaskMonitor.EmptyMonitor(), seed);
        } else {
            classifier = new IncrementalClassifierRunnable(dataExtractor, paramAlgo, new TaskMonitor.EmptyMonitor());
        }
        
        classifier.run();
        
        
        dir.mkdir();
        
        Map<String, Integer> confMatrixTraining = classifier.getConfusionMatrixFromTrainingData();
        Map<String, Integer> confMatrixTesting = classifier.getConfusionMatrixFromTestData();       
        
        List<RulesGeneratorDT.ClassifyRule> rulesList = classifier.getRulesGenerator().getRules();
        Collections.sort(rulesList);
        
        PostProcessDialog.writeStatRules(rulesList, new File(dir, "statrules.csv"));
        
        int totalCond = 0;
        try (FileWriter fw = new FileWriter(new File(dir, "rules.txt"))) {
            fw.write("/* Score\tTrue posit.\tFalse posit.\tNb conditions */\n");
            for(RulesGeneratorDT.ClassifyRule rule : rulesList) {
                fw.write(String.format("/* %g\t%d\t%d\t%d */ %s\n", rule.getScore(), rule.getTp(), rule.getFp(), rule.getNbConditions(), rule.getRule().toString()));
                totalCond += rule.getNbConditions();
            }
        }
        
        try (FileWriter fw = new FileWriter(new File(dir, "indices.txt"))) {
            fw.write(String.format("MCC training : %g\n", PostProcessDialog.calcMCC(confMatrixTraining)));
            if(confMatrixTesting != null) {
                fw.write(String.format("MCC testing : %g\n", PostProcessDialog.calcMCC(confMatrixTesting)));
            }
            fw.write(String.format("Nb rules : %d\n", rulesList.size()));
            fw.write(String.format("Nb conditions : %d\n", totalCond));
            fw.write("\nTraining confusion matrix\n");
            fw.write(String.format("Training TP : %d\n", confMatrixTraining.get("TP")));
            fw.write(String.format("Training FP : %d\n", confMatrixTraining.get("FP")));
            fw.write(String.format("Training TN : %d\n", confMatrixTraining.get("TN")));
            fw.write(String.format("Training FN : %d\n", confMatrixTraining.get("FN")));
            
            if(confMatrixTesting != null) {
                fw.write("\nTesting confusion matrix\n");
            fw.write(String.format("Testing TP : %d\n", confMatrixTesting.get("TP")));
            fw.write(String.format("Testing FP : %d\n", confMatrixTesting.get("FP")));
            fw.write(String.format("Testing TN : %d\n", confMatrixTesting.get("TN")));
            fw.write(String.format("Testing FN : %d\n", confMatrixTesting.get("FN")));
            }
        }

    }
    
    private static Map<String, String> extractAndCheckParams(List<String> args, List<String> mandatoryParams, List<String> optionalParams) {
        Map<String, String> params = new LinkedHashMap<>();
                
        while(!args.isEmpty() && !args.get(0).startsWith("--")) {
            String arg = args.remove(0);
            if(arg.contains("=")) {
                String[] tok = arg.split("=");
                params.put(tok[0], tok[1]);
            } else {
                params.put(arg, null);
            }
        }
        
        // check mandatory parameters
        if(!params.keySet().containsAll(mandatoryParams)) {
            HashSet<String> set = new HashSet<>(mandatoryParams);
            set.removeAll(params.keySet());
            throw new IllegalArgumentException("Mandatory parameters are missing : " + Arrays.deepToString(set.toArray()));
        }
        
        // check unknown parameters if optionalParams is set
        if(optionalParams != null) {
            HashSet<String> set = new HashSet<>(params.keySet());
            set.removeAll(mandatoryParams);
            set.removeAll(optionalParams);
            if(!set.isEmpty()) {
                throw new IllegalArgumentException("Unknown parameters : " + Arrays.deepToString(set.toArray()));
            }
        }
        
        return params;
    }

}

/**
 * CLI range parsing
 * 
 * @author Gilles Vuidel
 */
class Range {
    private double min, max, inc;
    private List<Double> values;
    
    private Range(double val) {
        this(val, 1, val);
    }

    private Range(double min, double max) {
        this(min, 1, max);
    }

    private Range(double min, double inc, double max) {
        this.min = min;
        this.max = max;
        this.inc = inc;
    }

    private Range(List<Double> values) {
        this.values = values;
        this.min = Collections.min(values);
        this.max = Collections.max(values);
        this.inc = 0;
    }

    /**
     * @return all values of the range
     */
    public List<Double> getValues() {
        if(values == null) {
            List<Double> lst = new ArrayList<>();
            for(double v = min; v <= max; v += inc) {
                lst.add(v);
            }
            return lst;
        } else {
            return values;
        }
    }

    /**
     * @return if this range contains only one number
     */
    public boolean isSingle() {
        return getSize() == 1;
    }

    /**
     * @return the minimum value
     */
    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getInc() {
        return inc;
    }

    /**
     * @return the number of values
     */
    public int getSize() {
        if(values == null) {
            int n = 0;
            for(double v = min; v <= max; v += inc) {
                n++;
            }
            return n;
        } else {
            return values.size();
        }
    }

    /**
     * Parse the string and extract the range.
     * It can be :
     * - a single number
     * - a list of number separated by comma
     * - a real range of the form min:max or min:inc:max
     * @param s the string containing the number 
     * @return the new range
     */
    public static Range parse(String s) {
        String [] tok = s.split(":");
        switch (tok.length) {
            case 1:
                tok = s.split(",");
                if(tok.length == 1) {
                    return new Range(Double.parseDouble(tok[0]));
                } else {
                    List<Double> values = new ArrayList<>(tok.length);
                    for(String tok1 : tok) {
                        values.add(Double.parseDouble(tok1));
                    }
                    return new Range(values);
                }
            case 2:
                return new Range(Double.parseDouble(tok[0]), Double.parseDouble(tok[1]));
            case 3:
                return new Range(Double.parseDouble(tok[0]), Double.parseDouble(tok[1]), Double.parseDouble(tok[2]));
            default:
                throw new IllegalArgumentException("Impossible to parse : " + s);
        }
    }    

    @Override
    public String toString() {
        if(values == null) {
            return min + "_" + inc + "_" + max;
        } else {
            return values.toString();
        }
    }

}
