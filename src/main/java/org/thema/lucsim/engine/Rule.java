/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.io.Serializable;

/**
 * Represents a transition rule from a state to another in a state layer.
 * @see State
 * @see StateLayer
 * @author rgrillot
 * @author alexis
 */
public class Rule implements Serializable {

    private State initState;
    private State finalState;
    private Condition condition;

    /**
     * Empty constructor.
     */
    public Rule() {}

    /**
     * Constructs a rule with the given initial and final states and a 
     * condition.
     * @param initState initial state to set or null for all states
     * @param finalState final state to set
     * @param condition condition to set
     */
    public Rule(State initState, State finalState, Condition condition) {
        this.initState = initState;
        this.finalState = finalState;
        this.condition = condition;
    }
    
    /**
     * Returns true if the rule has "all" as initial state.
     * @return whether the rule has "all" as initial state
     */
    public boolean isAllInit() {
        return initState == null;
    }

    /**
     * Sets the initial state to null, which translates by an "all" initial 
     * state.
     */
    public void setAllInit() {
        initState = null;
    }

    /**
     * Returns the rule's initial state.
     * @return rule's initial state
     */
    public State getInitState() {
        return initState;
    }

    /**
     * Sets the initial state to a given state.
     * @param initState initial state to set
     */
    public void setInitState(State initState) {
        this.initState = initState;
    }

    /**
     * Returns the rule's final state.
     * @return rule's final state
     */
    public State getFinalState() {
        return finalState;
    }

    /**
     * Sets the finale state to a given state.
     * @param finalState final state to set
     */
    public void setFinalState(State finalState) {
        this.finalState = finalState;
    }

    /**
     * Returns the rule's condition.
     * @return rule's condition
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Sets the condition to a given condition.
     * @param condition condition to set
     */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }
    
    public boolean containInitState(State state) {
        if(initState == null) {
            return !States.NODATA_STATE.equals(state);
        } else {
            return initState.equals(state);
        }
        
    }

    /**
     * Returns a string representation of the rule.
     * @return string representation of the rule
     */
    @Override
    public String toString() {
        return (isAllInit() ? "all" : initState.getName()) + " -> " + finalState.getName() + " : " + condition.toString() + ";";
    }
}
