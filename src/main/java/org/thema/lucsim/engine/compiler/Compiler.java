/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine.compiler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.Rule;
import org.thema.lucsim.engine.RulesBlock;
import org.thema.lucsim.engine.SimLayer;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.engine.States;
import org.thema.lucsim.parser.AnalyzerConstants;
import org.thema.lucsim.parser.Token;

/**
 *
 * @author alexis
 */
public class Compiler {

    private static String SIMLAYER = "simLayer";
    
    private Project project;

    public Compiler(Project project) {
        this.project = project;
    }
    
    public File compile(List<RulesBlock> ruleBlocks) throws IOException {
        File tmpDir = Files.createTempDirectory("lucsim_compil").toFile();
        for(int i = 0; i < ruleBlocks.size(); i++) {
            String code = genJavaCode(i, ruleBlocks.get(i));
            compile(i, code, tmpDir);
        }
        return tmpDir;
    }
    
    private void compile(int indBlock, String javaCode, File tmpDir) throws IOException {
        /*Creating dynamic java source code file object*/
        SimpleJavaFileObject fileObject = new DynamicJavaSourceCodeObject("org.thema.lucsim.engine.compiler.Block" + indBlock, javaCode);
        Logger.getLogger(Compiler.class.getName()).info("Compile block " + indBlock + " : \n" + javaCode);
        JavaFileObject javaFileObjects[] = new JavaFileObject[]{fileObject};
        /* Prepare a list of compilation units (java source code file objects) to input to compilation task*/
        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(javaFileObjects);
        /*Prepare any compilation options to be used during compilation*/
        String[] compileOptions = new String[]{"-d", tmpDir.getAbsolutePath()};
        Iterable<String> compilationOptionss = Arrays.asList(compileOptions);
        /*Instantiating the java compiler*/
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        try (StandardJavaFileManager stdFileManager = compiler.getStandardFileManager(null, Locale.getDefault(), null)) {
            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
            /*Create a compilation task from compiler by passing in the required input objects prepared above*/
            CompilationTask compilerTask = compiler.getTask(null, stdFileManager, diagnostics, compilationOptionss, null, compilationUnits);
            boolean status = compilerTask.call();
            if (!status) {//If compilation error occurs
                /*Iterate through each compilation problem and print it*/
                for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
                    throw new RuntimeException(String.format("Error while compiling java code on line %d in %s", diagnostic.getLineNumber(), diagnostic));
                }
            }
        }
    }
    
    public String genJavaCode(int blockId, RulesBlock block) {
        String header = "package org.thema.lucsim.engine.compiler;\nimport org.thema.lucsim.engine.*;\n"
            + "public final class Block" + blockId + " implements BlockExec {\n"
            + "\tfinal SimLayer " + SIMLAYER + ";\n";
        String constructor = "\n\tpublic Block" + blockId + "(Project project) {\n"
            + "\t\t" + SIMLAYER + " = project.getSimulation().getSimLayer();\n";
        for(Layer l : project.getLayers()) {
            if(l instanceof SimLayer) {
                continue;
            }
            if(l instanceof StateLayer) {
                header += "\tfinal StateLayer " + l.getName() + ";\n";
                constructor += "\t\t" + l.getName() + " = project.getStateLayer(\"" + l.getName() + "\");\n";
            } else {
                header += "\tfinal NumLayer " + l.getName() + ";\n";
                constructor += "\t\t" + l.getName() + " = project.getNumLayer(\"" + l.getName() + "\");\n";
            }
        }
            
        header += constructor + "\t}\n\n"
            + "\tpublic int execute(final Cell cell){\n"
            + "\t\tfinal int initState = (int)cell.getStateImage(" + SIMLAYER + ");\n";
            
        Set<Layer> usedLayer = new HashSet<>();
        String code = "";
        int i = 1;
        for(Rule rule : block.getRules()) {
            code += "\n\tprivate boolean rule_" + i + "(final Cell cell) {\n";
            code += "\t\treturn " + genJavaCode(rule.getCondition().getTokenPost(), SIMLAYER, usedLayer) 
                    + ";\n\t}\n";
            
            header += "\t\tif(initState" + (rule.isAllInit() ? " != " + States.NODATA_VAL : " == " + rule.getInitState().getValue()) 
                    + " && rule_" + i + "(cell))\n\t\t\treturn " + rule.getFinalState().getValue() + ";\n";
            i++;
        }

        
        return header + "\t\treturn -1;\n\t}\n"+ code + "}\n";
    }
    
    /**
     * Fonction d'évaluation d'un linkedList postfixé (représentant une
     * condition)
     *
     * @param postfix
     * @param cell
     * @param couche
     * @return supérieur 0 si la condition est fausse, sinon supérieur a zero
     */
    private String genJavaCode(Deque<Token> postfix, String currentLayer, Set<Layer> usedLayer) {
        String a, b, c, d;
        Token token = postfix.pollLast();
        
        switch (token.kind) {//le token est retiré

            /**
             * *************** Constantes *************
             */
            case AnalyzerConstants.INTEGER:
            case AnalyzerConstants.FLOAT:
                return token.image;
                
            case AnalyzerConstants.TRUE:
            case AnalyzerConstants.FALSE:
                return getJava(token);

            /**
             * *************** Unite *************
             */
            case AnalyzerConstants.METER:
                return currentLayer + ".getNbCellForMeter(" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.DEGREE:
                return genJavaCode(postfix, SIMLAYER, usedLayer);

            case AnalyzerConstants.PERCENT:
                return "(" + genJavaCode(postfix, SIMLAYER, usedLayer) + "/ 100.0)";


            /**
             * ************** unary logical operator **********
             */
            
            case AnalyzerConstants.NOT:
                return " !(" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";



            /**
             * ************* Binary operator ****************
             */
            case AnalyzerConstants.OR:
            case AnalyzerConstants.AND:
            case AnalyzerConstants.XOR:
            case AnalyzerConstants.EQUAL:
            case AnalyzerConstants.GEQ:
            case AnalyzerConstants.LEQ:
            case AnalyzerConstants.NEQ:
            case AnalyzerConstants.GREAT:
            case AnalyzerConstants.LESS:    
            case AnalyzerConstants.PLUS:
            case AnalyzerConstants.MINUS:
            case AnalyzerConstants.TIMES:
            case AnalyzerConstants.DIVIDE:
            case AnalyzerConstants.MODULO:
                b = genJavaCode(postfix, SIMLAYER, usedLayer);
                a = genJavaCode(postfix, SIMLAYER, usedLayer);
                return "(" + a + ") " + getJava(token) + " (" + b + ")";


            /**
             * ************** function  **************
             */
            case AnalyzerConstants.CELLDURATION:
                return SIMLAYER + ".getDuration(cell.getX(), cell.getY())";

            case AnalyzerConstants.STEP:
                return "project.getSimulation().getStep()";

            case AnalyzerConstants.NBSAMESTEP:
                return SIMLAYER + ".nbSameStep()";

            case AnalyzerConstants.NBCELLDURATION:
                return SIMLAYER + ".nbCellDuration((int) " + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.RANDOM:
                return "Math.random()";

            case AnalyzerConstants.INT:
                return "(int) (" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.ROUND:
                return "Math.round(" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.NBCELLNEU:
                return "cell.nbCellNeu(" + currentLayer + ", " + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.NBCELLMOO:
                return "cell.nbCellMoo(" + currentLayer + ", " + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.NBCELLSQ:
            case AnalyzerConstants.NBCELLCIR:
            case AnalyzerConstants.PCELLSQ:
            case AnalyzerConstants.PCELLCIR:
                b = genJavaCode(postfix, SIMLAYER, usedLayer);
                a = genJavaCode(postfix, SIMLAYER, usedLayer);
                return "cell." + getJava(token) + "(" + currentLayer + ", " + a + ", (int)" + b + ")";


            case AnalyzerConstants.NBCELLANG:
            case AnalyzerConstants.PCELLANG:
                d = genJavaCode(postfix, SIMLAYER, usedLayer);
                c = genJavaCode(postfix, SIMLAYER, usedLayer);
                b = genJavaCode(postfix, SIMLAYER, usedLayer);
                a = genJavaCode(postfix, SIMLAYER, usedLayer);
                return "cell." + getJava(token) + "(" + currentLayer + ", " + a + ", " + b + ", " + c + ", " + d + ")";


            case AnalyzerConstants.NORTH:
            case AnalyzerConstants.SOUTH:
            case AnalyzerConstants.EAST:
            case AnalyzerConstants.WEST:
            case AnalyzerConstants.NORTH_EAST:
            case AnalyzerConstants.NORTH_WEST:
            case AnalyzerConstants.SOUTH_EAST:
            case AnalyzerConstants.SOUTH_WEST:                    
                return "cell." + getJava(token) + "(" + currentLayer + ")";


            case AnalyzerConstants.NBCELL:
                return currentLayer + ".nbCell(" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";

            case AnalyzerConstants.PCELL:
                return currentLayer + ".pCell(" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";


            /**
             * ***************** couche***************
             */
            case AnalyzerConstants.INTERRO:
                Token layer = postfix.pollLast();
                usedLayer.add(project.getLayer(layer.image));
                return "cell.getStateImage(" + layer.image + ") == (" + genJavaCode(postfix, SIMLAYER, usedLayer) + ")";


            case AnalyzerConstants.NAME:
                if(project.isLayerExists(token.image)) {
                    usedLayer.add(project.getLayer(token.image));
                    return "cell.getStateImage(" + token.image + ")";
                } else {
                    return " " + project.getStates().getState(token.image).getValue() + " ";
                }
                

            case AnalyzerConstants.DOT:
                layer = postfix.pollLast();
                usedLayer.add(project.getLayer(layer.image));
                return genJavaCode(postfix, layer.image, usedLayer);


            default:
                throw new IllegalArgumentException("Unknown token " + token.toString());
        }
        
    }
    
    
    private String getJava(Token token) {
        switch(token.kind) {
            case AnalyzerConstants.OR : return "||";
            case AnalyzerConstants.AND : return "&&";
            case AnalyzerConstants.XOR : return "^";
            case AnalyzerConstants.NOT : return "!";

            case AnalyzerConstants.TRUE : return "true";
            case AnalyzerConstants.FALSE : return "false";

            case AnalyzerConstants.EQUAL : return "==";
            case AnalyzerConstants.NEQ : return "!=";                
            case AnalyzerConstants.GEQ : 
            case AnalyzerConstants.LEQ : 
            case AnalyzerConstants.GREAT : 
            case AnalyzerConstants.LESS : 
                return token.image;

            case AnalyzerConstants.PLUS : 		
            case AnalyzerConstants.MINUS : 		
            case AnalyzerConstants.TIMES : 	
            case AnalyzerConstants.DIVIDE : 
                return token.image;
            case AnalyzerConstants.MODULO : return "%";	

/*FUNCTIONS*/

// LOCALS
            case AnalyzerConstants.NBCELLNEU : return "nbCellNeu";
            case AnalyzerConstants.NBCELLMOO : return "nbCellMoo" ;
            case AnalyzerConstants.NBCELLSQ : return "nbCellSq" ;
            case AnalyzerConstants.NBCELLCIR : return "nbCellCir";
            case AnalyzerConstants.PCELLSQ : return "pCellSq";
            case AnalyzerConstants.PCELLCIR : return "pCellCir";
            case AnalyzerConstants.NBCELLANG : return "nbCellAng";
            case AnalyzerConstants.PCELLANG : return "pCellAng";
            case AnalyzerConstants.CELLDURATION : return "cellDuration";
            case AnalyzerConstants.NORTH :      return "north"	;	
            case AnalyzerConstants.SOUTH :      return  "south" ;	
            case AnalyzerConstants.EAST :       return  "east"	;
            case AnalyzerConstants.WEST :       return  "west"	;	
            case AnalyzerConstants.NORTH_EAST : return  "northEast";		
            case AnalyzerConstants.NORTH_WEST : return  "northWest";		
            case AnalyzerConstants.SOUTH_EAST : return  "southEast";		
            case AnalyzerConstants.SOUTH_WEST : return  "southWest";		

// GLOBALS

            case AnalyzerConstants.NBCELL : return          "nbCell" 	;
            case AnalyzerConstants.PCELL : return           "pCell" 	;
            case AnalyzerConstants.STEP : return            "step" 		;	  			
            case AnalyzerConstants.NBSAMESTEP : return      "nbSameStep" 	;
            case AnalyzerConstants.NBCELLDURATION : return  "nbCellDuration"; 	

            default:
                throw new IllegalArgumentException("Unknown token " + token);
        }
    }
}
