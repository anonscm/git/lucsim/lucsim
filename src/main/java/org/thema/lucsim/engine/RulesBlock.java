/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a set of rules (rules that will be executed at the same time at
 * the computation of a simulation step).
 * @see Rule
 * @author rgrillot
 * @author alexis
 */
public class RulesBlock implements Serializable {

    /**
     * Enumeration of the differents types of blocks
     */
    public enum BlockType {

        /**
         * Execute the block one time
         */
        SIMPLE,

        /**
         * Execute the block a given number of times
         */
        REPEAT,

        /**
         * Execute the block if a given condition is true
         */
        IF,

        /**
         * Execute the block while a given condition is true
         */
        WHILE,

        /**
         * No block at all
         */
        NOBLOCK
    }
    
    private BlockType type;
    private int nbRepeat;
    private Condition condition;

    /**
     * List of the rules composing the block
     */
    private List<Rule> rules;

    /**
     * Creates a new RulesBlock for the SIMPLE or NOBLOCK types.
     * @param type SIMPLE or NOBLOCK type
     * @throws IllegalArgumentException if the given type if different from 
     * SIMPLE or NOBLOCK
     */
    public RulesBlock(BlockType type) {
        if(type != BlockType.SIMPLE && type != BlockType.NOBLOCK) {
            throw new IllegalArgumentException("BlockType must be SIMPLE or NOBLOCK");
        }
        this.type = type;
        rules = new ArrayList<>();
    }

    /**
     * Creates a new RulesBlock for the REPEAT type.
     * @param nbRepeat number of times to execute the block
     */
    public RulesBlock(int nbRepeat) {
        rules = new ArrayList<>();
        type = BlockType.REPEAT;
        this.nbRepeat = nbRepeat;
    }

    /**
     * Creates a new RulesBlock for the IF or WHILE types.
     * @param type IF or WHILE type
     * @param condition condition on which the block is executed
     * @throws IllegalArgumentException if the given type if different from 
     * IF or WHILE
     */
    public RulesBlock(BlockType type, Condition condition) {
        if(type != BlockType.IF && type != BlockType.WHILE) {
            throw new IllegalArgumentException("BlockType must be IF or WHILE");
        }
        rules = new ArrayList<>();
        this.type = type;
        this.condition = condition;
    }

    /**
     * Returns the number of rules composing the block.
     * @return number of rules composing this block
     */
    public int getNbRules() {
        return rules.size();
    }

    /**
     * Returns the list of rules composing the block.
     * @return list of rules composing this block.
     */
    public List<Rule> getRules() {
        return rules;
    }

    /**
     * Adds a given rule to the block.
     * @param rule rule to add
     */
    public void add(Rule rule) {
        rules.add(rule);
    }

    /**
     * Returns the rule at a given index.
     * @param i index of the rule to fetch
     * @return rule at the given index
     */
    public Rule getElement(int i) {
        return rules.get(i);
    }

    /**
     * Returns the type of the block.
     * @return type of the block
     */
    public BlockType getType() {
        return type;
    }

    /**
     * Returns the number of times to execute the block (REPEAT type only).
     * @return number of times to execute the block
     */
    public int getNbRepeat() {
        return nbRepeat;
    }

    /**
     * Returns the condition on which to execute the block (IF or WHILE types
     * only).
     * @return condition on which to execute the block
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Returns the rules having a given state as an initial state.
     * @param state initial state to check
     * @return rules having the given state as an initial state
     */
    public List<Rule> getRules(State state, List<Rule> result) {
        if(result == null) {
            result = new ArrayList<>();
        } else {
            result.clear();
        }
        for (Rule rule : rules) {
            if (rule.containInitState(state)) {
                result.add(rule);
            }
        }
        return result;
    }
    
    @Override
    public String toString() {
        String result = "";
        switch(type) {
            case IF: result += "if(" + condition + ") {\n"; break;
            case REPEAT: result += "repeat(" + nbRepeat + ") {\n"; break;
            case WHILE: result += "while(" + condition + ") {\n"; break;
            case SIMPLE: result += "{\n"; break;
        }
        
        for(Rule rule : rules) {
            result += (type != BlockType.NOBLOCK ? "\t" : "") + rule.toString() + "\n";
        }
        
        return result + (type != BlockType.NOBLOCK ? "}\n" : "");
    }
}
