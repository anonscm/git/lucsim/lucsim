/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.Color;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;
import org.thema.drawshape.style.RasterStyle;
import org.thema.drawshape.style.SimpleStyle;
import org.thema.drawshape.style.table.UniqueColorTable;

/**
 * Represents a set of states.
 * @author gvuidel
 */
public class States {
    
    /**
     * Value reserved for NODATA 
     */
    public static final int NODATA_VAL = 255;

    /**
     * NODATA state
     */
    public static final State NODATA_STATE = new State(NODATA_VAL, new Color(0, 0, 0, 0), "NODATA");
    
    /**
     * List of states of a state layer
     */
    private List<State> states;
    
    private transient RasterStyle rasterStyle;

    /**
     * Default constructor.
     */
    public States() {
        states = new ArrayList<>();
    }

    /**
     * Returns this states object list of states.
     * @return
     */
    public List<State> getStates() {
        return states;
    }
    
    /**
     * Returns the state having a given value; throws an exception if this value 
     * does not correspond to any state.
     * @param value value to use to retrieve the state
     * @return state having the given value
     * @throws UnknownStateException if no state of the list has the given value
     */
    public State getState(int value) throws UnknownStateException {
        if(value == NODATA_VAL) {
            return NODATA_STATE;
        }
        for(State state : states) {
            if(state.getValue() == value) {
                return state;
            }
        }
        
        throw new UnknownStateException(String.valueOf(value));
    }
    
    
    /**
     * Returns the state having a given name; throws an exception if this name
     * does not correspond to any state.
     *
     * @param name name to use to retrieve the state
     * @return state having the given name
     * @throws UnknownStateException if no state of the list has the given name
     */
    public State getState(String name) throws UnknownStateException {
        for(State state : states) {
            if(name.equals(state.getName())) {
                return state;
            }
        }

        throw new UnknownStateException(name);
    }
    
    /**
     * Checks if the list of states contains a state having a given name.
     * @param name name to use to check
     * @return whether the list of states contains a state having the given name
     */
    public boolean containsState(String name) {
        return states.stream().anyMatch(s -> s.getName().equals(name));
    }
    
    /**
     * Checks if the list of states contains a state having a given value.
     * @param value value to use to check
     * @return whether the list of states contains a state having the given 
     * value
     */
    public boolean containsState(int value) {
        return states.stream().anyMatch(s -> s.getValue() == value);
    }
    
    /**
     * Returns the state at a given index in the list of states.
     * @param index index of the state to fetch
     * @return state at the given index
     */
    public State getStateFromIndex(int index) {
        return states.get(index);
    }   
    
    /**
     * Returns the index of a given state in the list of states.
     * @param state state to search
     * @return index of the state in the list of states or -1 if the state is 
     * not found
     */
    public int getStateIndex(State state) {
        return states.indexOf(state);
    }
    
    /**
     * Adds a state to the list of states.
     * The state is created, added and returned; it only has a value, but 
     * neither name nor color.
     * @return added state, or null if no state was added
     */
    public State addState() {
        for(int i = 0; i < StateLayer.MAXSTATE; i++) {
            if (!containsState(i)) {
                State state = new State(i);
                addState(state);
                return state;
            }
        }
        
        return null;
    }
    
    /**
     * Adds a given state to the list of states.
     * @param state state to add
     */
    public void addState(State state) {
        states.add(state);

        Collections.sort(states);

        updateStyle();
    }
    
    /**
     * Removes a state of the list of states.
     * @param state state to delete
     */
    public void removeState(State state)  {
        final boolean exist = states.remove(state);

        if (exist) {
            updateStyle();
        } 
    }

    /**
     * Updates the style (colors).
     */
    public void updateStyle() {
        getRasterStyle().setColorBuilder(createUniqueColorTable());
        getRasterStyle().update();
    }
    
    private UniqueColorTable createUniqueColorTable() {
        final ArrayList<Color> colors = new ArrayList<>(states.size());
        final ArrayList<Double> values = new ArrayList<>(states.size());

        for (State state : states) {
            colors.add(state.getColor());
            values.add((double) state.getValue());
        }

        UniqueColorTable uct = new UniqueColorTable(values, colors);

        return uct;
    }

    /**
     * Returns the raster style.
     * @return raster style
     */
    public synchronized RasterStyle getRasterStyle() {
        if(rasterStyle == null) {
            rasterStyle = new RasterStyle(createUniqueColorTable(), false, false);
        }
        return rasterStyle;
    }
    
    /**
     * Updates the list of states with a given image.
     * @param img image to use for the update
     */
    public void updateStates(RenderedImage img) {
        final boolean hashState[] = new boolean[StateLayer.MAXSTATE+1];
        Arrays.fill(hashState, false);
        for(State state : states) {
            hashState[state.getValue()] = true;
        }
        ColorModel cm = img.getColorModel();
        final int width = img.getWidth();
        final int height = img.getHeight();
        RandomIter it = RandomIterFactory.create(img, null);
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                int value = it.getSample(i, j, 0);
                if (value >= 0 && value <= StateLayer.MAXSTATE) {
                    if (!hashState[value]) {
                        if(value == NODATA_VAL) {
                            addState(NODATA_STATE);
                        } else {
                            Color color;
                            try {
                                color = new Color(cm.getRGB(value));
                            } catch(IllegalArgumentException ex) {
                                color = SimpleStyle.randomColor();
                            }
                            State state = new State(value, color);
                            addState(state);
                        }
                        hashState[value] = true;
                    }
                } else {
                    throw new IllegalStateException("States values outside range [0-255]");
                }
            }
        }
    }
    
    /**
     * Creates a list of states from a given image.
     * @param img image to retrieve the states from
     * @return states object with a list of states filled from the given image
     */
    public static States createStatesFromGrid(RenderedImage img) {
        States states = new States();
        states.updateStates(img);
        return states;
    }
}
