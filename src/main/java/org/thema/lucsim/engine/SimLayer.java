/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Represents the layer used in the simulation.
 * @author rgrillot
 * @author alexis
 * @see Layer
 */
public final class SimLayer extends StateLayer implements Serializable {

    /**
     * Initial layer. Note: this layer will not be saved in the XML file
     */
    private StateLayer initLayer;
    /**
     * Raster of the image
     */
    private byte [] sim;
    /**
     * Duration for which a cell has not been modified; two dimensions in one 
     * array, access to the element at table[i][j] is done by 
     * table[i * nbColumns + j]
     * @see Cell
     */
    private int [] duration;
    

    /**
     * Constructs a new simulation layer from an existing state layer.
     * @param l state layer to copy
     */
    public SimLayer(StateLayer l) {
        super(l.getName() + "_sim", l.getRaster(null), l.getEnvelope(), l.getStates());
        this.initLayer = l;
        duration = new int[getWidth() * getHeight()];
        
        WritableRaster raster = Raster.createBandedRaster(DataBuffer.TYPE_BYTE, getWidth(), getHeight(), 1, null);
        sim = ((DataBufferByte)raster.getDataBuffer()).getData();
        for(int y = 0; y < getHeight(); y++) {
            for(int x = 0; x < getWidth(); x++) {
                sim[y*getWidth()+x] = (byte)initLayer.getElement(x, y);
            }
        }
        setRaster(raster);
        nbCell = calcNbCell();
    }

    /**
     * Returns the initial layer of this simulation layer.
     * @return initial layer of this simulation layer
     */
    public StateLayer getInitLayer() {
        return initLayer;
    }
    
    
    /**
     * Creates the list of values to save the simulation steps data (used in the 
     * simulation graph).
     * @return list of values of the simulation steps data
     */
    public int nbSameStep() {
        int min = duration[0];
        for(int dur : duration) {
            if(dur < min) {
                min = dur;
            }
        }
        return min;
    }

    /**
     * Returns the number of cells having a duration equals to a given one.
     * @param dur duration to compare
     * @return number of cells having a duration equals to the given one
     * @see SimLayer#duration
     */
    public int nbCellDuration(int dur) {
        int cpt = 0;

        for(int d : duration) {
            if (d == dur) {
                cpt++;
            }
        }

        return cpt;
    }

    /**
     * Resets this simulation layer to its initial layer.
     */
    public void reset(){
        final int width = getWidth();
        final int height = getHeight();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                sim[j*width+i] = (byte) initLayer.getElement(i, j);
            }
        }

        Arrays.fill(duration, 0); 
        nbCell = calcNbCell();
    }
    
   
    /**
     * Returns the value of the state of a cell at a given position in this 
     * simulation layer's initial layer.
     * @param x horizontal coordinate of the cell to get
     * @param y vertical coordinate of the cell to get
     * @return value of the state of a cell at a given position in this 
     * simulation layer's initial layer
     */
    public double getInitElement(int x, int y) {
        return initLayer.getElement(x, y);
    }

    /**
     * Overridden method for speed purposes.
     * @param x 
     * @param y
     * @return 
     */
    @Override
    public double getElement(int x, int y) {
        return sim[y*getWidth()+x] & 0xFF;
    }
    
    /**
     * Sets a given value to a state of the cell at a given position of this 
     * layer.
     * @param x horizontal coordinate of the cell to get
     * @param y vertical coordinate of the cell to get
     * @param value
     */
    public void setElement(int x, int y, int value) {
        final int ind = y * getWidth() + x;
        nbCell[sim[ind] & 0xFF]--;
        sim[ind] = (byte) value;
        duration[ind] = 0;
        nbCell[value]++;
    }

    /**
     * Returns the duration for which a cell at a given position has not been 
     * modified
     * retourne le temps de non changement d'états de la case située à position
     *
     * @param x horizontal coordinate of the cell to get
     * @param y vertical coordinate of the cell to get
     * @return duration for which a cell at the given position has not been 
     * modified
     */
    public int getDuration(int x, int y) {
        return duration[y * getWidth() + x];
    }

    /**
     * Increments the duration of the cell at a given position.
     * @param x horizontal coordinate of the cell to get
     * @param y vertical coordinate of the cell to get
     */
    public void incDuration(int x, int y) {
        duration[y * getWidth() + x]++;
    }  

    /**
     * Creates the list of values to save 
     * Creer la liste des valeurs pour la sauvegarde des données des étape de
     * simulation (Utilisé dans la graphique de simulation)
     *
     * @return
     */
    @Override
    public int[] listeNbCell() {
        return nbCell;
    }
    
    private int[] calcNbCell() {
        int result[] = new int[MAXSTATE+1];
        for (byte value : sim) {
            result[(value & 0xFF)]++;
        }
        return result;
    }
}
