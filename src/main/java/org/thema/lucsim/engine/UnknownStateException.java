/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

/**
 * Represents the exception thrown when a state not present in a layer is 
 * queried.
 * @author rgrillot
 * @author alexis
 * @see State
 * @see Layer
 */
public class UnknownStateException extends RuntimeException {

    /**
     * chaine de caractère expliquant l'erreur produite
     */
    private String error;
    private String state;

    /**
     * Default constructor.
     * @param stateName name of the unknown state
     */
    public UnknownStateException(String stateName) {
        state = stateName;

        error = "State \"" + stateName + "\" is unknown.\n";
    }

    @Override
    public String getMessage() {
        return error;
    }

    /**
     * Returns the name of the state that caused the exception.
     * @return name of the state that caused the exception
     */
    public String getState() {
        return state;
    }
}
