/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.thema.lucsim.analysis.MarkovChain;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.thema.common.Util;
import org.thema.lucsim.parser.Analyzer;
import org.thema.lucsim.parser.ParseException;

/**
 * Represents the user's project, with the layers, the states and the transition
 * rules.
 * @author rgrillot
 * @author alexis
 * @see Layer
 * @see State
 * @see Rule
 */
public class Project implements Serializable {

    /**
     * Project's state layers
     */
    private List<StateLayer> stateLayers;
    
    /**
     * Project's numerical layers
     */
    private List<NumLayer> numLayers;
    
    /**
     * List of states of the state layers
     */
    private States states;

    /**
     * Project's transition rules
     * @see RulesBlock
     */
    private transient List<RulesBlock> rules;
    
    /**
     * String representing every rule that as been typed by the user, even if 
     * they are false or not validated; it is this string that will be saved
     */
    private String stringRules;
    
    private StateLayer simuLayer;
    
    private transient Simulation simulation;
    
    private MarkovChain markovChain;
    
    /**
     * Default constructor.
     * @param layer state layer of the project
     */
    public Project(StateLayer layer) {
        stateLayers = new ArrayList<>();
        numLayers = new ArrayList<>();
        stateLayers.add(layer);
        this.states = layer.getStates();
        stringRules = "";
        simuLayer = layer;
    }

    /**
     * Returns the project's state layers
     * @return project's state layers
     */
    public List<StateLayer> getStateLayers() {
        return stateLayers;
    }

    /**
     * Returns the project's numerical layers
     * @return project's numerical layers
     */
    public List<NumLayer> getNumLayers() {
        return numLayers;
    }

    /**
     * Returns the project's layers (state/numerical layers, and the simulation 
     * layer if there is one).
     * @return project's layers
     */
    public List<Layer> getLayers() {
        List<Layer> layers = new ArrayList<>();
        if(simulation != null) {
            layers.add(simulation.getSimLayer());
        }
        layers.addAll(stateLayers);
        layers.addAll(numLayers);
        
        return layers;
    }
    
    /**
     * Returns the layer (state/numerical layer) having a given name, null if it 
     * does not exist.
     * @param name name of the layer to get
     * @return layer having the given name
     * @throws UnknownLayerException if no layer has the given name
     */
    public Layer getLayer(String name) throws UnknownLayerException {
        for(Layer l : stateLayers) {
            if(l.getName().equals(name)) {
                return l;
            }
        }
        for(Layer l : numLayers) {
            if(l.getName().equals(name)) {
                return l;
            }
        }

        throw new UnknownLayerException(name);
    }
    
    /**
     * Returns the numerical layer having a given name, null if it * does not 
     * exist.
     * @param name name of the layer to get
     * @return layer having the given name
     * @throws UnknownLayerException if no layer has the given name
     */
    public NumLayer getNumLayer(String name) throws UnknownLayerException {
        for(NumLayer l : numLayers) {
            if(l.getName().equals(name)) {
                return l;
            }
        }

        throw new UnknownLayerException(name);
    }
    
    /**
     * Returns the state layer having a given name, null if it does not exist.
     * @param name name of the layer to get
     * @return layer having the given name
     * @throws UnknownLayerException if no layer has the given name
     */
    public StateLayer getStateLayer(String name) throws UnknownLayerException {
        for(StateLayer l : stateLayers) {
            if(l.getName().equals(name)) {
                return l;
            }
        }

        throw new UnknownLayerException(name);
    }

    /**
     * Checks if a layer having a given name exists.
     * @param name name to check
     * @return whether a layer has a given name.
     */
    public boolean isLayerExists(String name)  {
        return getLayerNames().contains(name);
    }

    /**
     * Returns a list of the layer's names (state/numerical layers).
     * @return  list of the layer's names
     */
    public List<String> getLayerNames() {
        List<String> names = new ArrayList<>();
        for(Layer l : numLayers) {
            names.add(l.getName());
        }
        for(Layer l : stateLayers) {
            names.add(l.getName());
        }
        return names;
    }

    /**
     * Adds a layer (state/numerical layer) to the project.
     * @param layer layer to add
     * @throws IllegalArgumentException if the layer already exists
     */
    public void addLayer(Layer layer) {
        if(isLayerExists(layer.getName())) {
            throw new IllegalArgumentException("The layer " + layer.getName() + " already exists");
        }
        if(layer instanceof StateLayer) {
            stateLayers.add((StateLayer)layer);
        } else {
            numLayers.add((NumLayer)layer);
        }
    }

    /**
     * Removes a layer from the project.
     * @param layer layer to remove
     */
    public void removeLayer(Layer layer)  {
        if(layer instanceof NumLayer) {
            numLayers.remove((NumLayer)layer);
        } else {
            if(simuLayer == layer || markovChain != null && markovChain.isLayerUsed((StateLayer)layer)) {
                throw new IllegalArgumentException("The layer cannot be removed, it is used in simulation or analysis.");
            }
            stateLayers.remove((StateLayer)layer);
        }
    }


    /**
     * Returns the index of a given layer in the layers list, or -1 if this 
     * layer is not in the list.
     * @param layer layer's index to fetch
     * @return index of a given layer in the layers list
     */
    public int indexOfLayer(Layer layer) {
        return getLayers().indexOf(layer);
    }
    
    /**
     * Sets the simulation layer as the layer having a given name.
     * @param name name of the layer to set as the simulation layer
     * @return simulation created with this layer
     * @throws UnknownLayerException if no layer has the given name
     */
    public Simulation setSimuLayer(String name) throws UnknownLayerException {
        simuLayer = (StateLayer) getLayer(name);
        simulation = new Simulation(this, simuLayer);
        return simulation;
    }
    
    /**
     * Sets the string representation of the project's rules to a given string.
     * @param stringRules string representation of the proejct's rules to set
     * @throws ParseException if the rules are not valid
     */
    public void setStringRules(String stringRules) throws ParseException {
        this.stringRules = stringRules;
        validateRules();
    }

    /**
     * Checks if the project's rules are valid.
     * @throws ParseException if the rules are not valid
     */
    public void validateRules() throws ParseException {
        getRulesBlocks().clear();
        try (StringReader sr = new StringReader(stringRules)) {
            Analyzer analyz = new Analyzer(sr);
            analyz.validationStart(this);
        }
    }
    
    /**
     * Returns every rule that as been typed by the user.
     * @return every rule that as been typed by the user
     */
    public String getStringRules() {
        return stringRules;
    }

    /**
     * Builds a string representation of the project's rules.
     * @return string representation of the project's rules
     */
    public String generateTextRules() {
        String newRules = "";
        for(RulesBlock rule : getRulesBlocks()) {
            newRules += rule.toString() + "\n";
        }
        return newRules;
    }
    
    /**
     * Returns the project's rules block.
     * @return project's rules blocks
     */
    public synchronized List<RulesBlock> getRulesBlocks() {
        if(rules == null) {
            rules = new ArrayList<>();
        }
        return rules;
    }

    /**
     * Returns the project's states.
     * @return project's states
     */
    public States getStates() {
        return states;
    }

    /**
     * Returns the project's simulation.
     * @return project's simulation
     */
    public synchronized Simulation getSimulation() {
        if(simulation == null) {
            simulation = new Simulation(this, simuLayer);
        }
        return simulation;
    }

    /**
     * Returns the project's Markov chain.
     * @return project's Markov chain
     */
    public MarkovChain getMarkovChain() {
        return markovChain;
    }

    /**
     * Sets the project's Markov chain to a given Markov chain.
     * @param markovChain Markov chain to set
     */
    public void setMarkovChain(MarkovChain markovChain) {
        this.markovChain = markovChain;
    }

    /**
     * Saves the project as an XML file and a resource folder.
     * @param prjFile file to save the project
     * @throws IOException if an error occurs while dealing with files
     */
    public void save(File prjFile) throws IOException {
        try (FileWriter fw = new FileWriter(prjFile)) {
            // saving the project in an XML file
            Util.getXStream().toXML(this, fw);
        }
        // writing in the resource "-maps" folder
        // creating the folder
        File maps = new File(prjFile.getParentFile(), prjFile.getName() + "-maps");
        maps.mkdir();
        for(Layer l : getStateLayers()) {
            l.saveLayer(maps);
        }
        for(Layer l : getNumLayers()) {
            l.saveLayer(maps);
        }
    }
    
    /**
     * Load a project.
     * @param file XML file project
     * @return a new loaded project
     * @throws IOException 
     */
    public static Project load(File file) throws IOException {
        
        XStream xstream = Util.getXStream();
        Project project = (Project) xstream.fromXML(file);

        // On crée un File image qui servira à charger les images dans les couches 
        File directory = new File(file.getParent(), file.getName() + "-maps");

        // On charge maintenant toutes les couches et on les affiche dans la liste des couches 
        for (Layer l : project.getLayers()) {
            File image = new File(directory, l.getName() + ".tif");
            l.loadLayer(image);
        }

        return project;
    }
}
