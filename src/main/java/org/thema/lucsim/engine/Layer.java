/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.geometry.Envelope2D;
import org.thema.data.IOImage;
import org.thema.drawshape.style.RasterStyle;

/**
 * Represents a cellular automaton layer.
 * @author rgrillot 
 * @author alexis
 * @see NumLayer
 * @see SimLayer
 * @see StateLayer
 */
public abstract class Layer implements Serializable {

    /**
     * Raster corresponding to the image
     */
    private transient Raster raster;
    
    private transient Envelope2D envelope;
    
    private transient AffineTransform world2Grid, grid2World;
    
    /**
     * Name of the layer
     */
    private String name;

    /**
     * Style of the raster
     */
    protected transient RasterStyle style;

    /**
     * Constructs a layer with a given name, an image representing
     * the layer (converted to a raster) and an envelope for the grid coverage.
     * @param name name of the layer
     * @param img image representing the layer
     * @param envelope envelope to use for the grid coverage
     */
    public Layer(String name, RenderedImage img, Envelope2D envelope) {
        this(name, (WritableRaster)img.getData(), envelope);
        
    }
    
    /**
     * Constructs a layer with a given name, an raster representing
     * the layer and an envelope for the grid coverage.
     * @param name name of the layer
     * @param raster raster representing the layer
     * @param envelope envelope to use for the grid coverage
     */
    protected Layer(String name, Raster raster, Envelope2D envelope) {
        this.name = name;
        this.raster = raster;
        this.envelope = envelope;
    }
    

    /**
     * Saves a layer to the TIF format in the folder "file" with the name
     * &lt;layer_name&gt;.tif.
     * @param dir folder where to save the file
     * @throws IOException if the given file is not a folder or a problem occurs
     */
    public void saveLayer(File dir) throws IOException {
        saveLayer(dir, getName());
    }
    
    /**
     * Saves a layer to the TIF format in the folder "file" with the given name
     * @param dir folder where to save the file
     * @param name filename without tif extension
     * @throws IOException if the given file is not a folder or a problem occurs
     */
    public void saveLayer(File dir, String name) throws IOException {
        if (!dir.isDirectory()) {
            throw new IOException("File is not directory");
        }
        File map = new File(dir, name + ".tif");
        IOImage.saveTiffCoverage(map, new GridCoverageFactory().create(name, (WritableRaster)raster, envelope));
    }


    /**
     * Loads the image in a given file.
     * @param file file of the image to load; has to be a TIF image (.tif) 
     * or AsciiGrid
     * @throws IOException if the file is not valid
     */
    public void loadLayer(File file) throws IOException {
        GridCoverage2D grid = IOImage.loadCoverage(file);
        this.envelope = grid.getEnvelope2D();
        this.raster = grid.getRenderedImage().getData();
    }

    /**
     * Sets the raster of this layer to a given raster.
     * @param raster the raster to set
     */
    protected final void setRaster(Raster raster) {
        this.raster = raster;
    }

    public boolean hasSameGrid(Layer l) {
        return getEnvelope().boundsEquals(l.getEnvelope(), 0, 1, 1e-3) && raster.getBounds().equals(l.raster.getBounds());
    }
    
    /**
     * Returns the envelope of this layer.
     * @return the envelope of this layer
     */
    public Envelope2D getEnvelope() {
        return envelope;
    }
    
    /**
     *  
     * @return
     */
    public AffineTransform getGrid2World() {
        if(grid2World == null) {
            final double sx = envelope.getWidth() / getWidth();
            final double sy = envelope.getHeight() / getHeight();
            grid2World = new AffineTransform(
                sx, 0, 0, -sy, 
                envelope.getMinX()+sx/2, envelope.getMaxY()-sy/2);
        }
        return grid2World;
    }
    
    /**
     *
     * @return
     */
    public AffineTransform getWorld2Grid() {
        if(world2Grid == null) {
            final double sx = getWidth() / envelope.getWidth();
            final double sy = getHeight() / envelope.getHeight();
            return new AffineTransform(sx, 0, 0, -sy, 
                    -envelope.getMinX()*sx, envelope.getMaxY()*sy);
        }
        return world2Grid;
    }
    
    /**
     * Returns the height of this layer.
     * @return height of this layer
     */
    public final int getHeight() {
        return raster.getHeight();
    }

    /**
     * Returns the width of this layer.
     * @return width of this layer
     */
    public final int getWidth() {
        return raster.getWidth();
    }

    /**
     * Returns the size of a cell of this layer.
     * @return size of a cell of this layer
     */
    public double getSizeCell() {
        return ((envelope.getMaxX() - envelope.getX()) / getWidth());
    }

    /**
     * Returns the number of cells of this layer per meter.
     * @param m number of meters
     * @return number of cells of this layer per meter
     */
    public int getNbCellForMeter(double m) {
        return (int) (m / getSizeCell());
    }

    /**
     * Returns the cell of this layer at a given position (point).
     * @param position position of the cell to get
     * @return cell of this layer at the given position (NaN if this position is
     * out of the grid)
     */
    public double getElement(Point2D position) {
        if (envelope.contains(position)) {
            Point2D p = getWorld2Grid().transform(position, null);
            return getElement((int)p.getX(), (int)p.getY());
        } else {
            return Double.NaN;
        }
    }

    /**
     * Returns the cell of this layer at a given position (x and y in grid coordinates).
     * @param x horizontal coordinate of the cell to get
     * @param y vertical coordinate of the cell to get
     * @return cell of this layer at the given position
     */
    public double getElement(int x, int y) {
        return raster.getSampleDouble(x, y, 0);
    }

    
    /**
     * Returns true if a cell exists at a given position (x and y in grid coordinates).
     * @param x horizontal coordinate of the cell to get
     * @param y vertical coordinate of the cell to get
     * @return true if a cell exists at the given position
     */
    public boolean contains(int x, int y) {
        return x >= 0 && y >= 0 && x < getWidth() && y < getHeight();
    }
    
    /**
     * Returns the name of this layer.
     * @return name of this layer
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    /** 
     * Sets the name of this layer to a given name.
     * @param name name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Returns a "sub-raster", with the size and coordinates of a given 
     * rectangle, of the raster of this layer.
     * @param rect rectangle to retrieve size and coordinates from
     * @return a "sub-raster", from the given size and coordinates of the given 
     * rectangle, of the raster of this layer
     */
    public Raster getRaster(Rectangle rect) {
        if(rect == null) {
            return raster;
        } else {
            return raster.createChild(rect.x, rect.y, rect.width, rect.height, rect.x, rect.y, null);
        }
    }

    /**
     * Returns the number of cells in a given state in this layer.
     * @param state state to take into account
     * @return number of cells in a given state in this layer
     */
    public int nbCell(final double state) {
        int result = 0;

        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                if (raster.getSampleDouble(j, i, 0) == state) {
                    result++;
                }
            }
        }

        return result;
    }

    /**
     * Returns the proportion of cells in a given state in this layer.
     * @param state state to take into account
     * @return proportion of cells in a given state in this layer.
     */
    public double pCell(final double state) {
        return (double) nbCell(state) / (double) (getHeight() * getWidth());
    }

    /**
     * Returns true if this layer is a state layer.
     * @return whether this layer is a state layer
     */
    abstract public boolean isStateLayer();

    /**
     * Returns a list of the states' names of this layer.
     * @return list of the states' names of this layer
     */
    abstract public ArrayList<String> listStateString();
    
    /**
     * Returns the style of this layer.
     * @return style of this layer
     */
    public synchronized RasterStyle getStyle() {
        if(style == null) {
            style = new RasterStyle();
        }
        return style;
    }
}
