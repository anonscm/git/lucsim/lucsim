/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.Color;
import java.io.Serializable;

/**
 * Classe représentant un état que peut prendre une cellule
 *
 * @author rgrillot
 * @author alexis
 * @see Cell
 */
public final class State implements Comparable, Serializable {

    /**
     * L'identifiant de l'état. ne peut pas être modifié
     */
    private int value;
    /**
     * La couleur que doit prendre une cellule étant dans cet état
     */
    private Color color;
    /**
     * Le nom de l'état
     */
    private String name;


    /**
     * Constructeur par défaut
     *
     * @param value la valeur correspondant à l'état
     */
    public State(int value) {
        this.value = value;
        color = Color.BLACK;
        name = "state" + value;
    }

    /**
     * constructeur.
     *
     * @param value la valeur correspondant à l'état
     * @param rgb la couleur de l'état (norme srgb : bits 0-7 = bleu, 8-15 =
     * vert, 16-23 = rouge)
     */
    public State(int value, Color rgb) {
        this(value);
        color = rgb;
    }

    /**
     * constructeur.
     *
     * @param value la valeur correspondant à l'état
     * @param color la couleur de l'état
     * @param name le nom de l'état
     */
    public State(int value, Color color, String name) {
        this.value = value;
        this.color = color;
        this.name = name;
    }

    /**
     * Pour obtenir la couleur de l'état
     *
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Pour donner une couleur à l'état
     *
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Pour donner une couleur à l'état
     *
     * @param rgb la couleur de l'état à mettre (bits 0-7 = bleu, 8-15 = vert,
     * 16-23 = rouge)
     */
    public void setColor(int rgb) {
        color = new Color(rgb);
    }

    /**
     * Pour obtenir le nom de l'état
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Pour donner un nom à l'état
     *
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Pour obtenir l'identifiant de l'état
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }

    @Override
    public int compareTo(Object o) {
        State state = (State) o;

        return Integer.compare(value, state.value);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        return this.value == ((State)obj).value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.value;
        return hash;
    }

    @Override
    public String toString() {
        return name;
    }
    
}
