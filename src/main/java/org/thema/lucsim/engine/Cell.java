/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.Raster;
import org.geotools.geometry.DirectPosition2D;

/** 
 * Represents a cell, component of a layer.
 * @author xinouch 
 * @author rgrillot 
 * @author alexis
 * @see Layer
 */
public final class Cell {

    private StateLayer simLayer;
    private Point position;
    
    private Point2D worldPos = null;

    /**
     * Default constructor.
     */
    public Cell(StateLayer simLayer) {
        this(simLayer, new Point(0, 0));
    }

    /**
     * Constructs a new cell with a given position.
     * @param p
     */
    public Cell(StateLayer simLayer, Point p) {
        this.simLayer = simLayer;
        position = new Point(p);
    }

    /**
     * Sets the position of the cell at a given position.
     * @param position position to set
     */
    public void setPosition(final Point position) {
        this.position.setLocation(position.x, position.y);
        worldPos = null;
    }

    /**
     * Sets the position of the cell at given x and y coordinates.
     * @param x x coordinate of the position to set
     * @param y y coordinate of the position to set
     */
    public void setPosition(int x, int y) {
        position.setLocation(x, y);
        worldPos = null;
    }
        
    /**
     * Returns the x coordinate of the cell's position.
     * @return x coordinate of the cell's position
     */
    public int getX() {
        return position.x;
    }
    
    /**
     * Returns the y coordinate of the cell's position.
     * @return y coordinate of the cell's position
     */
    public int getY() {
        return position.y;
    }
    
    /**
     * @return the position of the cell in world coordinate
     */
    public Point2D getWorldPosition() {
        if(worldPos == null) {
            worldPos = simLayer.getGrid2World().transform(position, new DirectPosition2D());
        }
        
        return worldPos;
    }
    
    private Point getPosition(Layer layer) {
        Point p = position;
        if(layer != simLayer && !simLayer.hasSameGrid(layer)) {
            p = new Point();
            layer.getWorld2Grid().transform(getWorldPosition(), p);
        } 
        return p;
        
    }
    
    /**
     * Returns the cell's state (value) in a given layer.
     * @param layer layer to query
     * @return cell's state (value) in the given layer
     */
    public double getStateImage(final Layer layer) {
        if(layer == simLayer || simLayer.hasSameGrid(layer)) {
            return layer.getElement(position.x, position.y);
        } else {
            return layer.getElement(getWorldPosition());
        }
    }

    /**
     * Returns the number of cells in a given state in a given layer, using 
     * the von Neumann neighborhood (4 cells: N, S, E, W) of the cell.
     * @param layer layer to query
     * @param state state to count
     * @return number of cells in the given state in a von Neumann neighborhood
     * in the given layer
     */
    public int nbCellNeu(final Layer layer, final double state) {
        int result = 0;
        
        if(layer == simLayer || simLayer.hasSameGrid(layer)) {
            if (position.x > 0 && layer.getElement(position.x-1, position.y) == state) {
                result++;
            }
            if (position.x < layer.getWidth()-1 && layer.getElement(position.x+1, position.y) == state) {
                result++;
            }
            if (position.y > 0 && layer.getElement(position.x, position.y-1) == state) {
                result++;
            }
            if ((position.y < layer.getHeight()-1) && (layer.getElement(position.x, position.y+1) == state)) {
                result++;
            }
        } else {
            if(getElement(layer, -1, 0) == state) {
                result++;
            }
            if(getElement(layer, 0, -1) == state) {
                result++;
            }
            if(getElement(layer, +1, 0) == state) {
                result++;
            }
            if(getElement(layer, 0, +1) == state) {
                result++;
            }
        }
        
        return result;
    }

    /**
     * Returns the number of cells in a given state in a given layer, using the 
     * Moore neighborhood (8 cells: N, NE, E, SE, S, SW, W, NW) of the cell.
     * @param layer layer to query
     * @param state state to count
     * @return number of cells in the given state in a Moore neighborhood in the 
     * given layer
     */
    public int nbCellMoo(final Layer layer, final double state) {
        return nbCellSq(layer, state, 1);
    }

    private Raster getRaster(final Layer layer, final int radius) {
        Point p = getPosition(layer);
        
        int originX = p.x - radius, originY = p.y - radius;
        int finalX = p.x + radius, finalY = p.y + radius;

        if (originX < 0) {
            originX = 0;
        }
        if (originY < 0) {
            originY = 0;
        }

        if (finalX >= layer.getWidth()) {
            finalX = layer.getWidth() - 1;
        }
        if (finalY >= layer.getHeight()) {
            finalY = layer.getHeight() - 1;
        }

        return layer.getRaster(new Rectangle(originX, originY, finalX-originX+1, finalY-originY+1));
    }
    
    
    /**
     * Returns the number of cells in a given state in a given layer, using the
     * squared neighborhood of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param radius max distance from the studied cell 
     * @return number of cells in the given state in a squared neighborhood in 
     * the given layer
     */
    public int nbCellSq(final Layer layer, final double state, final int radius) {
        int result = 0;
        
        final Raster raster = getRaster(layer, radius);
        final Rectangle r = raster.getBounds();
        final int maxX = (int)r.getMaxX();
        final int maxY = (int)r.getMaxY();
        for (int j = (int)r.getMinY(); j < maxY; j++) {
            for (int i = (int)r.getMinX(); i < maxX; i++) {
                if (raster.getSampleDouble(i, j, 0) == state) {
                    result++;
                }
            }
        }
        if(layer == simLayer && raster.getSampleDouble(position.x, position.y, 0) == state) {
            result--;
        }
        
        return result;
    }

    /**
     * Returns the proportion of cells in a given state in a given layer, using 
     * the squared neighborhood of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param radius max distance from the studied cell 
     * @return proportion of cells in the given state in a squared neighborhood
     * in the given layer
     */
    public double pCellSq(final Layer layer, final double state, final int radius) {
        Rectangle r = getRaster(layer, radius).getBounds();
        final double nbcells = r.getWidth()*r.getHeight() - 1;
        return (double) nbCellSq(layer, state, radius) / nbcells;
    }
    
    /**
     * Returns the number of cells in a given state in a given layer, using the
     * circular neighborhood of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param radius radius of the circle
     * @return number of cells in the given state in a circular neighborhood in
     * the given layer
     */
    public int nbCellCir(final Layer layer, final double state, final int radius) {
        int[] nb = cellCir(layer, state, radius);
        return nb[0];

    }

    /**
     * Returns the proportion of cells in a given state in a given layer, using 
     * the neighborhood of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param radius radius of the circle
     * @return proportion of cells in the given state in a circular neighborhood 
     * in the given layer
     */
    public double pCellCir(final Layer layer, final double state, final int radius) {
        int[] nb = cellCir(layer, state, radius);
        return nb[0] / (double)nb[1];
    }
    
    /**
     * Returns the number of cells in a given state in a given layer, using the
     * circular neighborhood, and the total number of cells in the 
     * neighborhood of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param radius radius of the circle
     * @return number of cells in a given state in a given layer, using a 
     * circular neighborhood, and the total number of cells in the neighborhood
     */
    private int[] cellCir(final Layer layer, final double state, final int radius) {
        int total = 0;
        int result = 0;

        final Point p = getPosition(layer);
        final int radius2 = radius * radius;
        final Raster raster = getRaster(layer, radius);
        final Rectangle r = raster.getBounds();
        final int maxX = (int)r.getMaxX();
        final int maxY = (int)r.getMaxY();
        for(int j = (int)r.getMinY(); j < maxY; j++) {
            final int dy2 = (p.y - j) * (p.y - j);
            for(int i = (int)r.getMinX(); i < maxX; i++) {
                if((p.x - i) * (p.x - i) + dy2 <= radius2) {
                    total++;
                    if (raster.getSampleDouble(i, j, 0) == state) {
                        result++;
                    }
                }
            }
        }
        
        if(layer == simLayer && raster.getSampleDouble(position.x, position.y, 0) == state) {
            result--;
        }
        
        return new int[]{result, total-1};
    }
    
    /**
     * Returns the number of cells in a given state in a given layer, in given
     * angles and radius of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param angle1 first angle
     * @param angle2 second angle
     * @param radius max distance from the studied cell 
     * @return number of cells in a given state in a given layer, in given 
     * angles and radius
     */
    public int nbCellAng(final Layer layer, final double state, final double angle1, final double angle2, final int radius) {

        final Point p = getPosition(layer);
        
        int nb = 0;
        final int radius2 = radius * radius;
        final Raster raster = getRaster(layer, radius);
        final Rectangle r = raster.getBounds();
        final int maxX = (int)r.getMaxX();
        final int maxY = (int)r.getMaxY();
        for (int j = (int)r.getMinY(); j < maxY; j++) {
            final int dy = p.y - j;
            final int dy2 = dy*dy;
            for (int i = (int)r.getMinX(); i < maxX; i++) {
                final int dx = i - p.x;
                final int dist = dx*dx + dy2;
                if(dist > 0 && dist <= radius2) {
                    if (raster.getSampleDouble(i, j, 0) == state) {
                        //maintenant on va tester l'angle
                        //on remet en position la cellule testé dans le repere de la cellule en execution
                        final double angle = ((90 - (Math.atan2(dy, dx) * 180 / Math.PI)) + 360) % 360;

                        if(angle1 < angle2) {
                            if (angle >= angle1 && angle <= angle2) {
                                nb++;
                            }
                        } else {
                            if(angle >= angle1 || angle <= angle2) {
                                nb++;
                            }
                        }
                    }
                }
            }
        }
        
        return nb;
    }

    /**
     * Returns the proportion of cells in a given state in a given layer, in 
     * given angles and radius of the cell.
     * @param layer layer to query
     * @param state state to count
     * @param angle1 first angle
     * @param angle2 second angle
     * @param radius max distance from the studied cell 
     * @return proportion of cells in a given state in a given layer, in given 
     * angles and radius
     */
    public double pCellAng(final Layer layer, final double state, final double angle1, final double angle2, final int radius) {
        final Point p = getPosition(layer);
        int total = 0;
        int nb = 0;
        final int radius2 = radius * radius;
        final Raster raster = getRaster(layer, radius);
        final Rectangle r = raster.getBounds();
        final int maxX = (int)r.getMaxX();
        final int maxY = (int)r.getMaxY();
        for (int j = (int)r.getMinY(); j < maxY; j++) {
            final int dy = p.y - j;
            final int dy2 = dy*dy;
            for (int i = (int)r.getMinX(); i < maxX; i++) {
                final int dx = i - p.x;
                final int dist = dx*dx + dy2;
                if (dist > 0 && dist <= radius2) {
                    //maitnenant on va tester l'angle
                    //on remet en position la cellule testé dans le repere de la cellule en execution
                    final double angle = ((90 - (Math.atan2(dy, dx) * 180 / Math.PI)) + 360) % 360;

                    if (angle1 < angle2) {
                        if (angle >= angle1 && angle <= angle2) {
                            total++;
                            if (raster.getSampleDouble(i, j, 0) == state) {
                                nb++;
                            }
                        }
                    } else {
                        if (angle >= angle1 || angle <= angle2) {
                            total++;
                            if (raster.getSampleDouble(i, j, 0) == state) {
                                nb++;
                            }
                        }
                    }
                }

            }
        }
        
        if (total == 0) {
            return 0;
        }
        return nb / (double)total;
    }

    /**
     * Returns the value of the cell to the North of the cell, or -1 if there 
     * is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the North of the cell, or -1 if there 
     * is no cell at the position
     */
    public double north(final Layer layer) {
        return getElement(layer, 0, -1);
    }

    /**
     * Returns the value of the cell to the South of the cell, or -1 if there 
     * is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the South of the cell, or -1 if there 
     * is no cell at the position
     */
    public double south(final Layer layer) {
        return getElement(layer, 0, +1);
    }

    /**
     * Returns the value of the cell to the East of the cell, or -1 if there 
     * is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the East of the cell, or -1 if there 
     * is no cell at the position
     */
    public double east(final Layer layer) {
        return getElement(layer, +1, 0);
    }

    /**
     * Returns the value of the cell to the West of the cell, or -1 if there 
     * is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the West of the cell, or -1 if there 
     * is no cell at the position
     */
    public double west(final Layer layer) {
        return getElement(layer, -1, 0);
    }

    /**
     * Returns the value of the cell to the North-East of the cell, or -1 if 
     * there is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the North-East of the cell, or -1 if there 
     * is no cell at the position
     */
    public double northEast(final Layer layer) {
        return getElement(layer, +1, -1);
    }

    /**
     * Returns the value of the cell to the North-West of the cell, or -1 if 
     * there is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the North-West of the cell, or -1 if there 
     * is no cell at the position
     */
    public double northWest(final Layer layer) {
        return getElement(layer, -1, -1);
    }

    /**
     * Returns the value of the cell to the South-East of the cell, or -1 if 
     * there is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the South-East of the cell, or -1 if there 
     * is no cell at the position
     */
    public double southEast(final Layer layer) {
        return getElement(layer, +1, +1);
    }

    /**
     * Returns the value of the cell to the South-West of the cell, or -1 if 
     * there is no cell at the position.
     * @param layer layer to query
     * @return value of the cell to the South-West of the cell, or -1 if there 
     * is no cell at the position
     */
    public double southWest(final Layer layer) {
        return getElement(layer, -1, +1);
    }
    
    private double getElement(Layer layer, int dx, int dy) {
        if(layer == simLayer || simLayer.hasSameGrid(layer)) {
            if (layer.contains(position.x + dx, position.y + dy)) {
                return layer.getElement(position.x + dx, position.y + dy);
            } else {
                return Double.NaN;
            }
        } else {
            DirectPosition2D p = new DirectPosition2D(position.x + dx, position.y + dy);
            simLayer.getGrid2World().transform(p, p);
            return layer.getElement(p);
        }
    }
}
