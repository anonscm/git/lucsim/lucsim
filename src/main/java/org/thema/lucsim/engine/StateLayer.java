/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.thema.lucsim.engine;

import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.geotools.geometry.Envelope2D;
import org.thema.drawshape.style.RasterStyle;

/**
 * Represents a state layer.
 * @author rgrillot
 * @author alexis
 * @see Layer
 */
public class StateLayer extends Layer implements Serializable {
    
    /**
     *
     */
    public static final int MAXSTATE = 255;

    private States states;
    
    /**
     * Stores number of cells for each states
     */
    protected transient int nbCell[];
    
    /**
     * Constructs a numeric layer with a given name, an image representing the 
     * layer and an envelope for the grid coverage.
     * @param name name of the layer
     * @param img image representing the layer
     * @param envelope envelope to use for the grid coverage
     * @param states states contained in the layer
     */
    public StateLayer(String name, RenderedImage img, Envelope2D envelope, States states) {
        super(name, img, envelope);
        if (!isImageForStateLayer(img)){
            throw new IllegalArgumentException("Layer can't be loaded because layer contains states outside limits [0 - 255]\n"
                    + "(maximum 255 states)");
        }
        this.states = states;
        states.updateStates(img);
    }
    
    /**
     * Internal constructor. 
     * Does not check state range and does not update states list.
     * @param name name of the layer
     * @param img image representing the layer
     * @param envelope envelope to use for the grid coverage
     * @param states states contained in the layer
     */
    protected StateLayer(String name, Raster img, Envelope2D envelope, States states) {
        super(name, img, envelope);
        this.states = states;
    }


    /**
     * Checks if a loaded TIF image contains only values acceptable in a state 
     * layer (integers between 0 and 255, as a state layer does not have states
     * whose values are below 0 and greater than 254, and 255 is reserved 
     * for the NODATA state).
     * @param img image to check
     * @return wether the image is suitable for a state layer
     */
    public static boolean isImageForStateLayer(RenderedImage img) {
        final int width = img.getWidth();
        final int height = img.getHeight();
        RandomIter iter = RandomIterFactory.create(img, null);
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {    
                double value = iter.getSampleDouble(i, j, 0);
                if (value < 0 || value > MAXSTATE || value != (int)value) {
                    return false;
                }
            }
        }
        return true;
    }
    
   
    /**
     * Creates the list of values to save the simulation steps data (used in the 
     * simulation graph).
     * @return list of values of the simulation steps data
     */
    public int[] listeNbCell() {
        if(nbCell == null) {
            int result[] = new int[MAXSTATE+1];
            for (int i = 0; i < getHeight(); i++) {
                for (int j = 0; j < getWidth(); j++) {
                    result[(int) getElement(j, i)]++;
                }
            }
            nbCell = result;
        }
        return nbCell;
    }
    
    /**
     * Returns the number of cells in a given state in this layer.
     * @param state state to take into account
     * @return number of cells in a given state in this layer
     */
    @Override
    public int nbCell(final double state) {
        return listeNbCell()[(int)state];
    }

    /**
     * Returns the states object of this layer.
     * @return states object of this layer
     * @see States
     */
    public States getStates() {
        return states;
    }

    /**
     * Returns the raster style corresponding to all statelayer
     * @return raster style common to all statelayer
     */
    @Override
    public synchronized RasterStyle getStyle() {
        if(style == null) {
            style = states.getRasterStyle();
        } 
        return style;
    }


    @Override
    public boolean isStateLayer() {
        return true;
    }

    /**
     * Returns a list of the states' names of this layer.
     * @return list of the states' names of this layer
     */
    @Override
    public ArrayList<String> listStateString() {
        ArrayList<String> arr = new ArrayList<>();
        listeNbCell();
        for (int i = 0; i < MAXSTATE; i++) {
            if(nbCell[i] > 0) {
                arr.add(states.getState(i).getName());
            }
        }
        return arr;
    }
    
    /**
     * Creates a comparison matrix between two state layers.
     * The layers must have the same dimensions. The row and column indices 
     * correspond to the states indexs.
     * @param layer layer to compare
     * @return omparison matrix between the two given state layers
     */
    public RealMatrix getMatrixComparison(StateLayer layer) {
        if(getWidth() != layer.getWidth() || getHeight() != layer.getHeight()) {
            throw new IllegalArgumentException("Layers dimension are not equals.");
        }
        RealMatrix matrix = MatrixUtils.createRealMatrix(states.getStates().size(), states.getStates().size());
        int [] tabInd = new int[MAXSTATE+1];
        Arrays.fill(tabInd, -1);
        for(int i = 0; i < MAXSTATE+1; i++) {
            try {
                State state = states.getState(i);
                tabInd[i] = states.getStateIndex(state);
            } catch(UnknownStateException ex) {
                // does nothing
            }
        }
        for(int y = 0; y < getHeight(); y++) {
            for(int x = 0; x < getWidth(); x++) {
                matrix.addToEntry(tabInd[(int)getElement(x, y)], tabInd[(int)layer.getElement(x, y)], 1);
            }
        }
        
        return matrix;
    }
    
    /**
     * Creates a vector containing the number of cells for each state. 
     * The vector indices correspond to the states indexs.
     * @return vector containing the number of cells for each state
     */
    public RealVector getNbCellVector() {
        int[] listeNbCell = listeNbCell();
        double[] v = new double[states.getStates().size()];
        for(int i = 0; i < listeNbCell.length; i++) {
            if(listeNbCell[i] > 0) {
                v[states.getStateIndex(states.getState(i))] = listeNbCell[i];
            }
        }
        return MatrixUtils.createRealVector(v);
    }
}
