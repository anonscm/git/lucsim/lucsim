/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 *
 * @author alexis
 */
public class DessinExe extends JPanel {

    /**
     * hauteur
     */
    private int h;
    /**
     * largeur
     */
    private int w;
    /**
     * coordonnée abscisse du coin en haut a gauche
     */
    private int x;
    /**
     * coordonnée ordonné du coin en faut a gauche
     */
    private int y;
    /**
     * couleur de la figure (ici rectangle)
     */
    private Color color;

    /**
     * Constructor
     *
     * @param a
     * @param b
     * @param width
     * @param height
     * @param c
     */
    public DessinExe(int a, int b, int width, int height, Color c) {

        h = height;

        w = width;

        x = a;

        y = b;

        color = c;
    }

    /**
     * Creation du rectangle transparent, de couleur color
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));

        g2.setColor(color);

        g2.fillRect(x, y, w, h + 3);
    }
}
