/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.lucsim.gui.analysis;

import javax.swing.table.AbstractTableModel;
import org.apache.commons.math3.linear.RealVector;
import org.thema.lucsim.engine.States;

/**
 *
 * @author Gilles Vuidel
 */
public class StateVectorTableModel extends AbstractTableModel{

    private RealVector vector;
    private States states;

    public StateVectorTableModel(RealVector vector, States states) {
        this.vector = vector;
        this.states = states;
    }
    
    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount() {
        return vector.getDimension();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return vector.getEntry(columnIndex);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return Double.class;
    }

    @Override
    public String getColumnName(int column) {
        return states.getStateFromIndex(column).getName();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
}
