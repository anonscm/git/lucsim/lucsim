/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.lucsim.gui.analysis;

import javax.swing.table.AbstractTableModel;
import org.apache.commons.math3.linear.RealMatrix;
import org.thema.lucsim.engine.States;

/**
 *
 * @author gvuidel
 */
public class StateMatrixTableModel extends AbstractTableModel{

    private RealMatrix matrix;
    private States states;

    public StateMatrixTableModel(RealMatrix matrix, States states) {
        this.matrix = matrix;
        this.states = states;
    }
    
    @Override
    public int getRowCount() {
        return matrix.getRowDimension(); 
    }

    @Override
    public int getColumnCount() {
        return matrix.getColumnDimension()+1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex == 0) {
            return states.getStateFromIndex(rowIndex).getName();
        } else {
            double val = matrix.getEntry(rowIndex, columnIndex-1);
            return val == 0 ? null : val;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnIndex == 0 ? String.class : Double.class;
    }

    @Override
    public String getColumnName(int column) {
        if(column == 0) {
            return "Init\\Final";
        }
        return states.getStateFromIndex(column-1).getName();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
}
