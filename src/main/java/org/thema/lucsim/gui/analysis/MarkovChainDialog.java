/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.lucsim.gui.analysis;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import org.thema.lucsim.analysis.MarkovChain;
import org.thema.lucsim.engine.StateLayer;

/**
 *
 * @author gvuidel
 */
public class MarkovChainDialog extends javax.swing.JDialog {

    public MarkovChain markov;
    
    /** Creates new form MarkovChainDialog */
    public MarkovChainDialog(java.awt.Frame parent, List<StateLayer> layers, MarkovChain markov) {
        super(parent, true);
        initComponents();
        setLocationRelativeTo(parent);
        
        initialComboBox.setModel(new DefaultComboBoxModel(layers.toArray()));
        finalComboBox.setModel(new DefaultComboBoxModel(layers.toArray()));
        startComboBox.setModel(new DefaultComboBoxModel(layers.toArray()));

        if(markov != null) {
            initialComboBox.setSelectedItem(markov.getInitLayer());
            finalComboBox.setSelectedItem(markov.getFinalLayer());
            startComboBox.setSelectedItem(markov.getStartLayer());
            powerSpinner.setValue(markov.getPower());
            this.markov = markov;
        } else {
            this.markov = new MarkovChain(layers.get(0), layers.get(0), 1, layers.get(0));
        }
        
        matrixTable.setModel(new StateMatrixTableModel(this.markov.getMatrix(), this.markov.getInitLayer().getStates()));
        vectorTable.setModel(new StateVectorTableModel(this.markov.getResultVector(), this.markov.getInitLayer().getStates()));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        vectorTable = new javax.swing.JTable();
        finalComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        initialComboBox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        closeButton = new javax.swing.JButton();
        powerSpinner = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        matrixTable = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        startComboBox = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Transition : Markov chain");

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        vectorTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        vectorTable.setName("vectorTable"); // NOI18N
        jScrollPane2.setViewportView(vectorTable);

        finalComboBox.setName("finalComboBox"); // NOI18N
        finalComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                componentActionPerformed(evt);
            }
        });

        jLabel2.setText("Final layer");
        jLabel2.setName("jLabel2"); // NOI18N

        initialComboBox.setName("initialComboBox"); // NOI18N
        initialComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                componentActionPerformed(evt);
            }
        });

        jLabel5.setText("Vector result");
        jLabel5.setName("jLabel5"); // NOI18N

        jLabel1.setText("Initial layer");
        jLabel1.setName("jLabel1"); // NOI18N

        closeButton.setText("Close");
        closeButton.setName("closeButton"); // NOI18N
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        powerSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        powerSpinner.setName("powerSpinner"); // NOI18N
        powerSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                powerSpinnerStateChanged(evt);
            }
        });

        jLabel3.setText("Start layer");
        jLabel3.setName("jLabel3"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        matrixTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        matrixTable.setName("matrixTable"); // NOI18N
        jScrollPane1.setViewportView(matrixTable);

        jLabel4.setText("Matrix power");
        jLabel4.setName("jLabel4"); // NOI18N

        startComboBox.setName("startComboBox"); // NOI18N
        startComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                componentActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(initialComboBox, 0, 169, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(finalComboBox, 0, 170, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(powerSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(444, 444, 444))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(closeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startComboBox, 0, 171, Short.MAX_VALUE)
                        .addGap(281, 281, 281))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(initialComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(finalComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(powerSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(startComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(closeButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeButtonActionPerformed

    private void componentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_componentActionPerformed
        // if constructor initialization, do nothing
        if(markov == null) {
            return;
        }
        
        if(evt.getSource() == startComboBox) {
            markov.setStartLayer((StateLayer)startComboBox.getSelectedItem());
        } else {
            markov = new MarkovChain((StateLayer)initialComboBox.getSelectedItem(), (StateLayer)finalComboBox.getSelectedItem(), 
                    (Integer)powerSpinner.getValue(), (StateLayer)startComboBox.getSelectedItem());
            matrixTable.setModel(new StateMatrixTableModel(markov.getMatrix(), markov.getInitLayer().getStates()));
            markov.getResultVector();
        }
        vectorTable.setModel(new StateVectorTableModel(this.markov.getResultVector(), this.markov.getInitLayer().getStates()));
    }//GEN-LAST:event_componentActionPerformed

    private void powerSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_powerSpinnerStateChanged
        componentActionPerformed(new ActionEvent(powerSpinner, 0, "power"));
    }//GEN-LAST:event_powerSpinnerStateChanged

  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeButton;
    private javax.swing.JComboBox finalComboBox;
    private javax.swing.JComboBox initialComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable matrixTable;
    private javax.swing.JSpinner powerSpinner;
    private javax.swing.JComboBox startComboBox;
    private javax.swing.JTable vectorTable;
    // End of variables declaration//GEN-END:variables
}
