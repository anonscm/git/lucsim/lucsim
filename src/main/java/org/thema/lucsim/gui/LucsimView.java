/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.apache.commons.math3.linear.RealVector;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.data.DataSourceException;
import org.geotools.geometry.Envelope2D;
import org.jdesktop.application.Action;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.SingleFrameApplication;
import org.thema.common.JavaLoader;
import org.thema.common.ProgressBar;
import org.thema.common.Util;
import org.thema.common.swing.PreferencesDialog;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOImage;
import org.thema.drawshape.SelectableShape;
import org.thema.drawshape.ui.MapInternalFrame;
import org.thema.lucsim.analysis.PotentialLayer;
import org.thema.lucsim.engine.ConventionNommage;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.NumLayer;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.Rule;
import org.thema.lucsim.engine.RulesBlock;
import org.thema.lucsim.engine.RulesBlock.BlockType;
import org.thema.lucsim.engine.SimLayer;
import org.thema.lucsim.engine.Simulation;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.engine.States;
import org.thema.lucsim.engine.UnknownLayerException;
import org.thema.lucsim.engine.UnknownStateException;
import org.thema.lucsim.gui.analysis.MarkovChainDialog;
import org.thema.lucsim.gui.analysis.PotentialDialog;
import org.thema.lucsim.gui.generator.dt.DecisionTreeDialog;
import org.thema.lucsim.gui.generator.dt.PostProcessDialog;
import org.thema.lucsim.gui.stat.DataExportDialog;
import org.thema.lucsim.gui.stat.Graph;
import org.thema.lucsim.gui.stat.ImagesComparison;
import org.thema.lucsim.parser.AnalyzerConstants;
import org.thema.lucsim.parser.ParseException;
import org.thema.lucsim.parser.Token;

/**
 * The application's main frame.
 */
public class LucsimView extends FrameView {

    private boolean useJDK = false;

    /**
     * main constructor
     *
     * @param app
     */
    public LucsimView(SingleFrameApplication app) {
        super(app);
        
        dlmLayerAC = new DefaultListModel();
        dlmStates = new DefaultListModel();
        initComponents();
        textAreaRules = new JTextPane();
        textAreaRules.setText("/*Start to write a rule*/");
        textAreaRules.setFont(Font.decode("Monospaced-Plain-15"));
        scrollPane.setViewportView(textAreaRules);
        tln = new TextLineNumber(textAreaRules, 1);
        scrollPane.setRowHeaderView(tln);
        textAreaRules.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                colorSyntax(false, null);
                isSaved = false;
            }
        });

        listStates.setComponentPopupMenu(popupMenuStates);
        listLayers.setComponentPopupMenu(popupMenuLayer);
        isSaved = true;
        //panel information sur etat
        panelPointGlob.setVisible(false);
        //buttonDistanceCord.setVisible(false); 
        arrCoord = new ArrayList<>();
        this.getFrame().setTitle("Lucsim " + JavaLoader.getVersion(LucsimApp.class)); //NOI18N
        this.getFrame().setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.getFrame().addWindowListener(exitListener);

        envelopeBox = new EnvCreation(this.getFrame());
        envelopeBox.setVisible(false);

        //page de demarrage
        startPage();
        jProgressBar.setStringPainted(true);
        jProgressBar.setString("Ready");
    }

    WindowListener exitListener = new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
            if (isSaved == false) {
                showWannaSave();
            }
            System.exit(0);
        }
    };

    /**
     * Montre le popup "voulez-vous sauvegarder" et permet la sauvegarde si
     * l'utilisateur répond "oui"
     */
    @Action
    private void showWannaSave() {
        int res = JOptionPane.showConfirmDialog(this.getFrame(), "Project is not saved. Do you want to save it now ?", "Project", JOptionPane.YES_NO_OPTION);

        if (res == JOptionPane.YES_OPTION) {
            try {
                save();
            } catch (IOException ex) {
                Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
                showErrorPopup(ex.getMessage());
            }
        }
    }

    /**
     * ouvre la fenêtre de création d'une extension spatiale (enveloppe)
     *
     * @param img l'image sur laquelle faire l'extension spatiale
     */
    @Action
    private Envelope2D showEnvelopeBox(RenderedImage img) {

        envelopeBox.setImage(img);
        envelopeBox.setLocationRelativeTo(this.getFrame());
        envelopeBox.setVisible(true);
        if (envelopeBox.isValidated()) {
            return envelopeBox.getEnvelope();
        } else {
            return null;
        }
    }

    /**
     * ouvre la fenêtre d'erreur
     *
     * @param errorMsg le message d'erreur à montrer à l'utilisateur
     */
    @Action
    public void showErrorPopup(String errorMsg) {
        JOptionPane.showMessageDialog(getFrame(), errorMsg);
    }

    /**
     * ouvre la fenêtre de création d'une position
     *
     * @return le point entré, null si annulation
     */
    @Action
    private Point2D showPointCreation() {
        PointCreation pointCreationBox = new PointCreation(getFrame());
        LucsimApp.getApplication().show(pointCreationBox);
        return pointCreationBox.getPosition();
    }

    /**
     * affiche la liste des états d'une cellule
     *
     * @param position la position a afficher
     */
    @Action
    private void showStateDisplay(Point2D position) {

        panelPointGlob.removeAll();
        stateDisplay = new PanelPoint(simulation, position);
        panelPointGlob.setLayout(new java.awt.BorderLayout());
        panelPointGlob.add(stateDisplay, BorderLayout.CENTER);
        panelPointGlob.validate();
        if (buttonDistanceCord.isSelected()) {
            distanceCord = new DistanceCord(arrCoord);
            panelPointGlob.setLayout(new java.awt.BorderLayout());
            panelPointGlob.add(distanceCord, BorderLayout.SOUTH);
            panelPointGlob.validate();
            panelPointGlob.repaint();
            panelPointGlob.setVisible(true);
            panelPointGlob.validate();
        }
        panelPointGlob.repaint();
        panelPointGlob.setVisible(true);

    }

    /**
     * ouvre la fenêtre d'edition des états puis met éventuellement à jour les
     * fenêtres ouvertes dans le desktopPane
     *
     * @param state le nom de l'état à modifier
     */
    @Action
    private void showStateEdition(State state) {
        StateEdition stateEditionBox = new StateEdition(getFrame(), state, project);
        LucsimApp.getApplication().show(stateEditionBox);
    }

    /**
     * ouvre la fenêtre d'edition des couches puis modifie le nom des fenêtres
     * ouvertes si besoin
     *
     * @param layer le nom de la couche à modifier
     */
    @Action
    private void showLayerEdition(final Layer layer) {
        final JFrame mainFrame = LucsimApp.getApplication().getMainFrame();
        String res = JOptionPane.showInputDialog(mainFrame, "Layer name", layer.getName());
        if(res == null || layer.getName().equals(res)) {
            return;
        }
        try {
            ConventionNommage.checkName(res, project);
        } catch (ConventionNommage.BadNameException ex) {
            JOptionPane.showMessageDialog(getFrame(), "Bad layer name : " + ex.getMessage());
            return;
        }
        
        layer.setName(res);
        /* On met à jour les fenêtres représentant les couches s'il y en a des ouvertes */
        MapInternalFrame mif = findMapInternalFrame(layer);
        if (mif != null) {
            mif.setName(layer.getName());
            mif.setTitle(layer.getName());
        }

    }

    /**
     * affiche un MapInteralFrame dans le desktopPane
     * <p>
     * Si la couche est déjà affichée dans un MapInternalFrame, on se contente
     * de le mettre au premier plan : on n'en crée pas un nouveau. Dans ce cas,
     * c'est le MapInternalFrame existant qui est retourné.</p>
     *
     * @param layer la couche à afficher
     * @param name le nom de la fenêtre à afficher. si null, le nom de la couche
     * layer est utilisé
     * @return le MapInternalFrame créé
     */
    public LayerInternalFrame showMapInternalFrame(final Layer layer, String name) {

        LayerInternalFrame mif = findMapInternalFrame(layer);

        //Si la fenetre n'existe pas
        if (mif == null) {
            mif = new LayerInternalFrame(name, layer);
            desktopPane.add(mif);
            addMouseListener(mif);
        }

        try {
            mif.setSelected(true);
            mif.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.WARNING, null, ex);
        }

        return mif;
    }

    /**
     * fonction qui creer une page de demarage dans le deskop pane
     */
    public final void startPage() {

        MapInternalFrame mif = new MapInternalFrame();
        mif.setVisible(true);
        mif.setTitle("Start Page");
        mif.setBackground(Color.WHITE);
        desktopPane.add(mif);

        try {
            mif.setSelected(true);
            mif.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.WARNING, null, ex);
        }
        StartPage startP = new StartPage();

        mif.add(startP);

        startP.getNewProject().addActionListener((ActionEvent e) -> {
            try {
                newProject();
            } catch (IOException ex) {
                Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
                showErrorPopup(ex.getMessage());
            }
        });

        startP.getOpenProject().addActionListener((ActionEvent e) -> {
            openProject();
        });

        startP.getCloseProject().addActionListener((ActionEvent e) -> {
            desktopPane.removeAll();
            desktopPane.repaint();
        });
    }

    /**
     * Sauvegarde le projet en cours dans un fichier ".ca", et les images dans
     * un sous-dossier "maps"
     */
    private void save() throws IOException {

        File file;
        if (fileProject == null) {
            file = Util.getFileSave(".ca");
        } else {
            file = fileProject;
        }
        if (file != null) {
            fileProject = file;
            try {
                validateRules();
            } catch (Exception ex) {
                Logger.getLogger(LucsimView.class.getName()).log(Level.WARNING, "", ex);
            }

            project.save(file);

            isSaved = true;
            getFrame().setTitle("Lucsim " + JavaLoader.getVersion(LucsimApp.class) + " - " + fileProject.getName());
        }
    }

    /**
     * Ferme le projet + simulation en cours
     */
    private void closeProject() {
        if (project == null) {
            return;
        }

        if (simulation != null && simulation.isCompute()) {
            simulation.stopSimulation();
        }

        /* On gère la sauvegarde */
        if (isSaved == false) {
            showWannaSave();
        }

        /* On supprime toutes les règles dans la zone de saisie
         * et on supprime toutes les règles / couches de la simulation */
        textAreaRules.setText("{\n"
                + "\t\n"
                + "}");

        /* On supprime toutes les entrées dans les listes d'états / couches */
        dlmLayerAC.clear();
        dlmStates.clear();

        /* On supprime toutes les fenêtres ouvertes dans le desktopPane */
        for (JInternalFrame jif : desktopPane.getAllFrames()) {
            jif.dispose();
        }

        panelPointGlob.removeAll();
        panelPointGlob.repaint();
        panelPointGlob.setVisible(false);

        /* On verrouille les entrées dans les menus */
        unlockEntries(false);

        isSaved = true;
        simulation = null;
        project = null;
    }

    /**
     * verrouille ou déverrouille les entrées dans les différents menus
     *
     * @param b true si on veut déverrouiller, false si on veut verrouiller
     */
    private void unlockEntries(boolean b) {
        /* La barre de menu simulation*/
        synchrone.setEnabled(b);
        asynchrone.setEnabled(b);

        /* On déverrouille les entrées dans les menus */
        dispGraphCheckBoxMenuItem.setEnabled(b);
        menuItemSave.setEnabled(b);
        menuItemSaveUnder.setEnabled(b);

        menuItemClose.setEnabled(b);
        buttonChoiceCoordPoint.setEnabled(b);
        generatorButton.setEnabled(b);

        addState.setEnabled(b);
        addLayer.setEnabled(b);
        addStateText.setEnabled(b);

        modifState.setEnabled(b);
        modifName.setEnabled(b);
        removeState.setEnabled(b);
        removelayer.setEnabled(b);
        setSimLayer.setEnabled(b);
        showGraphButton.setEnabled(b);
        /* et dans les popup menus */
        popupMenuItemACLayerShowSimulation.setEnabled(b);
        popupMenuItemACLayerClone.setEnabled(b);
        popupMenuItemStatesModificate.setEnabled(b);
        popupMenuItemStatesAdd.setEnabled(b);
        popupMenuItemLayerAdd.setEnabled(b);
        popupMenuItemLayerChangeName.setEnabled(b);
        popupMenuItemLayerDelete.setEnabled(b);

    }

    /**
     * Trouve le MapInternalFrame ouvert dans le desktopPane s'il existe
     *
     * @param name le nom du MapInternalFrame à trouver
     * @return null si le MapInternalFrame cherché n'existe pas, un pointeur
     * vers le premier MapInternalFrame portant le nom name.
     */
    private LayerInternalFrame findMapInternalFrame(Layer layer) {
        for (JInternalFrame jif : desktopPane.getAllFrames()) {
            if (!(jif instanceof LayerInternalFrame)) {
                continue;
            }
            LayerInternalFrame lif = (LayerInternalFrame) jif;
            if (lif.getEngineLayer() == layer) {
                return lif;
            }
        }
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        listLayers = new javax.swing.JList();
        jToolBar3 = new javax.swing.JToolBar();
        addLayer = new javax.swing.JButton();
        modifName = new javax.swing.JButton();
        removelayer = new javax.swing.JButton();
        setSimLayer = new javax.swing.JButton();
        showGraphButton = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listStates = new javax.swing.JList();
        jToolBar4 = new javax.swing.JToolBar();
        addState = new javax.swing.JButton();
        addStateText = new javax.swing.JButton();
        modifState = new javax.swing.JButton();
        removeState = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        buttonValidate = new javax.swing.JButton();
        generatorButton = new javax.swing.JButton();
        panelPointGlob = new javax.swing.JPanel();
        buttonCloseCoordPoint = new javax.swing.JButton();
        buttonChoiceCoordPoint = new javax.swing.JButton();
        decommentsLigne = new javax.swing.JButton();
        commentsLigne = new javax.swing.JButton();
        buttonDistanceCord = new javax.swing.JButton();
        fullScreenRules = new javax.swing.JButton();
        scrollPane = new javax.swing.JScrollPane();
        posCurseur = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        start = new javax.swing.JButton();
        one = new javax.swing.JButton();
        stop = new javax.swing.JButton();
        reset = new javax.swing.JButton();
        jToolBar2 = new javax.swing.JToolBar();
        jProgressBar = new javax.swing.JProgressBar();
        synchrone = new javax.swing.JToggleButton();
        asynchrone = new javax.swing.JToggleButton();
        stepSimulationTxt = new javax.swing.JTextField();
        desktopPane = new javax.swing.JDesktopPane();
        indicBlockExe = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        menuItemNew = new javax.swing.JMenuItem();
        menuItemOpen = new javax.swing.JMenuItem();
        menuItemSave = new javax.swing.JMenuItem();
        menuItemSaveUnder = new javax.swing.JMenuItem();
        menuItemClose = new javax.swing.JMenuItem();
        prefMenuItem = new javax.swing.JMenuItem();
        byteBut = new javax.swing.JMenuItem();
        javax.swing.JMenuItem menuItemExit = new javax.swing.JMenuItem();
        statMenu = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuItemDataExtractor = new javax.swing.JMenuItem();
        analysisMenu = new javax.swing.JMenu();
        markovMenuItem = new javax.swing.JMenuItem();
        potentialMenuItem = new javax.swing.JMenuItem();
        rulesMenu = new javax.swing.JMenu();
        creationRuleMenu = new javax.swing.JMenu();
        menuItemDecisionTree = new javax.swing.JMenuItem();
        ruleConstraintsMenuItem = new javax.swing.JMenuItem();
        ruleRemConstraintsMenuItem = new javax.swing.JMenuItem();
        displayMenu = new javax.swing.JMenu();
        dispGraphCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        dispRulesCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        dispStatesLayersCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        dispMapCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        dispSimBarCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        popupMenuLayer = new javax.swing.JPopupMenu();
        popupMenuItemLayerAdd = new javax.swing.JMenuItem();
        popupMenuItemLayerChangeName = new javax.swing.JMenuItem();
        popupMenuItemLayerDelete = new javax.swing.JMenuItem();
        popupMenuItemACLayerShowSimulation = new javax.swing.JMenuItem();
        popupMenuItemACLayerClone = new javax.swing.JMenuItem();
        popupMenuStates = new javax.swing.JPopupMenu();
        popupMenuItemStatesModificate = new javax.swing.JMenuItem();
        popupMenuItemStatesAdd = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();

        jSplitPane2.setDividerLocation(220);

        listLayers.setModel(dlmLayerAC);
        listLayers.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listLayers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listLayersMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(listLayers);

        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(LucsimView.class);
        addLayer.setIcon(resourceMap.getIcon("addLayer.icon")); // NOI18N
        addLayer.setToolTipText(resourceMap.getString("addLayer.toolTipText")); // NOI18N
        addLayer.setEnabled(false);
        addLayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addLayerActionPerformed(evt);
            }
        });
        jToolBar3.add(addLayer);

        modifName.setIcon(resourceMap.getIcon("modifName.icon")); // NOI18N
        modifName.setToolTipText(resourceMap.getString("modifName.toolTipText")); // NOI18N
        modifName.setEnabled(false);
        modifName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifNameActionPerformed(evt);
            }
        });
        jToolBar3.add(modifName);

        removelayer.setIcon(resourceMap.getIcon("removelayer.icon")); // NOI18N
        removelayer.setToolTipText(resourceMap.getString("removelayer.toolTipText")); // NOI18N
        removelayer.setEnabled(false);
        removelayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removelayerActionPerformed(evt);
            }
        });
        jToolBar3.add(removelayer);

        setSimLayer.setIcon(resourceMap.getIcon("setSimLayer.icon")); // NOI18N
        setSimLayer.setToolTipText(resourceMap.getString("setSimLayer.toolTipText")); // NOI18N
        setSimLayer.setEnabled(false);
        setSimLayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setSimLayerActionPerformed(evt);
            }
        });
        jToolBar3.add(setSimLayer);

        showGraphButton.setIcon(resourceMap.getIcon("showGraphButton.icon")); // NOI18N
        showGraphButton.setText(resourceMap.getString("showGraphButton.text")); // NOI18N
        showGraphButton.setToolTipText(resourceMap.getString("showGraphButton.toolTipText")); // NOI18N
        showGraphButton.setEnabled(false);
        showGraphButton.setFocusable(false);
        showGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showGraphButton.setName("showGraphButton"); // NOI18N
        showGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showGraphButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(showGraphButton);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jToolBar3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jToolBar3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE))
        );

        listStates.setModel(dlmStates);
        listStates.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listStates.setToolTipText(resourceMap.getString("listStates.toolTipText")); // NOI18N
        listStates.setCellRenderer(new ListRendererState());
        listStates.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listStatesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(listStates);

        jToolBar4.setFloatable(false);
        jToolBar4.setRollover(true);

        addState.setIcon(resourceMap.getIcon("addState.icon")); // NOI18N
        addState.setToolTipText(resourceMap.getString("addState.toolTipText")); // NOI18N
        addState.setEnabled(false);
        addState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addStateActionPerformed(evt);
            }
        });
        jToolBar4.add(addState);

        addStateText.setIcon(resourceMap.getIcon("addStateText.icon")); // NOI18N
        addStateText.setToolTipText(resourceMap.getString("addStateText.toolTipText")); // NOI18N
        addStateText.setEnabled(false);
        addStateText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addStateTextMouseClicked(evt);
            }
        });
        jToolBar4.add(addStateText);

        modifState.setIcon(resourceMap.getIcon("modifState.icon")); // NOI18N
        modifState.setToolTipText(resourceMap.getString("modifState.toolTipText")); // NOI18N
        modifState.setEnabled(false);
        modifState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifStateActionPerformed(evt);
            }
        });
        jToolBar4.add(modifState);

        removeState.setIcon(resourceMap.getIcon("removeState.icon")); // NOI18N
        removeState.setToolTipText(resourceMap.getString("removeState.toolTipText")); // NOI18N
        removeState.setEnabled(false);
        removeState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeStateActionPerformed(evt);
            }
        });
        jToolBar4.add(removeState);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jToolBar4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 204, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                    .addGap(47, 47, 47)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane2.setLeftComponent(jPanel4);

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(0.85);
        jSplitPane1.setToolTipText(resourceMap.getString("jSplitPane1.toolTipText")); // NOI18N

        buttonValidate.setIcon(resourceMap.getIcon("buttonValidate.icon")); // NOI18N
        buttonValidate.setToolTipText(resourceMap.getString("buttonValidate.toolTipText")); // NOI18N
        buttonValidate.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        buttonValidate.setBorderPainted(false);
        buttonValidate.setContentAreaFilled(false);
        buttonValidate.setMaximumSize(new java.awt.Dimension(26, 26));
        buttonValidate.setMinimumSize(new java.awt.Dimension(26, 26));
        buttonValidate.setName("buttonValidate"); // NOI18N
        buttonValidate.setPreferredSize(new java.awt.Dimension(26, 26));
        buttonValidate.setSelectedIcon(resourceMap.getIcon("buttonValidate.selectedIcon")); // NOI18N
        buttonValidate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonValidateActionPerformed(evt);
            }
        });

        generatorButton.setIcon(resourceMap.getIcon("generatorButton.icon")); // NOI18N
        generatorButton.setToolTipText(resourceMap.getString("generatorButton.toolTipText")); // NOI18N
        generatorButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        generatorButton.setBorderPainted(false);
        generatorButton.setContentAreaFilled(false);
        generatorButton.setEnabled(false);
        generatorButton.setMaximumSize(new java.awt.Dimension(26, 26));
        generatorButton.setMinimumSize(new java.awt.Dimension(26, 26));
        generatorButton.setName("generatorButton"); // NOI18N
        generatorButton.setPreferredSize(new java.awt.Dimension(26, 26));
        generatorButton.setSelectedIcon(resourceMap.getIcon("generatorButton.selectedIcon")); // NOI18N
        generatorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generatorButtonActionPerformed(evt);
            }
        });

        panelPointGlob.setToolTipText(resourceMap.getString("panelPointGlob.toolTipText")); // NOI18N

        javax.swing.GroupLayout panelPointGlobLayout = new javax.swing.GroupLayout(panelPointGlob);
        panelPointGlob.setLayout(panelPointGlobLayout);
        panelPointGlobLayout.setHorizontalGroup(
            panelPointGlobLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 249, Short.MAX_VALUE)
        );
        panelPointGlobLayout.setVerticalGroup(
            panelPointGlobLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        buttonCloseCoordPoint.setFont(resourceMap.getFont("buttonCloseCoordPoint.font")); // NOI18N
        buttonCloseCoordPoint.setIcon(resourceMap.getIcon("buttonCloseCoordPoint.icon")); // NOI18N
        buttonCloseCoordPoint.setToolTipText(resourceMap.getString("buttonCloseCoordPoint.toolTipText")); // NOI18N
        buttonCloseCoordPoint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        buttonCloseCoordPoint.setBorderPainted(false);
        buttonCloseCoordPoint.setContentAreaFilled(false);
        buttonCloseCoordPoint.setSelectedIcon(resourceMap.getIcon("buttonCloseCoordPoint.selectedIcon")); // NOI18N
        buttonCloseCoordPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCloseCoordPointActionPerformed(evt);
            }
        });

        buttonChoiceCoordPoint.setFont(resourceMap.getFont("buttonChoiceCoordPoint.font")); // NOI18N
        buttonChoiceCoordPoint.setIcon(resourceMap.getIcon("buttonChoiceCoordPoint.icon")); // NOI18N
        buttonChoiceCoordPoint.setToolTipText(resourceMap.getString("buttonChoiceCoordPoint.toolTipText")); // NOI18N
        buttonChoiceCoordPoint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        buttonChoiceCoordPoint.setBorderPainted(false);
        buttonChoiceCoordPoint.setContentAreaFilled(false);
        buttonChoiceCoordPoint.setEnabled(false);
        buttonChoiceCoordPoint.setSelectedIcon(resourceMap.getIcon("buttonChoiceCoordPoint.selectedIcon")); // NOI18N
        buttonChoiceCoordPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonChoiceCoordPointActionPerformed(evt);
            }
        });

        decommentsLigne.setFont(resourceMap.getFont("decommentsLigne.font")); // NOI18N
        decommentsLigne.setIcon(resourceMap.getIcon("decommentsLigne.icon")); // NOI18N
        decommentsLigne.setToolTipText(resourceMap.getString("decommentsLigne.toolTipText")); // NOI18N
        decommentsLigne.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        decommentsLigne.setBorderPainted(false);
        decommentsLigne.setContentAreaFilled(false);
        decommentsLigne.setMaximumSize(new java.awt.Dimension(26, 26));
        decommentsLigne.setMinimumSize(new java.awt.Dimension(26, 26));
        decommentsLigne.setPreferredSize(new java.awt.Dimension(31, 31));
        decommentsLigne.setSelectedIcon(resourceMap.getIcon("decommentsLigne.selectedIcon")); // NOI18N
        decommentsLigne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decommentsLigneActionPerformed(evt);
            }
        });

        commentsLigne.setFont(resourceMap.getFont("commentsLigne.font")); // NOI18N
        commentsLigne.setIcon(resourceMap.getIcon("commentsLigne.icon")); // NOI18N
        commentsLigne.setToolTipText(resourceMap.getString("commentsLigne.toolTipText")); // NOI18N
        commentsLigne.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        commentsLigne.setBorderPainted(false);
        commentsLigne.setContentAreaFilled(false);
        commentsLigne.setMaximumSize(new java.awt.Dimension(26, 26));
        commentsLigne.setMinimumSize(new java.awt.Dimension(26, 26));
        commentsLigne.setPreferredSize(new java.awt.Dimension(31, 31));
        commentsLigne.setSelectedIcon(resourceMap.getIcon("commentsLigne.selectedIcon")); // NOI18N
        commentsLigne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                commentsLigneActionPerformed(evt);
            }
        });

        buttonDistanceCord.setFont(resourceMap.getFont("buttonDistanceCord.font")); // NOI18N
        buttonDistanceCord.setIcon(resourceMap.getIcon("buttonDistanceCord.icon")); // NOI18N
        buttonDistanceCord.setToolTipText(resourceMap.getString("buttonDistanceCord.toolTipText")); // NOI18N
        buttonDistanceCord.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        buttonDistanceCord.setBorderPainted(false);
        buttonDistanceCord.setContentAreaFilled(false);
        buttonDistanceCord.setSelectedIcon(resourceMap.getIcon("buttonDistanceCord.selectedIcon")); // NOI18N
        buttonDistanceCord.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonDistanceCordMouseClicked(evt);
            }
        });

        fullScreenRules.setIcon(resourceMap.getIcon("fullScreenRules.icon")); // NOI18N
        fullScreenRules.setToolTipText(resourceMap.getString("fullScreenRules.toolTipText")); // NOI18N
        fullScreenRules.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        fullScreenRules.setBorderPainted(false);
        fullScreenRules.setContentAreaFilled(false);
        fullScreenRules.setMaximumSize(new java.awt.Dimension(26, 26));
        fullScreenRules.setMinimumSize(new java.awt.Dimension(26, 26));
        fullScreenRules.setName("fullScreenRules"); // NOI18N
        fullScreenRules.setPreferredSize(new java.awt.Dimension(26, 26));
        fullScreenRules.setSelectedIcon(resourceMap.getIcon("fullScreenRules.selectedIcon")); // NOI18N
        fullScreenRules.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fullScreenRulesActionPerformed(evt);
            }
        });

        scrollPane.setFont(resourceMap.getFont("scrollPane.font")); // NOI18N
        scrollPane.setName("scrollPane"); // NOI18N

        posCurseur.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        posCurseur.setText(resourceMap.getString("posCurseur.text")); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonValidate, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(generatorButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(fullScreenRules, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(commentsLigne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(decommentsLigne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelPointGlob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(buttonChoiceCoordPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonDistanceCord, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(buttonCloseCoordPoint))
                    .addComponent(posCurseur, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(commentsLigne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(decommentsLigne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(buttonValidate, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(generatorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fullScreenRules, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(buttonCloseCoordPoint)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonChoiceCoordPoint)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonDistanceCord)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(posCurseur))
                    .addComponent(scrollPane)
                    .addComponent(panelPointGlob, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel3);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setName("jPanel2"); // NOI18N

        jPanel1.setName("jPanel1"); // NOI18N

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setName("jToolBar1"); // NOI18N

        start.setIcon(resourceMap.getIcon("start.icon")); // NOI18N
        start.setToolTipText(resourceMap.getString("start.toolTipText")); // NOI18N
        start.setName("start"); // NOI18N
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startActionPerformed(evt);
            }
        });
        jToolBar1.add(start);

        one.setIcon(resourceMap.getIcon("one.icon")); // NOI18N
        one.setToolTipText(resourceMap.getString("one.toolTipText")); // NOI18N
        one.setName("one"); // NOI18N
        one.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oneActionPerformed(evt);
            }
        });
        jToolBar1.add(one);

        stop.setIcon(resourceMap.getIcon("stop.icon")); // NOI18N
        stop.setToolTipText(resourceMap.getString("stop.toolTipText")); // NOI18N
        stop.setName("stop"); // NOI18N
        stop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopActionPerformed(evt);
            }
        });
        jToolBar1.add(stop);

        reset.setIcon(resourceMap.getIcon("reset.icon")); // NOI18N
        reset.setToolTipText(resourceMap.getString("reset.toolTipText")); // NOI18N
        reset.setName("reset"); // NOI18N
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });
        jToolBar1.add(reset);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);
        jToolBar2.setName("jToolBar2"); // NOI18N

        jProgressBar.setName("jProgressBar"); // NOI18N
        jToolBar2.add(jProgressBar);

        buttonGroup2.add(synchrone);
        synchrone.setIcon(resourceMap.getIcon("synchrone.icon")); // NOI18N
        synchrone.setSelected(true);
        synchrone.setText(resourceMap.getString("synchrone.text")); // NOI18N
        synchrone.setToolTipText(resourceMap.getString("synchrone.toolTipText")); // NOI18N
        synchrone.setName("synchrone"); // NOI18N
        synchrone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                synchroneActionPerformed(evt);
            }
        });
        jToolBar2.add(synchrone);

        buttonGroup2.add(asynchrone);
        asynchrone.setIcon(resourceMap.getIcon("asynchrone.icon")); // NOI18N
        asynchrone.setToolTipText(resourceMap.getString("asynchrone.toolTipText")); // NOI18N
        asynchrone.setName("asynchrone"); // NOI18N
        asynchrone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                asynchroneActionPerformed(evt);
            }
        });
        jToolBar2.add(asynchrone);

        stepSimulationTxt.setEditable(false);
        stepSimulationTxt.setForeground(resourceMap.getColor("stepSimulationTxt.foreground")); // NOI18N
        stepSimulationTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        stepSimulationTxt.setText(resourceMap.getString("stepSimulationTxt.text")); // NOI18N
        stepSimulationTxt.setToolTipText(resourceMap.getString("stepSimulationTxt.toolTipText")); // NOI18N
        stepSimulationTxt.setName("stepSimulationTxt"); // NOI18N
        stepSimulationTxt.setPreferredSize(new java.awt.Dimension(50, 27));
        jToolBar2.add(stepSimulationTxt);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        desktopPane.setName("desktopPane"); // NOI18N
        desktopPane.setPreferredSize(new java.awt.Dimension(200, 200));

        indicBlockExe.setFont(resourceMap.getFont("indicBlockExe.font")); // NOI18N
        indicBlockExe.setForeground(resourceMap.getColor("indicBlockExe.foreground")); // NOI18N
        indicBlockExe.setText(resourceMap.getString("indicBlockExe.text")); // NOI18N
        indicBlockExe.setName("indicBlockExe"); // NOI18N
        desktopPane.add(indicBlockExe);
        indicBlockExe.setBounds(0, 0, 4, 17);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 954, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel2);

        jSplitPane2.setRightComponent(jSplitPane1);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1130, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
        );

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        menuItemNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        menuItemNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/thema/lucsim/gui/resources/ButtonsPetit/New.png"))); // NOI18N
        menuItemNew.setText(resourceMap.getString("menuItemNew.text")); // NOI18N
        menuItemNew.setName("menuItemNew"); // NOI18N
        menuItemNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemNewActionPerformed(evt);
            }
        });
        fileMenu.add(menuItemNew);
        menuItemNew.getAccessibleContext().setAccessibleDescription(resourceMap.getString("menuItemNew.AccessibleContext.accessibleDescription")); // NOI18N

        menuItemOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        menuItemOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/thema/lucsim/gui/resources/ButtonsPetit/Open.png"))); // NOI18N
        menuItemOpen.setText(resourceMap.getString("menuItemOpen.text")); // NOI18N
        menuItemOpen.setActionCommand(resourceMap.getString("menuItemOpen.actionCommand")); // NOI18N
        menuItemOpen.setName("menuItemOpen"); // NOI18N
        menuItemOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemOpenActionPerformed(evt);
            }
        });
        fileMenu.add(menuItemOpen);
        menuItemOpen.getAccessibleContext().setAccessibleDescription(resourceMap.getString("menuItemOpen.AccessibleContext.accessibleDescription")); // NOI18N

        menuItemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        menuItemSave.setIcon(resourceMap.getIcon("menuItemSave.icon")); // NOI18N
        menuItemSave.setText(resourceMap.getString("menuItemSave.text")); // NOI18N
        menuItemSave.setDisabledIcon(resourceMap.getIcon("menuItemSave.disabledIcon")); // NOI18N
        menuItemSave.setEnabled(false);
        menuItemSave.setName("menuItemSave"); // NOI18N
        menuItemSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSaveActionPerformed(evt);
            }
        });
        fileMenu.add(menuItemSave);

        menuItemSaveUnder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemSaveUnder.setIcon(resourceMap.getIcon("menuItemSaveUnder.icon")); // NOI18N
        menuItemSaveUnder.setText(resourceMap.getString("menuItemSaveUnder.text")); // NOI18N
        menuItemSaveUnder.setDisabledIcon(resourceMap.getIcon("menuItemSaveUnder.disabledIcon")); // NOI18N
        menuItemSaveUnder.setEnabled(false);
        menuItemSaveUnder.setName("menuItemSaveUnder"); // NOI18N
        menuItemSaveUnder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSaveUnderActionPerformed(evt);
            }
        });
        fileMenu.add(menuItemSaveUnder);

        menuItemClose.setIcon(resourceMap.getIcon("menuItemClose.icon")); // NOI18N
        menuItemClose.setText(resourceMap.getString("menuItemClose.text")); // NOI18N
        menuItemClose.setDisabledIcon(resourceMap.getIcon("menuItemClose.disabledIcon")); // NOI18N
        menuItemClose.setEnabled(false);
        menuItemClose.setName("menuItemClose"); // NOI18N
        menuItemClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemCloseActionPerformed(evt);
            }
        });
        fileMenu.add(menuItemClose);
        menuItemClose.getAccessibleContext().setAccessibleDescription(resourceMap.getString("menuItemClose.AccessibleContext.accessibleDescription")); // NOI18N

        prefMenuItem.setIcon(resourceMap.getIcon("prefMenuItem.icon")); // NOI18N
        prefMenuItem.setText(resourceMap.getString("prefMenuItem.text")); // NOI18N
        prefMenuItem.setName("prefMenuItem"); // NOI18N
        prefMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prefMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(prefMenuItem);

        byteBut.setText(resourceMap.getString("byteBut.text")); // NOI18N
        byteBut.setName("byteBut"); // NOI18N
        byteBut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                byteButActionPerformed(evt);
            }
        });
        fileMenu.add(byteBut);

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance().getContext().getActionMap(LucsimView.class, this);
        menuItemExit.setAction(actionMap.get("quit")); // NOI18N
        menuItemExit.setIcon(resourceMap.getIcon("menuItemExit.icon")); // NOI18N
        menuItemExit.setText(resourceMap.getString("menuItemExit.text")); // NOI18N
        menuItemExit.setName("menuItemExit"); // NOI18N
        fileMenu.add(menuItemExit);
        menuItemExit.getAccessibleContext().setAccessibleDescription(resourceMap.getString("menuItemExit.AccessibleContext.accessibleDescription")); // NOI18N

        menuBar.add(fileMenu);

        statMenu.setText(resourceMap.getString("statMenu.text")); // NOI18N
        statMenu.setName("statMenu"); // NOI18N

        jMenuItem3.setIcon(resourceMap.getIcon("jMenuItem3.icon")); // NOI18N
        jMenuItem3.setText(resourceMap.getString("jMenuItem3.text")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        statMenu.add(jMenuItem3);

        menuItemDataExtractor.setIcon(resourceMap.getIcon("menuItemDataExtractor.icon")); // NOI18N
        menuItemDataExtractor.setText(resourceMap.getString("menuItemDataExtractor.text")); // NOI18N
        menuItemDataExtractor.setName("menuItemDataExtractor"); // NOI18N
        menuItemDataExtractor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemDataExtractorActionPerformed(evt);
            }
        });
        statMenu.add(menuItemDataExtractor);

        menuBar.add(statMenu);

        analysisMenu.setText(resourceMap.getString("analysisMenu.text")); // NOI18N
        analysisMenu.setName("analysisMenu"); // NOI18N

        markovMenuItem.setText(resourceMap.getString("markovMenuItem.text")); // NOI18N
        markovMenuItem.setName("markovMenuItem"); // NOI18N
        markovMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                markovMenuItemActionPerformed(evt);
            }
        });
        analysisMenu.add(markovMenuItem);

        potentialMenuItem.setText(resourceMap.getString("potentialMenuItem.text")); // NOI18N
        potentialMenuItem.setName("potentialMenuItem"); // NOI18N
        potentialMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                potentialMenuItemActionPerformed(evt);
            }
        });
        analysisMenu.add(potentialMenuItem);

        menuBar.add(analysisMenu);

        rulesMenu.setText(resourceMap.getString("rulesMenu.text")); // NOI18N
        rulesMenu.setName("rulesMenu"); // NOI18N

        creationRuleMenu.setText(resourceMap.getString("creationRuleMenu.text")); // NOI18N
        creationRuleMenu.setName("creationRuleMenu"); // NOI18N

        menuItemDecisionTree.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        menuItemDecisionTree.setBackground(resourceMap.getColor("menuItemDecisionTree.background")); // NOI18N
        menuItemDecisionTree.setText(resourceMap.getString("menuItemDecisionTree.text")); // NOI18N
        menuItemDecisionTree.setName("menuItemDecisionTree"); // NOI18N
        menuItemDecisionTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemDecisionTreeActionPerformed(evt);
            }
        });
        creationRuleMenu.add(menuItemDecisionTree);

        rulesMenu.add(creationRuleMenu);

        ruleConstraintsMenuItem.setText(resourceMap.getString("ruleConstraintsMenuItem.text")); // NOI18N
        ruleConstraintsMenuItem.setName("ruleConstraintsMenuItem"); // NOI18N
        ruleConstraintsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ruleConstraintsMenuItemActionPerformed(evt);
            }
        });
        rulesMenu.add(ruleConstraintsMenuItem);

        ruleRemConstraintsMenuItem.setText(resourceMap.getString("ruleRemConstraintsMenuItem.text")); // NOI18N
        ruleRemConstraintsMenuItem.setEnabled(false);
        ruleRemConstraintsMenuItem.setName("ruleRemConstraintsMenuItem"); // NOI18N
        ruleRemConstraintsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ruleRemConstraintsMenuItemActionPerformed(evt);
            }
        });
        rulesMenu.add(ruleRemConstraintsMenuItem);

        menuBar.add(rulesMenu);

        displayMenu.setText(resourceMap.getString("displayMenu.text")); // NOI18N
        displayMenu.setName("displayMenu"); // NOI18N

        dispGraphCheckBoxMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        dispGraphCheckBoxMenuItem.setText(resourceMap.getString("dispGraphCheckBoxMenuItem.text")); // NOI18N
        dispGraphCheckBoxMenuItem.setName("dispGraphCheckBoxMenuItem"); // NOI18N
        dispGraphCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemShowGraphActionPerformed(evt);
            }
        });
        displayMenu.add(dispGraphCheckBoxMenuItem);

        dispRulesCheckBoxMenuItem.setSelected(true);
        dispRulesCheckBoxMenuItem.setText(resourceMap.getString("dispRulesCheckBoxMenuItem.text")); // NOI18N
        dispRulesCheckBoxMenuItem.setName("dispRulesCheckBoxMenuItem"); // NOI18N
        dispRulesCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regleAfficheActionPerformed(evt);
            }
        });
        displayMenu.add(dispRulesCheckBoxMenuItem);

        dispStatesLayersCheckBoxMenuItem.setSelected(true);
        dispStatesLayersCheckBoxMenuItem.setText(resourceMap.getString("dispStatesLayersCheckBoxMenuItem.text")); // NOI18N
        dispStatesLayersCheckBoxMenuItem.setName("dispStatesLayersCheckBoxMenuItem"); // NOI18N
        dispStatesLayersCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                coucheAfficheActionPerformed(evt);
            }
        });
        displayMenu.add(dispStatesLayersCheckBoxMenuItem);

        dispMapCheckBoxMenuItem.setSelected(true);
        dispMapCheckBoxMenuItem.setText(resourceMap.getString("dispMapCheckBoxMenuItem.text")); // NOI18N
        dispMapCheckBoxMenuItem.setName("dispMapCheckBoxMenuItem"); // NOI18N
        dispMapCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imageCoucheAfficheActionPerformed(evt);
            }
        });
        displayMenu.add(dispMapCheckBoxMenuItem);

        dispSimBarCheckBoxMenuItem.setSelected(true);
        dispSimBarCheckBoxMenuItem.setText(resourceMap.getString("dispSimBarCheckBoxMenuItem.text")); // NOI18N
        dispSimBarCheckBoxMenuItem.setName("dispSimBarCheckBoxMenuItem"); // NOI18N
        dispSimBarCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                barreSimuActionPerformed(evt);
            }
        });
        displayMenu.add(dispSimBarCheckBoxMenuItem);

        menuBar.add(displayMenu);

        popupMenuLayer.setName("popupMenuLayer"); // NOI18N

        popupMenuItemLayerAdd.setText(resourceMap.getString("popupMenuItemLayerAdd.text")); // NOI18N
        popupMenuItemLayerAdd.setEnabled(false);
        popupMenuItemLayerAdd.setName("popupMenuItemLayerAdd"); // NOI18N
        popupMenuItemLayerAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemLayerAddActionPerformed(evt);
            }
        });
        popupMenuLayer.add(popupMenuItemLayerAdd);
        popupMenuItemLayerAdd.getAccessibleContext().setAccessibleDescription(resourceMap.getString("popupMenuItemStaticLayerAdd.AccessibleContext.accessibleDescription")); // NOI18N

        popupMenuItemLayerChangeName.setText(resourceMap.getString("popupMenuItemLayerChangeName.text")); // NOI18N
        popupMenuItemLayerChangeName.setEnabled(false);
        popupMenuItemLayerChangeName.setName("popupMenuItemLayerChangeName"); // NOI18N
        popupMenuItemLayerChangeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemLayerChangeNameActionPerformed(evt);
            }
        });
        popupMenuLayer.add(popupMenuItemLayerChangeName);
        popupMenuItemLayerChangeName.getAccessibleContext().setAccessibleDescription(resourceMap.getString("popupMenuItemStaticLayerChangeName.AccessibleContext.accessibleDescription")); // NOI18N

        popupMenuItemLayerDelete.setText(resourceMap.getString("popupMenuItemLayerDelete.text")); // NOI18N
        popupMenuItemLayerDelete.setEnabled(false);
        popupMenuItemLayerDelete.setName("popupMenuItemLayerDelete"); // NOI18N
        popupMenuItemLayerDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemLayerDeleteActionPerformed(evt);
            }
        });
        popupMenuLayer.add(popupMenuItemLayerDelete);
        popupMenuItemLayerDelete.getAccessibleContext().setAccessibleDescription(resourceMap.getString("popupMenuItemStaticLayerDelete.AccessibleContext.accessibleDescription")); // NOI18N

        popupMenuItemACLayerShowSimulation.setText(resourceMap.getString("popupMenuItemACLayerShowSimulation.text")); // NOI18N
        popupMenuItemACLayerShowSimulation.setEnabled(false);
        popupMenuItemACLayerShowSimulation.setName("popupMenuItemACLayerShowSimulation"); // NOI18N
        popupMenuItemACLayerShowSimulation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemACLayerShowSimulationActionPerformed(evt);
            }
        });
        popupMenuLayer.add(popupMenuItemACLayerShowSimulation);

        popupMenuItemACLayerClone.setText(resourceMap.getString("popupMenuItemACLayerClone.text")); // NOI18N
        popupMenuItemACLayerClone.setToolTipText(resourceMap.getString("popupMenuItemACLayerClone.toolTipText")); // NOI18N
        popupMenuItemACLayerClone.setEnabled(false);
        popupMenuItemACLayerClone.setName("popupMenuItemACLayerClone"); // NOI18N
        popupMenuItemACLayerClone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemACLayerCloneActionPerformed(evt);
            }
        });
        popupMenuLayer.add(popupMenuItemACLayerClone);
        popupMenuItemACLayerClone.getAccessibleContext().setAccessibleName(resourceMap.getString("popupMenuItemACLayerClone.AccessibleContext.accessibleName")); // NOI18N
        popupMenuItemACLayerClone.getAccessibleContext().setAccessibleDescription(resourceMap.getString("popupMenuItemACLayerClone.AccessibleContext.accessibleDescription")); // NOI18N

        popupMenuStates.setName("popupMenuStates"); // NOI18N

        popupMenuItemStatesModificate.setText(resourceMap.getString("popupMenuItemStatesModificate.text")); // NOI18N
        popupMenuItemStatesModificate.setEnabled(false);
        popupMenuItemStatesModificate.setName("popupMenuItemStatesModificate"); // NOI18N
        popupMenuItemStatesModificate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemStatesModificateActionPerformed(evt);
            }
        });
        popupMenuStates.add(popupMenuItemStatesModificate);
        popupMenuItemStatesModificate.getAccessibleContext().setAccessibleDescription(resourceMap.getString("popupMenuItemStatesModificate.AccessibleContext.accessibleDescription")); // NOI18N

        popupMenuItemStatesAdd.setText(resourceMap.getString("popupMenuItemStatesAdd.text")); // NOI18N
        popupMenuItemStatesAdd.setEnabled(false);
        popupMenuItemStatesAdd.setName("popupMenuItemStatesAdd"); // NOI18N
        popupMenuItemStatesAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupMenuItemStatesAddActionPerformed(evt);
            }
        });
        popupMenuStates.add(popupMenuItemStatesAdd);

        setComponent(mainPanel);
        setMenuBar(menuBar);
    }// </editor-fold>//GEN-END:initComponents

    private void addLayer() throws IOException {
        if (project == null) {
            showErrorPopup("You should create a project to\n"
                    + "add layers \n\n"
                    + "Go to \"Project -> New project\".");
            return;
        }

        File f = Util.getFile(".tif|.asc", "Image");
        //on fait rien si bouton annuler
        if (f == null) {
            return;
        }

        RenderedImage img;
        Envelope2D envelope;
        try {
            GridCoverage2D grid = IOImage.loadCoverage(f);
            img = grid.getRenderedImage();
            envelope = grid.getEnvelope2D();
        } catch (DataSourceException ex) {//si le .tfw n'existe pas, on crée une enveloppe
            img = ImageIO.read(f);
            envelope = showEnvelopeBox(img);
            if (envelope == null) {
                return;
            }
        }

        String name = f.getName().substring(0, f.getName().length() - 4);
        Layer layer;
        if (StateLayer.isImageForStateLayer(img)) {
            int res = JOptionPane.showConfirmDialog(LucsimApp.getApplication().getMainFrame(), "Is it a state layer ?", "Add layer", JOptionPane.YES_NO_CANCEL_OPTION);
            if(res == JOptionPane.CANCEL_OPTION) {
                return;
            }
            if(res == JOptionPane.YES_OPTION) {
                layer = new StateLayer(name, img, envelope, project.getStates());
            } else {
                layer = new NumLayer(name, img, envelope);
            }
        } else {
            layer = new NumLayer(name, img, envelope);
        }

        project.addLayer(layer);

        /* On ajoute la couche dans la liste */
        dlmLayerAC.addElement(layer);
        listLayers.updateUI();
        /* On affiche l'image */
        showMapInternalFrame(layer, null);
        /*edition nom de la couche statique*/

        isSaved = false;

        showLayerEdition(layer);

    }

    /**
     * Update rule project and validate them
     *
     * @return true if rules has changed or not
     * @throws ParseException
     */
    private boolean validateRules() throws ParseException {
        //On verifie l'ensembles des bloc'
        String rules = textAreaRules.getText();
        boolean change = !rules.equals(project.getStringRules());

        if (change) {
            project.setStringRules(rules);
            isSaved = false;
        } else {
            project.validateRules();
        }
        return change;
    }

    /**
     * Valide les règles courantes. Si elles sont valides, elles sont compilées
     * si jdk==true
     *
     */
    private void validerRegle() {

        /* On vérifie la syntaxe des règles */
        try {
            validateRules();
            // la compilation est plus lente on ne l'exécute que si il y a eu un changement dans les règles
            if (useJDK) {
                try {
                    simulation.compileRules();
                } catch (Exception ex) {
                    Logger.getLogger(LucsimView.class.getName()).log(Level.WARNING, "Impossible to compile rules", ex);
                    showErrorPopup("Impossible to compile rules. Switch to interpreted mode.\nDetail : " + ex);
                    simulation.interpretRules();
                    byteButActionPerformed(null);
                }
            } else {
                simulation.interpretRules();
            }

        } catch (ParseException pe) {
            int[] tab = pe.erreurPos();
            colorSyntax(true, tab);
            showErrorPopup(pe.getMessage());
            project.getRulesBlocks().clear();
        } catch (UnknownLayerException ule) {
            String l = ule.getLayer();
            String[] err = {l};
            String text = textAreaRules.getText();
            final StyledDocument doc = textAreaRules.getStyledDocument();
            final MutableAttributeSet normal = new SimpleAttributeSet();
            StyleConstants.setForeground(normal, Color.black);
            StyleConstants.setBold(normal, false);
            SwingUtilities.invokeLater(() -> {
                doc.setCharacterAttributes(0, doc.getLength(), normal, true);
            });

            colorWords(err, text, doc, Color.blue, false, false, false, false);
            showErrorPopup(ule.getMessage());
            project.getRulesBlocks().clear();
        } catch (UnknownStateException use) {
            String l = use.getState();
            String[] err = {l};
            String text = textAreaRules.getText();
            final StyledDocument doc = textAreaRules.getStyledDocument();
            final MutableAttributeSet normal = new SimpleAttributeSet();
            StyleConstants.setForeground(normal, Color.black);
            StyleConstants.setBold(normal, false);
            SwingUtilities.invokeLater(() -> {
                doc.setCharacterAttributes(0, doc.getLength(), normal, true);
            });

            colorWords(err, text, doc, new Color(59, 116, 23), true, false, false, false);
            showErrorPopup(use.getMessage());
            project.getRulesBlocks().clear();
        } catch (NumberFormatException nfe) {
            showErrorPopup(nfe.getMessage());
            project.getRulesBlocks().clear();
        }

    }

    /**
     * crée un nouveau projet en effaçant celui existant s'il y a
     *
     * @param evt l'evennement d'appuie sur "nouveau projet"
     */
    private void menuItemNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemNewActionPerformed
        try {
            newProject();
        } catch (IOException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
            showErrorPopup(ex.getMessage());
        }

    }//GEN-LAST:event_menuItemNewActionPerformed

    public Project getProject() {
        return project;
    }
    
    /**
     * crée un nouveau projet en effaçant celui existant s'il y a lance
     * l'enveloppe si besoin creer une grille vide si demandé
     */
    private void newProject() throws IOException {

        File f = Util.getFile(".tif|.asc", "Image");
        if (f == null) {
            return;
        }
        StateLayer layer;
        States states = new States();
        String name = f.getName().substring(0, f.getName().length() - 4);
        try {
            GridCoverage2D grid = IOImage.loadCoverage(f);
            /* On ajoute l'image en tant que couche dans la simulation */
            layer = new StateLayer(name, grid.getRenderedImage(), grid.getEnvelope2D(), states);
        } catch (DataSourceException ex) {//si le .tfw n'existe pas, on crée une enveloppe
            BufferedImage img = ImageIO.read(f);
            Envelope2D envelope = showEnvelopeBox(img);
            if (envelope == null) {
                return;
            }
            layer = new StateLayer(name, img, envelope, states);
        }

        closeProject();
        project = new Project(layer);

        /* On ajoute la couche dans la liste des couches AC */
        dlmLayerAC.addElement(layer);
        /* On affiche l'image */
        showMapInternalFrame(layer, null);
        /* On affiche les états dans la liste */
        dlmStates.clear();
        for (State state : project.getStates().getStates()) {
            dlmStates.addElement(state);
        }

        // On crée la simulation (& on instancie les regles de la couche Ac)
        simulation = project.getSimulation();
        //Une fenetre graphe est crée, mais pas encore affichée
        newGraph();
        listLayers.setCellRenderer(new ListRendererLayer(project));
        /* On déverrouille les menus bloqués */
        unlockEntries(true);
        /*coloration syntasique*/
        colorSyntax(false, null);
        isSaved = false;

        save();

    }

    /**
     * affiche dans le desktopPanel la couche AC dans son état initial
     *
     * @param evt l'evennement d'appuie sur sur un élément de la liste des
     * couches statiques
     */
    /**
     * met a jour l'image de la simulation
     */
    public void updateImageSimulation() {

        /*on met  a  jour la zone ou est indiqué l'étape de simation*/
        stepSimulationTxt.setText(simulation.getStep() + "");

        /* on affiche l'image */
        SimLayer layer1 = simulation.getSimLayer();

        LayerInternalFrame mif = findMapInternalFrame(layer1);
        if (mif == null) {
            showMapInternalFrame(layer1, layer1.getName() + " - Simulation");
        } else {
            mif.repaintMap();
            if (!mif.isSelected()) {
                try {
                    mif.setSelected(true);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
                }
            }
        }

        /*on sauvegarde les données des etat pour cette étape de simulation pour construire les graphe*/
        frameGraph.addStepSim(layer1.listeNbCell());
    }

    /**
     * affiche une nouvelle fenêtre contenant la couche AC dans le desktopPanel
     * et calcul une ou plusieurs étapes de la simulation.
     *
     * @param one exécute une seule étape ?
     */
    private void simulation(final boolean one) {

        if (simulation == null) {
            showErrorPopup("Select a simulation layer");
            return;
        }
        /*si en cours de simulation on ne fait rien */
        if (simulation.isCompute()) {
            return;
        }

        /*on valide les regles si il y a eu du changement*/
        validerRegle();

        /*pour regarder si il existe des regles sinon on lance pas simulation*/
        int nbRules = 0;
        for (RulesBlock rules : project.getRulesBlocks()) {
            nbRules += rules.getNbRules();
        }

        if (nbRules == 0) {
            return;
        }

        /* On crée un nouveau thread pour la simulation */
        new Thread(() -> {
            /* On empêche tout changement de mode de fonctionnement de l'automate */
            enabledBarreSimulation(false);
            
            ProgressMonitor mon = new ProgressMonitor();
            simulation.run(LucsimView.this, mon, one);
            mon.close();
            
            /* On empêche tout changement de mode de fonctionnement de l'automate sauf reinitialisation */
            enabledBarreSimulation(!simulation.isFinish());
            stop.setEnabled(!simulation.isFinish());
            reset.setEnabled(true);
        }).start();

    }

    public void setIndicText(String text) {
        indicBlockExe.setText(text);
    }

    /**
     * Fonction permettant de bloquer les boutons de la barre de simulation
     *
     * @param b boolean, true pour debloquer, false pour bloquer la berre
     */
    public void enabledBarreSimulation(boolean b) {
        synchrone.setEnabled(b);
        asynchrone.setEnabled(b);
        start.setEnabled(b);
        one.setEnabled(b);
        reset.setEnabled(b);
    }

    /**
     * affiche la fenêtre de modification d'un état
     *
     * @param evt l'evennement lorsqu'on double-clique sur un état dans la liste
     * des états
     */
	private void listStatesMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_listStatesMouseClicked
	{//GEN-HEADEREND:event_listStatesMouseClicked
            if ((evt.getButton() == MouseEvent.BUTTON1) && (evt.getClickCount() >= 2)) {
                modifStateActionPerformed(null);
            }

	}//GEN-LAST:event_listStatesMouseClicked

    /**
     * Arrête la simulation
     */
    public void stopSimulation() {
        simulation.stopSimulation();
        enabledBarreSimulation(true);
        stop.setEnabled(false);
    }

    /**
     * Réinitialise la simulation en remttant la couche initial dans la
     * simulation et met a jour la fenetre interne
     */
    public void resetSimulation() {

        /* puis on réinitialise la couche */
        simulation.reset();

        //les boutons sont mis a jour
        jProgressBar.setValue(0);
        enabledBarreSimulation(true);
        stop.setEnabled(true);
        //on reaffiche la couche reinitialisé
        SimLayer layer = simulation.getSimLayer();
        updateImageSimulation();
        frameGraph.resetSim(layer);

        textAreaRules.removeAll();
        colorSyntax(false, null);
        indicBlockExe.setText("");

    }

    /**
     * lance la fonction de simulation (simulation en continue)
     *
     * @param evt l'evennement d'appuie sur "lancer la simulation" ou "arrêter
     * la simulaiton"
     */
    /**
     * action change le mode de fonctionnement de l'automate de synchrone à
     * asynchrone et inversement
     *
     * @param evt l'evennement d'appuie sur "passer en mode (a)synchrone" dans
     * menu
     */
    /**
     * fonction change le mode de fonctionnement de l'automate de synchrone à
     * asynchrone et inversement
     *
     * @param evt l'evennement d'appuie sur "passer en mode (a)synchrone"
     */
    private void passageAsynchroneSynchrone() {
        simulation.setSynchronous(!simulation.isSynchronous());
    }

    /**
     * ferme le projet en cours
     *
     * @param evt l'evennement d'appuie sur "Fermer le projet en cours"
     */
private void menuItemCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemCloseActionPerformed
    closeProject();
}//GEN-LAST:event_menuItemCloseActionPerformed

    /**
     * Permet l'ajout d'une couche Statique
     *
     * @param evt l'evennement d'appuie sur "ajouter une couche Statique"
     */
private void popupMenuItemLayerAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemLayerAddActionPerformed
    try {
            addLayer();
        } catch (IOException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
            showErrorPopup(ex.getMessage());
        }
}//GEN-LAST:event_popupMenuItemLayerAddActionPerformed

    public void deleteLayer() {

        /* On récupère la couche */
        if (listLayers.isSelectionEmpty() || project.getLayers().size() == 1) {
            return;
        }

        int index = listLayers.getSelectedIndex();

        /* On supprime du projet */
        Layer layer = (Layer) dlmLayerAC.getElementAt(index);

        project.removeLayer(layer);

        /* On supprime de la liste */
        dlmLayerAC.remove(index);
        listLayers.updateUI();
        isSaved = false;
        /* On supprime l'éventuelle fenêtre la représentant dans le desktopPane */
        MapInternalFrame mif = findMapInternalFrame(layer);
        if (mif != null) {
            mif.dispose();
        }

    }

    /**
     * permet la suppression d'une couche statique
     *
     * @param evt l'evennement d'appuie sur "supprimer une couche"
     */
private void popupMenuItemLayerDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemLayerDeleteActionPerformed
    deleteLayer();
}//GEN-LAST:event_popupMenuItemLayerDeleteActionPerformed

    /**
     * permet de changer le nom d'une couche statique
     *
     * @param evt l'evennement d'appuie sur "changer le nom de la couche
     * statique"
     */
private void popupMenuItemLayerChangeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemLayerChangeNameActionPerformed
    changeName();
}//GEN-LAST:event_popupMenuItemLayerChangeNameActionPerformed

    /**
     * permet de changer le nom de la couche ac
     */

    public void changeName(){
        /* On récupère la couche */
        int index = listLayers.getSelectedIndex();
        if (index >= 0) {
            Layer layer = (Layer) dlmLayerAC.getElementAt(index);
            showLayerEdition(layer);
            listLayers.updateUI();
        }
    }

    /**
     * permet la modification d'un état
     *
     * @param evt l'evennement d'appuie sur "modifier l'état"
     */
private void popupMenuItemStatesModificateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemStatesModificateActionPerformed
    modifStateActionPerformed(evt);
}//GEN-LAST:event_popupMenuItemStatesModificateActionPerformed

    /**
     * Permet de cloner la couche AC en une nouvelle couche statique
     *
     * @param evt l'évenement de l'action de la souris
     */
private void popupMenuItemACLayerCloneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemACLayerCloneActionPerformed
// TODO 
//    NumLayer layer = simulation.getProject().getLayerAC().CloneInLayerStatic(simulation.getProject().getLayerAC().getName() +"_clone");
//    project.addLayer(layer);
//
//    /* On ajoute la couche dans la liste */
//    dlmLayerAC.addElement(layer);
//
//    /* On affiche l'image */
//    showMapInternalFrame(layer, null, null);

    isSaved = false;
}//GEN-LAST:event_popupMenuItemACLayerCloneActionPerformed

    /**
     * Sauvegarde le projet en cours dans un fichier ".ca", et les images dans
     * un sous-dossier "maps"
     *
     * @param evt
     */
private void menuItemSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSaveActionPerformed
    try {
        save();
    } catch (IOException ex) {
        Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
        showErrorPopup(ex.getMessage());
    }
}//GEN-LAST:event_menuItemSaveActionPerformed

    /**
     * Bouton "ouvrir" -> Ouvre un projet CaDev ".ca"
     *
     * @param evt
     */
private void menuItemOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemOpenActionPerformed
    openProject();
}//GEN-LAST:event_menuItemOpenActionPerformed

    /**
     * Ouvre un projet CaDev ".ca"
     */
    private void openProject() {

        File file = Util.getFile(".ca", "Projet CaDev");
        if (file == null) {
            return;
        }

        closeProject();

        fileProject = file;

        try {
            project = Project.load(file);
            // On affiche les couches dans la liste des couches 
            for (Layer l : project.getLayers()) {
                dlmLayerAC.addElement(l);
            }

            // On affiche les états dans la liste 
            dlmStates.clear();
            for (State state : project.getStates().getStates()) {
                dlmStates.addElement(state);
            }
            textAreaRules.setText(project.getStringRules());
            // On crée la simulation (& on instancie les regles de la couche Ac)
            simulation = project.getSimulation();
            //Une fenetre graphe est crée, mais pas encore affichée
            newGraph();
            listLayers.setCellRenderer(new ListRendererLayer(project));
            // On déverrouille les menus bloqués 
            unlockEntries(true);
            /*coloration syntasique*/
            colorSyntax(false, null);
            isSaved = true;
            getFrame().setTitle("Lucsim " + JavaLoader.getVersion(LucsimApp.class) + " - " + fileProject.getName());
        } catch (IOException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
            showErrorPopup(ex.getMessage());
        }

    }

    /**
     * Bouton qui ajoute un état dans la couche AC et l'affiche dans la liste
     * des états
     *
     * @param evt
     */
private void popupMenuItemStatesAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemStatesAddActionPerformed
    addState();
}//GEN-LAST:event_popupMenuItemStatesAddActionPerformed

    /**
     * Supprime un état dans la couche AC et maj la liste des états
     */
    private void removeState(State s, int index) {
        //verification que les couches n'ont pas cet etat 
        for (StateLayer l : project.getStateLayers()) {
            if (l.nbCell(s.getValue()) > 0) {
                showErrorPopup("Cells of the layer " + l.getName() + " are in this state \n"
                        + "It's impossible to delete this state for the moment");
                return;
            }
        }
        project.getStates().removeState(s);
        dlmStates.remove(index);
        listStates.updateUI();
    }

    /**
     * Ajoute un état dans la couche AC et l'affiche dans la liste des états
     */
    private void addState() {

        State state = project.getStates().addState();
        if (state == null) {
            showErrorPopup("Layer contains too many states\n"
                    + "It's impossible too add state");
            return;
        }

        dlmStates.addElement(state);
        showStateEdition(state);
        listStates.updateUI();
        raffraichirGraph();
    }

    /**
     * Affiche la couche AC dans l'état actuel de la simulation
     *
     * @param evt
     */
private void popupMenuItemACLayerShowSimulationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupMenuItemACLayerShowSimulationActionPerformed
    /* On récupère la couche */
    final int index = listLayers.getSelectedIndex();
    if (index == 0) {
        final StateLayer layer = (StateLayer) dlmLayerAC.getElementAt(index);
        /* On affiche la couche dans un mapviewer */
        showMapInternalFrame(layer, layer.getName() + " - Simulation");
    }
}//GEN-LAST:event_popupMenuItemACLayerShowSimulationActionPerformed

    /**
     * Permet d'afficher la liste des états que prend une cellule à une position
     * particulière
     *
     * @param evt
     */
private void listLayersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listLayersMouseClicked

    if ((evt.getButton() == MouseEvent.BUTTON1) && (evt.getClickCount() >= 2)) {
        /* On récupère la couche */
        final int index = listLayers.locationToIndex(evt.getPoint());
        Layer layer = (Layer) dlmLayerAC.getElementAt(index);
        showMapInternalFrame(layer, null);
    }
}//GEN-LAST:event_listLayersMouseClicked

    /**
     * Raffaichie le graphique (lors de la simulation par exemple)
     */
    private void raffraichirGraph() {
        frameGraph.updatePanel();
        frameGraph.validate();
    }

    /**
     * Creation d'une nouvelle fenetre pour les graphiques de simulation
     */
    private void newGraph() {
        frameGraph = new Graph(project);
        frameGraph.setLocationRelativeTo(this.getFrame());
        frameGraph.setVisible(false);
    }

    /**
     * Ouvre la fenetre des graphiques (fenetre deja crée, on la met visible)
     *
     * @param evt
     */
private void menuItemShowGraphActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemShowGraphActionPerformed
    frameGraph.setVisible(dispGraphCheckBoxMenuItem.isSelected());
}//GEN-LAST:event_menuItemShowGraphActionPerformed

    /**
     * Bouton start simulation de la barre de simulation
     *
     * @param evt
     */
private void startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startActionPerformed
    simulation(false);
}//GEN-LAST:event_startActionPerformed

    /**
     * Bouton lancer une étape de simulation de la barre de simulation
     *
     * @param evt
     */
private void oneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oneActionPerformed
    simulation(true);
}//GEN-LAST:event_oneActionPerformed

    /**
     * Bouton stop simulation de la barre de simulation
     *
     * @param evt
     */
private void stopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopActionPerformed
    stopSimulation();
}//GEN-LAST:event_stopActionPerformed

    /**
     * Bouton de la barre de simulation pour reinisialiser la simulation en
     * cours
     *
     * @param evt
     */
private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
    resetSimulation();
}//GEN-LAST:event_resetActionPerformed

    /**
     * Bouton pour passer l'automate en mode synchrone
     *
     * @param evt
     */
private void synchroneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_synchroneActionPerformed
    if (!simulation.isSynchronous()) {
        passageAsynchroneSynchrone();
    }
}//GEN-LAST:event_synchroneActionPerformed

    /**
     * Bouton pour passer l'automate en mode asynchrone
     *
     * @param evt
     */
private void asynchroneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_asynchroneActionPerformed
    if (simulation.isSynchronous()) {
        passageAsynchroneSynchrone();
    }
}//GEN-LAST:event_asynchroneActionPerformed

    /**
     * Bouton pour cacher/ouvrir la zone des "etats de toute les couches pour un
     * point"
     *
     * @param evt
     */
private void buttonCloseCoordPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCloseCoordPointActionPerformed
    panelPointGlob.setVisible(!panelPointGlob.isVisible());
}//GEN-LAST:event_buttonCloseCoordPointActionPerformed

    /**
     * Bouton pour rentrer un point manuelement pour avoir les etat des couche a
     * ce point
     *
     * @param evt
     */
private void buttonChoiceCoordPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonChoiceCoordPointActionPerformed

    Point2D position = showPointCreation();
    panelPointGlob.setVisible(true);
    showStateDisplay(position);

}//GEN-LAST:event_buttonChoiceCoordPointActionPerformed

    /**
     * Bouton affichage ou non des regles
     *
     * @param evt
     */
private void regleAfficheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regleAfficheActionPerformed
    jSplitPane1.getBottomComponent().setVisible(dispRulesCheckBoxMenuItem.isSelected());
    jSplitPane1.resetToPreferredSizes();
}//GEN-LAST:event_regleAfficheActionPerformed

    /**
     * Bouton affichage ou non de la gestion des couche (panel gauche)
     *
     * @param evt
     */
private void coucheAfficheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_coucheAfficheActionPerformed
    jSplitPane2.getLeftComponent().setVisible(dispStatesLayersCheckBoxMenuItem.isSelected());
    jSplitPane2.resetToPreferredSizes();
}//GEN-LAST:event_coucheAfficheActionPerformed

    /**
     * Bouton d'affichage ou non de l'image (deskop panel)
     *
     * @param evt
     */
private void imageCoucheAfficheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imageCoucheAfficheActionPerformed
    jSplitPane1.getTopComponent().setVisible(dispMapCheckBoxMenuItem.isSelected());
    jSplitPane1.resetToPreferredSizes();
}//GEN-LAST:event_imageCoucheAfficheActionPerformed

    /*
     * Simulation bouton Enbled quand stop
     */
 /*
     * Bouton affiche de la barre de simulation (menu affichage)
     */
private void barreSimuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_barreSimuActionPerformed
    jPanel1.setVisible(dispSimBarCheckBoxMenuItem.isSelected());
}//GEN-LAST:event_barreSimuActionPerformed

    /*
     * Bouton qui permet l'ajout du nom de l'état dans la zone de saisie de texte
     */
private void addStateTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addStateTextMouseClicked

    if (!listStates.isSelectionEmpty()) {
        /* On récupère l'état */
        final int index = listStates.getAnchorSelectionIndex();
        final State state = (State) dlmStates.getElementAt(index);
        int caret = textAreaRules.getCaretPosition();
        String t = textAreaRules.getText();
        String s1 = t.substring(0, caret);
        String s2 = t.substring(caret, t.length());
        textAreaRules.setText(s1 + " " + state.getName() + " " + s2);
        colorSyntax(false, null);
    }
}//GEN-LAST:event_addStateTextMouseClicked

    /**
     * fonction sur la distance entre les deux dernier point clickés
     *
     * @param evt
     */
private void buttonDistanceCordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonDistanceCordMouseClicked

    if (buttonDistanceCord.isSelected()) {
        if (arrCoord.size() > 0) {
            buttonDistanceCord.setSelected(false);
            Point p = new Point();
            double a = arrCoord.get(arrCoord.size() - 1)[0];
            double b = arrCoord.get(arrCoord.size() - 1)[1];
            p.setLocation(a, b);
            showStateDisplay(p);
        }
    } else {
        buttonDistanceCord.setSelected(true);
        distanceCord = new DistanceCord(arrCoord);
        panelPointGlob.setLayout(new java.awt.BorderLayout());
        panelPointGlob.add(distanceCord, BorderLayout.CENTER);
        panelPointGlob.validate();
        panelPointGlob.repaint();
        panelPointGlob.setVisible(true);
    }

}//GEN-LAST:event_buttonDistanceCordMouseClicked

private void addStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addStateActionPerformed
    addState();
    isSaved = false;
}//GEN-LAST:event_addStateActionPerformed

private void removeStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeStateActionPerformed

    if (simulation.getStep() > 0) {
        showErrorPopup("We are in current simulation, reset \n to delete state");
        return;
    }

    if (!listStates.isSelectionEmpty()) {
        /* On récupère l'état */
        final int index = listStates.getAnchorSelectionIndex();
        final State state = (State) dlmStates.getElementAt(index);
        removeState(state, index);
        /*on met a  jour le graphique si il est instancier*/
        raffraichirGraph();

    } else {
        showErrorPopup("You must select state");
    }
    isSaved = false;

}//GEN-LAST:event_removeStateActionPerformed

private void prefMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prefMenuItemActionPerformed
    PreferencesDialog prefDialog = new PreferencesDialog(this.getFrame(), true);
    prefDialog.setProcPanelVisible(true);
    prefDialog.setVisible(true);
}//GEN-LAST:event_prefMenuItemActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        ImagesComparison imagesComparison = new ImagesComparison(getFrame(), project);
        imagesComparison.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void byteButActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_byteButActionPerformed
        if (useJDK) {
            useJDK = false;
            byteBut.setIcon(null);
        } else {
            useJDK = true;
            byteBut.setIcon(new ImageIcon("src/main/resources/org/thema/lucsim/gui/resources/affiche.png"));
            validerRegle();
        }
    }//GEN-LAST:event_byteButActionPerformed

    /**
     * Bouton pour mettre les règles en plein écran
     *
     * @param evt
     */
    private void fullScreenRulesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fullScreenRulesActionPerformed
        dispMapCheckBoxMenuItem.doClick();
    }//GEN-LAST:event_fullScreenRulesActionPerformed

    /*
     * bouton qui decommente en ligne les //
     */
    private void decommentsLigneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decommentsLigneActionPerformed
        String t = textAreaRules.getText();

        if (!t.isEmpty()) {
            String textComment = "";
            int debut = textAreaRules.getSelectionStart() - 1;
            int fin = textAreaRules.getSelectionEnd();
            while (t.charAt(debut) != '\n' && debut > 0) {
                debut--;
            }

            if (debut > 0) {
                debut--;
            }

            for (int i = 0; i < t.length() - 2; i++) {
                if (t.charAt(i) == '/' && t.charAt(i + 1) == '/' && t.charAt(i + 2) == '\t' && i >= debut && i <= fin) {
                    i += 2;
                } else {
                    textComment += t.charAt(i);
                }
            }
            textComment += t.charAt(t.length() - 2);
            textComment += t.charAt(t.length() - 1);

            textAreaRules.setText(textComment);

            //coloriage syntax
            colorSyntax(false, null);
        }
    }//GEN-LAST:event_decommentsLigneActionPerformed

    /*
     * Bouton qui comente en ligne comme ceci //
     */
    private void commentsLigneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_commentsLigneActionPerformed

        String t = textAreaRules.getText();

        if (!t.isEmpty()) {

            int a, b;
            Stack<Integer> arrSaut = new Stack<>();
            Stack<Integer> arrSaut2 = new Stack<>();
            a = textAreaRules.getSelectionStart();
            b = textAreaRules.getSelectionEnd();
            while (t.charAt(a) != '\n' && a > 0) {
                a--;
            }

            for (int i = a; i < b; i++) {
                if (t.charAt(i) == '\n') {
                    arrSaut.push(i);
                }
            }

            while (!arrSaut.isEmpty()) {
                arrSaut2.push(arrSaut.pop());
            }

            String com = "";
            for (int i = 0; i < t.length(); i++) {
                if (!arrSaut2.isEmpty()) {
                    if (arrSaut2.peek() == i - 1) {
                        arrSaut2.pop();
                        com = com + "//\t";
                    }
                }

                com = com + t.charAt(i);
            }

            textAreaRules.setText(com);
            colorSyntax(false, null);
        }
    }//GEN-LAST:event_commentsLigneActionPerformed

    /*
     * Bouton du generateur de regle
     */
    private void generatorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generatorButtonActionPerformed
        RulesGeneratorAssistant generator = new RulesGeneratorAssistant(project, LucsimApp.getApplication().getMainFrame());
        generator.setLocationRelativeTo(null);
        generator.setVisible(true);

        String txt = generator.getTxtFinal();
        if (!"".equals(txt)) {
            textAreaRules.setText(textAreaRules.getText() + "\n" + txt);
            colorSyntax(false, null);
            validerRegle();
        }
    }//GEN-LAST:event_generatorButtonActionPerformed

    /**
     * Bouton valider
     */
    private void buttonValidateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonValidateActionPerformed
        validerRegle();
    }//GEN-LAST:event_buttonValidateActionPerformed

    private void menuItemSaveUnderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSaveUnderActionPerformed
        fileProject = null;
        try {
            save();
        } catch (IOException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
            showErrorPopup(ex.getMessage());
        }
    }//GEN-LAST:event_menuItemSaveUnderActionPerformed

    private void addLayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addLayerActionPerformed
        try {
            addLayer();
        } catch (IOException ex) {
            Logger.getLogger(LucsimView.class.getName()).log(Level.SEVERE, "", ex);
            showErrorPopup(ex.getMessage());
        }
    }//GEN-LAST:event_addLayerActionPerformed

    private void removelayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removelayerActionPerformed
        deleteLayer();
    }//GEN-LAST:event_removelayerActionPerformed

    private void modifNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifNameActionPerformed
        changeName();
    }//GEN-LAST:event_modifNameActionPerformed

    private void modifStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifStateActionPerformed
        if (listStates.isSelectionEmpty()) {
            return;
        }

        /* On récupère l'état */
        final int index = listStates.getAnchorSelectionIndex();
        final State state = (State) dlmStates.getElementAt(index);

        /* On ouvre la fenêtre d'édition d'état */
        showStateEdition(state);

        for (JInternalFrame frm : desktopPane.getAllFrames()) {
            if (frm instanceof MapInternalFrame) {
                ((MapInternalFrame) frm).getMapViewer().getMap().fullRepaint();
            }
        }
        listStates.updateUI();

        /*on met a  jour le graphique si il est instancie*/
        raffraichirGraph();
        isSaved = false;

    }//GEN-LAST:event_modifStateActionPerformed

private void setSimLayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setSimLayerActionPerformed
    if (simulation.isCompute()) {
        return;
    }
    Layer layer = (Layer) listLayers.getSelectedValue();
    if (layer instanceof StateLayer) {
        simulation = project.setSimuLayer(layer.getName());
        frameGraph.resetSim(simulation.getSimLayer());
        listLayers.repaint();
        isSaved = false;
    }
}//GEN-LAST:event_setSimLayerActionPerformed

    private void markovMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_markovMenuItemActionPerformed
        MarkovChainDialog dlg = new MarkovChainDialog(this.getFrame(), project.getStateLayers(), project.getMarkovChain());
        dlg.setVisible(true);
        project.setMarkovChain(dlg.markov);
        isSaved = false;
    }//GEN-LAST:event_markovMenuItemActionPerformed

    private void ruleConstraintsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ruleConstraintsMenuItemActionPerformed
        try {
            validateRules();
        } catch (Exception ex) {
            showErrorPopup("Rules are not valid !\n" + ex);
            return;
        }

        RuleConstraintDialog dlg = new RuleConstraintDialog(getFrame(), project);
        dlg.setVisible(true);
        if (!dlg.isOk()) {
            return;
        }
        if (!dlg.useMarkov() && !dlg.usePotential()) {
            return;
        }

        if (dlg.useMarkov()) {
            RealVector vector = project.getMarkovChain().getResultVector();
            for (RulesBlock rules : project.getRulesBlocks()) {
                for (Rule rule : rules.getRules()) {
                    State state = rule.getFinalState();
                    rule.getCondition().addAndCondition(Arrays.asList(
                            new Token(AnalyzerConstants.NBCELL, "nbcell"),
                            new Token(AnalyzerConstants.NAME, state.getName()),
                            new Token(AnalyzerConstants.LESS, "<"),
                            new Token(AnalyzerConstants.INTEGER, "" + (int) vector.getEntry(project.getStates().getStateIndex(state)))));
                }
            }
        }
        if (dlg.usePotential()) {
            for (RulesBlock rules : project.getRulesBlocks()) {
                for (Rule rule : rules.getRules()) {
                    rule.getCondition().addAndCondition(Arrays.asList(
                            new Token(AnalyzerConstants.NAME, dlg.getPotLayer().getName()),
                            new Token(AnalyzerConstants.GEQ, ">="),
                            new Token(AnalyzerConstants.FLOAT, "" + dlg.getMinPotValue())));
                }
            }
        }

        String newRules = project.generateTextRules();
        ruleBeforeConstraints = textAreaRules.getText();
        textAreaRules.setText(newRules);
        textAreaRules.setEditable(false);
        ruleConstraintsMenuItem.setEnabled(false);
        ruleRemConstraintsMenuItem.setEnabled(true);
        validerRegle();
    }//GEN-LAST:event_ruleConstraintsMenuItemActionPerformed

    private void ruleRemConstraintsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ruleRemConstraintsMenuItemActionPerformed
        textAreaRules.setText(ruleBeforeConstraints);
        textAreaRules.setEditable(true);
        ruleConstraintsMenuItem.setEnabled(true);
        ruleRemConstraintsMenuItem.setEnabled(false);
        validerRegle();
    }//GEN-LAST:event_ruleRemConstraintsMenuItemActionPerformed

    private void potentialMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_potentialMenuItemActionPerformed
        final PotentialDialog dlg = new PotentialDialog(this.getFrame(), project);
        dlg.setVisible(true);
        if (!dlg.isOk()) {
            return;
        }
        new Thread(() -> {
            TaskMonitor monitor = new TaskMonitor(LucsimView.this.getFrame(), "Potential...");
            final PotentialLayer potLayer = PotentialLayer.create(dlg.getResultName(), dlg.getBaseLayer(),
                    dlg.getDistPower(), dlg.getMass(), dlg.getMaxDist(), monitor);
            monitor.close();
            project.addLayer(potLayer);
            EventQueue.invokeLater(() -> {
                dlmLayerAC.addElement(potLayer);
                listLayers.updateUI();
                showMapInternalFrame(potLayer, null);
            });
        }).start();

    }//GEN-LAST:event_potentialMenuItemActionPerformed

    private void menuItemDecisionTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemDecisionTreeActionPerformed
        if (project == null || project.getStateLayers().size() < 2) {
            JOptionPane.showMessageDialog(getFrame(), "The project must have at least two state layers.");
            return;
        }
        
        DecisionTreeDialog decisionTreeDialog = new DecisionTreeDialog(this.getFrame(), this);
        decisionTreeDialog.setVisible(true);
        
    }//GEN-LAST:event_menuItemDecisionTreeActionPerformed

    private void menuItemDataExtractorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemDataExtractorActionPerformed
       new DataExportDialog(this.getFrame(), project).setVisible(true);
    }//GEN-LAST:event_menuItemDataExtractorActionPerformed

    private void showGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showGraphButtonActionPerformed
        Layer l = (Layer)listLayers.getSelectedValue();
        if(l != null) {
            new Graph(project, l).setVisible(true);
        }
    }//GEN-LAST:event_showGraphButtonActionPerformed

    public void setTextRule(String rules) {
        try {
            project.setStringRules(rules);
        } catch (ParseException ex) {
            Logger.getLogger(PostProcessDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        textAreaRules.setText(rules);
        colorSyntax(false, null);
    }
    
    /**
     * Fonction qui colorie tout les mot d'une phrase suivant un tableau de
     * string
     *
     * @param strsToHighlight
     * @param text
     * @param doc
     * @param color
     */
    private void colorWords(String[] strsToHighlight, String text, final StyledDocument doc, Color color, boolean bold, boolean under, boolean italic, boolean strike) {

        for (String strToHL : strsToHighlight) {
            Pattern p = Pattern.compile(strToHL);
            Matcher m = p.matcher(text);

            while (m.find()) {
                MutableAttributeSet attri = new SimpleAttributeSet();
                StyleConstants.setForeground(attri, color);
                StyleConstants.setBold(attri, bold);
                StyleConstants.setUnderline(attri, under);
                StyleConstants.setItalic(attri, italic);
                StyleConstants.setStrikeThrough(attri, strike);

                final int startC = m.start();
                final int endC = m.end();
                final int length;

                //commentaire par ligne //
                if ("//".equals(strsToHighlight[0])) {
                    int cpt = 0;
                    int i = m.start();
                    while (text.charAt(i) != '\n' && i + 1 < text.length()) {
                        i++;
                        cpt++;
                    }
                    length = cpt;
                } else {//commentaire entre balise /* ET */
                    if ("/\\*".equals(strsToHighlight[0])) {
                        int cpt = 0;
                        int i = m.start() + 1;
                        while ((text.charAt(i - 1) != '*' || text.charAt(i) != '/') && i + 2 < text.length()) {
                            i++;
                            cpt++;
                        }
                        if (i + 1 >= text.length()) {
                            length = 0;
                        } else {
                            length = cpt + 3;
                        }
                    } else {
                        length = endC - startC;
                    }
                }

                final MutableAttributeSet style = attri;

                SwingUtilities.invokeLater(() -> {
                    doc.setCharacterAttributes(startC, length, style, true);
                });
            }
        }
    }

    /**
     * Fonction qui lance le coloriage des
     * mots a partir de ses tableau de mot
     *
     * @param erreurSouligne 
     * @param tab
     */
    public void colorSyntax(boolean erreurSouligne, int[] tab) {

        String[] strsToHighlightBoucle = {"repeat", "while", "if"};

        String[] strsToHighlightFunction = {"nbCellNeu", "nbCellMoo", "nbCellSq", "nbCellCir", "pCellSq",
            "pCellCir", "nbCellAng", "pCellAng", "cellDuration", "North", "South", "East", "West", "NorthEast", "NorthWest",
            "SouthEast", "SouthWest", "nbCell", "pCell", "step", "nbSameStep", "nbcellneu", "nbcellmoo", "nbcellsq",
            "nbcellcir", "pcellsq", "pcellcir", "nbcellang", "pcellang", "cellduration", "north", "south", "east", "west",
            "northeast", "northwest", "southeast", "southwest", "nbcell", "pcell", "nbsamestep"};

        String[] strsToHighlightComp = {"=", ">", "<", ">=", "<=", "<>", "mod", "MOD", "random", "int", "round"}; // + * / % -

        String[] strsToHighlightBool = {"not", "NOT", "or", "OR", "xor", "XOR", "and", "AND", "true", "false", "TRUE", "FALSE"};

        String[] strsToHighlightSep = {"->", ":", ";"};

        String[] strsToHighlightLayer = project != null ? project.getLayerNames().toArray(new String[0]) : null;

        String[] strsToHighlightState = project != null ? ConventionNommage.getStateNames(project).toArray(new String[0]) : null;

        String[] strsToHighlightComLigne = {"//"};

        String[] strsToHighlightComCrochet = {"/\\*"};

        String text = textAreaRules.getText();//.replaceAll("\n", " ");

        final StyledDocument doc = textAreaRules.getStyledDocument();

        final MutableAttributeSet normal = new SimpleAttributeSet();

        StyleConstants.setForeground(normal, Color.black);

        StyleConstants.setBold(normal, false);

        SwingUtilities.invokeLater(() -> {
            doc.setCharacterAttributes(0, doc.getLength(), normal, true);
        });

        colorWords(strsToHighlightBoucle, text, doc, new Color(147, 136, 48), true, false, true, false);
        colorWords(strsToHighlightComp, text, doc, Color.red, false, false, false, false);
        colorWords(strsToHighlightBool, text, doc, new Color(237, 121, 0), false, false, false, false);
        colorWords(strsToHighlightComp, text, doc, Color.red, false, false, false, false);
        colorWords(strsToHighlightSep, text, doc, new Color(185, 83, 214), true, false, false, false);
        colorWords(strsToHighlightFunction, text, doc, Color.black, true, false, true, false);

        if (project != null) { //pour la coloration sans ouverture de projet
            colorWords(strsToHighlightLayer, text, doc, new Color(102, 51, 0), false, false, true, false);
            colorWords(strsToHighlightState, text, doc, new Color(59, 116, 23), true, false, false, false);
        }

        colorWords(strsToHighlightComLigne, text, doc, Color.blue, false, false, false, false);
        colorWords(strsToHighlightComCrochet, text, doc, Color.blue, false, false, false, false);

        textAreaRules.setAutoscrolls(true);

        //POUR SOULIGNER L'ERREUR SI DETECTION PAR PARSER
        if (erreurSouligne) {

            int compteurLigne = 1;
            int L1 = 0;

            int L2 = 0;

            for (int i = 0; i < text.length(); i++) {
                if (text.charAt(i) == '\n') {
                    compteurLigne++;

                    if (compteurLigne == tab[0]) {
                        L1 = i + 1;
                    } else if (compteurLigne == tab[0] + 1) {
                        L2 = i + 1;

                    }
                }
            }

            final int Ligne1 = L1;
            final int Ligne2 = L2;

            MutableAttributeSet attri = new SimpleAttributeSet();

            StyleConstants.setBackground(attri, new Color(255, 218, 185));
            StyleConstants.setBold(attri, true);

            final MutableAttributeSet style = attri;

            SwingUtilities.invokeLater(() -> {
                doc.setCharacterAttributes(Ligne1, Ligne2 - Ligne1, style, true);
            });
        }
        //////////////////////////////
    }

    /**
     * Color le block en cours d'éxecution (surligne)
     *
     * @param block
     */
    public void colorBlockExecution(int block) {

        if (project.getRulesBlocks().get(block).getType() != BlockType.NOBLOCK) {
            String text = textAreaRules.getText();
            colorSyntax(false, null);
            int nbBlockCpt = 0;
            for (int i = 0; i < block; i++) {
                if (project.getRulesBlocks().get(i).getType() != BlockType.NOBLOCK) {
                    nbBlockCpt++;
                }
            }

            //POUR SOULIGNER L'ERREUR SI DETECTION PAR PARSER
            int a1 = 0;
            int a2 = 1;
            int compteur = -1;
            int i = 0;

            while (i < text.length() && compteur != nbBlockCpt) {
                if (text.charAt(i) == '{') {
                    compteur++;
                }
                if (text.charAt(i) == '\n') {
                    a1++;
                }
                i++;
            }

            int j = i;
            while (j < text.length() && text.charAt(j) != '}') {
                if (text.charAt(j) == '\n') {
                    a2++;
                }
                j++;
            }

            textAreaRules.removeAll();
            de = new DessinExe(0, 15 * a1, textAreaRules.getWidth(), 15 * a2, new Color(173, 216, 230));
            de.setBounds(new Rectangle(0, 0, textAreaRules.getWidth(), textAreaRules.getHeight()));
            de.setLayout(null);
            de.setOpaque(false);
            de.setBackground(new Color(210, 250, 250));
            textAreaRules.add(de);
        } else {
            textAreaRules.removeAll();
        }

        textAreaRules.repaint();
    }

    /**
     * Fonction qui permet avec un clic sur une image d'une internal frame de
     * lancer l'affichage d'un panel contenant la liste des etats de toutes les
     * couches au point ou le clic a eu lieu
     */
    private void addMouseListener(MapInternalFrame mif) {

        //listerner
        mif.getMapViewer().getMap().addShapeMouseListener((Point2D pd, List<SelectableShape> list, MouseEvent me, int i) -> {
            panelPointGlob.setVisible(true);
            if (pd != null) {
                if (arrCoord.isEmpty()) {
                    double pt[] = {pd.getX(), pd.getY()};
                    arrCoord.add(pt);
                } else if (arrCoord.get(arrCoord.size() - 1)[0] != pd.getX() && arrCoord.get(arrCoord.size() - 1)[1] != pd.getY()) {
                    double pt[] = {pd.getX(), pd.getY()};
                    arrCoord.add(pt);
                }
                
                showStateDisplay(pd);
                
            }
        });

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addLayer;
    private javax.swing.JButton addState;
    private javax.swing.JButton addStateText;
    private javax.swing.JMenu analysisMenu;
    private javax.swing.JToggleButton asynchrone;
    private javax.swing.JButton buttonChoiceCoordPoint;
    private javax.swing.JButton buttonCloseCoordPoint;
    private javax.swing.JButton buttonDistanceCord;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton buttonValidate;
    private javax.swing.JMenuItem byteBut;
    private javax.swing.JButton commentsLigne;
    private javax.swing.JMenu creationRuleMenu;
    private javax.swing.JButton decommentsLigne;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JCheckBoxMenuItem dispGraphCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem dispMapCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem dispRulesCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem dispSimBarCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem dispStatesLayersCheckBoxMenuItem;
    private javax.swing.JMenu displayMenu;
    private javax.swing.JButton fullScreenRules;
    private javax.swing.JButton generatorButton;
    private javax.swing.JLabel indicBlockExe;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JProgressBar jProgressBar;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JToolBar jToolBar4;
    private javax.swing.JList listLayers;
    private javax.swing.JList listStates;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuItem markovMenuItem;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuItemClose;
    private javax.swing.JMenuItem menuItemDataExtractor;
    private javax.swing.JMenuItem menuItemDecisionTree;
    private javax.swing.JMenuItem menuItemNew;
    private javax.swing.JMenuItem menuItemOpen;
    private javax.swing.JMenuItem menuItemSave;
    private javax.swing.JMenuItem menuItemSaveUnder;
    private javax.swing.JButton modifName;
    private javax.swing.JButton modifState;
    private javax.swing.JButton one;
    private javax.swing.JPanel panelPointGlob;
    private javax.swing.JMenuItem popupMenuItemACLayerClone;
    private javax.swing.JMenuItem popupMenuItemACLayerShowSimulation;
    private javax.swing.JMenuItem popupMenuItemLayerAdd;
    private javax.swing.JMenuItem popupMenuItemLayerChangeName;
    private javax.swing.JMenuItem popupMenuItemLayerDelete;
    private javax.swing.JMenuItem popupMenuItemStatesAdd;
    private javax.swing.JMenuItem popupMenuItemStatesModificate;
    private javax.swing.JPopupMenu popupMenuLayer;
    private javax.swing.JPopupMenu popupMenuStates;
    private javax.swing.JLabel posCurseur;
    private javax.swing.JMenuItem potentialMenuItem;
    private javax.swing.JMenuItem prefMenuItem;
    private javax.swing.JButton removeState;
    private javax.swing.JButton removelayer;
    private javax.swing.JButton reset;
    private javax.swing.JMenuItem ruleConstraintsMenuItem;
    private javax.swing.JMenuItem ruleRemConstraintsMenuItem;
    private javax.swing.JMenu rulesMenu;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JButton setSimLayer;
    private javax.swing.JButton showGraphButton;
    private javax.swing.JButton start;
    private javax.swing.JMenu statMenu;
    private javax.swing.JTextField stepSimulationTxt;
    private javax.swing.JButton stop;
    private javax.swing.JToggleButton synchrone;
    // End of variables declaration//GEN-END:variables

    /**
     * Les listModel pour les listes de couches / états
     */
    private DefaultListModel dlmLayerAC;
    /**
     * Les listModel pour les listes de couches / états
     */
    private DefaultListModel dlmStates;
    /**
     * popup pour la création d'enveloppe lors d'ouverture d'image sans
     * extension spatiale
     */
    private EnvCreation envelopeBox;
    /**
     * la simulation courante
     */
    private Simulation simulation;
    /**
     * The current project, can be null if no project is loaded
     */
    private Project project;

    /**
     * vrai si le projet en cours est sauvegardé, faux sinon, ou s'il y a eu des
     * modifications depuis la dernière sauvegarde
     */
    private boolean isSaved;
    /**
     * Fenetre des graphiques de simulation
     */
    private Graph frameGraph;
    /**
     * Jpanel contenant la liste les etat de toutes les couche pour un point.
     */
    private PanelPoint stateDisplay;
    /**
     * jPanel contenant calculant et affichant la distance entre les deux
     * dernier point clické sur la grille
     */
    private DistanceCord distanceCord;
    /**
     * Arrayliste pour sauvegarder les coordonnée des points clickés
     */
    private ArrayList<double[]> arrCoord;
    /**
     * Zone de dessin pour le coloriage des bloc lors de l'execution
     */
    private DessinExe de;
    /**
     * panel d'edition de regle
     */
    private JTextPane textAreaRules;

    private String ruleBeforeConstraints;
    /**
     * panel num de ligne
     */
    private final TextLineNumber tln;

    /**
     * pour sauvegarde auto
     */
    private File fileProject = null;

    public class ProgressMonitor implements ProgressBar {

        private double progress;
        private boolean cancel = false;

        private void updateText() {
            double range = jProgressBar.getMaximum() - jProgressBar.getMinimum();
            if (range <= 0) {
                range = 1;
            }
            double p = 100 * (progress - jProgressBar.getMinimum()) / range;
            jProgressBar.setString(String.format("%.1f%%", p));
        }

        @Override
        public void setMaximum(final int max) {
            invoke(() -> {
                jProgressBar.setMaximum(max);
                updateText();
            });

        }

        @Override
        public void setMinimum(final int min) {
            invoke(() -> {
                jProgressBar.setMinimum(min);
                updateText();
            });
        }

        @Override
        public void setProgress(double val) {
            double range = jProgressBar.getMaximum() - jProgressBar.getMinimum();
            if (range <= 0) {
                range = 1;
            }
            int p = (int) (1000 * (jProgressBar.getValue() - jProgressBar.getMinimum()) / range);
            final int np = (int) (1000 * (val - jProgressBar.getMinimum()) / range);
            progress = val;
            if (p != np) {
                invoke(() -> {
                    jProgressBar.setValue((int) progress);
                    jProgressBar.setString(String.format("%.1f%%", np / 10.0));
                });
            }
        }

        @Override
        public void incProgress(double nb) {
            synchronized (this) {
                progress += nb;
            }
            setProgress(progress);
        }

        @Override
        public void setNote(final String note) {
            invoke(() -> {
                jProgressBar.setString(note);
            });
        }

        @Override
        public ProgressBar getSubProgress(double size) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setIndeterminate(final boolean bool) {
            invoke(() -> {
                jProgressBar.setIndeterminate(bool);
            });
        }

        @Override
        public boolean inProgress() {
            return progress != jProgressBar.getMinimum() && progress != jProgressBar.getMaximum();
        }

        @Override
        public void reset() {
            invoke(() -> {
                setProgress(jProgressBar.getMinimum());
                jProgressBar.setString("");
                jProgressBar.setIndeterminate(false);
                cancel = false;
            });
        }

        @Override
        public void close() {
            invoke(() -> {
                setProgress(jProgressBar.getMaximum());
                jProgressBar.setString("Ready");
                jProgressBar.setIndeterminate(false);
            });
        }

        @Override
        public double getProgress() {
            return progress;
        }

        @Override
        public String getNote() {
            return jProgressBar.getString();
        }

        @Override
        public boolean isCanceled() {
            return cancel;
        }

        private void invoke(Runnable run) {
            if (SwingUtilities.isEventDispatchThread()) {
                run.run();
            } else {
                SwingUtilities.invokeLater(run);
            }

        }
    }
}