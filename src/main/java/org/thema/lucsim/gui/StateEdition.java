/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.lucsim.gui;

import java.awt.event.MouseEvent;
import org.thema.lucsim.engine.ConventionNommage;
import org.thema.lucsim.engine.State;
import java.awt.Color;
import javax.swing.JColorChooser;
import javax.swing.JOptionPane;
import org.thema.lucsim.engine.Project;

/**
 * Cette classe définit la fenêtre de modification d'un état
 *
 * @author rgrillot
 * @author alexis
 */
public class StateEdition extends javax.swing.JDialog {

    /**
     * Creates new form StateEdition
     *
     * @param parent parent frame
     * @param state l'état à modifier
     * @param project current project
     */
    public StateEdition(java.awt.Frame parent, State state, Project project) {
        super(parent, true);
        this.project = project;
        this.state = state;
        initComponents();
        setLocationRelativeTo(parent);
        setTitle("State " + state.getName());
        textValue.setText(String.valueOf(state.getValue()));
        textName.setText(state.getName());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        labelValue = new javax.swing.JLabel();
        textValue = new javax.swing.JTextField();
        labelName = new javax.swing.JLabel();
        textName = new javax.swing.JTextField();
        labelColor = new javax.swing.JLabel();
        labelColorDisplay = new javax.swing.JLabel();
        buttonCancel = new javax.swing.JButton();
        buttonValidate = new javax.swing.JButton();

        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 359, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 204, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(StateEdition.class);
        setTitle(resourceMap.getString("Form.title")); // NOI18N
        setModal(true);
        setName("Form"); // NOI18N
        setResizable(false);

        labelValue.setLabelFor(textValue);
        labelValue.setText(resourceMap.getString("labelValue.text")); // NOI18N
        labelValue.setName("labelValue"); // NOI18N

        textValue.setEditable(false);
        textValue.setText(resourceMap.getString("textValue.text")); // NOI18N
        textValue.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        textValue.setDragEnabled(true);
        textValue.setName("textValue"); // NOI18N

        labelName.setLabelFor(textName);
        labelName.setText(resourceMap.getString("labelName.text")); // NOI18N
        labelName.setName("labelName"); // NOI18N

        textName.setText(resourceMap.getString("textName.text")); // NOI18N
        textName.setDragEnabled(true);
        textName.setName("textName"); // NOI18N

        labelColor.setLabelFor(labelColorDisplay);
        labelColor.setText(resourceMap.getString("labelColor.text")); // NOI18N
        labelColor.setName("labelColor"); // NOI18N

        labelColorDisplay.setBackground(state.getColor());
        labelColorDisplay.setForeground(state.getColor());
        labelColorDisplay.setText(resourceMap.getString("labelColorDisplay.text")); // NOI18N
        labelColorDisplay.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        labelColorDisplay.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        labelColorDisplay.setName("labelColorDisplay"); // NOI18N
        labelColorDisplay.setOpaque(true);
        labelColorDisplay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelColorDisplayMouseClicked(evt);
            }
        });

        buttonCancel.setText(resourceMap.getString("buttonCancel.text")); // NOI18N
        buttonCancel.setName("buttonCancel"); // NOI18N
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        buttonValidate.setText(resourceMap.getString("buttonValidate.text")); // NOI18N
        buttonValidate.setName("buttonValidate"); // NOI18N
        buttonValidate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonValidateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelName)
                    .addComponent(labelColor)
                    .addComponent(labelValue))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textValue, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                    .addComponent(textName)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(buttonValidate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonCancel))
                    .addComponent(labelColorDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelValue))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelName))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelColorDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(buttonCancel)
                            .addComponent(buttonValidate)))
                    .addComponent(labelColor))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonCancelActionPerformed
	{//GEN-HEADEREND:event_buttonCancelActionPerformed
            dispose();
	}//GEN-LAST:event_buttonCancelActionPerformed

	private void buttonValidateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonValidateActionPerformed
	{//GEN-HEADEREND:event_buttonValidateActionPerformed
 
            if (!state.getName().equals(textName.getText())) {
                try {
                    ConventionNommage.checkName(textName.getText(), project);
                } catch (ConventionNommage.BadNameException ex) {
                    JOptionPane.showMessageDialog(this, "Bad state name : " + ex.getMessage());
                    return;
                }
            }
            
            state.setName(textName.getText());
            state.setColor(labelColorDisplay.getBackground());
            project.getStates().updateStyle();
            dispose();
            
	}//GEN-LAST:event_buttonValidateActionPerformed

	private void labelColorDisplayMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_labelColorDisplayMouseClicked
	{//GEN-HEADEREND:event_labelColorDisplayMouseClicked
            if (evt.getButton() == MouseEvent.BUTTON1) {
                Color color = JColorChooser.showDialog(this,
                        "Color state " + state.getName(),
                        state.getColor());
                if (color != null) {
                    labelColorDisplay.setBackground(color);
                }
                
            }
	}//GEN-LAST:event_labelColorDisplayMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonValidate;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelColor;
    private javax.swing.JLabel labelColorDisplay;
    private javax.swing.JLabel labelName;
    private javax.swing.JLabel labelValue;
    private javax.swing.JTextField textName;
    private javax.swing.JTextField textValue;
    // End of variables declaration//GEN-END:variables
    /**
     * l'état à modifier
     */
    private State state;
    /**
     * simulation de la couche ac
     */
    private Project project;
    
}
