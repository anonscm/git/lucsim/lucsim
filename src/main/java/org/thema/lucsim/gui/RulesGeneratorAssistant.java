/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.lucsim.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.thema.lucsim.engine.ConventionNommage;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.State;

/**
 *
 * @author alexis
 */
public class RulesGeneratorAssistant extends javax.swing.JDialog implements ActionListener {

    /*simulation*/
    private Project project;
    /*Les textes pour constituer un block*/
    private String txtFinal;
    private String texte;
    private String block;
    private String regle;
    private String conditionBlock;
    private String conditionBlockEnCours;
    private String regleEnCours;
    private String etatIni;
    private String etatFinal;
    private String condition;
    private String conditionEnCours;

    /**
     *
     * @param project
     * @param contentPane
     */
    public RulesGeneratorAssistant(Project project, JFrame contentPane) {

        super(JOptionPane.getFrameForComponent(contentPane), true);

        initComponents();

        this.project = project;

        txtFinal = "";

        texte = "{\n}";

        block = "";

        regle = "";

        conditionBlock = "";

        conditionBlockEnCours = "";

        etatIni = "";

        etatFinal = "";

        condition = "";

        regleEnCours = "";

        conditionEnCours = "";

        initialiserButton();

        simpleBlock.addActionListener(this);

        whileBlock.addActionListener(this);

        ifBlock.addActionListener(this);

        repeatBlock.addActionListener(this);

        condBlock1.addActionListener(this);

        fonctionStateBlock1.addActionListener(this);

        operatorBlock.addActionListener(this);

        condBlock2.addActionListener(this);

        fonctionStateBlock2.addActionListener(this);

        notButtonBlock.addActionListener(this);

        stateInitial.addActionListener(this);

        stateFinal.addActionListener(this);

        validateRuleButton.addActionListener(this);

        condition1.addActionListener(this);

        operator.addActionListener(this);

        condition2.addActionListener(this);

        buttonNewCond.addActionListener(this);

        notButton.addActionListener(this);

        ok.addActionListener(this);

        annuler.addActionListener(this);

        aideButton.addActionListener(this);

        informations.addActionListener(this);

        boolFonction(false, false, false, false);

        stateInitial.setSelectedIndex(0);

        if (!project.getStates().getStates().isEmpty()) {
            stateFinal.setSelectedIndex(0);
        }
        condition1.setSelectedIndex(0);
    }

    /**
     * mise a jour entier (bloc + regle)
     */
    private void majTexte() {

        texte = block + "{\n\n" + regle + regleEnCours + "\n}";

        texteArea.setText(texte);

        colorSyntax(texteArea);
    }

    /**
     * initialisation des boutons
     */
    private void initialiserButton() {
        for (State state : project.getStates().getStates()) {
            stateInitial.addItem(state.getName());
            stateFinal.addItem(state.getName());
            fonctionState1.addItem(state.getName());
            fonctionState2.addItem(state.getName());
            fonctionStateBlock1.addItem(state.getName());
            fonctionStateBlock2.addItem(state.getName());
        }
    }

    /**
     * boolean sur les liste des etat des condition de regle et des entier
     * représentant le perimetre des fonction
     *
     * @param bool1
     * @param bool2
     * @param bool3
     * @param bool4
     */
    private void boolFonction(boolean bool1, boolean bool2, boolean bool3, boolean bool4) {

        fonctionState1.setVisible(bool1);
        fonctionInteger1.setVisible(bool2);
        fonctionState2.setVisible(bool3);
        fonctionInteger2.setVisible(bool4);

    }

    /**
     * boolean bouton sur la liste des etat des condition des blocs
     *
     * @param bool1
     * @param bool3
     */
    private void boolFonctionBlock(boolean bool1, boolean bool3) {
        fonctionStateBlock1.setVisible(bool1);
        fonctionStateBlock2.setVisible(bool3);
    }

    /**
     * actualise la regle en cours
     */
    private void actualiserRegleEnCours() {
        regleEnCours = "\t" + etatIni + " -> " + etatFinal + " : " + condition + conditionEnCours + " ;\n";
        majTexte();
    }

    /**
     * actualiser la condition en cours de la règle
     */
    private void actualiserConditionEnCours() {

        boolean b1, b2, b3, b4;

        String operand1 = condition1.getSelectedItem().toString();

        String operand2 = condition2.getSelectedItem().toString();

        String operateur = operator.getSelectedItem().toString();

        if ("Entier (saisir)".equals(operand1)) {//entier

            operand1 = nombreCond1.getText();

            nombreCond1.setVisible(true);

            b1 = false;
            b2 = false;
        } else {

            nombreCond1.setVisible(false);

            if ("step()".equals(operand1) || "cellDuration()".equals(operand1) || "N()".equals(operand1) || "S()".equals(operand1) || "E()".equals(operand1) || "W()".equals(operand1) || "NE()".equals(operand1) || "NW()".equals(operand1) || "SE()".equals(operand1) || "SW()".equals(operand1)) {

                b1 = false;
                b2 = false;
            } else {  //fonction
                if ("nbCell".equals(operand1) || "pCell".equals(operand1) || "nbCellMoo".equals(operand1) || "nbCellNeu".equals(operand1)) {

                    b1 = true;
                    b2 = false;

                    operand1 = operand1 + "(" + fonctionState1.getSelectedItem().toString() + ")";
                } else {

                    b1 = true;
                    b2 = true;

                    operand1 = operand1 + "(" + fonctionState1.getSelectedItem().toString() + "," + fonctionInteger1.getText() + ")";
                }
            }

        }
        //entier
        if ("Entier (saisir)".equals(operand2)) {

            operand2 = nombreCond2.getText();

            nombreCond2.setVisible(true);

            b3 = false;

            b4 = false;
        } else {

            nombreCond2.setVisible(false);

            if ("step()".equals(operand2) || "cellDuration()".equals(operand2)) {

                b3 = false;

                b4 = false;
            } else {//fonction

                if ("nbCell".equals(operand2) || "pCell".equals(operand2) || "nbCellMoo".equals(operand2) || "nbCellNeu".equals(operand2)) {

                    b3 = true;
                    b4 = false;

                    operand2 = operand2 + "(" + fonctionState2.getSelectedItem().toString() + ")";
                } else {

                    b3 = true;
                    b4 = true;

                    operand2 = operand2 + "(" + fonctionState2.getSelectedItem().toString() + "," + fonctionInteger2.getText() + ")";
                }
            }
        }

        boolFonction(b1, b2, b3, b4);

        conditionEnCours = operand1 + operateur + operand2;

        if (notButton.isSelected() == true) {

            conditionEnCours = "not(" + conditionEnCours + ")";
        }

        actualiserRegleEnCours();

        majTexte();
    }

    /**
     * actualiser les condition des blocs
     */
    public void actualiserConditionBlock() {

        boolean b1, b2, b3, b4;

        String operand1 = condBlock1.getSelectedItem().toString();

        String operand2 = condBlock2.getSelectedItem().toString();

        String operateur = operatorBlock.getSelectedItem().toString();

        if ("Entier (saisir)".equals(operand1)) {

            operand1 = nombreCondBlock1.getText();

            nombreCondBlock1.setVisible(true);

            b1 = false;
        } else {

            nombreCondBlock1.setVisible(false);

            if ("step()".equals(operand1) || "cellDuration()".equals(operand1)) {
                b1 = false;
            } else {

                b1 = true;

                operand1 = operand1 + "(" + fonctionStateBlock1.getSelectedItem().toString() + ")";
            }

        }
        //entier
        if ("Entier (saisir)".equals(operand2)) {

            operand2 = nombreCondBlock2.getText();

            nombreCondBlock2.setVisible(true);

            b3 = false;
        } else {

            nombreCondBlock2.setVisible(false);

            if ("step()".equals(operand2) || "cellDuration()".equals(operand2)) {

                b3 = false;
            } else { //fonctions 

                b3 = true;

                operand2 = operand2 + "(" + fonctionStateBlock2.getSelectedItem().toString() + ")";
            }
        }

        boolFonctionBlock(b1, b3);

        conditionBlockEnCours = operand1 + operateur + operand2;

        if (notButtonBlock.isSelected() == true) {

            conditionBlockEnCours = "not(" + conditionBlockEnCours + ")";

        }

        majTexte();

    }

    /**
     * actualiser les block
     */
    public void actuBlock() {

        if (ifBlock.isSelected() == true) {
            ifBlock.doClick();

        } else if (whileBlock.isSelected() == true) {

            whileBlock.doClick();

        }
    }

    /**
     * event des boutons
     *
     * @param e
     */
    public void actionPerformed(ActionEvent e) {

        /*BLOCK*/
        if (e.getSource() == simpleBlock) {

            block = "";

            majTexte();
        } else if (e.getSource() == ifBlock) {

            actualiserConditionBlock();

            block = "if(" + conditionBlock + conditionBlockEnCours + ")";

            majTexte();
        } else if (e.getSource() == whileBlock) {

            actualiserConditionBlock();

            block = "while(" + conditionBlock + conditionBlockEnCours + ")";

            majTexte();
        } else if (e.getSource() == repeatBlock) {

            block = "repeat(" + repeatTexte.getText() + ")";

            majTexte();
        } else if (e.getSource() == notButtonBlock || e.getSource() == condBlock1 || e.getSource() == fonctionStateBlock1 || e.getSource() == operatorBlock || e.getSource() == condBlock2 || e.getSource() == fonctionStateBlock2) {

            actuBlock();
        } /*ETAT DE TRANSITION*/ else if (e.getSource() == stateInitial) {

            etatIni = stateInitial.getSelectedItem().toString();

            actualiserRegleEnCours();
        } else if (e.getSource() == stateFinal) {

            etatFinal = stateFinal.getSelectedItem().toString();

            actualiserRegleEnCours();
        } /*BOUTON*/ else if (e.getSource() == validateRuleButton) {

            regle = regle + regleEnCours;

            regleEnCours = "";

            condition = "";

            conditionEnCours = "";

            majTexte();
            stateInitial.setSelectedIndex(0);

            if (!project.getStates().getStates().isEmpty()) {
                stateFinal.setSelectedIndex(0);
            }
            condition1.setSelectedIndex(0);
        } /*CONDITION*/ else if (e.getSource() == condition1 || e.getSource() == condition2 || e.getSource() == operator) {

            actualiserConditionEnCours();
        } else if (e.getSource() == buttonNewCond) {

            String op = newCond.getSelectedItem().toString();

            condition = condition + conditionEnCours + " " + op + " ";

            conditionEnCours = "";

            actualiserConditionEnCours();
        } else if (e.getSource() == notButton) {

            if (notButton.isSelected() == true) {

                notButton.setSelected(true);

            } else {

                notButton.setSelected(false);
            }

            actualiserConditionEnCours();
        } /*Bouton OK/ANNULER*/ else if (e.getSource() == ok) {

            validerBlock();

            this.dispose();
        } else if (e.getSource() == annuler) {

            this.dispose();
        } /*aide*/ else if (e.getSource() == aideButton) {

            JOptionPane.showMessageDialog(this, "Fonctions: \n\n\n"
                    + "nbCellMoo : Retourne le nombre de cellules d'un certain état (en parametre) sur les 8 cases autour de la cellule en execution\n\n"
                    + "nbCellNeu : Retourne le nombre de cellules d'un certain état (en parametre) sur les 4 cases autour de la cellule en execution (Sud, Nord, Est, Ouest)\n\n"
                    + "nbCellCir : Retourne le nombre de cellules d'un certain état (en parametre) sur un voisinage en cercle d'un rayon (en parametre et en nombre de cellule) de la cellule en execution\n\n"
                    + "nbCellSq : Retourne le nombre de cellules d'un certain état (en parametre) sur un voisinage en carré d'un rayon (en parametre et en nombre de cellule) de la cellule en execution\n\n"
                    + "pCellCir : Retourne le pourcentage de cellules (entre 0 et 1) d'un certain état (en parametre) sur un voisinage en cercle d'un rayon (en parametre et en nombre de cellule) de la cellule en execution\n\n"
                    + "pCellSq : Retourne le pourcentage de cellules d'un certain état (en parametre) sur un voisinage en carré d'un rayon (en parametre et en nombre de cellule) de la cellule en execution\n\n"
                    + "cellDuration() : retourne le temps depuis lequel la cellule n'a pas changé d'état dans la couche AC\n\n"
                    + "nbCell : Retourne le nombre de cellules d'un état en paramètre\n\n"
                    + "pCell : Retourne le pourcentage de cellules d'un état en paramètre (entre 0 et 1)\n\n"
                    + "north() : Retourne la valeur de la cellule au Nord\n\n"
                    + "sout() : Retourne la valeur de la cellule au Sud\n\n"
                    + "east() : Retourne la valeur de la cellule a l'Est\n\n"
                    + "west() : Retourne la valeur de la cellule a l'Ouest\n\n"
                    + "northEast() : Retourne la valeur de la cellule au Nord-Est\n\n"
                    + "northWest() : Retourne la valeur de la cellule au Nord-Ouest\n\n"
                    + "southEast() : Retourne la valeur de la cellule au Sud-Est\n\n"
                    + "southWest() : Retourne la valeur de la cellule au Sud-Ouest\n\n"
                    + "step() : Retourne le numéro de l'étape de simulation en cours\n\n"
                    + "nbSameStape() : Donne le nombre d'étapes sans aucun changement dans toute la couche\n\n");

        } /*information*/ else if (e.getSource() == informations) {

            JOptionPane.showMessageDialog(this, "Les règles doivent être dans un bloc comme ceci: \n"
                    + "{\n    ETAT INITIAL -> ETAT FINAL : CONDITION; \n} \n\n"
                    + "Toute les cellules vont exécuter toutes les règle du bloc en cours, et une cellule va passer \n "
                    + "dans l'état final si elle est de l'état initiale ET la condition sur celle-ci est vrai \n\n"
                    + "Commencez par choisir un bloc et une condition de bloc pour les bloc if et while \n"
                    + "Puis établissez les etats de transitions pour la première règle\n"
                    + "Creez la condition pour celle-ci (il peut y en avoir plusieurs)\n"
                    + "Puis valider seulement pour écrire une nouvelle règle dans ce même bloc\n\n"
                    + "Rien ne doit être ecrit dans ce cadre, ormis des modifications juste avant la validation \n"
                    + "(bouton OK)");

        }
    }

    /**
     * retourne tout le texte creé par le générateur dans la variable finale
     */
    public void validerBlock() {
        txtFinal = texteArea.getText();
    }

    /**
     * retourne le texte final
     *
     * @return
     */
    public String getTxtFinal() {
        return this.txtFinal;
    }

    /**
     * **********COLORIAGE SYNTAXIQUE************
     */
    /**
     * Fonction qui colorie tout les mot d'une phrase suivant un tableau de
     * string
     *
     * @param strsToHighlight
     * @param text
     * @param doc
     * @param color
     */
    private void colorWords(List<String> strsToHighlight, String text, final StyledDocument doc, Color color, boolean bold, boolean under, boolean italic, boolean strike) {

        for (String strToHL : strsToHighlight) {
            Pattern p = Pattern.compile(strToHL);
            Matcher m = p.matcher(text);

            while (m.find() == true) {
                MutableAttributeSet attri = new SimpleAttributeSet();
                StyleConstants.setForeground(attri, color);
                StyleConstants.setBold(attri, bold);
                StyleConstants.setUnderline(attri, under);
                StyleConstants.setItalic(attri, italic);
                StyleConstants.setStrikeThrough(attri, strike);

                final int start = m.start();
                final int end = m.end();
                final int length;
                if ("//".equals(strsToHighlight.get(0))) {
                    int cpt = 0;
                    int i = m.start();
                    while (text.charAt(i) != '\n' && i + 1 < text.length()) {
                        i++;
                        cpt++;
                    }
                    length = cpt;

                } else {
                    if ("/~".equals(strsToHighlight.get(0))) {
                        int cpt = 0;
                        int i = m.start() + 1;
                        while ((text.charAt(i - 1) != '~' || text.charAt(i) != '/') && i + 2 < text.length()) {
                            i++;
                            cpt++;
                        }
                        if (i + 1 >= text.length()) {
                            length = 0;
                        } else {
                            length = cpt + 3;
                        }
                    } else {
                        length = end - start;
                    }

                }

                final MutableAttributeSet style = attri;

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        doc.setCharacterAttributes(start, length, style, true);
                    }
                });
            }
        }
    }

    /**
     * Fonction qui recupere le texte d'un TextePane et lance le coloriage des
     * mot a partir de ses tableau de mot
     *
     * @param c
     */
    public void colorSyntax(JTextPane c) {

        List<String> strsToHighlightBoucle = Arrays.asList("repeat", "while", "if");
        List<String> strsToHighlightFunction = Arrays.asList("nbCellNeu", "nbCellMoo", "nbCellSq", "nbCellCir", "pCellSq", "pCellCir", "cellDuration", "N", "S", "E", "W", "NE", "NW", "SE", "SW", "nbCell", "pCell", "step", "nbSameSte");
        List<String> strsToHighlightComp = Arrays.asList("=", ">", "<", ">=", "<=", "<>", "mod", "MOD", "random", "int", "round"); // + * / % -
        List<String> strsToHighlightBool = Arrays.asList("not", "NOT", "or", "OR", "xor", "XOR", "and", "AND", "true", "false", "TRUE", "FALSE");
        List<String> strsToHighlightSep = Arrays.asList("->", ":", ";");
        List<String> strsToHighlightLayer = project.getLayerNames();
        List<String> strsToHighlightState = ConventionNommage.getStateNames(project);
        List<String> strsToHighlightComLigne = Arrays.asList("//");
        List<String> strsToHighlightComCrochet = Arrays.asList("/~");

        String text = c.getText();//.replaceAll("\n", " ");
        final StyledDocument doc = c.getStyledDocument();
        final MutableAttributeSet normal = new SimpleAttributeSet();
        StyleConstants.setForeground(normal, Color.black);
        StyleConstants.setBold(normal, false);

        SwingUtilities.invokeLater(() -> {
            //couleur, bold etc
            doc.setCharacterAttributes(0, doc.getLength(), normal, true);
        });
        colorWords(strsToHighlightBoucle, text, doc, new Color(147, 136, 48), true, false, true, false);
        colorWords(strsToHighlightFunction, text, doc, Color.black, true, false, true, false);
        colorWords(strsToHighlightComp, text, doc, Color.red, false, false, false, false);
        colorWords(strsToHighlightBool, text, doc, new Color(237, 121, 0), false, false, false, false);
        colorWords(strsToHighlightSep, text, doc, new Color(185, 83, 214), true, false, false, false);
        colorWords(strsToHighlightLayer, text, doc, Color.darkGray, false, false, true, false);
        colorWords(strsToHighlightState, text, doc, new Color(59, 116, 23), true, false, false, false);
        colorWords(strsToHighlightComLigne, text, doc, Color.blue, false, false, false, false);
        colorWords(strsToHighlightComCrochet, text, doc, Color.blue, false, false, false, false);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        ok = new javax.swing.JButton();
        annuler = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        reglePane = new javax.swing.JPanel();
        paneCondition = new javax.swing.JPanel();
        notButton = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        condition1 = new javax.swing.JComboBox();
        operator = new javax.swing.JComboBox();
        condition2 = new javax.swing.JComboBox();
        newCond = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        buttonNewCond = new javax.swing.JButton();
        fonctionState1 = new javax.swing.JComboBox();
        fonctionState2 = new javax.swing.JComboBox();
        fonctionInteger1 = new javax.swing.JTextField();
        fonctionInteger2 = new javax.swing.JTextField();
        nombreCond1 = new javax.swing.JTextField();
        aideButton = new javax.swing.JButton();
        nombreCond2 = new javax.swing.JTextField();
        validateRuleButton = new javax.swing.JButton();
        paneStateIni = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        stateInitial = new javax.swing.JComboBox();
        paneStateFinal = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        stateFinal = new javax.swing.JComboBox();
        paneBlock = new javax.swing.JPanel();
        simpleBlock = new javax.swing.JRadioButton();
        ifBlock = new javax.swing.JRadioButton();
        whileBlock = new javax.swing.JRadioButton();
        repeatBlock = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        repeatTexte = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        condBlock1 = new javax.swing.JComboBox();
        condBlock2 = new javax.swing.JComboBox();
        nombreCondBlock1 = new javax.swing.JTextField();
        fonctionStateBlock1 = new javax.swing.JComboBox();
        operatorBlock = new javax.swing.JComboBox();
        nombreCondBlock2 = new javax.swing.JTextField();
        fonctionStateBlock2 = new javax.swing.JComboBox();
        newCondBlock = new javax.swing.JComboBox();
        buttonNewCondBlock = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        notButtonBlock = new javax.swing.JRadioButton();
        textePane = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        texteArea = new javax.swing.JTextPane();
        jLabel8 = new javax.swing.JLabel();
        informations = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setName("Form"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(RulesGeneratorAssistant.class);
        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        ok.setIcon(resourceMap.getIcon("ok.icon")); // NOI18N
        ok.setText(resourceMap.getString("ok.text")); // NOI18N
        ok.setName("ok"); // NOI18N

        annuler.setIcon(resourceMap.getIcon("annuler.icon")); // NOI18N
        annuler.setText(resourceMap.getString("annuler.text")); // NOI18N
        annuler.setName("annuler"); // NOI18N

        jPanel2.setBackground(resourceMap.getColor("jPanel2.background")); // NOI18N
        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(750, 668));

        reglePane.setBackground(resourceMap.getColor("reglePane.background")); // NOI18N
        reglePane.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        reglePane.setName("reglePane"); // NOI18N

        paneCondition.setBackground(resourceMap.getColor("paneCondition.background")); // NOI18N
        paneCondition.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        paneCondition.setName("paneCondition"); // NOI18N

        notButton.setText(resourceMap.getString("notButton.text")); // NOI18N
        notButton.setName("notButton"); // NOI18N

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        condition1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Entier (saisir)", "nbCellNeu", "nbCellMoo", "nbCellSq", "pCellSq", "nbCellCir", "pCellCir", "cellDuration()", "nbCell", "pCell", "north()", "south()", "east()", "west()", "northWest()", "northEast()", "southEast()", "southWest()", "step()", "nbSameStape()" }));
        condition1.setName("condition1"); // NOI18N

        operator.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "=", ">", "<", ">=", "<=", "<>" }));
        operator.setName("operator"); // NOI18N

        condition2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Entier (saisir)", "true", "false", "nbCellNeu", "nbCellMoo", "nbCellSq", "pCellSq", "nbCellCir", "pCellCir", "cellDuration()", "nbCell", "pCell", "north()", "south()", "east()", "west()", "northWest()", "northEast()", "southEast()", "southWest()", "step()", "nbSameStape()" }));
        condition2.setName("condition2"); // NOI18N

        newCond.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "and", "or", "xor" }));
        newCond.setName("newCond"); // NOI18N

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N

        buttonNewCond.setText(resourceMap.getString("buttonNewCond.text")); // NOI18N
        buttonNewCond.setName("buttonNewCond"); // NOI18N
        buttonNewCond.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonNewCondMouseClicked(evt);
            }
        });

        fonctionState1.setName("fonctionState1"); // NOI18N
        fonctionState1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fonctionState1ActionPerformed(evt);
            }
        });

        fonctionState2.setName("fonctionState2"); // NOI18N
        fonctionState2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fonctionState2ActionPerformed(evt);
            }
        });

        fonctionInteger1.setText(resourceMap.getString("fonctionInteger1.text")); // NOI18N
        fonctionInteger1.setName("fonctionInteger1"); // NOI18N
        fonctionInteger1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                fonctionInteger1KeyReleased(evt);
            }
        });

        fonctionInteger2.setText(resourceMap.getString("fonctionInteger2.text")); // NOI18N
        fonctionInteger2.setName("fonctionInteger2"); // NOI18N
        fonctionInteger2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                fonctionInteger2KeyReleased(evt);
            }
        });

        nombreCond1.setText(resourceMap.getString("nombreCond1.text")); // NOI18N
        nombreCond1.setName("nombreCond1"); // NOI18N
        nombreCond1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreCond1KeyReleased(evt);
            }
        });

        aideButton.setIcon(resourceMap.getIcon("aideButton.icon")); // NOI18N
        aideButton.setText(resourceMap.getString("aideButton.text")); // NOI18N
        aideButton.setName("aideButton"); // NOI18N

        nombreCond2.setText(resourceMap.getString("nombreCond2.text")); // NOI18N
        nombreCond2.setName("nombreCond2"); // NOI18N
        nombreCond2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreCond2KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout paneConditionLayout = new javax.swing.GroupLayout(paneCondition);
        paneCondition.setLayout(paneConditionLayout);
        paneConditionLayout.setHorizontalGroup(
            paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneConditionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(paneConditionLayout.createSequentialGroup()
                        .addGroup(paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(paneConditionLayout.createSequentialGroup()
                                .addGroup(paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(paneConditionLayout.createSequentialGroup()
                                        .addComponent(condition1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nombreCond1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fonctionState1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(fonctionInteger1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(operator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)))
                                .addComponent(condition2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nombreCond2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fonctionState2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(paneConditionLayout.createSequentialGroup()
                                .addComponent(aideButton, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 255, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(newCond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonNewCond)
                            .addComponent(fonctionInteger2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(notButton))
                .addGap(195, 195, 195))
        );
        paneConditionLayout.setVerticalGroup(
            paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneConditionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(condition1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombreCond1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fonctionState1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fonctionInteger1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(operator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(condition2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombreCond2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fonctionState2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fonctionInteger2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(notButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(paneConditionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aideButton)
                    .addComponent(jLabel6)
                    .addComponent(newCond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNewCond))
                .addContainerGap())
        );

        validateRuleButton.setIcon(resourceMap.getIcon("validateRuleButton.icon")); // NOI18N
        validateRuleButton.setText(resourceMap.getString("validateRuleButton.text")); // NOI18N
        validateRuleButton.setName("validateRuleButton"); // NOI18N

        paneStateIni.setBackground(resourceMap.getColor("paneStateIni.background")); // NOI18N
        paneStateIni.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        paneStateIni.setName("paneStateIni"); // NOI18N

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        stateInitial.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "all" }));
        stateInitial.setName("stateInitial"); // NOI18N

        javax.swing.GroupLayout paneStateIniLayout = new javax.swing.GroupLayout(paneStateIni);
        paneStateIni.setLayout(paneStateIniLayout);
        paneStateIniLayout.setHorizontalGroup(
            paneStateIniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneStateIniLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(26, 26, 26)
                .addComponent(stateInitial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(280, Short.MAX_VALUE))
        );
        paneStateIniLayout.setVerticalGroup(
            paneStateIniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneStateIniLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(paneStateIniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stateInitial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        paneStateFinal.setBackground(resourceMap.getColor("paneStateFinal.background")); // NOI18N
        paneStateFinal.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        paneStateFinal.setName("paneStateFinal"); // NOI18N

        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        stateFinal.setName("stateFinal"); // NOI18N

        javax.swing.GroupLayout paneStateFinalLayout = new javax.swing.GroupLayout(paneStateFinal);
        paneStateFinal.setLayout(paneStateFinalLayout);
        paneStateFinalLayout.setHorizontalGroup(
            paneStateFinalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneStateFinalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(56, 56, 56)
                .addComponent(stateFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        paneStateFinalLayout.setVerticalGroup(
            paneStateFinalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneStateFinalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(paneStateFinalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stateFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout reglePaneLayout = new javax.swing.GroupLayout(reglePane);
        reglePane.setLayout(reglePaneLayout);
        reglePaneLayout.setHorizontalGroup(
            reglePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reglePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(reglePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(paneCondition, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(reglePaneLayout.createSequentialGroup()
                        .addComponent(paneStateIni, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(paneStateFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(validateRuleButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        reglePaneLayout.setVerticalGroup(
            reglePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reglePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(reglePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(paneStateFinal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(paneStateIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(paneCondition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(validateRuleButton))
        );

        paneBlock.setBackground(resourceMap.getColor("paneBlock.background")); // NOI18N
        paneBlock.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        paneBlock.setName("paneBlock"); // NOI18N

        buttonGroup1.add(simpleBlock);
        simpleBlock.setSelected(true);
        simpleBlock.setText(resourceMap.getString("simpleBlock.text")); // NOI18N
        simpleBlock.setName("simpleBlock"); // NOI18N

        buttonGroup1.add(ifBlock);
        ifBlock.setText(resourceMap.getString("ifBlock.text")); // NOI18N
        ifBlock.setName("ifBlock"); // NOI18N

        buttonGroup1.add(whileBlock);
        whileBlock.setText(resourceMap.getString("whileBlock.text")); // NOI18N
        whileBlock.setName("whileBlock"); // NOI18N

        buttonGroup1.add(repeatBlock);
        repeatBlock.setText(resourceMap.getString("repeatBlock.text")); // NOI18N
        repeatBlock.setName("repeatBlock"); // NOI18N

        jLabel5.setFont(resourceMap.getFont("jLabel5.font")); // NOI18N
        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        repeatTexte.setText(resourceMap.getString("repeatTexte.text")); // NOI18N
        repeatTexte.setName("repeatTexte"); // NOI18N
        repeatTexte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                repeatTexteKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(repeatTexte);

        javax.swing.GroupLayout paneBlockLayout = new javax.swing.GroupLayout(paneBlock);
        paneBlock.setLayout(paneBlockLayout);
        paneBlockLayout.setHorizontalGroup(
            paneBlockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneBlockLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(paneBlockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(simpleBlock)
                    .addComponent(whileBlock)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneBlockLayout.createSequentialGroup()
                        .addComponent(repeatBlock)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ifBlock))
                .addContainerGap())
        );
        paneBlockLayout.setVerticalGroup(
            paneBlockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneBlockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(simpleBlock)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(paneBlockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, paneBlockLayout.createSequentialGroup()
                        .addComponent(whileBlock)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ifBlock)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(repeatBlock))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1.setBackground(resourceMap.getColor("jPanel1.background")); // NOI18N
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel9.setFont(resourceMap.getFont("jLabel9.font")); // NOI18N
        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        condBlock1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Entier (saisir)", "cellDuration()", "nbCell", "pCell", "step()", "nbSameStape()" }));
        condBlock1.setName("condBlock1"); // NOI18N
        condBlock1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                condBlock1MouseClicked(evt);
            }
        });

        condBlock2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Entier (saisir)", "cellDuration()", "nbCell", "pCell", "step()", "nbSameStape()" }));
        condBlock2.setName("condBlock2"); // NOI18N
        condBlock2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                condBlock2MouseClicked(evt);
            }
        });

        nombreCondBlock1.setText(resourceMap.getString("nombreCondBlock1.text")); // NOI18N
        nombreCondBlock1.setName("nombreCondBlock1"); // NOI18N
        nombreCondBlock1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreCondBlock1KeyReleased(evt);
            }
        });

        fonctionStateBlock1.setName("fonctionStateBlock1"); // NOI18N
        fonctionStateBlock1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fonctionStateBlock1MouseClicked(evt);
            }
        });

        operatorBlock.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "=", ">", "<", ">=", "<=", "<>" }));
        operatorBlock.setName("operatorBlock"); // NOI18N
        operatorBlock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                operatorBlockMouseClicked(evt);
            }
        });

        nombreCondBlock2.setText(resourceMap.getString("nombreCondBlock2.text")); // NOI18N
        nombreCondBlock2.setName("nombreCondBlock2"); // NOI18N
        nombreCondBlock2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreCondBlock2KeyReleased(evt);
            }
        });

        fonctionStateBlock2.setName("fonctionStateBlock2"); // NOI18N
        fonctionStateBlock2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fonctionStateBlock2MouseClicked(evt);
            }
        });

        newCondBlock.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "and", "or", "xor" }));
        newCondBlock.setName("newCondBlock"); // NOI18N

        buttonNewCondBlock.setText(resourceMap.getString("buttonNewCondBlock.text")); // NOI18N
        buttonNewCondBlock.setName("buttonNewCondBlock"); // NOI18N
        buttonNewCondBlock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonNewCondBlockMouseClicked(evt);
            }
        });

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N

        notButtonBlock.setText(resourceMap.getString("notButtonBlock.text")); // NOI18N
        notButtonBlock.setName("notButtonBlock"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(condBlock1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nombreCondBlock1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fonctionStateBlock1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(notButtonBlock))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(newCondBlock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonNewCondBlock))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(operatorBlock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(condBlock2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nombreCondBlock2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fonctionStateBlock2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel9))
                .addContainerGap(235, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(condBlock1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombreCondBlock1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fonctionStateBlock1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(operatorBlock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(condBlock2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombreCondBlock2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fonctionStateBlock2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(notButtonBlock)
                    .addComponent(jLabel10)
                    .addComponent(newCondBlock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNewCondBlock))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        textePane.setBackground(resourceMap.getColor("textePane.background")); // NOI18N
        textePane.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        textePane.setName("textePane"); // NOI18N

        jScrollPane5.setName("jScrollPane5"); // NOI18N

        texteArea.setFont(resourceMap.getFont("texteArea.font")); // NOI18N
        texteArea.setForeground(resourceMap.getColor("texteArea.foreground")); // NOI18N
        texteArea.setText(resourceMap.getString("texteArea.text")); // NOI18N
        texteArea.setName("texteArea"); // NOI18N
        jScrollPane5.setViewportView(texteArea);

        javax.swing.GroupLayout textePaneLayout = new javax.swing.GroupLayout(textePane);
        textePane.setLayout(textePaneLayout);
        textePaneLayout.setHorizontalGroup(
            textePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(textePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE)
                .addContainerGap())
        );
        textePaneLayout.setVerticalGroup(
            textePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, textePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel8.setFont(resourceMap.getFont("jLabel8.font")); // NOI18N
        jLabel8.setForeground(resourceMap.getColor("jLabel8.foreground")); // NOI18N
        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(paneBlock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textePane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(reglePane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textePane, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(paneBlock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reglePane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        informations.setIcon(resourceMap.getIcon("informations.icon")); // NOI18N
        informations.setText(resourceMap.getString("informations.text")); // NOI18N
        informations.setName("informations"); // NOI18N

        jLabel11.setFont(resourceMap.getFont("jLabel11.font")); // NOI18N
        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N

        jLabel12.setFont(resourceMap.getFont("jLabel12.font")); // NOI18N
        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 924, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 334, Short.MAX_VALUE)
                        .addComponent(annuler, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ok))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 509, Short.MAX_VALUE)
                        .addComponent(informations, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(informations, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(5, 5, 5)))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(36, 36, 36)
                                .addComponent(jLabel7))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12)))
                        .addGap(13, 13, 13))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(annuler, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ok, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void nombreCond1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreCond1KeyReleased
    actualiserConditionEnCours();
}//GEN-LAST:event_nombreCond1KeyReleased

private void repeatTexteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_repeatTexteKeyReleased
    repeatBlock.doClick();
}//GEN-LAST:event_repeatTexteKeyReleased

private void buttonNewCondMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonNewCondMouseClicked
}//GEN-LAST:event_buttonNewCondMouseClicked

private void nombreCondBlock1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreCondBlock1KeyReleased
    actuBlock();
}//GEN-LAST:event_nombreCondBlock1KeyReleased

private void nombreCondBlock2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreCondBlock2KeyReleased
    actuBlock();
}//GEN-LAST:event_nombreCondBlock2KeyReleased

private void buttonNewCondBlockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonNewCondBlockMouseClicked
    String op = newCondBlock.getSelectedItem().toString();
    conditionBlock = conditionBlock + conditionBlockEnCours + " " + op + " ";
    conditionBlockEnCours = "";
    actuBlock();
}//GEN-LAST:event_buttonNewCondBlockMouseClicked

private void condBlock1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_condBlock1MouseClicked
    actuBlock();
}//GEN-LAST:event_condBlock1MouseClicked

private void fonctionStateBlock1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fonctionStateBlock1MouseClicked
    actuBlock();
}//GEN-LAST:event_fonctionStateBlock1MouseClicked

private void operatorBlockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_operatorBlockMouseClicked
    actuBlock();
}//GEN-LAST:event_operatorBlockMouseClicked

private void condBlock2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_condBlock2MouseClicked
    actuBlock();
}//GEN-LAST:event_condBlock2MouseClicked

private void fonctionStateBlock2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fonctionStateBlock2MouseClicked
    actuBlock();
}//GEN-LAST:event_fonctionStateBlock2MouseClicked

private void fonctionState1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fonctionState1ActionPerformed
    actualiserConditionEnCours();
}//GEN-LAST:event_fonctionState1ActionPerformed

private void fonctionState2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fonctionState2ActionPerformed
    actualiserConditionEnCours();
}//GEN-LAST:event_fonctionState2ActionPerformed

private void fonctionInteger1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fonctionInteger1KeyReleased
    actualiserConditionEnCours();
}//GEN-LAST:event_fonctionInteger1KeyReleased

private void fonctionInteger2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fonctionInteger2KeyReleased
    actualiserConditionEnCours();
}//GEN-LAST:event_fonctionInteger2KeyReleased

private void nombreCond2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreCond2KeyReleased
    actualiserConditionEnCours();
}//GEN-LAST:event_nombreCond2KeyReleased
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton aideButton;
    private javax.swing.JButton annuler;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton buttonNewCond;
    private javax.swing.JButton buttonNewCondBlock;
    private javax.swing.JComboBox condBlock1;
    private javax.swing.JComboBox condBlock2;
    private javax.swing.JComboBox condition1;
    private javax.swing.JComboBox condition2;
    private javax.swing.JTextField fonctionInteger1;
    private javax.swing.JTextField fonctionInteger2;
    private javax.swing.JComboBox fonctionState1;
    private javax.swing.JComboBox fonctionState2;
    private javax.swing.JComboBox fonctionStateBlock1;
    private javax.swing.JComboBox fonctionStateBlock2;
    private javax.swing.JRadioButton ifBlock;
    private javax.swing.JButton informations;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JComboBox newCond;
    private javax.swing.JComboBox newCondBlock;
    private javax.swing.JTextField nombreCond1;
    private javax.swing.JTextField nombreCond2;
    private javax.swing.JTextField nombreCondBlock1;
    private javax.swing.JTextField nombreCondBlock2;
    private javax.swing.JRadioButton notButton;
    private javax.swing.JRadioButton notButtonBlock;
    private javax.swing.JButton ok;
    private javax.swing.JComboBox operator;
    private javax.swing.JComboBox operatorBlock;
    private javax.swing.JPanel paneBlock;
    private javax.swing.JPanel paneCondition;
    private javax.swing.JPanel paneStateFinal;
    private javax.swing.JPanel paneStateIni;
    private javax.swing.JPanel reglePane;
    private javax.swing.JRadioButton repeatBlock;
    private javax.swing.JTextPane repeatTexte;
    private javax.swing.JRadioButton simpleBlock;
    private javax.swing.JComboBox stateFinal;
    private javax.swing.JComboBox stateInitial;
    private javax.swing.JTextPane texteArea;
    private javax.swing.JPanel textePane;
    private javax.swing.JButton validateRuleButton;
    private javax.swing.JRadioButton whileBlock;
    // End of variables declaration//GEN-END:variables
}
