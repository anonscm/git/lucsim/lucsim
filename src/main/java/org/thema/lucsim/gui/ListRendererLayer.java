/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui;

import org.thema.lucsim.engine.Layer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.StateLayer;

/**
 * formatte l'affichage des Jlists pour les listes des couches
 *
 * @author rgrillot
 * @author alexis
 */
public class ListRendererLayer extends DefaultListCellRenderer {

    private Project project;

    public ListRendererLayer(Project project) {
        this.project = project;
    }
    
    /**
     * méthode permettant de spécifier l'affichage des listes
     *
     * @param list liste à afficher
     * @param value objet à afficher
     * @param index indice de l'objet dans la liste
     * @param isSelected précise si l'élément est sélectionné ou non
     * @param cellHasFocus la liste a-t-elle le focus
     * @return
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected,
            boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        Layer layer = (Layer) value;

        if(layer == project.getSimulation().getSimLayer().getInitLayer()) {
            setFont(getFont().deriveFont(getFont().getStyle() | Font.BOLD));
        } 
        
        if(layer instanceof StateLayer){
            setForeground(Color.BLACK);
        } else {
            setForeground(new Color(100, 100, 100));
        }

        return this;
    }
}
