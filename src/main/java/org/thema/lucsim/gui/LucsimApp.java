/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.thema.common.Config;
import org.thema.common.JavaLoader;
import org.thema.lucsim.engine.CLI;

/**
 * The main class of the application.
 */
public class LucsimApp extends SingleFrameApplication {

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {
        show(new LucsimView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     *
     * @param root
     */
    @Override
    protected void configureWindow(java.awt.Window root) {
        
    }

    /**
     * A convenient static getter for the application instance.
     *
     * @return the instance of LucsimApp
     */
    public static LucsimApp getApplication() {
        return Application.getInstance(LucsimApp.class);
    }

    /**
     * Main method launching the application.
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Config.setNodeClass(LucsimApp.class);
        // Relaunch java with preferences memory if no args
        if(args.length == 0) {
            if(JavaLoader.forkJava(LucsimApp.class, 1024)) {
                return;
            }
        }
        
        // Default execution (UI)
        // Set language to english
        Locale.setDefault(Locale.ENGLISH);
        
        
        if(args.length > 0 && !args[0].equals(JavaLoader.NOFORK)) {
            CLI.execute(args);
            return;
        }
        
        // Catch all uncaught exceptions and show a message
        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            if(e instanceof CancellationException) {
                JOptionPane.showMessageDialog(null, "Execution has been cancelled");
            } else {
                Logger.getLogger("").log(Level.SEVERE, null, e);
                JOptionPane.showMessageDialog(null, "An error has occurred : " + e);
            }
        });
        
        launch(LucsimApp.class, args);
    }
}
