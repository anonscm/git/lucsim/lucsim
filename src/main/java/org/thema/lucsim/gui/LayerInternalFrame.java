/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui;

import org.thema.drawshape.image.RasterShape;
import org.thema.drawshape.layer.DefaultGroupLayer;
import org.thema.drawshape.layer.RasterLayer;
import org.thema.drawshape.ui.MapInternalFrame;
import org.thema.lucsim.engine.Layer;

/**
 *
 * @author gvuidel
 */
public class LayerInternalFrame extends MapInternalFrame {
    private Layer layer;

    public LayerInternalFrame(String name, Layer layer) {
        this.layer = layer;
        if (name == null) {
            name = layer.getName();
        }
        DefaultGroupLayer gl = new DefaultGroupLayer("Root", true);
        RasterLayer l = new RasterLayer(name, new RasterShape(layer.getRaster(null), layer.getEnvelope(), layer.getStyle(), true));
        gl.addLayerFirst(l);
        getMapViewer().setRootLayer(gl);
        setVisible(true);
        setName(name);
        setTitle(name);
    }

    public Layer getEngineLayer() {
        return layer;
    }
    
    public void repaintMap() {
        layer.getStyle().update();
        getMapViewer().getMap().fullRepaint();
    }
    
}
