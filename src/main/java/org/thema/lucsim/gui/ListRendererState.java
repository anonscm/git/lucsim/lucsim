/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui;

import org.thema.lucsim.engine.State;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.border.BevelBorder;

/**
 * formatte l'affichage des Jlists pour la liste des états
 *
 * @author rgrillot
 * @author alexis
 */
public class ListRendererState extends DefaultListCellRenderer {

    /**
     * méthode permettant de spécifier l'affichage des listes
     *
     * @param list liste à afficher
     * @param value objet à afficher
     * @param index indice de l'objet dans la liste
     * @param isSelected précise si l'élément est sélectionné ou non
     * @param cellHasFocus la liste a-t-elle le focus
     * @return
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        
        final int iconSize = 15;
        State state = (State) value;

        /* On met le texte à afficher */
        setText(" " + state.getValue() + " - " + state.getName());
        /* On met l'icone avec la couleur de l'état */
        BufferedImage image = new BufferedImage(iconSize + 10, iconSize, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        // on dessine le carré plein de couleur
        g.setColor(state.getColor());
        g.fillRect(0, 0, iconSize + 10, iconSize);
        // on dessine un contour noir
        g.setColor(Color.black);
        g.drawRect(0, 0, iconSize + 10 - 1, iconSize - 1);

        setIcon(new ImageIcon(image));

        return this;
    }

}
