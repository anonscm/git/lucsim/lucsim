/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui.stat;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author alexis
 */
public class ModelImageProp extends AbstractTableModel {

    private Object donnees[][];
    private String titres[];

    public ModelImageProp(Object donnees[][], String titres[]) {
        this.donnees = donnees;
        this.titres = titres;
    }

    @Override
    public int getColumnCount() {
        return donnees[0].length;
    }

    @Override
    public Object getValueAt(int parm1, int parm2) {
        return donnees[parm1][parm2];
    }

    @Override
    public int getRowCount() {
        return donnees.length;
    }

    @Override
    public String getColumnName(int col) {
        return titres[col];
    }
}
