/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.lucsim.gui.stat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JRadioButton;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.stat.CalcDataExtractor;

/**
 *
 * @author Gilles Vuidel
 */
public class DataExtractorPanel extends javax.swing.JPanel {

    private final Project project;
    private final String[] functions = new String[]{"nbCellSq", "nbCellCir"};
    private final List<JRadioButton> buttons;

    public DataExtractorPanel() {
        this(null);
    }
    /**
     * Creates new form DataExtractorPanel
     */
    public DataExtractorPanel(Project project) {
        this.project = project;
        initComponents();
        fillComponents();

        buttons = new ArrayList<>();
        buttons.add(radioButton1For1);
        buttons.add(radioButton1For2);
        buttons.add(radioButton1For3);
        buttons.add(radioButton1For4);
        buttons.add(radioButton1For5);
        buttons.add(radioButton1For10);
    }

    private void fillComponents() {
        if (project != null) {
            fillComboBoxWithLayers(comboBoxLayer1);
            fillComboBoxWithLayers(comboBoxLayer2);
            setCustomSelectedLayer(comboBoxLayer2);
            fillComboBoxWithStates(comboBoxState);
            fillComboBoxWithFunctions(comboBoxFunctions);
            fillListWithLayers(listStaticLayers);
        }
    }
    
    private void setButtonsEnabled(boolean enabled) {
        buttons.stream().forEach(button -> button.setEnabled(enabled));
    }

    /**
     * Fills a given JComboBox with the project's StateLayer.
     * @param comboBox the JComboBox to fill
     */
    private void fillComboBoxWithLayers(JComboBox comboBox) {
        comboBox.setModel(new DefaultComboBoxModel(project.getStateLayers().toArray()));
    }

    /**
     * Fills a given JComboBox with the project's State name.
     * @param comboBox the JComboBox to fill
     */
    public void fillComboBoxWithStates(JComboBox comboBox) {
        StateLayer stateLayer = (StateLayer) comboBoxLayer2.getSelectedItem();
        DefaultComboBoxModel model = new DefaultComboBoxModel(stateLayer.getStates().getStates().toArray());
        model.insertElementAt("(All)", 0);
        comboBox.setModel(model);
    }

    /**
     * Fills a given JComboBox with an array of function names.
     * @param comboBox the JComboBox to fill
     */
    public void fillComboBoxWithFunctions(JComboBox comboBox) {
        comboBox.setModel(new DefaultComboBoxModel(functions));
    }

    /**
     * Selects the JComboBox's second item, if there is more than one item.
     * @param comboBox the JComboBox to fill
     */
    public void setCustomSelectedLayer(JComboBox comboBox) {
        if (comboBox.getItemCount() > 1) {
            comboBox.setSelectedIndex(1);
        }
    }

    /**
     * Fills a given JList with the layers not used as former/newer layer.
     * @param listLayers list to fill
     */
    public void fillListWithLayers(JList listLayers) {
        List<Layer> layers = project.getLayers();
        Iterator<Layer> lit = layers.iterator();
        while (lit.hasNext()) {
            Layer layer = lit.next();
            Layer layerComboBox1 = (Layer) comboBoxLayer1.getSelectedItem();
            Layer layerComboBox2 = (Layer) comboBoxLayer2.getSelectedItem();

            if (layer.getName().equals(layerComboBox1.getName())
                    || layer.getName().equals(layerComboBox2.getName())
                    || layer.getName().endsWith("_sim")) {
                lit.remove();
            }
        }

        listLayers.setListData(layers.toArray());
        listLayers.setSelectionInterval(0, listLayers.getModel().getSize() - 1);
    }

    public CalcDataExtractor getDataExtractor() {
        StateLayer layer1 = (StateLayer) comboBoxLayer1.getSelectedItem();
        StateLayer layer2 = (StateLayer) comboBoxLayer2.getSelectedItem();

        if ((layer1.getHeight() != layer2.getHeight())
                || (layer1.getWidth() != layer2.getWidth())) {
            throw new IllegalArgumentException(layer1.getName() 
                    + " is not of the same size than "
                    + layer2.getName() + ".");
        } else if (((int) spinnerMinRadius.getValue()) > ((int) spinnerMaxRadius.getValue())) {
            throw new IllegalArgumentException("The minimal radius must be less than the maximal radius.");
        }
        State targetState = comboBoxState.getSelectedIndex() == 0 ? null : (State)comboBoxState.getSelectedItem();
        return new CalcDataExtractor(layer1, layer2, listStaticLayers.getSelectedValuesList(), 
                (String) comboBoxFunctions.getSelectedItem(), checkBoxRing.isSelected(), 
                (int) spinnerMinRadius.getValue(), (int) spinnerMaxRadius.getValue(), (int) spinnerStep.getValue(), 
                targetState, Double.valueOf(textFieldWeight.getText()));
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupWeightRatios = new javax.swing.ButtonGroup();
        panelState = new javax.swing.JPanel();
        labelStateTitle = new javax.swing.JLabel();
        labelState = new javax.swing.JLabel();
        comboBoxState = new javax.swing.JComboBox<State>();
        panelRadius = new javax.swing.JPanel();
        labelRadius = new javax.swing.JLabel();
        labelMinRadius = new javax.swing.JLabel();
        labelMaxRadius = new javax.swing.JLabel();
        spinnerMinRadius = new javax.swing.JSpinner();
        spinnerMaxRadius = new javax.swing.JSpinner();
        labelStep = new javax.swing.JLabel();
        spinnerStep = new javax.swing.JSpinner();
        panelLayers = new javax.swing.JPanel();
        labelLayers = new javax.swing.JLabel();
        labelLayer1 = new javax.swing.JLabel();
        labelLayer2 = new javax.swing.JLabel();
        comboBoxLayer1 = new javax.swing.JComboBox<Layer>();
        comboBoxLayer2 = new javax.swing.JComboBox<Layer>();
        labelProgressState = new javax.swing.JLabel();
        panelData = new javax.swing.JPanel();
        labelAdditionalLayersTitle = new javax.swing.JLabel();
        labelAdditionalLayers = new javax.swing.JLabel();
        scrollPaneAdditionalLayers = new javax.swing.JScrollPane();
        listStaticLayers = new javax.swing.JList<Layer>();
        labelAdditionalLayersTip1 = new javax.swing.JLabel();
        labelAdditionalLayersTip2 = new javax.swing.JLabel();
        panelFunction = new javax.swing.JPanel();
        labelFunctionTitle = new javax.swing.JLabel();
        labelFunction = new javax.swing.JLabel();
        comboBoxFunctions = new javax.swing.JComboBox<String>();
        labelRing = new javax.swing.JLabel();
        checkBoxRing = new javax.swing.JCheckBox();
        panelWeight = new javax.swing.JPanel();
        labelProgressState2 = new javax.swing.JLabel();
        labelWeightTitle = new javax.swing.JLabel();
        labelWeight = new javax.swing.JLabel();
        checkBoxWeight = new javax.swing.JCheckBox();
        textFieldWeight = new javax.swing.JTextField();
        radioButton1For1 = new javax.swing.JRadioButton();
        radioButton1For2 = new javax.swing.JRadioButton();
        radioButton1For3 = new javax.swing.JRadioButton();
        radioButton1For4 = new javax.swing.JRadioButton();
        radioButton1For5 = new javax.swing.JRadioButton();
        radioButton1For10 = new javax.swing.JRadioButton();

        panelState.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelState.setName("panelState"); // NOI18N
        panelState.setPreferredSize(new java.awt.Dimension(200, 150));

        labelStateTitle.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        labelStateTitle.setText("State");
        labelStateTitle.setName("labelStateTitle"); // NOI18N

        labelState.setText("Target state :");
        labelState.setName("labelState"); // NOI18N

        comboBoxState.setName("comboBoxState"); // NOI18N
        comboBoxState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxStateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelStateLayout = new javax.swing.GroupLayout(panelState);
        panelState.setLayout(panelStateLayout);
        panelStateLayout.setHorizontalGroup(
            panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStateLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelStateLayout.createSequentialGroup()
                        .addComponent(labelStateTitle)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelStateLayout.createSequentialGroup()
                        .addComponent(labelState)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboBoxState, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelStateLayout.setVerticalGroup(
            panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStateLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelStateTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelState)
                    .addComponent(comboBoxState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelRadius.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelRadius.setName("panelRadius"); // NOI18N
        panelRadius.setPreferredSize(new java.awt.Dimension(200, 150));

        labelRadius.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        labelRadius.setText("Neighborhood radius");
        labelRadius.setName("labelRadius"); // NOI18N

        labelMinRadius.setText("Start radius          :");
        labelMinRadius.setName("labelMinRadius"); // NOI18N

        labelMaxRadius.setText("End radius            :");
        labelMaxRadius.setName("labelMaxRadius"); // NOI18N

        spinnerMinRadius.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        spinnerMinRadius.setName("spinnerMinRadius"); // NOI18N
        spinnerMinRadius.setValue(1);

        spinnerMaxRadius.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(10), Integer.valueOf(1), null, Integer.valueOf(1)));
        spinnerMaxRadius.setName("spinnerMaxRadius"); // NOI18N
        spinnerMaxRadius.setValue(10);

        labelStep.setText("Step                     :");
        labelStep.setName("labelStep"); // NOI18N

        spinnerStep.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        spinnerStep.setName("spinnerStep"); // NOI18N

        javax.swing.GroupLayout panelRadiusLayout = new javax.swing.GroupLayout(panelRadius);
        panelRadius.setLayout(panelRadiusLayout);
        panelRadiusLayout.setHorizontalGroup(
            panelRadiusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRadiusLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelRadiusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRadiusLayout.createSequentialGroup()
                        .addComponent(labelStep)
                        .addGap(5, 5, 5)
                        .addComponent(spinnerStep))
                    .addGroup(panelRadiusLayout.createSequentialGroup()
                        .addComponent(labelMaxRadius)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerMaxRadius))
                    .addGroup(panelRadiusLayout.createSequentialGroup()
                        .addComponent(labelMinRadius)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerMinRadius, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))
                    .addComponent(labelRadius, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelRadiusLayout.setVerticalGroup(
            panelRadiusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRadiusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelRadius)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelRadiusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelMinRadius)
                    .addComponent(spinnerMinRadius, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelRadiusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelMaxRadius)
                    .addComponent(spinnerMaxRadius, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelRadiusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelStep)
                    .addComponent(spinnerStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelLayers.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelLayers.setName("panelLayers"); // NOI18N
        panelLayers.setPreferredSize(new java.awt.Dimension(200, 150));

        labelLayers.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        labelLayers.setText("Layers");
        labelLayers.setName("labelLayers"); // NOI18N

        labelLayer1.setText("First layer          :");
        labelLayer1.setName("labelLayer1"); // NOI18N

        labelLayer2.setText("Second layer  :");
        labelLayer2.setName("labelLayer2"); // NOI18N

        comboBoxLayer1.setToolTipText("");
        comboBoxLayer1.setName("comboBoxLayer1"); // NOI18N

        comboBoxLayer2.setName("comboBoxLayer2"); // NOI18N

        labelProgressState.setName("labelProgressState"); // NOI18N

        javax.swing.GroupLayout panelLayersLayout = new javax.swing.GroupLayout(panelLayers);
        panelLayers.setLayout(panelLayersLayout);
        panelLayersLayout.setHorizontalGroup(
            panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayersLayout.createSequentialGroup()
                        .addGroup(panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelLayer1)
                            .addComponent(labelLayer2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboBoxLayer2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboBoxLayer1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayersLayout.createSequentialGroup()
                        .addComponent(labelLayers)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelProgressState)))
                .addContainerGap())
        );
        panelLayersLayout.setVerticalGroup(
            panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelProgressState)
                    .addGroup(panelLayersLayout.createSequentialGroup()
                        .addComponent(labelLayers)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelLayer1)
                            .addComponent(comboBoxLayer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelLayer2)
                            .addComponent(comboBoxLayer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelData.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelData.setName("panelData"); // NOI18N
        panelData.setPreferredSize(new java.awt.Dimension(200, 150));

        labelAdditionalLayersTitle.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        labelAdditionalLayersTitle.setText("Additional layers");
        labelAdditionalLayersTitle.setName("labelAdditionalLayersTitle"); // NOI18N

        labelAdditionalLayers.setText("Extract data from:");
        labelAdditionalLayers.setName("labelAdditionalLayers"); // NOI18N

        scrollPaneAdditionalLayers.setName("scrollPaneAdditionalLayers"); // NOI18N

        listStaticLayers.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        listStaticLayers.setName("listStaticLayers"); // NOI18N
        scrollPaneAdditionalLayers.setViewportView(listStaticLayers);

        labelAdditionalLayersTip1.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        labelAdditionalLayersTip1.setText("(Ctrl+click to pick");
        labelAdditionalLayersTip1.setName("labelAdditionalLayersTip1"); // NOI18N

        labelAdditionalLayersTip2.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        labelAdditionalLayersTip2.setText("multiple options)");
        labelAdditionalLayersTip2.setName("labelAdditionalLayersTip2"); // NOI18N

        javax.swing.GroupLayout panelDataLayout = new javax.swing.GroupLayout(panelData);
        panelData.setLayout(panelDataLayout);
        panelDataLayout.setHorizontalGroup(
            panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDataLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDataLayout.createSequentialGroup()
                        .addComponent(labelAdditionalLayersTitle)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDataLayout.createSequentialGroup()
                        .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelAdditionalLayers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelAdditionalLayersTip1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelAdditionalLayersTip2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPaneAdditionalLayers, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelDataLayout.setVerticalGroup(
            panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDataLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelAdditionalLayersTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDataLayout.createSequentialGroup()
                        .addComponent(labelAdditionalLayers)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelAdditionalLayersTip1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelAdditionalLayersTip2))
                    .addComponent(scrollPaneAdditionalLayers, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );

        panelFunction.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelFunction.setName("panelFunction"); // NOI18N
        panelFunction.setPreferredSize(new java.awt.Dimension(200, 150));

        labelFunctionTitle.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        labelFunctionTitle.setText("Function");
        labelFunctionTitle.setName("labelFunctionTitle"); // NOI18N

        labelFunction.setText("Function to use   :");
        labelFunction.setName("labelFunction"); // NOI18N

        comboBoxFunctions.setName("comboBoxFunctions"); // NOI18N

        labelRing.setText("Ring                     :");
        labelRing.setName("labelRing"); // NOI18N

        checkBoxRing.setName("checkBoxRing"); // NOI18N

        javax.swing.GroupLayout panelFunctionLayout = new javax.swing.GroupLayout(panelFunction);
        panelFunction.setLayout(panelFunctionLayout);
        panelFunctionLayout.setHorizontalGroup(
            panelFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFunctionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFunctionLayout.createSequentialGroup()
                        .addComponent(labelFunctionTitle)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelFunctionLayout.createSequentialGroup()
                        .addGroup(panelFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelFunctionLayout.createSequentialGroup()
                                .addComponent(labelFunction)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboBoxFunctions, 0, 105, Short.MAX_VALUE))
                            .addGroup(panelFunctionLayout.createSequentialGroup()
                                .addComponent(labelRing)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkBoxRing)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        panelFunctionLayout.setVerticalGroup(
            panelFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFunctionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelFunctionTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxFunctions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelFunction))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelRing)
                    .addComponent(checkBoxRing))
                .addContainerGap(90, Short.MAX_VALUE))
        );

        panelWeight.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelWeight.setName("panelWeight"); // NOI18N
        panelWeight.setPreferredSize(new java.awt.Dimension(200, 150));

        labelProgressState2.setName("labelProgressState2"); // NOI18N

        labelWeightTitle.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        labelWeightTitle.setText("Weight");
        labelWeightTitle.setName("labelWeightTitle"); // NOI18N

        labelWeight.setText("Weight                 :");
        labelWeight.setName("labelWeight"); // NOI18N
        labelWeight.setPreferredSize(new java.awt.Dimension(87, 14));

        checkBoxWeight.setName("checkBoxWeight"); // NOI18N
        checkBoxWeight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBoxWeightActionPerformed(evt);
            }
        });

        textFieldWeight.setText("1.0");
        textFieldWeight.setEnabled(false);
        textFieldWeight.setName("textFieldWeight"); // NOI18N

        buttonGroupWeightRatios.add(radioButton1For1);
        radioButton1For1.setSelected(true);
        radioButton1For1.setText("1:1");
        radioButton1For1.setActionCommand("1");
        radioButton1For1.setEnabled(false);
        radioButton1For1.setName("radioButton1For1"); // NOI18N
        radioButton1For1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1For1ActionPerformed(evt);
            }
        });

        buttonGroupWeightRatios.add(radioButton1For2);
        radioButton1For2.setText("1:2");
        radioButton1For2.setActionCommand("2");
        radioButton1For2.setEnabled(false);
        radioButton1For2.setName("radioButton1For2"); // NOI18N
        radioButton1For2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1For2ActionPerformed(evt);
            }
        });

        buttonGroupWeightRatios.add(radioButton1For3);
        radioButton1For3.setText("1:3");
        radioButton1For3.setActionCommand("3");
        radioButton1For3.setEnabled(false);
        radioButton1For3.setName("radioButton1For3"); // NOI18N
        radioButton1For3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1For3ActionPerformed(evt);
            }
        });

        buttonGroupWeightRatios.add(radioButton1For4);
        radioButton1For4.setText("1:4");
        radioButton1For4.setActionCommand("4");
        radioButton1For4.setEnabled(false);
        radioButton1For4.setName("radioButton1For4"); // NOI18N
        radioButton1For4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1For4ActionPerformed(evt);
            }
        });

        buttonGroupWeightRatios.add(radioButton1For5);
        radioButton1For5.setText("1:5");
        radioButton1For5.setActionCommand("5");
        radioButton1For5.setEnabled(false);
        radioButton1For5.setName("radioButton1For5"); // NOI18N
        radioButton1For5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1For5ActionPerformed(evt);
            }
        });

        buttonGroupWeightRatios.add(radioButton1For10);
        radioButton1For10.setText("1:10");
        radioButton1For10.setActionCommand("10");
        radioButton1For10.setEnabled(false);
        radioButton1For10.setName("radioButton1For10"); // NOI18N
        radioButton1For10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1For10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelWeightLayout = new javax.swing.GroupLayout(panelWeight);
        panelWeight.setLayout(panelWeightLayout);
        panelWeightLayout.setHorizontalGroup(
            panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelWeightLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelWeightLayout.createSequentialGroup()
                        .addComponent(labelWeightTitle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelProgressState2))
                    .addGroup(panelWeightLayout.createSequentialGroup()
                        .addComponent(labelWeight, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkBoxWeight)
                        .addGap(86, 86, 86))
                    .addComponent(textFieldWeight, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelWeightLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioButton1For3)
                            .addComponent(radioButton1For1)
                            .addComponent(radioButton1For2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioButton1For10)
                            .addComponent(radioButton1For4)
                            .addComponent(radioButton1For5))
                        .addGap(53, 53, 53)))
                .addContainerGap())
        );
        panelWeightLayout.setVerticalGroup(
            panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelWeightLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelWeightTitle)
                    .addComponent(labelProgressState2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkBoxWeight))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioButton1For1)
                    .addComponent(radioButton1For4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioButton1For2)
                    .addComponent(radioButton1For5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelWeightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioButton1For3)
                    .addComponent(radioButton1For10)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 894, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(panelFunction, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                        .addComponent(panelLayers, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(panelData, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                        .addComponent(panelRadius, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(panelWeight, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                        .addComponent(panelState, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE))
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 373, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(panelLayers, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                        .addComponent(panelData, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                        .addComponent(panelState, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(panelRadius, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                        .addComponent(panelWeight, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                        .addComponent(panelFunction, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap()))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxStateActionPerformed
        if(comboBoxState.getSelectedIndex() == 0) {
            checkBoxWeight.setEnabled(false);
            checkBoxWeight.setSelected(false);
        } else {
            checkBoxWeight.setEnabled(true);
        }
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_comboBoxStateActionPerformed

    private void checkBoxWeightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBoxWeightActionPerformed
        if (checkBoxWeight.isSelected()) {
            setButtonsEnabled(true);

            StateLayer formerLayer = (StateLayer) comboBoxLayer1.getSelectedItem();
            StateLayer modernLayer = (StateLayer) comboBoxLayer2.getSelectedItem();
            State targetState = (State) comboBoxState.getSelectedItem();
            double ratio = CalcDataExtractor.estimWeight(formerLayer, modernLayer, targetState) /
                    Integer.parseInt(buttonGroupWeightRatios.getSelection().getActionCommand());
            
            textFieldWeight.setText(String.valueOf(ratio));
            textFieldWeight.setEnabled(true);
        } else {
            textFieldWeight.setEnabled(false);
            textFieldWeight.setText("1.0");
            setButtonsEnabled(false);
        }
    }//GEN-LAST:event_checkBoxWeightActionPerformed

    private void radioButton1For1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1For1ActionPerformed
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_radioButton1For1ActionPerformed

    private void radioButton1For2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1For2ActionPerformed
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_radioButton1For2ActionPerformed

    private void radioButton1For3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1For3ActionPerformed
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_radioButton1For3ActionPerformed

    private void radioButton1For4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1For4ActionPerformed
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_radioButton1For4ActionPerformed

    private void radioButton1For5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1For5ActionPerformed
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_radioButton1For5ActionPerformed

    private void radioButton1For10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1For10ActionPerformed
        checkBoxWeightActionPerformed(evt);
    }//GEN-LAST:event_radioButton1For10ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupWeightRatios;
    private javax.swing.JCheckBox checkBoxRing;
    private javax.swing.JCheckBox checkBoxWeight;
    private javax.swing.JComboBox<String> comboBoxFunctions;
    private javax.swing.JComboBox<Layer> comboBoxLayer1;
    private javax.swing.JComboBox<Layer> comboBoxLayer2;
    private javax.swing.JComboBox<State> comboBoxState;
    private javax.swing.JLabel labelAdditionalLayers;
    private javax.swing.JLabel labelAdditionalLayersTip1;
    private javax.swing.JLabel labelAdditionalLayersTip2;
    private javax.swing.JLabel labelAdditionalLayersTitle;
    private javax.swing.JLabel labelFunction;
    private javax.swing.JLabel labelFunctionTitle;
    private javax.swing.JLabel labelLayer1;
    private javax.swing.JLabel labelLayer2;
    private javax.swing.JLabel labelLayers;
    private javax.swing.JLabel labelMaxRadius;
    private javax.swing.JLabel labelMinRadius;
    private javax.swing.JLabel labelProgressState;
    private javax.swing.JLabel labelProgressState2;
    private javax.swing.JLabel labelRadius;
    private javax.swing.JLabel labelRing;
    private javax.swing.JLabel labelState;
    private javax.swing.JLabel labelStateTitle;
    private javax.swing.JLabel labelStep;
    private javax.swing.JLabel labelWeight;
    private javax.swing.JLabel labelWeightTitle;
    private javax.swing.JList<Layer> listStaticLayers;
    private javax.swing.JPanel panelData;
    private javax.swing.JPanel panelFunction;
    private javax.swing.JPanel panelLayers;
    private javax.swing.JPanel panelRadius;
    private javax.swing.JPanel panelState;
    private javax.swing.JPanel panelWeight;
    private javax.swing.JRadioButton radioButton1For1;
    private javax.swing.JRadioButton radioButton1For10;
    private javax.swing.JRadioButton radioButton1For2;
    private javax.swing.JRadioButton radioButton1For3;
    private javax.swing.JRadioButton radioButton1For4;
    private javax.swing.JRadioButton radioButton1For5;
    private javax.swing.JScrollPane scrollPaneAdditionalLayers;
    private javax.swing.JSpinner spinnerMaxRadius;
    private javax.swing.JSpinner spinnerMinRadius;
    private javax.swing.JSpinner spinnerStep;
    private javax.swing.JTextField textFieldWeight;
    // End of variables declaration//GEN-END:variables
}
