/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui.stat;

import javax.swing.table.AbstractTableModel;
import org.jfree.data.xy.XYDataset;

/**
 *
 * @author alexis
 */
public class TableXYModel extends AbstractTableModel {

    private XYDataset dataset;
    private int n;

    public TableXYModel(XYDataset dataset, int number) {
        this.dataset = dataset;
        this.n = number;
    }

    @Override
    public int getRowCount() {
        return dataset.getSeriesCount();
    }

    @Override
    public int getColumnCount() {
        return n;
    }

    @Override
    public Object getValueAt(int item, int columnIndex) {
        if (columnIndex == 0) {
            return dataset.getSeriesKey(item);
        }

        return dataset.getYValue(item, columnIndex - 1);
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "State";
        }

        return "Step " + (column - 1) + "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
