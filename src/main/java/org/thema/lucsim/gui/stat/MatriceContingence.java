/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui.stat;

import au.com.bytecode.opencsv.CSVWriter;
import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import org.thema.common.Util;
import org.thema.common.io.IOFile;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.States;

/**
 *
 * @author alexis
 */
public class MatriceContingence extends javax.swing.JPanel {

    private States states;
    private Layer image1;
    private Layer image2;


    /**
     * Creates new form MatriceContingence
     */
    public MatriceContingence(Layer image1, Layer image2, int mode, States states) {
        initComponents();
        this.image1 = image1;
        this.image2 = image2;
        this.states = states;
        
        //Si image sont la meme taille
        //La comparaison du nombre d'état est pas obligatoire
        if (!((image1.getWidth() == image2.getWidth()) && (image1.getHeight() == image2.getHeight()))) {
            throw new IllegalArgumentException("Images have not the same size !");
        }

        ArrayList<String> arr1 = image1.listStateString();
        ArrayList<String> arr2 = image2.listStateString();

        //creation matrice
        int[][] matrice = new int[arr1.size() + 1][arr2.size() + 1];
        for (int i = 0; i < arr1.size() + 1; i++) {
            for (int j = 0; j < arr2.size() + 1; j++) {
                matrice[i][j] = 0;
            }
        }
        //calcul matrice
        for (int i = 0; i < image1.getWidth(); i++) {
            for (int j = 0; j < image1.getHeight(); j++) {
                double val1 = image1.getElement(i, j);
                double val2 = image2.getElement(i, j);
                String v1 = ""; String v2 = "";
                if(image1.isStateLayer()){
                    v1 = states.getState((int)val1).getName();
                } else {
                    v1 = (int)val1 +"";
                }
                if(image2.isStateLayer()){
                    v2 = states.getState((int)val2).getName();
                } else {
                    v2 = (int)val2 +"";
                }
                matrice[arr1.indexOf(v1)+1][arr2.indexOf(v2)+1]++;
            }
        }


        String[] titre = new String[arr2.size() + 1];
        for (int i = 0; i < arr2.size() + 1; i++) {
            titre[i] = "";
        }


        String[][] donnees = new String[arr1.size() + 1][arr2.size() + 1];
        for (int i = 0; i < arr1.size() + 1; i++) {
            for (int j = 0; j < arr2.size() + 1; j++) {
                donnees[i][j] = matrice[i][j] + "";
            }
        }
        donnees[0][0] = "State";
        //etat en haut
        for (int i = 0; i < arr2.size() ; i++) {
            donnees[0][i+1] = arr2.get(i);
        }
        //etat a gauche
        for (int i = 0; i < arr1.size() ; i++) {
            donnees[i+1][0] = arr1.get(i);
        }

        if (mode == 1) {
            for (int i = 1; i < arr1.size() + 1; i++) {
                double totLigne = 0;
                for (int j = 1; j < arr2.size() + 1; j++) {
                    totLigne += Integer.parseInt(donnees[i][j]);
                }
                for (int j = 1; j < arr2.size() + 1; j++) {
                    double val = (double) Integer.parseInt(donnees[i][j]);
                    donnees[i][j] = (double) Math.round(((double) ((val * 100) / totLigne)) * 1000) / 1000 + "";
                    if (Double.parseDouble(donnees[i][j]) == 0) {
                        donnees[i][j] = "0";
                    }

                }
            }
        }

        ModelImageProp model = new ModelImageProp(donnees, titre);
        table.setShowGrid(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setDefaultRenderer(Object.class, new CellRendererColor());
        table.setModel(model);
        model.fireTableDataChanged();
        table.getTableHeader().setDefaultRenderer(new CellRendererHeader());

   
        toComp.setText(image2.getName());
        String s = image1.getName();
        String s2 = "";
        for (int i = 0; i < s.length(); i++) {
            s2 += s.charAt(i) + "\n";
        }
        fromComp.setText(s2);
    }

    
    private class CellRendererHeader extends DefaultTableCellRenderer {

        private CellRendererHeader() {
            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            c.setBackground(Color.ORANGE);
            c.setForeground(Color.BLACK);
            return c;
        }
    }

    private class CellRendererColor extends JLabel implements TableCellRenderer {

        private JLabel comp = new JLabel();
        private String val = new String();
        private ArrayList<String> arr1;
        private ArrayList<String> arr2 ;
        
        public CellRendererColor(){
            arr1 = image1.listStateString();
            arr2 = image2.listStateString();
        }
       

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            comp.setOpaque(true);
            comp.setForeground(Color.BLACK);
            if (value != null) {
                val = value.toString();
                comp.setText(val);
            }
            comp.setHorizontalAlignment(SwingConstants.CENTER);
            comp.setForeground(Color.BLACK);
            comp.setBackground(Color.LIGHT_GRAY);
            if (row == 0 || column == 0) {
                comp.setBackground(Color.DARK_GRAY);
                comp.setForeground(Color.WHITE);
            }
            
            
            int rowState = 0 , columnState = 0 ;
            if(column != 0 && row != 0 ) {
                if (image1.isStateLayer()) {
                    rowState = states.getState(arr1.get(row - 1)).getValue();
                } else {
                    rowState = Integer.parseInt(arr1.get(row - 1));
                }
                if (image2.isStateLayer()) {
                    columnState = states.getState(arr2.get(column - 1)).getValue();
                } else {
                    columnState = Integer.parseInt(arr2.get(column - 1));
                }
            }
            
            
            //code couleur perte/gain
            if (row != 0 && column != 0) {
                if (rowState == columnState) {
                    int cpt = 0;
                    for (int i = 1; i < table.getColumnCount(); i++) {
                        if (!"0".equals((String) table.getValueAt(row, i)) && column != i) {
                            cpt++;
                        }
                        if(cpt > 0 ){
                            comp.setBackground(Color.BLUE);
                        }
                    }
                } else {
                    if (!"0".equals((String) table.getValueAt(row, column))) {
                        comp.setBackground(Color.RED);
                    }
                }
            }

            if (isSelected) {
                comp.setBackground(Color.ORANGE);
            }

            return comp;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        exportBut = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        toComp = new javax.swing.JLabel();
        fromComp = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setName("Form"); // NOI18N

        jPanel4.setName("jPanel4"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table.setName("table"); // NOI18N
        jScrollPane1.setViewportView(table);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(MatriceContingence.class);
        exportBut.setText(resourceMap.getString("exportBut.text")); // NOI18N
        exportBut.setName("exportBut"); // NOI18N
        exportBut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportButActionPerformed(evt);
            }
        });

        jPanel2.setBackground(resourceMap.getColor("jPanel2.background")); // NOI18N
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jPanel2.setName("jPanel2"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 14, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 14, Short.MAX_VALUE)
        );

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jPanel3.setBackground(resourceMap.getColor("jPanel3.background")); // NOI18N
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        jPanel3.setName("jPanel3"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 14, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 14, Short.MAX_VALUE)
        );

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        toComp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        toComp.setText(resourceMap.getString("toComp.text")); // NOI18N
        toComp.setName("toComp"); // NOI18N

        fromComp.setText(resourceMap.getString("fromComp.text")); // NOI18N
        fromComp.setName("fromComp"); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addGap(33, 33, 33)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 234, Short.MAX_VALUE)
                        .addComponent(exportBut)
                        .addGap(9, 9, 9))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(fromComp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 409, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(toComp, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(toComp)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(97, 97, 97)
                        .addComponent(fromComp)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exportBut)
                    .addComponent(jLabel2))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGap(0, 522, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 334, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void exportButActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportButActionPerformed

        File file = Util.getFileSave(".csv");
        if (file == null) {
            return;
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), '\t')) {
            IOFile.exportTable(table.getModel(), writer, true);
        } catch (IOException ex) {
            Logger.getLogger(MatriceContingence.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }//GEN-LAST:event_exportButActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton exportBut;
    private javax.swing.JLabel fromComp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table;
    private javax.swing.JLabel toComp;
    // End of variables declaration//GEN-END:variables
}
