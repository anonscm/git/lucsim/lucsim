/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.gui.stat;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.thema.lucsim.engine.StateLayer;

/**
 * Table model for state layer.
 * Shows the number of cells and the percentage of each state in the layer.
 * 
 * @author Gilles Vuidel
 */
public class StateLayerTableModel extends AbstractTableModel {

    private StateLayer layer;
    private List<String> nameStates;
    private int[] nbCell;
    private int total;

    public StateLayerTableModel(StateLayer layer) {
        this.layer = layer;
        nbCell = layer.listeNbCell();
        nameStates = layer.listStateString();
        total = 0;
        for(int nb : nbCell) {
            total += nb;
        }
    }

    @Override
    public int getRowCount() {
        return 2;
    }

    @Override
    public int getColumnCount() {
        return nameStates.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int item) {
        int nb = nbCell[layer.getStates().getState(nameStates.get(item)).getValue()];
        if(rowIndex == 0) {
            return nb;
        } else {
            return String.format("%g%%", 100 * (double)nb / total);
        }
    }

    @Override
    public String getColumnName(int column) {
        return nameStates.get(column);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
