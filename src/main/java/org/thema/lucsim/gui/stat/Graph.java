/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.lucsim.gui.stat;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.UnknownStateException;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.DefaultXYDataset;
import org.thema.common.Util;
import org.thema.lucsim.engine.NumLayer;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.SimLayer;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.engine.States;

/**
 * Fenetre des Graphiques de simulation
 *
 * @author alexis
 */
public class Graph extends javax.swing.JFrame implements ActionListener {

    private Project project;
    private List<int[]> tabList;         //matrice de souvegarde des données pour creer les graphs
    private Layer layer;

    /**
     * Creates new form Graph for the given layer
     * @param project the current project
     * @param layer the layer to display graph
     */
    public Graph(Project project, Layer layer) {
        this.layer = layer;
        this.project = project;                             //recupere simulation
        initComponents();                        //composant graphique, fenetre, bouton, panel
        layerNameLabel.setText(layer.getName());
        setTitle("Graph " + layer.getName());
        
        init();
    }
    /**
     * Creates new form Graph for the simulation layer
     * @param project the current project
     */
    public Graph(Project project) {
        this.project = project;  
        this.layer = project.getSimulation().getSimLayer();
        
        setTitle("Simulation graph");
        initComponents();                        //composant graphique, fenetre, bouton, panel
        layerNameLabel.setText("Simulation");
        tabList = new ArrayList<>();                             //matrice des souvegardes du nombre de cellule par etat par etape de simulation
        int[] nbCell = ((SimLayer)layer).listeNbCell();
        tabList.add(Arrays.copyOf(nbCell, nbCell.length));
        init();
        setAlwaysOnTop(true);
    }
    
    private void init() {
        curveRadioButton.addActionListener(this);              //graphique evolution
        pieRadioButon.addActionListener(this);              //graphique camembert
        histoRadioButton.addActionListener(this);              //graphique barre
        save.addActionListener(this);            // bouton enregistrer    
        color1.addActionListener(this);          //bouton radio coucleur des etats
        color2.addActionListener(this);          //bouton radio couleur prédéfenies
        bins.addActionListener(this);            //nombre de barre d'un histogramme 

        updateButton(layer);
        updatePanel();
    }

    /**
     * Focntion de creation d'une serie (courbe) pour un etat suivant le tableau
     * d'enregistrement
     *
     * @param etat (pour chaque etat on a une serie)
     * @return matrice de la serie pour le graphe evolution
     */
    private double[][] createSeries(int etat) {
        double[][] series;
        series = new double[2][tabList.size()];
        for (int i = 0; i < tabList.size(); i++) { //pour toutes les étapes deja éffectuées
            series[0][i] = i;//ordonnée (nombre de cellule par état) 
            series[1][i] = tabList.get(i)[etat]; //abscisse (etape simu)                          
        }

        return series;
    }

    /**
     * met les bouton "grapqieu" "camembert" et "barre" on ou off, pour la
     * couche statique
     *
     * @param b
     */
    private void updateButton(Layer l) {
        curveRadioButton.setVisible(l instanceof SimLayer);
        pieRadioButon.setVisible(l instanceof StateLayer);
        histoRadioButton.setVisible(l instanceof StateLayer);
        binsTxt.setVisible(l instanceof NumLayer);
        bins.setVisible(l instanceof NumLayer);
        stepLabel.setVisible((l instanceof SimLayer) && !curveRadioButton.isSelected());
        stepSpinner.setVisible((l instanceof SimLayer) && !curveRadioButton.isSelected());
        
        if(curveRadioButton.isSelected() && !curveRadioButton.isVisible() && !(l instanceof NumLayer)) {
            pieRadioButon.doClick();
        }
    }

    /**
     * fonction permettant de repaindre la zone (panel) du graphique en
     * reclickant sur le bouton radio correpondant, donc de recreer le graph.
     * fonction utiliser lors de la simulation
     */
    public void updatePanel() {
        if(curveRadioButton.isVisible() && curveRadioButton.isSelected()){
            graphEvolution();
        } else if (pieRadioButon.isVisible() && pieRadioButon.isSelected()) {
            graphPie();
        } else if (histoRadioButton.isVisible() && histoRadioButton.isSelected()) {
            grapheHist();
        } else {
            graphStatique();
        }   
    }
    
    public void addStepSim(int [] nbCell) {
        tabList.add(Arrays.copyOf(nbCell, nbCell.length));
        updatePanel();
    }
    
    public void resetSim(SimLayer l) {
        tabList.clear();
        tabList.add(Arrays.copyOf(l.listeNbCell(), l.listeNbCell().length));
        updatePanel();
    }

    /**
     * Fonction qui attend les evenement clickable (différent graphique, bouton
     * radio liste déroulante, et bouton enregistrement)
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == curveRadioButton) {
            graphEvolution();
        } else if (e.getSource() == pieRadioButon) {
            graphPie();
        } else if (e.getSource() == histoRadioButton) {
            grapheHist();
        } //bouton radio couche actuel ou couche Ini -> on repaint le panneau
        else if (e.getSource() == color1 || e.getSource() == color2 || e.getSource() == bins) {
            updatePanel();
        } //bouton enregistrer -> souvegarde en png le panel qui contient le graphique
        else if (e.getSource() == save) {
            save();
        }
    }

    /**
     * Graphique d'evolution
     */
    private void graphEvolution() {

        DefaultXYDataset dataset = new DefaultXYDataset();
        XYItemRenderer render = new StandardXYItemRenderer();

        int compteSerie = 0;
        for (State state : project.getStates().getStates()) {
            if(state.equals(States.NODATA_STATE)) {
                continue;
            }
            if (color1.isSelected()) {
                render.setSeriesPaint(compteSerie, state.getColor());
                compteSerie++;
            }
            dataset.addSeries(state.getName(), createSeries(state.getValue()));
        }

        JFreeChart chart = ChartFactory.createXYLineChart("", "Simulation step", "Number of cells", dataset, PlotOrientation.VERTICAL, true, false, false);
        chart.getXYPlot().setRenderer(render);
        ChartPanel cPanel = new ChartPanel(chart);

        //creation tableau
        TableXYModel tableauGraph = new TableXYModel(dataset, project.getSimulation().getStep() + 2);
        JTable table = new JTable(tableauGraph);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane scrollpane = new JScrollPane(table);

        splitPane.setBottomComponent(scrollpane);
        splitPane.setTopComponent(cPanel);
    }

    /**
     * Graphque du camembert
     */
    private void graphPie() {

        final DefaultPieDataset pieDataset = new DefaultPieDataset();
        ArrayList<Integer> arr = new ArrayList<>();

        int compteSerie = 0;
        int[] nbCell;
        StateLayer l = (StateLayer) layer;
        if (l instanceof SimLayer) {
            nbCell = tabList.get((Integer)stepSpinner.getValue());
        } else {
            nbCell = l.listeNbCell();
        }
        
        for (State state : project.getStates().getStates()) {
            if(state.equals(States.NODATA_STATE)) {
                continue;
            }
            pieDataset.setValue(state.getName(), nbCell[state.getValue()]);
            compteSerie++;
            arr.add(state.getValue()); 
        }
        
        JFreeChart pieChart = ChartFactory.createPieChart("Number of cells per state", pieDataset, true, false, false);
        PiePlot piePlot = (PiePlot) pieChart.getPlot();

        if (color1.isSelected()) { //modif couleur
            for (int i = 0; i < compteSerie; i++) {
                try {
                    Color c = project.getStates().getState(arr.get(i)).getColor();
                    piePlot.setSectionPaint(i, c);

                } catch (UnknownStateException ex) {
                    Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        ChartPanel cPanel = new ChartPanel(pieChart);

        //creation tableau
        JTable table = new JTable(new StateLayerTableModel((StateLayer) layer));
        JScrollPane scrollpane = new JScrollPane(table);

        splitPane.setBottomComponent(scrollpane);
        splitPane.setTopComponent(cPanel);
        
    }

    /**
     * Graphique histogramme (barre)
     */
    private void grapheHist() {

        int compteSerie = 0;
        ArrayList<Integer> arr = new ArrayList<>();

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int[] nbCell;
        StateLayer l = (StateLayer) layer;
        if (l instanceof SimLayer) {
            nbCell = tabList.get((Integer)stepSpinner.getValue());
        } else {
            nbCell = l.listeNbCell();
        }
        for (State state : project.getStates().getStates()) {
            if(state.equals(States.NODATA_STATE)) {
                continue;
            }
            dataset.addValue(nbCell[state.getValue()], state.getName(), "");
            compteSerie++;
            arr.add(state.getValue()); 
        }

        JFreeChart barChart = ChartFactory.createBarChart("Number of cells per state", "State", "Number of cells", dataset, PlotOrientation.VERTICAL, true, true, false);
        BarRenderer r = (BarRenderer) barChart.getCategoryPlot().getRenderer();

        if (color1.isSelected()) { //modif couleur
            for (int i = 0; i < compteSerie; i++) {
                try {
                    Color c = project.getStates().getState(arr.get(i)).getColor();
                    r.setSeriesPaint(i, c);

                } catch (UnknownStateException ex) {
                    Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        ChartPanel cPanel = new ChartPanel(barChart);

        //creation tableau
        JTable table = new JTable(new StateLayerTableModel((StateLayer) layer));
        JScrollPane scrollpane = new JScrollPane(table);
        
        splitPane.setBottomComponent(scrollpane);
        splitPane.setTopComponent(cPanel);
        
    }

    /**
     * Histogramme pour les couches statiques
     */
    private void graphStatique() {

        int binsNb = Integer.parseInt(bins.getText());
        NumLayer layerS = (NumLayer) layer;
        
        double[] values = layerS.tabPoint();
        double max = values[0];
        double min = max;
        for (double val : values) {
            if (val > max) {
                max = val;
            }
            if (val < min) {
                min = val;
            }
        }

        int widthHist = (int) (max - min + 1);
        bins.setText(widthHist + "");

        HistogramDataset data = new HistogramDataset();
        if (binsNb != widthHist && binsNb > 0) {
            data.addSeries("Nb", values, binsNb);
            bins.setText(binsNb + "");
        } else {
            data.addSeries("Nb", values, widthHist);
        }

        JFreeChart chart = ChartFactory.createXYBarChart(null, null, false, null,
                data, PlotOrientation.VERTICAL, false, false, false);

        XYPlot plot = (XYPlot) chart.getPlot();
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, new Color(255, 0, 0, 128));
        ((NumberAxis) plot.getRangeAxis()).setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        ChartPanel cPanel = new ChartPanel(chart, 500, 500, 100, 100, 2000, 2000, true, true, true, true, true, true);
        splitPane.setBottomComponent(null);
        splitPane.setTopComponent(cPanel);
    }

    /**
     * Bouton sauvegarder
     */
    private void save() {
        File file = Util.getFileSave(".png|.csv");
        if(file == null) {
            return;
        }

        Component comp = splitPane.getTopComponent();
        BufferedImage img = new BufferedImage(comp.getWidth(), comp.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = img.createGraphics();
        comp.paint(g2);
        try {
            ImageIO.write(img, "png", file);
        } catch (IOException e) {

        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        curveRadioButton = new javax.swing.JRadioButton();
        pieRadioButon = new javax.swing.JRadioButton();
        histoRadioButton = new javax.swing.JRadioButton();
        bins = new javax.swing.JTextField();
        binsTxt = new javax.swing.JLabel();
        stepSpinner = new javax.swing.JSpinner();
        stepLabel = new javax.swing.JLabel();
        save = new javax.swing.JButton();
        color1 = new javax.swing.JRadioButton();
        color2 = new javax.swing.JRadioButton();
        splitPane = new javax.swing.JSplitPane();
        layerNameLabel = new javax.swing.JLabel();

        jPanel2.setName("jPanel2"); // NOI18N

        buttonGroup1.add(curveRadioButton);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(Graph.class);
        curveRadioButton.setText(resourceMap.getString("curveRadioButton.text")); // NOI18N
        curveRadioButton.setName("curveRadioButton"); // NOI18N

        buttonGroup1.add(pieRadioButon);
        pieRadioButon.setSelected(true);
        pieRadioButon.setText(resourceMap.getString("pieRadioButon.text")); // NOI18N
        pieRadioButon.setName("pieRadioButon"); // NOI18N

        buttonGroup1.add(histoRadioButton);
        histoRadioButton.setText(resourceMap.getString("histoRadioButton.text")); // NOI18N
        histoRadioButton.setName("histoRadioButton"); // NOI18N

        bins.setText(resourceMap.getString("bins.text")); // NOI18N
        bins.setName("bins"); // NOI18N

        binsTxt.setText(resourceMap.getString("binsTxt.text")); // NOI18N
        binsTxt.setName("binsTxt"); // NOI18N

        stepSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(0), null, Integer.valueOf(1)));
        stepSpinner.setName("stepSpinner"); // NOI18N
        stepSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                stepSpinnerStateChanged(evt);
            }
        });

        stepLabel.setText(resourceMap.getString("stepLabel.text")); // NOI18N
        stepLabel.setName("stepLabel"); // NOI18N

        save.setIcon(resourceMap.getIcon("save.icon")); // NOI18N
        save.setText(resourceMap.getString("save.text")); // NOI18N
        save.setName("save"); // NOI18N

        buttonGroup4.add(color1);
        color1.setSelected(true);
        color1.setText(resourceMap.getString("color1.text")); // NOI18N
        color1.setName("color1"); // NOI18N

        buttonGroup4.add(color2);
        color2.setText(resourceMap.getString("color2.text")); // NOI18N
        color2.setName("color2"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(pieRadioButon)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(histoRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE))
                        .addGap(36, 36, 36))
                    .addComponent(save, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(curveRadioButton)
                            .addComponent(color1)
                            .addComponent(color2))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(stepLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stepSpinner))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(binsTxt)
                        .addGap(15, 15, 15)
                        .addComponent(bins)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(curveRadioButton)
                .addGap(8, 8, 8)
                .addComponent(pieRadioButon)
                .addGap(8, 8, 8)
                .addComponent(histoRadioButton)
                .addGap(20, 20, 20)
                .addComponent(color1)
                .addGap(5, 5, 5)
                .addComponent(color2)
                .addGap(33, 33, 33)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(binsTxt)
                    .addComponent(bins, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stepSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stepLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(save, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        splitPane.setDividerLocation(300);
        splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitPane.setResizeWeight(0.8);
        splitPane.setName("splitPane"); // NOI18N

        layerNameLabel.setFont(resourceMap.getFont("layerNameLabel.font")); // NOI18N
        layerNameLabel.setText(resourceMap.getString("layerNameLabel.text")); // NOI18N
        layerNameLabel.setName("layerNameLabel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(layerNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(layerNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(splitPane)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void stepSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_stepSpinnerStateChanged
    updatePanel();
}//GEN-LAST:event_stepSpinnerStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField bins;
    private javax.swing.JLabel binsTxt;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JRadioButton color1;
    private javax.swing.JRadioButton color2;
    private javax.swing.JRadioButton curveRadioButton;
    private javax.swing.JRadioButton histoRadioButton;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel layerNameLabel;
    private javax.swing.JRadioButton pieRadioButon;
    private javax.swing.JButton save;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JLabel stepLabel;
    private javax.swing.JSpinner stepSpinner;
    // End of variables declaration//GEN-END:variables
}
