/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.generator.dt;

import java.util.List;
import org.thema.common.ProgressBar;
import org.thema.lucsim.stat.DataExtractor;
import weka.classifiers.trees.HoeffdingTree;
import weka.core.Instance;

/**
 * Gathers the instructions to run to build a tree with the specified
 * parameters, display it and extract its rules.
 */
public class IncrementalClassifierRunnable extends TreeBinaryClassifier {

    private String params;
    private HoeffdingTree hoeffdingTree;
    

    /**
     * Constructs a TreeBinaryClassifier with the two Layer and the different
 parameters chosen by the user within a DecisionTreeDialog.
     * @param dataExtractor 
     * @param paramList list of parameters to build the tree
     * @param progressBar progress bar to update in order to inform the user of
     * the task's progress
     */
    public IncrementalClassifierRunnable(DataExtractor dataExtractor, List<Double> paramList, ProgressBar progressBar) {
        super(dataExtractor, progressBar);
        // if the user chose parameters differing from the default ones,
        // then these are applied
        if (!paramList.isEmpty()) {
            params 
                    = "-L "
                    + (int) Math.round(paramList.get(0))
                    + " -S "
                    + (int) Math.round(paramList.get(1))
                    + " -E "
                    + paramList.get(2)
                    + " -H "
                    + paramList.get(3)
                    + " -M "
                    + paramList.get(4)
                    + " -G "
                    + paramList.get(5)
                    + " -N "
                    + paramList.get(6) 
                    + " -B";
        } else {
            params = null;
        }
    }

    /**
     * Constructs a TreeBinaryClassifier with the two Layer and the different
 parameters chosen by the user within a DecisionTreeDialog.
     * @param dataExtractor 
     * @param params string of parameters to build the tree
     * @param progressBar progress bar to update in order to inform the user of
     * the task's progress
     */
    public IncrementalClassifierRunnable(DataExtractor dataExtractor, String params, ProgressBar progressBar) {
        super(dataExtractor, progressBar);
        this.params = params;
    }
    
    @Override
    public void run() throws Exception {

        hoeffdingTree = new HoeffdingTree();

        progressBar.setMinimum(0);

        if(params != null && !params.trim().isEmpty()) {
            hoeffdingTree.setOptions(weka.core.Utils.splitOptions(params));
        }
        // building the tree to match the dataset layout
        hoeffdingTree.buildClassifier(dataExtractor.getEmptyDataset());

        progressBar.setNote("Building the dataset and training the tree...");
        progressBar.setIndeterminate(true);

        dataExtractor.reset();
        while(dataExtractor.hasNext()) {
            Instance instance = dataExtractor.next();
            hoeffdingTree.updateClassifier(instance);
        }

        treeStr = hoeffdingTree.toString(); 
        treeGraph = hoeffdingTree.graph();

        progressBar.setNote("Processing the post-data...");
        
        dataExtractor.reset();
        confusionMatrixFromTrainingData = buildConfusionMatrixFromData(hoeffdingTree, dataExtractor);
        
        progressBar.setIndeterminate(false);
        progressBar.setNote("Done.");
    }

}
