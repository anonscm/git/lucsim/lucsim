/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.generator.dt;
 
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.thema.lucsim.engine.Condition;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.Rule;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.parser.AnalyzerConstants;
import org.thema.lucsim.parser.Token;
import weka.classifiers.trees.HoeffdingTree;
import weka.gui.treevisualizer.Edge;
import weka.gui.treevisualizer.Node;
import weka.gui.treevisualizer.TreeBuild;
 
/**
 * Represents the structure that, starting from a tree, can extract its
 * rules.
 */
public class RulesGeneratorDT {
    
    public static class SubCondition {
        private String function;
        private State state;
        private int radius;
        private String comp;
        private double nb;

        public SubCondition(String function, State state, int radius, String comp, double nb) {
            this.function = function;
            this.state = state;
            this.radius = radius;
            this.comp = comp;
            this.nb = nb;
            if(!">".equals(comp) && !"<=".equals(comp)) {
                throw new IllegalArgumentException("Bad comparator : " + comp);
            }
        }
        
        public SubCondition(String layer, String comp, double nb) {
            this(layer, null, 0, comp, nb);
        }

        public String getFunction() {
            return function;
        }

        public State getState() {
            return state;
        }

        public int getRadius() {
            return radius;
        }

        public String getComp() {
            return comp;
        }

        public double getNb() {
            return nb;
        }

        public boolean compIsGreater() {
            return comp.equals(">");
        }
        
        public boolean isLayerCond() {
            return state == null;
        }
        
        public boolean hasSameLeftPart(SubCondition other) {
            return Objects.equals(this.function, other.function) && 
                    Objects.equals(this.state, other.state) &&
                    this.radius == other.radius &&
                    Objects.equals(this.comp, other.comp);
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            
            final SubCondition other = (SubCondition) obj;
            return Objects.equals(this.function, other.function) && 
                    Objects.equals(this.state, other.state) &&
                    this.radius == other.radius &&
                    Objects.equals(this.comp, other.comp) &&
                    Double.doubleToLongBits(this.nb) == Double.doubleToLongBits(other.nb);
        }

        @Override
        public String toString() {
            if(isLayerCond()) {
                return function + " " + comp + " " + nb;
            } else {
                return function + "(" + state + ", " + radius + ") " + comp + " " + nb;
            }
        }
        
        
    }
    
    public class ClassifyRule implements Comparable<ClassifyRule>{
        private List<SubCondition> conditions;
        private State initState;
        private int tp, fp;
        
        private Rule rule;
        
        public ClassifyRule(List<SubCondition> conditions, State initState, int tp, int fp) {
            this.conditions = new ArrayList<>(conditions);
            this.initState = initState;
            this.tp = tp;
            this.fp = fp;
            removeUselessConditions();
            rule = generateRule();
        }

        public List<SubCondition> getConditions() {
            return conditions;
        }

        public State getInitState() {
            return initState;
        }

        public Rule getRule() {
            return rule;
        }

        public int getTp() {
            return tp;
        }

        public int getFp() {
            return fp;
        }
        
        private void removeUselessConditions() {
            // remove conditions with same left part
            List<SubCondition> keepCond = new ArrayList<>();
            while(!conditions.isEmpty()) {
                SubCondition cond = conditions.remove(0);
                List<SubCondition> sameCond = new ArrayList<>();
                for(SubCondition c : conditions) {
                    if(cond.hasSameLeftPart(c)) {
                        sameCond.add(c);
                    }
                }
                for(SubCondition c : sameCond) {
                    if(cond.compIsGreater() && c.nb > cond.nb) {
                        cond = c;
                    }
                    if(!cond.compIsGreater() && c.nb < cond.nb) {
                        cond = c;
                    }
                }
                conditions.removeAll(sameCond);
                keepCond.add(cond);
            }
            conditions = keepCond;
            
            // remove conditions with same state, comparator and where radius is lower but n greater
            for(SubCondition c1 : new ArrayList<>(conditions)) {
                for(SubCondition c2 : new ArrayList<>(conditions)) {
                    // is same state and same comparator ?
                    if(c1 != c2 && c1.getState() != null && c1.getState().equals(c2.getState()) && c1.getComp().equals(c2.getComp())) {
                        // is radius lower but n greater
                        if(c1.getRadius() < c2.getRadius() && c1.getNb() >= c2.getNb()) {
                            if(c1.compIsGreater()) {
                                conditions.remove(c2);
                            } else {
                                conditions.remove(c1);
                            }
                        }
                    }
                }
            }
        }
        
        public int getNbConditions() {
            int nb = rule.isAllInit() ? 1 : 2;
            for(Token token : rule.getCondition().getTokenPost()) {
                if(token.kind == AnalyzerConstants.AND) {
                    nb++;
                }
            }
            return nb;
        }
        
        public double getScore() {
            return tp / (double)(tp+fp);
        }

        @Override
        public int compareTo(ClassifyRule r) {
            if(r.getScore() == getScore()) {
                return Integer.compare(r.tp, tp);
            } 
            return Double.compare(r.getScore(), getScore());
        }

        private Rule generateRule() {
            List<Token> tokens = new ArrayList<>();
            for(SubCondition c : conditions) {
                if(c.isLayerCond()) {
                    tokens.add(new Token(AnalyzerConstants.NAME, c.function));
                } else {
                    switch (c.function) {
                        case "nbCellSq":
                            tokens.add(new Token(AnalyzerConstants.NBCELLSQ, "nbCellSq"));
                            break;
                        case "nbCellCir":
                            tokens.add(new Token(AnalyzerConstants.NBCELLCIR, "nbCellCir"));
                            break;
                        default:
                            throw new IllegalArgumentException("Invalid function name: " + c.function);
                    }
 
                    tokens.add(new Token(AnalyzerConstants.NAME, c.state.getName()));
                    tokens.add(new Token(AnalyzerConstants.INTEGER, "" + c.radius));
                }
                if (c.compIsGreater()) {
                    tokens.add(new Token(AnalyzerConstants.GREAT, ">"));
                } else {
                    tokens.add(new Token(AnalyzerConstants.LEQ, "<="));
                }
                tokens.add(new Token(AnalyzerConstants.FLOAT, "" + c.nb));
                
                tokens.add(new Token(AnalyzerConstants.AND, "and"));
            }
            tokens.remove(tokens.size()-1);
            return new Rule(initState, targetState, new Condition(tokens));
        }
    }
 
    private final String treeGraph;
    private final StateLayer formerLayer;
    private final List<Layer> layers;
    private final State targetState;
    private final String function;
    private final double weight;
    private List<ClassifyRule> rules;
 
    /**
     * Constructs an object able to extract rules from a tree given as a DOT
     * string.
     * @param treeGraph DOT-formatted String representing the tree
     * @param formerLayer former Layer
     * @param layers list potentially empty of the additionnal layers
     * @param targetState State targeted
     * @param function String representing the function chosen by the user
     */
    public RulesGeneratorDT(String treeGraph, StateLayer formerLayer, List<Layer> layers, State targetState, String function, double weight) {
        this.treeGraph = treeGraph;
        this.formerLayer = formerLayer;
        this.layers = layers;
        this.targetState = targetState;
        this.function = function;
        this.weight = weight;
        rules = new ArrayList<>();
    }
 
    /**
     * Method used to extract rules from a tree, knowing the State chosen as the
     * target state by the user.
     * @see HoeffdingTree
     */
    public void extractRulesFromTree() {
        StringReader readerTree = new StringReader(treeGraph);
        TreeBuild treeBuild = new TreeBuild();
        Node root = treeBuild.create(readerTree);

        rules = new ArrayList<>();
        generateRules(root, new LinkedList<>(), null, rules);
    }
    
    // just for testing...
    private int nbCond;
    public String getFactorRule() {
        StringReader readerTree = new StringReader(treeGraph);
        TreeBuild treeBuild = new TreeBuild();
        Node root = treeBuild.create(readerTree);
        nbCond = 0;
        String rule = genFactorRule(root);
        System.out.println("Nb factor cond : " + nbCond);
        return "all -> " + targetState.getName() + " : " + rule + ";";
    }
     
    private String genFactorRule(Node node) {
        if(isLeaf(node)) {
            if (node.getLabel().startsWith("true")) {
                return "1";
            } else {
                return "0";
            }
        } else {
            int indChild = 0;
            String rule = "";
            while (node.getChild(indChild) != null) {
                Edge edge = node.getChild(indChild);
                String cond;
                if (node.getLabel().equals(formerLayer.getName())) {
                    String stateName = edge.getLabel().substring(2, edge.getLabel().length());
                    cond = formerLayer.getName() + "=" + stateName;
                } else {
                    cond = getConditionFromNode(node.getLabel(), edge.getLabel(), function).toString();
                }
                
                String subCond = genFactorRule(edge.getTarget());
                
                if(subCond.equals("0")) {
                    indChild++;
                    continue;
                } else if(subCond.equals("1")) {
                    rule += " or " + cond;
                    nbCond++;
                } else {
                    rule += " or (" + cond + " and (" + subCond + "))";
                    nbCond++;
                }
                
                indChild++;
            }
            return rule.isEmpty() ? "0" : rule.substring(4);
        }
    }
    
    private void generateRules(Node node, List<SubCondition> conditions, State initState, List<ClassifyRule> rules) {
        if(isLeaf(node)) {
            // if the leaf is labeled "true"
            if (node.getLabel().startsWith("true")) {
                int tp, fp = 0;
                if (node.getLabel().contains("/")) {
                    String label = node.getLabel().substring(6, node.getLabel().length() - 1);
                    String[] parts = label.split("/");
                    double truePositives = Double.valueOf(parts[0]);
                    fp = Double.valueOf(parts[1]).intValue();
                    tp = (int) Math.round((truePositives - fp) / weight);
                } else {
                    String label = node.getLabel().substring(6, node.getLabel().length() - 1);
                    tp = (int) Math.round(Double.valueOf(label) / weight);
                }
                if(tp > 0) {
                    rules.add(new ClassifyRule(conditions, initState, tp, fp));
                }
            } 
        } else { // threre is a split
            int indChild = 0;
            while (node.getChild(indChild) != null) {
                Edge edge = node.getChild(indChild);
                if (node.getLabel().equals(formerLayer.getName())) {
                    String stateName = edge.getLabel().substring(2, edge.getLabel().length());
                    if(!stateName.equals(targetState.getName())) {
                        generateRules(edge.getTarget(), conditions, formerLayer.getStates().getState(stateName), rules);
                    }
                } else {
                    SubCondition cond = getConditionFromNode(node.getLabel(), edge.getLabel(), function);
                    conditions.add(cond);
                    generateRules(edge.getTarget(), conditions, initState, rules);
                    conditions.remove(conditions.size()-1);
                }
                indChild++;
            }
        }
    }
    
 
    /**
     * Method checking if a given Node is a leaf.
     *
     * @param node Node to check
     * @return whether the Node is a leaf
     */
    private boolean isLeaf(Node node) {
        return (node.getChild(0) == null);
    }
 
    private SubCondition getConditionFromNode(String nodeLabel, String edgeLabel, String function) {
        // the following is common to both previous splits
        // an Edge label is formatted like this: "<comparator> <float>"
        String comp = "";
        if (edgeLabel.contains("<=")) {
            comp = "<=";
        } else if (edgeLabel.contains(">")) {
            comp = ">";
        }
 
        // fetch the <float>
        double nb = Double.valueOf(edgeLabel.substring(comp.length() + 1, edgeLabel.length()).replace(",", "."));

        // if the split happened on a numerical layer
        // we have to set the name of the former layer
        if (isLabelANumLayerName(nodeLabel)) {
            return new SubCondition(nodeLabel, comp, nb);
        }
        // else, if the split happened on classic neighborhood data
        // we have to set the function and its parameters
        else {
            // a Node label is formatted like this: "<function>_<statename>_<distance>px"
            String[] parts = nodeLabel.split("_");
 
            // here, only the Node "name" is needed (the state to look at)
            State state = formerLayer.getStates().getState(parts[1]);
 
            // there, only the "distance" is needed
            // thus, the "px" is removed (the size of the neighborhood)
            int radius = Integer.parseInt(parts[2].substring(0, parts[2].length() - 2));
            
            return new SubCondition(function, state, radius, comp, nb);
        }
        
    }
    
    /**
     * Method checking whether a String is an additional layer name.
     * @param toCheck String to check
     * @return whether the String to search is an additional layer name
     */
    private boolean isLabelANumLayerName(String toCheck) {
        return layers.stream().anyMatch(layer -> layer.getName().equals(toCheck));
    }
 
    /**
     * Getter for the class' attribute rules. This attribute is a List of the
     * different rules created during the process.
     *
     * @return class' attribute rules
     */
    public List<ClassifyRule> getRules() {
        return rules;
    }


}