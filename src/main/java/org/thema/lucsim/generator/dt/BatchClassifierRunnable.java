/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.generator.dt;

import java.util.List;
import java.util.Random;
import org.thema.common.ProgressBar;
import org.thema.lucsim.gui.generator.dt.DecisionTreeDialog;
import org.thema.lucsim.stat.DataExtractor;
import weka.classifiers.trees.J48;
import weka.core.Instances;

/**
 * Gathers the instructions to run to build a tree with the specified
 * parameters, display it and extract its rules.
 */
public class BatchClassifierRunnable extends TreeBinaryClassifier {

    private final double trainingDatasetRatio;
    private String params;

    private final int seed;
    private J48 j48Tree;
    private Instances completeDataset;
    
    /**
     * Constructs a TreeBinaryClassifier with two layers and the different
 parameters chosen by the user within a DecisionTreeDialog.
     * @param dataExtractor 
     * @param trainingDatasetRatio percentage of the dataset to use as a 
     * training set (1-99)
     * @param paramList list of parameters to build the tree
     * @param progressBar progress bar to update in order to inform the user of
     * the task's progress
     * @param seed random generator seed
     * @see DecisionTreeDialog
     */
    public BatchClassifierRunnable(DataExtractor dataExtractor, double trainingDatasetRatio, 
            List<Double> paramList, ProgressBar progressBar, int seed) {
        super(dataExtractor, progressBar);
        this.trainingDatasetRatio = trainingDatasetRatio;
        this.seed = seed;
        
        if (!paramList.isEmpty()) {
            params = " -M "
                    + (int) Math.round(paramList.get(2));

            if (paramList.get(0) == 1) {
                params += " -U ";
            } else if (paramList.get(3) == 1) {
                params += " -R -N " + (int) Math.round(paramList.get(4));
            } else {
                params += " -C " + paramList.get(1);
            }

            if (paramList.get(5) == 1) {
                params += " -B ";
            }
        } else {
            params = null;
        }
    }
    
    /**
     * Constructs a TreeBinaryClassifier with two layers and the different
 parameters chosen by the user within a DecisionTreeDialog.
     * @param dataExtractor 
     * @param trainingDatasetRatio percentage of the dataset to use as a 
     * training set (1-99)
     * @param params String of parameters to build the tree
     * @param progressBar progress bar to update in order to inform the user of
     * the task's progress
     * @param seed random generator seed
     * @see DecisionTreeDialog
     */
    public BatchClassifierRunnable(DataExtractor dataExtractor, double trainingDatasetRatio, 
            String params, ProgressBar progressBar, int seed) {
        super(dataExtractor, progressBar);
        this.trainingDatasetRatio = trainingDatasetRatio;
        this.seed = seed;
        this.params = params;
    }

    @Override
    public void run() throws Exception {

        j48Tree = new J48();
        j48Tree.setSeed(seed);

        if(params != null && !params.trim().isEmpty()) {
            j48Tree.setOptions(weka.core.Utils.splitOptions(params));
        }

        progressBar.setMinimum(0);

        // building the training dataset, which will gather every instance
        progressBar.setNote("Building the dataset...");
        progressBar.setIndeterminate(true);
        completeDataset = dataExtractor.getFullDataset();
        
        completeDataset.randomize(new Random(seed));

        int trainSize = (int) Math.round(trainingDatasetRatio * completeDataset.numInstances());
        int testSize = completeDataset.numInstances() - trainSize;
        Instances trainDataset = new Instances(completeDataset, 0, trainSize);
        Instances testDataset = null;
        if(testSize > 0) {
            testDataset = new Instances(completeDataset, trainSize, testSize); 
        }
        
        progressBar.setNote("Training...");
        j48Tree.buildClassifier(trainDataset);

        treeStr = j48Tree.toString();
        treeGraph = j48Tree.graph();

        progressBar.setNote("Processing the post-data...");

        confusionMatrixFromTrainingData = buildConfusionMatrixFromData(j48Tree, trainDataset.iterator());
        if(testDataset != null) {
            confusionMatrixFromTestData = buildConfusionMatrixFromData(j48Tree, testDataset.iterator());
        }

        progressBar.setIndeterminate(false);
        progressBar.setNote("Done.");
    }

}
