/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.generator.dt;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import org.thema.common.ProgressBar;
import org.thema.lucsim.stat.DataExtractor;
import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

/**
 * Abstract class representing the different steps towards the creation of a 
 * classifier.
 * @see Runnable
 */
public abstract class TreeBinaryClassifier {

    protected final DataExtractor dataExtractor;
    protected final ProgressBar progressBar;
    
    // results
    protected String treeGraph, treeStr;
    protected Map<String, Integer> confusionMatrixFromTrainingData;
    protected Map<String, Integer> confusionMatrixFromTestData;
    
    private RulesGeneratorDT rulesGenerator;

    public TreeBinaryClassifier(DataExtractor dataExtractor, ProgressBar progressBar) {
        this.dataExtractor = dataExtractor;
        this.progressBar = progressBar;
    }
    
    public abstract void run() throws Exception;

    public RulesGeneratorDT getRulesGenerator() {
        if(rulesGenerator == null) {
            // extracting the rules from the tree
            rulesGenerator = new RulesGeneratorDT(treeGraph, dataExtractor.getFormerLayer(), dataExtractor.getLayers(), 
                    dataExtractor.getTargetState(), dataExtractor.getFunction(), dataExtractor.getWeight());
            rulesGenerator.extractRulesFromTree();
        }
        return rulesGenerator;
    }

    /**
     * Method building a frame containing a rendered version of a DOT-formatted
     * string.
     * @return the visible frame 
     */
    public JFrame renderTree() {
        JFrame frm = new JFrame("Rendered tree");
        frm.getContentPane().setLayout(new BorderLayout());
        TreeVisualizer treeVisualizer = new TreeVisualizer(null, treeGraph, new PlaceNode2());
        frm.getContentPane().add(treeVisualizer, BorderLayout.CENTER);
        frm.setVisible(true);
        treeVisualizer.fitToScreen();
        frm.setExtendedState(JFrame.MAXIMIZED_BOTH);
        
        return frm;
    }
    
    /**
     * Returns a text representation of the built tree.
     * @return text representation of the built tree
     */
    public String getTreeStr() {
        return treeStr;
    }
    
    /**
     * Builds a confusion matrix for a given test dataset, by classifying each 
     * instance of this dataset with the model.
     * @param classifier 
     * @param data testing dataset
     * @return confusion matrix of the given dataset classified by the model
     */
    protected Map<String, Integer> buildConfusionMatrixFromData(Classifier classifier, Iterator<Instance> data) throws Exception {
        int truePositives = 0;
        int trueNegatives = 0;
        int falsePositives = 0;
        int falseNegatives = 0;

        while(data.hasNext()) {
            Instance instance = data.next();
            double predicate = classifier.classifyInstance(instance);
            if (instance.classValue() == 1 && predicate == 1) {
                truePositives++;
            } else if (instance.classValue() == 0 && predicate == 0) {
                trueNegatives++;
            } else if (instance.classValue() == 0 && predicate == 1) {
                falsePositives++;
            } else if (instance.classValue() == 1 && predicate == 0) {
                falseNegatives++;
            }
        }

        Map<String, Integer> matrix = new TreeMap<>();
        matrix.put("TP", truePositives);
        matrix.put("TN", trueNegatives);
        matrix.put("FP", falsePositives);
        matrix.put("FN", falseNegatives);

        return matrix;
    }
    
    /**
     * Getter on the built confusion matrix. 
     * @return confusion matrix
     */
    public Map<String, Integer> getConfusionMatrixFromTestData() {
        return confusionMatrixFromTestData;
    }
    
    /**
     * Getter on the confusion matrix.
     * @return confusion matrix
     */
    public Map<String, Integer> getConfusionMatrixFromTrainingData() {
        return confusionMatrixFromTrainingData;
    }
}
