/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.generator.dt;
 
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.thema.lucsim.engine.Condition;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.Rule;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.parser.AnalyzerConstants;
import org.thema.lucsim.parser.Token;
import weka.classifiers.trees.HoeffdingTree;
import weka.gui.treevisualizer.Edge;
import weka.gui.treevisualizer.Node;
import weka.gui.treevisualizer.TreeBuild;
 
/**
 * Represents the structure that, starting from a tree, can extract its
 * rules.
 */
public class FactorRulesGeneratorDT {
 
    private final String treeGraph;
    private final StateLayer formerLayer;
    private final List<Layer> layers;
    private final State targetState;
    private final boolean redRules;
    private final String function;
    private ArrayList<Rule> rules;
 
    /**
     * Constructs an object able to extract rules from a tree given as a DOT
     * string.
     * @param treeGraph DOT-formatted String representing the tree
     * @param formerLayer former Layer
     * @param layers list potentially empty of the additionnal layers
     * @param targetState State targeted
     * @param redRules whether to keep redundant rules
     * @param function String representing the function chosen by the user
     */
    public FactorRulesGeneratorDT(String treeGraph, StateLayer formerLayer, List<Layer> layers, State targetState, boolean redRules, String function) {
        this.treeGraph = treeGraph;
        this.formerLayer = formerLayer;
        this.layers = layers;
        this.targetState = targetState;
        this.redRules = redRules;
        this.function = function;
        rules = new ArrayList<>();
    }
 
    /**
     * Method used to extract rules from a tree, knowing the State chosen as the
     * target state by the user.
     * @see HoeffdingTree
     */
    public void extractRulesFromTree() {
        try (Reader readerTree = new StringReader(treeGraph)) {
            TreeBuild treeBuild = new TreeBuild();
            Node root = treeBuild.create(readerTree);
 
            Rule rule = generateAllInitStateRule(root, null);
            if (rule != null) {
                rules.add(rule);
            }
            generateDefinedInitStateRules(root).stream().forEach(r -> {
                rules.add(r);
            });
        }
        catch (IOException ex) {
            Logger.getLogger(FactorRulesGeneratorDT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    /**
     * Method building nothing or one factorized rule with an "all" init state 
     * from a root Node of a tree, if said tree contains at least one "true" 
     * labeled leaf (and at least one split on neighborhood data). 
     * A depth-first search is executed on the tree, ignoring the cases where a
     * split happens on the cell's state in the former layer. Each time a "true"
     * labeled leaf is met, an or condition (represented as a postfixed list of 
     * tokens) is added to the current list of tokens.
     * @param root root Node of the tree
     * @param newInitState state on the edge of the split leading to the subtree
     * the method was called on, null if called from the root
     * @return rule created from the tree whose root node is root, null if no 
     * rule could be created
     */
    private Rule generateAllInitStateRule(Node root, State newInitState) {
        // if the tree contains only one leaf or one split on the cell's state
        // in the former layer
        if ((isLeaf(root)) 
                || (root.getLabel().equals(formerLayer.getName()))
                || ((newInitState == null) && (!hasAtLeastOneTrueLeafWithoutFlSplit(root)))) {
            return null;
        }
 
        boolean firstCondition = true;
 
        Map<String, Boolean> visited = new TreeMap<>();
 
        // the stack used for the depth-first search
        Stack<Node> nodes = new Stack<>();
         
        // the main stack of lists of tokens
        Stack<List<Token>> tokens = new Stack<>();
         
        // the stack of lists of tokens representing an or condition which will
        // be added to the main stack then cleared
        Stack<List<Token>> orCondition = new Stack<>();
         
        Node currentNode, child;
 
        // the root node is pushed onto the stack and marked as visited
        nodes.push(root);
        visited.put(root.getRefer(), true);
 
        // while there is nodes that are not studied yet
        while (!nodes.isEmpty()) {
 
            // the node on the top of the stack is peeked
            currentNode = nodes.peek();
             
            child = null;
 
            String nodeLabel = currentNode.getLabel();
 
            // if the current node is a leaf
            if (isLeaf(currentNode)) {
                // if the split that led to this leaf is a split on 
                // neighborhood data
                int sameStateEdgeId;
                // if this node does not result from a split on the cell's state 
                // in the  former layer, or it does but the given root node is
                // the root of a split on the cell's state in the former layer 
                // subtree and this same state is met again in the same kind of 
                // split
                if ((!currentNode.getParent(0).getSource().getLabel().equals(formerLayer.getName()))
                        || ((newInitState != null)
                        && (currentNode.getParent(0).getSource().getLabel().equals(formerLayer.getName()))
                        && ((sameStateEdgeId = hasSameStateEdge(currentNode.getParent(0).getSource(), formerLayer, newInitState)) > -1))
                        && currentNode.getParent(0).getSource().getChild(sameStateEdgeId).getTarget().getRefer().equals(currentNode.getRefer())) {
                    // if the leaf is labeled "true"
                    if (nodeLabel.startsWith("true")) {
                        List<Token> list = orCondition.peek();
                        // if it is not the first condition (as the postfixed
                        // notation is used, an or condition is formatted as 
                        // follows: <cond1> <cond2> or)
                        if (!firstCondition) {
                            list.add(new Token(AnalyzerConstants.OR, "or"));
                        }
                        // each list forming the or condition is pushed to the 
                        // main token stack of lists
                        orCondition.stream().forEach(l -> tokens.push(l));
                        firstCondition = false;
                        // the or condition is cleared
                        orCondition.clear();
                    }
                    // else the leaf is labeled "false", then the condition that 
                    // led to it is removed
                    else {
                        if (!orCondition.isEmpty()) {
                            orCondition.pop();
                        }
                    }
                }
            } else {
            // if the current node is a node
                int leastChildId = 0;
                
                // the least index corresponding to an existing and non-visited
                // child is retrieved
                while ((currentNode.getChild(leastChildId) != null) 
                        && ((visited.get(currentNode.getChild(leastChildId).getTarget().getRefer()) != null) 
                        && ((visited.get(currentNode.getChild(leastChildId).getTarget().getRefer()))))) {
                    leastChildId++;
                }
                
                // if the previous while loop has found a valid index
                if (leastChildId <= getMaxChildrenIndex(currentNode)) {
                    child = currentNode.getChild(leastChildId).getTarget();
 
                    String edgeLabel = currentNode.getChild(leastChildId).getLabel();
 
                    // if the split of this node is a split on 
                    // neighborhood data
                    if (!nodeLabel.equals(formerLayer.getName())) {
                        // the tokens corresponding to the the labels of the
                        // node and edge are retrieved and converted to postfix
                        List<Token> list = Condition.convertInfixToPostfix(getTokensForLabels(nodeLabel, edgeLabel, function));
                        // if it is not the first condition (as the postfixed
                        // notation is used, an and condition is formatted as 
                        // follows: <cond1> <cond2> and)
                        if (!orCondition.isEmpty()) {
                            list.add(new Token(AnalyzerConstants.AND, "and"));
                        }
                        // this new part of Condition is pushed onto the stack
                        // of the or Condition
                        orCondition.push(list);
                    }
                }
            }
 
            // if the current node is not a leaf and has at least one 
            // non-visited child
            if (child != null) {
                // the child is marked as visited
                visited.put(child.getRefer(), true);
                // if the child is a split on neighborhood data, the subtree
                // having the child as a root node contains at least one true 
                // leaf and does not have as children a false leaf an a split on 
                // the cell's state in the former layer, then the child is added 
                // to the stack of nodes
                int sameStateEdgeId;
                if ((!child.getLabel().equals(formerLayer.getName()))
                        && (((newInitState == null) && (hasAtLeastOneTrueLeafWithoutFlSplit(child)) && (!oneFalseOneFLSplit(child)))
                        || ((newInitState != null) && (hasAtLeastOneTrueLeaf(child, newInitState))))) {
                    nodes.push(child);
                }
                // else if the method was called on a subtree with a defined 
                // init state, the child is a split on cell's state in the 
                // former layer and has a state equals to the given 
                // defined one, then this grand child is added to the stack of 
                // nodes
                else if ((newInitState != null) 
                        && (child.getLabel().equals(formerLayer.getName())) 
                        && (((sameStateEdgeId = hasSameStateEdge(child, formerLayer, newInitState)) > -1))) {
                    nodes.push(child.getChild(sameStateEdgeId).getTarget());
                }
                // else browsing it is not worth and thus it is not added to the 
                // stack of nodes, and the condition that led to it is removed
                else {
                    if (!orCondition.isEmpty()) {
                        orCondition.pop();
                    }
                }
            }
            // else the node has been used and is removed of the stack of nodes
            else {
                nodes.pop();
            }
        }
         
        // if or Conditions were added to the main stack of lists of tokens, 
        // then a rule is created from these tokens
        if (!tokens.isEmpty()) {
            return new Rule(null, targetState, new Condition(mergeLists(tokens), true));
        }
         
        return null;
    }
     
    /**
     * Checks if a given node (which is a split on the cell's state in the 
     * former layer) has an edge with a state equals to a given one.
     * @param node node of the split
     * @param layer layer containing the state
     * @param state state to check
     * @return index of the edge equals to the given state if it exists, -1 
     * otherwise
     */
    private int hasSameStateEdge(Node node, StateLayer layer, State state) {
        if (!node.getLabel().equals(layer.getName())) {
            return -1;
        }
        
        for (int i = 0; i <= getMaxChildrenIndex(node); i++) {
            String edgeLabel = node.getChild(i).getLabel();
            String stateName = edgeLabel.substring(2, edgeLabel.length());
            State edgeState = layer.getStates().getState(stateName);
            
            if (edgeState.getValue() == state.getValue()) {
                return i;
            }
        }
        
        return -1;
    }
    
    /**
     * Method building one rule (with a defined initial State) for each "true"
     * labeled leaf or subtree resulting from a split on the cell's state in the 
     * former layer.
     * A depth-first search is executed on the tree, ignoring the leaves not
     * resulting from a split on the cell's state in the former layer. A 
     * Condition buffer is updated at each step of the DFS to match the 
     * Condition leading to the current Node; if an interesting split is met:
     * <br>
     * - if it results in a "true" labeled leaf, the Condition buffer is copied
     *   to create a new rule with the retrieved initial state;<br>
     * - if it results in a subtree, a method returning a factorized Condition
     *   of this subtree is called, and a new rule is created with a Condition
     *   equals to the copied buffer joined to the subtree factorized Condition 
     * and the retrieved initial state.
     * @param root root Node of the tree
     * @return list of rules that were created, eventually empty
     */
    private List<Rule> generateDefinedInitStateRules(Node root) {
        
        List<Rule> newRules = new ArrayList<>();
        
        Map<String, Boolean> visited = new TreeMap<>();
 
        // the stack of lists of tokens representing a condition buffer, which
        // whill be updated
        Stack<List<Token>> conditionBuffer = new Stack<>();
 
        // the stack used for the depth-first search
        Stack<Node> nodes = new Stack<>();
         
        Node currentNode, child;
 
        // the root node is pushed onto the stack and marked as visited
        nodes.push(root);
        visited.put(root.getRefer(), true);
 
        // while there is nodes that are not studied yet
        while (!nodes.isEmpty()) {            
            int leastChildId = 0;
            
            // the node on the top of the stack is peeked
            currentNode = nodes.peek();
             
            child = null;
 
            String nodeLabel = currentNode.getLabel();
 
            // if the current node is a node
            if (!isLeaf(currentNode)) {
                // the least index corresponding to an existing and non-visited
                // child is retrieved
                while ((currentNode.getChild(leastChildId) != null) 
                        && ((visited.get(currentNode.getChild(leastChildId).getTarget().getRefer()) != null) 
                        && ((visited.get(currentNode.getChild(leastChildId).getTarget().getRefer()))))) {
                    leastChildId++;
                }
                 
                // if the previous while loop has found a valid index
                if (leastChildId <= getMaxChildrenIndex(currentNode)) {
                    child = currentNode.getChild(leastChildId).getTarget();
 
                    String edgeLabel = currentNode.getChild(leastChildId).getLabel();
 
                    // if the split of the current node is a split on the cell's
                    // state in the former layer
                    if (nodeLabel.equals(formerLayer.getName())) {
                        if (isRoot(currentNode) || (!isRoot(currentNode) && !isUnderAFLSplit(currentNode.getParent(0).getSource()))) {
                        // if the least child non-visited of this split is a 
                        // leaf
                        if (isLeaf(child)) {
                            // if it is a "true" labeled leaf
                            if (child.getLabel().startsWith("true")) {
                                // the state of the edge leading to this leaf
                                // is retrieved (the label is formatted as 
                                // follows: "= state0")
                                String stateName = edgeLabel.substring(2, edgeLabel.length());
                                State newInitState = formerLayer.getStates().getState(stateName);

                                // if the path that led to this node does not 
                                // pass by more than one state in splits on the 
                                // cell's state in the former value
                                if (newInitState.getValue() != targetState.getValue() || redRules) {
                                    // a new Conditon is created from the 
                                    // Condition buffer
                                    Condition newCondition = createConditionFromStack(conditionBuffer);
                                    // a new Rule is created with the retrieved 
                                    // state and the new Condition
                                    newRules.add(new Rule(newInitState, targetState, newCondition));
                                }
                            }
                        }
                        // else it is a node
                        else {
                                // the state of the edge leading to this leaf
                                // is retrieved (the label is formatted as 
                                // follows: "= state0")
                                String stateName = edgeLabel.substring(2, edgeLabel.length());
                                State newInitState = formerLayer.getStates().getState(stateName);

                                // if the subtree having this node as a root node 
                                // contains at least one true leaf
                                if (hasAtLeastOneTrueLeaf(child, newInitState)) {
                                    // if this new state is different from the 
                                    // target state or the user chose to keep
                                    // redundant rules
                                    if (newInitState.getValue() != targetState.getValue() || redRules) {
                                        // a new Conditon is created from the Condition
                                        // buffer
                                        Condition before = createConditionFromStack(conditionBuffer);
                                        Rule rule = generateAllInitStateRule(child, newInitState);
                                        Condition after = (rule == null ? null : new Condition(rule.getCondition()));
                                        if ((before != null) && (after == null)) {
                                            newRules.add(new Rule(newInitState, targetState, before));
                                        }
                                        else if ((before == null) && (after != null)) {
                                            newRules.add(new Rule(newInitState, targetState, after));
                                        } 
                                        else if ((before != null) && (after != null)) {
                                            before.addAndCondition(after);
                                            newRules.add(new Rule(newInitState, targetState, before));
                                        }
                                    }
                                }
                        }
                    }
                    }
                    // if the split of the current node is a split on 
                    // neighborhood data
                    else {
                        // the tokens corresponding to the the labels of the 
                        // node and edge are retrieved and converted to postfix
                        List<Token> list = Condition.convertInfixToPostfix(getTokensForLabels(nodeLabel, edgeLabel, function));
                        // if it is not the first condition (as the postfixed
                        // notation is used, an and condition is formatted as 
                        // follows: <cond1> <cond2> and)
                        if (!conditionBuffer.isEmpty()) {
                            list.add(new Token(AnalyzerConstants.AND, "and"));
                        }
                        // this new part of Condition is pushed onto the stack
                        // of the Condition buffer
                        conditionBuffer.push(list);
                    }
                }
            }
 
            // if the current node is not a leaf and has at least one 
            // non-visited child
            if (child != null) {
                // the child is marked as visited and pushed onto the stack
                visited.put(child.getRefer(), true);
                nodes.push(child);
            }
            // else the node has been used and is removed of the stack of nodes
            else {
                // the Condition buffer is updated
                Node poppedNode = nodes.pop();
                // if the split that led to the current node is not a split on
                // the cell's state in the former layer (as it was ignored
                // previously, it shall not be taken into account now)
                if ((!conditionBuffer.isEmpty()) 
                        && (!poppedNode.getParent(0).getSource().getLabel().equals(formerLayer.getName()))) {
                    conditionBuffer.pop();
                }
            }
        }
        
        return newRules;
    }
     
    /**
     * Checks if a given node is situated under a cell's state in the former 
     * layer split in the tree.
     * @param node starting node
     * @return whether the given node is situated under a cell's state in the 
     * former layer split in the tree
     */
    private boolean isUnderAFLSplit(Node node) {
        while (!isRoot((node))) {
            if (node.getLabel().equals(formerLayer.getName())) {
                return true;
            }
            
            node = node.getParent(0).getSource();
        }
        return false;
    }
    
    /**
     * Method merging a stack of lists of tokens to a single list of tokens.
     * @param stack Stack of lists of tokens
     * @return list of every tokens contained in the stack
     */
    private List<Token> mergeLists (Stack<List<Token>> stack) {
        List<Token> tmp = new ArrayList<>();
        stack.stream().forEach(list -> {
            tmp.addAll(list);
        });
        return tmp;
    }
 
    /**
     * Method creating a Condition corresponding to a given stack of lists of 
     * tokens.
     * @param tokensStack stack of lists of tokens to use to create the 
     * condition
     * @return Condition corresponding to the given stack of lists of tokens
     */
    private Condition createConditionFromStack(Stack<List<Token>> tokensStack) {
        Condition condition = null;
 
        List<Token> metaTokens = mergeLists(tokensStack);
         
        if (!metaTokens.isEmpty()) {
            condition = new Condition(metaTokens, true);
        }
 
        return condition;
    }
 
    /**
     * Method checking if a given Node is the root.
     *
     * @param node Node to check
     * @return whether the Node is the root
     */
    private boolean isRoot(Node node) {
        return (node.getParent(0) == null);
    }
 
    /**
     * Method checking if a given Node is a leaf.
     *
     * @param node Node to check
     * @return whether the Node is a leaf
     */
    private boolean isLeaf(Node node) {
        return (node.getChild(0) == null);
    }
 
    /**
     * Method converting two labels (Strings representing the Node and the
     * condition on the Edge) to a list of tokens. This list of tokens will be
     * used to add conditions to a rule later on.
     *
     * @param nodeLabel String returned by the method getLabel() on the Node
     * @param edgeLabel String returned by the method getLabel() on the Edge
     * @param function String representing the function chosen by the user
     * @return List of Token corresponding to the two given labels.
     * @see Node
     * @see Edge
     */
    private List<Token> getTokensForLabels(String nodeLabel, String edgeLabel, String function) {
        List<Token> tokens = new ArrayList<>();
 
        // if the split happened on a numerical layer
        // we have to set the name of the former layer
        if (isLabelANumLayerName(nodeLabel)) {
            tokens.add(new Token(AnalyzerConstants.NAME, nodeLabel));
        }
        // else, if the split happened on classic neighborhood data
        // we have to set the function and its parameters
        else {
            switch (function) {
                case "nbCellSq":
                    tokens.add(new Token(AnalyzerConstants.NBCELLSQ, "nbCellSq"));
                    break;
                case "nbCellCir":
                    tokens.add(new Token(AnalyzerConstants.NBCELLCIR, "nbCellCir"));
                    break;
                default:
                    throw new IllegalArgumentException("Invalid function name: " + function);
            }
 
            // a Node label is formatted like this: "<Node.name>_<distance>px"
            String[] parts = nodeLabel.split("_");
 
            // here, only the Node "name" is needed (the state to look at)
            tokens.add(new Token(AnalyzerConstants.NAME, parts[0]));
 
            // there, only the "distance" is needed
            // thus, the "px" is removed (the size of the neighborhood)
            int i = Integer.parseInt(parts[1].substring(0, parts[1].length() - 2));
            tokens.add(new Token(AnalyzerConstants.INTEGER, "" + i));
        }
 
        // the following is common to both previous splits
        // an Edge label is formatted like this: "<comparator> <float>"
        int opSize = 0;
        if (edgeLabel.contains("<=")) {
            tokens.add(new Token(AnalyzerConstants.LEQ, "<="));
            opSize = 2;
        }
        else if (edgeLabel.contains(">=")) {
            tokens.add(new Token(AnalyzerConstants.GEQ, ">="));
            opSize = 2;
        }
        else if (edgeLabel.contains("<")) {
            tokens.add(new Token(AnalyzerConstants.LESS, "<"));
            opSize = 1;
        }
        else if (edgeLabel.contains(">")) {
            tokens.add(new Token(AnalyzerConstants.GREAT, ">"));
            opSize = 1;
        }
 
        // fetch the <float>
        float f = Float.valueOf(edgeLabel.substring(opSize + 1, edgeLabel.length()).replace(",", "."));
        tokens.add(new Token(AnalyzerConstants.FLOAT, "" + f));
 
        return tokens;
    }
    
    /**
     * Method checking whether a String is an additional layer name.
     * @param toCheck String to check
     * @return whether the String to search is an additional layer name
     */
    private boolean isLabelANumLayerName(String toCheck) {
        return layers.stream().anyMatch(layer -> layer.getName().equals(toCheck));
    }
 
    /**
     * Getter for the class' attribute rules. This attribute is a List of the
     * different rules created during the process.
     *
     * @return class' attribute rules
     */
    public List<Rule> getRules() {
        return rules;
    }
 
    /**
     * Sets the attribute rules to null.
     */
    public void clearRules() {
        rules = null;
    }
 
    /**
     * Method returning the max children index for a given Node (equals to the 
     * number of children minus one)
     * @param parent
     * @return max children index for a given Node 
     */
    private int getMaxChildrenIndex(Node parent) {
        int nbChildren = 0;
        while (parent.getChild(nbChildren) != null) {
            nbChildren++;
        }
        return (nbChildren - 1);
    }
 
    /**
     * TODO : bad method return always true !!!
     * 
     * Method checking whether one leaf of the tree is "true" (ignoring those 
     * under a split on the cell's state in the former layer different from a 
     * given one). 
     * A depth-first search is executed on the tree, returning false if a "true"
     * labeled leaf is met, and true at the end of the search.
     * @param root root node of the tree
     * @param newInitState state to compare
     * @return whether all the leaves of the given tree are false
     */
    private boolean hasAtLeastOneTrueLeaf(Node root, State newInitState) {
        Map<String, Boolean> visited = new TreeMap<>();
 
        Stack<Node> stack = new Stack<>();
 
        Node currentNode, child;
 
        stack.push(root);
        visited.put(root.getRefer(), true);
        while (!stack.isEmpty()) {
            int leastChildId = 0;
             
            currentNode = stack.peek();
 
            child = null;
            // if the current node is a leaf
            if (isLeaf(currentNode)) {
                // if it is a "true" labeled leaf
                if (currentNode.getLabel().startsWith("true")) {
                    // if it results from a split on classic neighborhood data
                    // or from a split on the cell's state in the former layer
                    // but with a state equals to the given one then the 
                    // condition is true
                    if ((!currentNode.getParent(0).getSource().getLabel().equals(formerLayer.getName()))
                            || ((currentNode.getParent(0).getSource().getLabel().equals(formerLayer.getName()))
                            && (!metAnotherEdgeState(currentNode, newInitState)))) {
                        return true;
                    }
                }
            }
            else {
                while ((currentNode.getChild(leastChildId) != null) 
                        && ((visited.containsKey(currentNode.getChild(leastChildId).getTarget().getRefer())) 
                        && visited.get(currentNode.getChild(leastChildId).getTarget().getRefer()))) {
                    leastChildId++;
                }
 
                if (leastChildId <= getMaxChildrenIndex(currentNode)) {
                    child = currentNode.getChild(leastChildId).getTarget();
                }
            }
 
            if (child != null) {
                visited.put(child.getRefer(), true);
                stack.push(child);
            }
            else {
                stack.pop();
            }
        }
 
        return true;
    }
    
    /**
     * Method checking whether one leaf of the tree is "true" (ignoring those 
     * under a split on the cell's state in the former layer). 
     * A depth-first search is executed on the tree, returning false if a "true"
     * labeled leaf is met, and true at the end of the search.
     * @param root root node of the tree
     * @return whether all the leaves of the given tree are false
     */
    private boolean hasAtLeastOneTrueLeafWithoutFlSplit(Node root) {
        Map<String, Boolean> visited = new TreeMap<>();
 
        Stack<Node> stack = new Stack<>();
 
        Node currentNode, child;
 
        stack.push(root);
        visited.put(root.getRefer(), true);
        while (!stack.isEmpty()) {
            int leastChildId = 0;
             
            currentNode = stack.peek();
 
            child = null;
            if (isLeaf(currentNode)) {
                if (currentNode.getLabel().startsWith("true")) {
                    if (!isUnderAFLSplit(currentNode)) {
                        return true;
                    }
                }
            }
            else {
                while ((currentNode.getChild(leastChildId) != null) 
                        && ((visited.containsKey(currentNode.getChild(leastChildId).getTarget().getRefer())) 
                        && visited.get(currentNode.getChild(leastChildId).getTarget().getRefer()))) {
                    leastChildId++;
                }
 
                if (leastChildId <= getMaxChildrenIndex(currentNode)) {
                    child = currentNode.getChild(leastChildId).getTarget();
                }
            }
 
            if (child != null) {
                visited.put(child.getRefer(), true);
                stack.push(child);
            }
            else {
                stack.pop();
            }
        }
 
        return true;
    }
    
    private boolean metAnotherEdgeState(Node node, State newInitState) {
        while (!isRoot(node)) {
            if (node.getParent(0).getLabel().startsWith("=")) {
                String edgeLabel = node.getParent(0).getLabel();
                String stateName = edgeLabel.substring(2, edgeLabel.length());
                State edgeState = formerLayer.getStates().getState(stateName);
                
                if (edgeState.getValue() != newInitState.getValue()) {
                    return true;
                }
            }
            
            node = node.getParent(0).getSource();
        }
        
        return false;
    }

    private boolean oneFalseOneFLSplit(Node node) {
        if (getMaxChildrenIndex(node) == 1) {
            if (((node.getChild(0).getTarget().getLabel().equals(formerLayer.getName())) && (node.getChild(1).getTarget().getLabel().startsWith("false")))
                    || ((node.getChild(0).getTarget().getLabel().startsWith("false")) && (node.getChild(1).getTarget().getLabel().equals(formerLayer.getName())))) {
                return true;
            }
        }
        
        return false;
    }
}