/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.analysis;

import java.awt.image.BandedSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Collection;
import java.util.concurrent.CancellationException;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.AbstractParallelFTask;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.ParallelFTask;
import org.thema.common.swing.TaskMonitor.EmptyMonitor;
import org.thema.lucsim.engine.NumLayer;
import org.thema.lucsim.engine.StateLayer;

/**
 *
 * @author Gilles Vuidel
 */
public class PotentialLayer extends NumLayer {

    private StateLayer baseLayer;
    private double distPower;
    private int maxDist;
    private double [] mass;
    
    private PotentialLayer(String name, WritableRaster img, StateLayer baseLayer, double distPower, double [] mass, int maxDist) {
        super(name, img, baseLayer.getEnvelope());
        this.baseLayer = baseLayer;
        this.distPower = distPower;
        this.mass = mass;
        this.maxDist = maxDist;
    }

    public StateLayer getBaseLayer() {
        return baseLayer;
    }
    
    public double getDistPower() {
        return distPower;
    }

    public double[] getMass() {
        return mass;
    }

    public int getMaxDist() {
        return maxDist;
    }

    public void updateImage(StateLayer baseLayer, double distPower, double [] mass, int maxDist) {
        WritableRaster img = calcPotential(baseLayer, distPower, mass, maxDist, new EmptyMonitor());
        setRaster(img);
    }
    
    /**
     * Creates a new potential layer and calculate the potential
     * @param name layer name
     * @param baseLayer base state layer for calculation
     * @param distPower the distance exponent
     * @param mass the mass associated with each state value, must be of MAXSTATE size
     * @param maxDist the maximum distance in pixel
     * @return the new potential layer
     */
    public static PotentialLayer create(String name, StateLayer baseLayer, double distPower, double [] mass, int maxDist, ProgressBar monitor) {
        WritableRaster img = calcPotential(baseLayer, distPower, mass, maxDist, monitor);
        return new PotentialLayer(name, img, baseLayer, distPower, mass, maxDist);
    }
    
    private static WritableRaster calcPotential(final StateLayer baseLayer, final double distPower, final double [] mass, final int maxDist, ProgressBar monitor) {
        final WritableRaster raster = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_DOUBLE, baseLayer.getWidth(), baseLayer.getHeight(), 1), null);
        final double maxDist2 = maxDist*maxDist;
        
        ParallelFTask<Void, Void> task = new AbstractParallelFTask<Void, Void>(monitor) {
              @Override
              protected Void execute(final int start, final int end) {
                  for(int y = start; y < end; y++) {
                      for(int x = 0; x < raster.getWidth(); x++) {
                          if(isCanceled()) {
                              return null;
                          }
                          final int minX = Math.max(x-maxDist, 0);
                          final int minY = Math.max(y-maxDist, 0);
                          final int maxX = Math.min(x+maxDist, raster.getWidth()-1);
                          final int maxY = Math.min(y+maxDist, raster.getHeight()-1);
                          double sum = 0;
                          for(int j = minY; j <= maxY; j++) {
                              for(int i = minX; i <= maxX; i++) {
                                  double m = mass[(int)baseLayer.getElement(i, j)];
                                  if(m > 0) {
                                      double dist2 = Math.pow(j-y, 2) + Math.pow(i-x, 2);
                                      if(dist2 > 0 && dist2 <= maxDist2) {
                                          sum += m / Math.pow(dist2, distPower / 2);
                                      }
                                  }
                              }
                          }

                          raster.setSample(x, y, 0, sum);
                      }
                      incProgress(1);
                  }
                  return null;
              }

              @Override
              protected int getSplitRange() {
                  return raster.getHeight();
              }
              @Override
              public void finish(Collection<Void> clctn) {
              }
              @Override
              public Void getResult() {
                  throw new UnsupportedOperationException("Not supported yet.");
              }

          };
        
        new ParallelFExecutor(task).executeAndWait();
        if(task.isCanceled()) {
            throw new CancellationException();
        }
        return raster;
    }
}
