/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.analysis;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.thema.lucsim.engine.StateLayer;

/**
 *
 * @author Gilles Vuidel
 */
public class MarkovChain {
    private StateLayer initLayer;
    private StateLayer finalLayer;
    private int power;
    private StateLayer startLayer;
    
    private transient RealMatrix matrix;
    private transient RealVector result;

    public MarkovChain(StateLayer initLayer, StateLayer finalLayer, int power, StateLayer startLayer) {
        this.initLayer = initLayer;
        this.finalLayer = finalLayer;
        this.power = power;
        this.startLayer = startLayer;
    }

    public synchronized RealMatrix getMatrix() {
        if(matrix == null) {
            matrix = initLayer.getMatrixComparison(finalLayer);
            normalizeMatrix();
            if(power > 1) {
                matrix = matrix.power(power);
            }
        }
        return matrix;
    }

    private void normalizeMatrix() {
        for(int i = 0; i < matrix.getRowDimension(); i++) {
            RealVector row = matrix.getRowVector(i);
            double sum = row.getL1Norm();
            if(sum > 0) {
                matrix.setRowVector(i, row.mapMultiplyToSelf(1 / sum));
            }
        }
    }
    
    public synchronized RealVector getResultVector() {
        if(result == null) {
            result = getMatrix().preMultiply(startLayer.getNbCellVector());
        }
        return result;
    }
    
    public boolean isLayerUsed(StateLayer l) {
         return l == initLayer || l == finalLayer || l == startLayer;
    }

    public StateLayer getFinalLayer() {
        return finalLayer;
    }

    public StateLayer getInitLayer() {
        return initLayer;
    }

    public int getPower() {
        return power;
    }

    public StateLayer getStartLayer() {
        return startLayer;
    }

    public void setStartLayer(StateLayer startLayer) {
        this.startLayer = startLayer;
        result = null;
    }
    
    
}
