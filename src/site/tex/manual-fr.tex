\documentclass{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amssymb,amsfonts,textcomp}
\usepackage{array}
\usepackage{hhline}
\usepackage{hyperref}
\hypersetup{colorlinks=true, linkcolor=blue, citecolor=blue, filecolor=blue, urlcolor=blue, pdftitle=, pdfauthor=Gilles Vuidel, pdfsubject=, pdfkeywords=}
\usepackage{graphicx}
\usepackage[top=2.501cm,bottom=2.501cm,left=2.501cm,right=2.501cm,nohead]{geometry}
\usepackage{float}
\usepackage{parskip}
\usepackage{multirow}
\usepackage{caption}
\usepackage{fancyvrb}
\makeatletter
\newcommand\arraybslash{\let\\\@arraycr}
\makeatother
% centering figures
\makeatletter
\g@addto@macro\@floatboxreset\centering
\makeatother
\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.3}

% saut de page après une section
%\let\oldsection\section
%\renewcommand\section{\clearpage\oldsection}

% saut après itemize
\let\EndItemize\enditemize
\def\enditemize{\EndItemize\medskip}

\begin{document}
 \begin{titlepage}
	
	\centering
	\includegraphics[scale=1]{img/logo.png}\\
	
	\bigskip
	\bigskip
	\bigskip	
	{\Huge
		\bfseries
		LucSim 1.6\\
		\bigskip
		Manuel d’utilisation\\
	}
	\bigskip
	\bigskip
	\bigskip
	\bigskip
	\bigskip
	
	{\Large		
		Gilles Vuidel et Jean-Philippe Antoni\\
		\bigskip
		23/10/2017\\
	}
	
\end{titlepage}

\setcounter{tocdepth}{2}
%\renewcommand\contentsname{}
\tableofcontents

\pagebreak

\chapter{Introduction}

\section{A propos de LucSim}

Le logiciel LucSim est un environnement complet pour le développement et la simulation d’automates
cellulaires, développé pour répondre à des besoins spécifiques en géographie, mais il peut être
aussi utilisé pour des problématiques dans d’autres disciplines.

\subsection{Auteurs}
Le programme LucSim a été conçu au laboratoire \href{http://thema.univ-fcomte.fr}{ThéMA} (\href{http://www.ubfc.fr}{Université de Bourgogne Franche-Comté} – \href{http://www.cnrs.fr}{CNRS}) par Jean-Philippe Antoni et Gilles Vuidel avec la contribution de : Valentin Garet, Alexis Picard et Rémi Ducceschi.

\subsection{Conditions d’utilisation}
Le programme LucSim est disponible librement sous licence \href{https://www.gnu.org/licenses/gpl-3.0.fr.html}{GPL}. Le code source est téléchargeable à partir du dépôt Git : \url{git://git.renater.fr/lucsim.git}.

Les utilisateurs de LucSim sont invités à citer la référence  suivante  dans leurs travaux :
??

\section{Configuration requise}

LucSim fonctionne sur n'importe quel ordinateur supportant Java 8 ou supérieur (PC sous Linux, Windows, Mac...). Toutefois, lorsqu'il s'agit de données très volumineuses, la quantité de mémoire vive (RAM) de l’ordinateur peut limiter la taille des zones d'étude. De plus, la puissance du processeur va déterminer la vitesse de leur calcul. Vous trouverez plus de détails à ce sujet dans le chapitre \nameref{perf}. 

\section{Installation et lancement du programme}

LucSim est téléchargeable à cette adresse : \url{http://sourcesup.renater.fr/lucsim}.

\begin{itemize}
	\item Télécharger et installer Java 8 ou + (\href{http://www.java.com}{java.com}). Installer de préférence la version 64 bits de Java.
	\item Télécharger lucsim-1.6.jar
	\item Lancer lucsim-1.6.jar
\end{itemize}


\section{Présentation générale}

Cette rubrique définit le vocabulaire et les principaux éléments utilisés dans le logiciel LucSim. Ces définitions reprennent souvent des notions connues dans le monde des automates cellulaires, des
systèmes d’information géographique et du traitement d’images.


\subsection{Les couches d'information}

Dans LucSim, une couche est une information localisée de base, souvent composée de
l’occupation du sol de la zone géographique à traiter. Une couche est composée d’une matrice à
deux dimensions (du point de vue mathématique) qui correspond à une image (du point de vue
graphique). Chaque élément de la matrice contient une valeur représentée par une couleur sur
l’image. Les éléments de la matrice sont donc autant de pixel en couleurs qui forment l’image. Pour
qualifier une couleur, on parlera de l’état de la cellule, appartenant à l’ensemble des états, soit
sur le plan graphique, à la palette des couleurs, de l’automate.

Figure couche.ai

Parallèlement aux images et aux états, l’automate est caractérisé par un certain nombre de règles
de transition qui définissent les changements d’état des cellules au cours du temps. Ces règles
sont des expressions définies par l’utilisateur selon un langage propre au logiciel (voir le chapitre \ref{rule}). Elles autorisent une cellule à changer d’état, en fonction de son voisinage et d'autres paramètres.


\subsection{Couche à états et couche statique}
Il existe deux types de couches dans LucSim : les couches à états (State layer) et les couches statiques (Static layer). A la différence des couches à états, les couches
statiques ne contiennent qu’une matrice et dans certains cas une palette de couleurs. Elles servent
de couches complémentaires utiles dans certains cas, en apportant une information
supplémentaire qui pourra être utilisée dans les règles de transition. Il n’est pas possible de doter les couches statiques de règles de transition.

\subsection{Projet}
Un projet LucSim est un fichier \verb|.ca| (format XML) contenant les états, les règles de transition, la liste des couches et les options et un sous-dossier contenant les couches du projet au format tiff.

\subsection{Interfaces}

LucSim peut être utilisé par deux interfaces : une interface graphique et une interface en ligne de commande.
L'interface graphique permet de créer un projet, visualiser les données, gérer les couches, les états, et les règles de transition et lancer des simulations. L'interface en ligne de commande permet quant à elle, de lancer ces même calculs sur des ordinateurs distants ainsi que sur des clusters.

\bigskip

Le chapitre suivant (\ref{gui}) décrit l'ensemble des fonctionnalités du logiciel disponibles à partir de l'interface graphique. Le chapitre \ref{cli} détaille comment utiliser LucSim en ligne de commande, le chapitre \ref{rule} décrit la syntaxe des règles de transition. Enfin, le chapitre \ref{perf} revient sur les modes de parallélisation, les performances et la gestion mémoire.


\chapter{Interface graphique}
\label{gui}

Une fois LucSim lancé, la première étape consiste à créer un projet.

\section{Création d'un projet}

La mise en place d’un nouveau projet LucSim s’effectue à partir du menu : Project / New project. L’utilisateur doit sélectionner une image raster au format Tiff ou AsciiGrid.

\begin{figure}[H]
%S	\includegraphics[scale=0.5]{img/new_project-fr.png} 
	\caption{Création d'un projet}
\end{figure}

Le projet créé stocke dans son dossier, la couche transformée en Tiff  ainsi qu'un fichier XML conservant les paramètres du projet avec une extension \verb|.ca|.
Pour ouvrir un projet, il suffit de sélectionner le fichier \verb|.ca| correspondant à partir du menu Project / Load project.


\section{Gestion des données}

Pour les données raster à importer, LucSim supporte deux formats de fichier : Tiff et AsciiGrid. Pour le format Tiff, le système de coordonnées peut être donné de deux manières : directement dans le fichier Tiff par l'extension GeoTiff ou bien par un fichier texte additionnel ayant le même nom et l'extension .tfw. 


\subsection{Couches complémentaires}



\chapter{Interface en ligne de commande}
\label{cli}

Dès lors qu'un projet a été créé depuis l'interface graphique, il est possible d'utiliser LucSim en ligne de commande. Ce mode est utile pour exécuter le logiciel sur un ordinateur distant sans interface graphique, lancer automatiquement plusieurs traitements à la suite, ou encore soumettre des exécutions sur un cluster.

\section{Démarrage}

\subsection{Lancer LucSim en ligne de commande}
Il faut tout d'abord ouvrir une fenêtre de terminal, puis aller dans le répertoire contenant PixScape avec la commande \textit{cd}.
Enfin, vous pouvez saisir la commande suivante pour afficher l'écran d'aide de PixScape :
\begin{Verbatim}
java -jar lucsim.jar --help
\end{Verbatim}
Résultat
\begin{Verbatim}
Usage :
java -jar lucsim.jar ...
...
...
\end{Verbatim}
Vous êtes prêt à utiliser LucSim en ligne de commande.

\subsection{Syntaxe}
\subsubsection{Définition}
Les commandes commencent par un double tiret : \verb|--project, --extract, ...|\\
Les options globales commencent par un tiret simple : \verb|-proc, ...|\\
Les paramètres n'ont pas de tiret : \verb|weight, target, ...|

\subsubsection{Séparateur}
Les espaces sont utilisés pour séparer les différents éléments d'une ligne de commande, par conséquent, vous ne pouvez pas avoir un nom qui contient des espaces.\\

\subsubsection{Syntaxe de l'écran d'aide}
Dans l'écran d'aide, les éléments entourés de crochets sont optionnels, et donc les éléments qui ne sont pas entourés par des crochets sont obligatoires. Le caractère \verb+|+ sépare les options possibles.

\section{Commandes}

\subsection{--help : affichage de l'aide}
Commande :
\begin{Verbatim}
java -jar lucsim-1.0.jar --help
\end{Verbatim}
Résultat :
\begin{Verbatim}
Usage :
java -jar lucsim.jar [-proc n]
--project project_file.xml COMMAND

Command list :
--extract layer1=layer_name layer2=layer_name function=nbCellSq|nbCellCirc radius=min:inc:max ...
--rulegen [data=file.csv] training=percent [seed=num] [output=dir] algo=J48|Hoeffding [algo ...
\end{Verbatim}


\subsection{--project : chargement d'un projet}
Cette commande définit le chemin vers le fichier xml du projet à charger.
\begin{Verbatim}[commandchars=\\\{\}]
java -jar lucsim-1.0.jar --project \textit{path2myproject/myproject.xml}
\end{Verbatim}
Charge le projet \textit{myproject} contenu dans le répertoire \textit{path2myproject}.

La commande \verb|--project| ne peut être utilisée qu'une seule fois et doit être la première commande de la ligne.
Toutes les commandes qui suivent dans ce manuel ont besoin d'un projet chargé.


\section{Options}

\subsection{Parallélisation : -proc}
\begin{Verbatim}[commandchars=\\\{\}]
-proc \textit{n}
\end{Verbatim}

LucSim supporte un mode de parallélisation en ligne de commande : par thread pour un ordinateur contenant plusieurs coeurs (\verb|-proc|).

Pour plus de détails voir la section \nameref{parallelism}.

Si l'option n'est renseignée, la parallélisation par thread sera utilisée avec le nombre de coeurs défini dans l'interface graphique.


\section{Exemples}

Dans cette section, quelques exemples de lignes de commande complètes sont illustrés.


\chapter{Règles de transition}
\label{rule}

Ce chapitre décrit l’ensemble des notions utilisées pour l'écriture de règles
de transition : syntaxe, blocs, conditions, opérateurs, fonctions. Ces règles sont le cœur de l’automate. Elles définissent son fonctionnement.


\section{Généralités}
La forme générique d’une règle de transition est la suivante :
\begin{verbatim}
	Etat_depart -> Etat_arrivee : CONDITION ;
\end{verbatim}

On peut lire : les cellules qui sont à l’état \verb|Etat_depart| passent à l’état \verb|Etat_arrivee| si \verb|CONDITION| est vraie.

\textbf{Exemple :}
\begin{verbatim}
	friche -> bati : pCellSq (bati, 5) >= 50% ;
\end{verbatim}

On peut lire : les cellules à l’état \verb|friche| passent à l’état \verb|bati| si, dans leur voisinage (respectif) carré de 11
pixels (5 * 2 + 1) de côté, 50\% au moins des cellules sont à l’état \verb|bati|.

Pour qu’une règle puisse être appliquée à des cellules de différents états de départ,
\verb|Etat_depart| peut être un ensemble d’états séparés par des virgules. Il existe aussi un mot clé
pour exprimer tous les états de la couche : \verb|all|

\textbf{Exemples :}

\begin{verbatim}
friche, solnu -> bati : CONDITION ;
\end{verbatim}

On peut lire : les cellules qui sont à l’état \verb|friche| et les cellules à l’état \verb|solnu| passent à l’état \verb|bati| si la condition est
vraie.

\begin{verbatim}
all -> bati : CONDITION ;
\end{verbatim}

On peut lire : les cellules, quel que soit leur état de départ, passent à l’état \verb|bati| si la condition est vraie.


\section{Expressions}
La condition d’une règle est une expression booléenne c’est-à-dire qu’elle renvoie VRAI ou FAUX. Elle est composée d'opérateurs, d'opérandes et de fonctions.

\subsection{Les opérateurs}
LucSim intègre quatre types d’opérateurs : les opérateurs booléens, les opérateurs de comparaison, les opérateurs arithmétiques et les opérateurs sur les couches. Ont peut les lister par ordre de priorité croissant :

\textbf{Opérateurs booléens}
\begin{tabbing}
	\hspace{20pt}\=\kill
	\> 
	\begin{tabular}{ll}
		or &ou logique (inclusif)\\
		xor &ou logique exclusif\\
		and &et logique\\
		not &non logique\\
	\end{tabular}
\end{tabbing} 

\textbf{Opérateurs de comparaison (priorité égale)}
\begin{tabbing}
	\hspace{20pt}\=\kill
	\> 
	\begin{tabular}{ll}
		= &égal\\
		< &strictement inférieur\\
		> &strictement supérieur\\
		<= &inférieur ou égal\\
		>= &supérieur ou égal\\
		<> & différent\\
	\end{tabular}
\end{tabbing} 

\textbf{Opérateurs arithmétiques}
\begin{tabbing}
	\hspace{20pt}\=\kill
	\> 
	\begin{tabular}{ll}
		+ - &addition soustraction\\
		mod &modulo (reste de la division entière)\\
		* / &multiplication division\\
		\% & pourcentage\\
	\end{tabular}
\end{tabbing} 


\subsection{Les fonctions locales}
Les fonctions locales interviennent dans la construction des règles et mettent en jeu un certain nombre d’états, souvent par rapport à leur voisinage.

	\verb|nbCellNeu(Etat)| : nombre de cellules d’état \verb|Etat| dans un voisinage de Von Neumann.\\
	\verb|nbCellMoo(Etat)| : nombre de cellules d’état \verb|Etat| dans un voisinage de Moore.\\
	\verb|nbCellSq(Etat, R)| : nombre de cellules d’état \verb|Etat| dans un voisinage carré de rayon R.\\
	\verb|nbCellCir (Etat, R)| : nombre de cellules d’état \verb|Etat| dans un voisinage circulaire de rayon R.\\
	\verb|nbCellAng(Etat, A1, A2, R)| : nombre de cellules d’état \verb|Etat| dans un voisinage circulaire de rayon R compris entre l'angle A1 et A2.\\
	\verb|pCellSq (Etat, R)| : proportion de cellules d’état \verb|Etat| dans un voisinage carré de rayon R.\\
	\verb|pCellCir (Etat, R)| : proportion de cellules d’état \verb|Etat| dans un voisinage circulaire de rayon R.\\
	\verb|pCellAng(Etat, A1, A2, R)| : proportion de cellules d’état \verb|Etat| dans un voisinage circulaire de rayon R compris entre l'angle A1 et A2.\\
\verb|cellDuration()| : cette fonction renvoie depuis quand (en nombre d’étape) la cellule est dans son état actuel.\\
\verb|north(), south(), east(), west()| :
ces fonctions renvoient la valeur (l’état) de la cellule se trouvant respectivement au nord, sud, est et ouest de la cellule active.\\
\verb|northeast(), northwest(), southeast(), soutwest()| :
ces fonctions renvoient la valeur (l’état) de la cellule se trouvant respectivement au nord-est, nord-ouest, sud-est et sud-ouest de la cellule active.

\textbf{Remarques}
Sur le plan de la vitesse d’exécution, et indépendamment de la machine utilisée, les fonctions quadratiques sont
plus rapides que les fonctions circulaires. Les fonctions peuvent être écrites en minuscules.

\subsection{Les fonctions globales}
Le fonctions globales renvoient des informations sur la matrice et sur la simulation en cours.

\verb|nbCell(Etat)| : cette fonction mesure le nombre de cellule qui sont dans l’état \verb|Etat| dans toute la matrice.\\
\verb|pCell(Etat)| : cette fonction mesure la proportion de cellule qui sont dans l’état \verb|Etat| dans toute la matrice.\\
\verb|step()| : cette fonction renvoie le numéro de l’étape courante.\\
\verb|nbSameStep()| : cette fonction renvoie le nombre d’étapes consécutives sans aucun changement d’état dans toutes les cellules de la matrice.

\subsection{Les fonctions mathématiques}
\verb|random()| : génère un nombre réel pseudo-aléatoire compris dans l’intervalle [0 ;1[.\\
\verb|int(val)| : renvoie la partie entière de \verb|val|.\\
\verb|round(val)| : arrondi \verb|val| à l’entier le plus proche.


\subsection{Les couches}
A l’intérieur d’une expression, il est possible de faire appel à d’autres couches que celle pour
laquelle la règle est écrite, c’est-à-dire de récupérer le voisinage ou la valeur de la cellule active
même si ce voisinage ou cette valeur sont stockés dans une autre couche.

\subsubsection{Récupération du voisinage d’une cellule}
La première consiste à préfixer les fonctions de voisinages par le nom de la couche (réservé aux couches à états), c’est-à-dire à écrire le nom de la couche sur laquelle la fonction doit s’appliquer avant la fonction. On sépare alors le préfixe du nom de la fonction par un point.

\textbf{Exemple :}

On suppose qu’il existe une couche AC s’appelant route et contenant les états rue, dep, nat, aut.
\begin{verbatim}
vide -> bati : route.nbcellsq(rue, 1) >= 1 ;
\end{verbatim}

On peut lire : les cellules passent de \verb|vide| à \verb|bati|, si dans la couche \verb|route|, il y a une cellule à l'état \verb|rue| qui touche la cellule active.

\subsubsection{Récupération de la valeur d’une cellule}
Il s’agit ici de récupérer la valeur d’une cellule quand celle-ci est stockée dans une autre couche.
Cette fois-ci, le type de la couche est indifférent.

\textbf{Exemple :}
\begin{verbatim}
vide -> bati : route <> rue ;
\end{verbatim}

Les cellules passent de \verb|vide| vers \verb|bati| si il n’y a pas une \verb|rue| à leur position propre dans la couche \verb|route|.

\section{Les voisinages}
\subsection{Quadratique}
Voisinage de rayon 1
\begin{verbatim}
0 0 0
0 X 0
0 0 0
\end{verbatim}


Voisinage de rayon 2
\begin{verbatim}
0 0 0 0 0
0 0 0 0 0
0 0 X 0 0
0 0 0 0 0
0 0 0 0 0
\end{verbatim}


\subsection{Circulaire}
Voisinage de rayon 1
\begin{verbatim}
  0 
0 X 0
  0 
\end{verbatim}

Voisinage de rayon 2
\begin{verbatim}
    0  
  0 0 0 
0 0 X 0 0
  0 0 0 
    0 
\end{verbatim}

Voisinage de rayon 3
\begin{verbatim}
      0  
  0 0 0 0 0
  0 0 0 0 0  
0 0 0 X 0 0 0
  0 0 0 0 0
  0 0 0 0 0
      0 
\end{verbatim}

    
\textbf{Remarque :}
le rayon doit être entier, sinon il est tronqué à l’entier inférieur.


\textbf{Exemple de règle :}
\begin{verbatim}
# un commentaire
vide->vegetation : pCellCir(vegetation, 8) <= 10% and # un autre commentaire
		nbCellSq (vegetation, 1) >= 1 ;
\end{verbatim}

Une cellule passe de l’état \verb|vide| à l’état \verb|vegetation| :
- si au plus 10\% des cellules sont dans l’état \verb|vegetation|, dans un rayon de 8 pixels autour de la cellule et
- si au moins une cellule est dans l’état \verb|vegetation| dans un carré de 1 pixel de rayon (3 pixels de côté).

\textbf{Remarque :}
On peut mettre des commentaires en les faisant précéder d’un dièse.

\section{Structures de contrôle}

\subsection{Les blocs}

Un bloc regroupe un ensemble de règles traitées en même temps. L’intérêt principal est de permettre l’exécution séquentielle de plusieurs blocs, c’est-à-dire, exécuter plusieurs ensembles de règles à la suite. La délimitation d’un bloc de règles s’écrit avec des accolades.

\textbf{Exemple :}
\begin{verbatim}
{
  vide -> vegetation : nbCellSq (végétation, 2) >= 1;
  vide -> eau : nbCellSq (eau, 2) >= 1;
}
\end{verbatim}

Les règles contenues dans un bloc sont exécutées à l’infini. Pour stopper l’exécution d’un bloc il est possible de définir une condition d’arrêt. Il suffit d’ajouter avant le bloc: while(« condition d’arrêt »). On peut lire : exécuter le bloc de règles tant que « condition d’arrêt » est vraie.

\textbf{Exemple :}
\begin{verbatim}
# Ce bloc s’exécute pendant une étape pour initialiser la couche
while(step <= 1)
{
  mort -> vie : random() >= 0.5 ;
}
# Puis celui-ci s’exécute à l’infini et correspond au jeu de la vie
{
  mort -> vie : nbCellMoo(vie) = 3 ;
  vie -> mort : nbCellMoo(vie) < 2 or nbCellMoo(vie) > 3 ;
}
\end{verbatim}

\textbf{Remarque :} la condition d’arrêt est une expression booléenne. De plus, elle ne peut pas contenir de fonctions locales.

On peut ainsi, mettre plusieurs blocs à la suite, qui seront exécutés séquentiellement. 





\chapter{Performances}
\label{perf}

\section{Compilation des règles}
Par défaut, l'exécution des règles de l'automate sont interprétées par LucSim. Pour diminuer le temps des simulations, il est possible de compiler les règles en code binaire. Pour cela, il faut tout d'abord installer un JDK (Java Development Kit), puis relancer LucSim et cocher l'option "Java compiler" du menu "Project". Les règles seront compilées et les simulations seront potentiellement beaucoup plus rapide.

\section{Parallélisation}
\label{parallelism}
Pour diminuer les temps d'exécution de calcul de visibilité, LucSim implémente une méthode de parallélisation : par thread pour un ordinateur seul.


\subsection{Un ordinateur : threads}
\label{thread}
La parallélisation par thread accélère le calcul sur une seule machine contenant plusieurs cœurs ou processeurs.
Si votre ordinateur a plus qu'un coeur (la plupart), vous pouvez tirer partie de la parallélisation par thread pour accélérer vos calculs avec LucSim. Le temps d'exécution est approximativement inversement proportionnel au nombre de coeurs utilisés. Ce mode de parallélisation est utilisable en interface graphique et en ligne de commande.

\subsubsection{Interface graphique}
La fenêtre "Préferences" accessible par le menu Fichier permet de régler le nombre de coeurs utilisés par LucSim. Il est défini par défaut au nombre coeurs de la machine moins 1. Après avoir modifié le nombre de coeurs utilisé par LucSim, il vaut mieux relancer LucSim pour être sûr que le changement soit bien pris en compte.

\subsubsection{Ligne de commande}
En ligne de commande, il faut définir le nombre de coeurs (ou processeurs) que PixScape peut utiliser avec l'option \verb|-proc| :
\begin{Verbatim}
	java -jar lucsim-1.0.jar -proc 8 --project path2myproject/myproject.xml ...
\end{Verbatim}
Par défaut, en ligne de commande, le nombre de coeurs utilisés correspond au nombre défini dans la fenêtre Préférences de l'interface graphique.

En augmentant le nombre de coeurs utilisés par LucSim, vous augmentez, par la même occasion, la taille de la mémoire utilisée par LucSim.


\section{Gestion mémoire}
\label{memory}
Par défaut, la taille de la mémoire utilisable par LucSim est dépendante du système et peut varier de 128 MB à plusieurs GB.
Si vous avez un gros projets, certaines commandes stopperont le programme à cause d'un manque de mémoire. De plus, si vous utilisez la parallélisation par thread (\nameref{thread}), LucSim aura besoin de plus de mémoire pour chaque coeur utilisé.
Dans tous les cas, si l'exécution se termine par une erreur OutOfMemoryError ou GC overhead, vous devez augmenter la mémoire disponible pour LucSim.

\subsection{Interface graphique}
La mémoire allouée pour LucSim peut être modifiée dans la fenêtre "Préférences" dans le menu Fichier / Préférences. Après avoir changé ce paramètre, LucSim doit être relancé pour qu'il tienne compte de la nouvelle taille mémoire.

En interface graphique, LucSim a besoin de plus de mémoire qu'en ligne de commande du fait de l'affichage des couches. Si vous êtes trop limité en mémoire vive, vous pouvez lancer les calculs en ligne de commande pour diminuer les besoins en mémoire de LucSim (voir \nameref{cli}).

\subsection{Interface en ligne de commande}
Pour définir la mémoire maximale utilisable par LucSim en ligne de commande, il faut utiliser l'option Java \verb|-Xmx| :
\begin{Verbatim}
	java -Xmx4g -jar lucsim-1.0.jar ... # 4Gb allocated
	java -Xmx1500m -jar lucsim-1.0.jar ... # 1500 Mb -> 1.5Gb allocated
\end{Verbatim}


Si vous ne pouvez pas allouer plus de 1 GB ou 1.5 GB alors que votre ordinateur a plus de mémoire vive, vous utilisez sûrement une version 32-bit de Java qui est limitée à moins de 2 GB.
Pour tester votre version de Java :
\begin{Verbatim}
	java -version
\end{Verbatim}
Si vous avez une version 32-bit, installez une version 64-bit de Java pour allouer plus de mémoire à LucSim.


\end{document}
