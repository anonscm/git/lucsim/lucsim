## Changelog

##### 1.6.8 (22/03/2022)
- CLI : add --rulexec command

##### 1.6.7 (22/09/2021)
- Update for Java 16 compatibility

##### 1.6.6 (07/09/2021)
- Update Théma libs

##### 1.6.5 (29/01/2019)
- Update for Java 9 compatibility

##### 1.6.4 (05/02/2018)
- Project creation : layer colors are not correctly displayed
- Add state layer : when colors cannot be retrieved from the imported image, color states are choosen randomly instead of black

##### 1.6.3
- manage correctly layers with different grid geometry
- cell duration was not counted in asynchonous mode

##### 1.6.2
- Remove old french help window

##### 1.6.1 (23/10/2017)
- Optimize nbCellAng and pCellAng
- Change azimuth orientation : 0° = North, 90° = East, 180° = South, ...

##### 1.6 (21/04/2017)
- Add command line interface (CLI)
- Rule compilation (with JDK) works with large rules set
- DT rule generator : add rule factorization (button Factor rule)

##### 1.5.7 (07/02/2017)
- Bug in simulation graph : lines are always flat

##### 1.5.6 (06/02/2017)
- Change UI manager to System UI
- Simulation layer : sometimes some color states are not drawn
- Show Lucsim version in the title bar of the main window

##### 1.5.5 (11/01/2017)
- Project save : does not save CRS to avoid errors at project saving

##### 1.5.4 (03/01/2017)
- Decision tree : calculate correlation coefficient, rule scores, conditions statistics
- Neighborhood dataset is better managed with Decision Tree

##### 1.5.3 (27/10/2016)
- Merge Graph frame with Image property

##### 1.5.2 (19/10/2016)
- Few UI changes

##### 1.5.1 (18/10/2016)
- When adding layer, user can choose between state or numeric layer
- When changing color state, displayed layers were not updated
- Merge Neighborhood and CSV export menu entry of Statistics menu
- Correct some UI translation

##### 1.5 (20/09/2016)
- Add rule creation by Decision Tree

##### 1.4.4 (04/05/2016)
- Rule compilation does not work on Windows due to temp dir /tmp

##### 1.4.3 (04/05/2016)
- Exclude CA execution on NODATA cells

##### 1.4.2 (23/10/2015)
- Optimize greatly nbCell global function
- JDK execution was never used

##### 1.4.1 (22/10/2015)
- Add rule constraints : markov chain and potential
- State layer : pixel value 255 is set to special state NODATA

##### 1.4 (20/10/2015)
WARNING Previous project is not compatible 

- Project can contain several state layers (ie. old ACLayer)
- States are common to all state layers
- lot of code refactoring
- java code generation have been put in Compiler class 

##### 1.3.1 (28/09/2015)
WARNING Previous project is not compatible (rename file main.tif into main-init.tif)

- optimize and clean Simulation class
- restrict CA layer states between 0 and 254

##### 1.3 (23/09/2015)
- mise à jour des libs
- restructuration et optimisation de Image Neighborhood
- optimisation des fonctions de voisinage

##### 1.2.1
- Correction bug Xstream sur fields hérités d'une classe abstraite 

##### 1.2.0 
- Ajout indication sur le calcul de voisinage 

##### 1.1.9 
- Image Neighbouring modifs
- bouton agrandi zone texte
- loading bar texte

##### 1.1.8 
- Resolution probleme de sauvegarde/reouverture projet
- Optimisations sur l'execution 

##### 1.1.7
- Message d'erreurs
- sauvegarder en sortant du logiciel si non sauvegarde
- pas de recompilation si besoins

##### 1.1.6
- Remove layer
- correction test
- layer init
- graphe (layer static)

##### 1.1.5
- Modification : stat (main)
- suppression des etats
- graphique
- voisinage exclusion cellule en question
- enregistrement des le debut
- enregistrement sous
- ligne et colonne affichées dans zone de texte
- fusion menu layer Ac et static
- bouton ajout static layer
- modif name
- delete static layer (probleme). 

##### 1.1.4 (12/03/2014)
- gestion langue et mémoire corrigée

##### 1.1.3 (10/03/2014)
- optimisation de l'exécution

##### 1.0.2 (13/12/2013)
- problème de packaging pour le chargement et l'enregistrement des fichiers tiff
- paramétrisation du nom du projet dans site.xml.vm