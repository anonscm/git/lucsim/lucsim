/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.engine;

import java.awt.Color;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.geotools.geometry.Envelope2D;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thema.common.swing.TaskMonitor.EmptyMonitor;
import org.thema.lucsim.parser.ParseException;

/**
 *
 * @author alexis
 */
public class TestEngine {
    
    private Envelope2D env;
    private Project project;
    private Simulation simulation;

    @Before
    public void setUp() throws Exception {      
        RenderedImage img = ImageIO.read(new File("src/test/java/image/main.tif"));
        env = new Envelope2D(DefaultGeographicCRS.WGS84, 0, 0 - img.getHeight() * 1, img.getWidth() * 1, img.getHeight() * 1);
        StateLayer layerAC = new StateLayer("main", img, env, new States());
        project = new Project(layerAC);
        
        img = ImageIO.read(new File("src/test/java/image/coeur.tif"));
        NumLayer layer = new NumLayer("coeur", img, env);
        project.addLayer(layer);
        
        project.getStates().getState(254).setName("blanc"); // en fait cyan
        project.getStates().getState(0).setName("noir");
        project.getStates().addState(new State(1, new Color(0, 0, 0), "rose"));
        project.getStates().addState(new State(2, new Color(0, 0, 0), "violet"));
        project.getStates().addState(new State(3, new Color(0, 0, 0), "bleu"));
        project.getStates().addState(new State(5, new Color(0, 0, 0), "jaune"));
        project.getStates().addState(new State(7, new Color(0, 0, 0), "rose2"));
        project.getStates().addState(new State(11, new Color(0, 0, 0), "rouge"));
        project.getStates().addState(new State(21, new Color(0, 0, 0), "vert"));
        
        project.setStringRules(RULES);
        simulation = project.setSimuLayer("main");
        simulation.setSynchronous(true);
    }

    @Test
    public void testInterpreter() throws IOException {
        simulation.interpretRules();
        runSimu();

    }

    @Test
    public void testCompiler() throws IOException {
        simulation.compileRules();
        runSimu();
    }
    
    @Test
    public void testRuleTranslation() throws IOException, ParseException {
        simulation.interpretRules();
        project.setStringRules(project.generateTextRules());
        runSimu();
    }
    
    public void runSimu() throws IOException {
        simulation.reset();
        int i = 1;
        while(!simulation.isFinish()) {
            simulation.run(null, new EmptyMonitor(), true);
            RenderedImage img = ImageIO.read(new File("src/test/java/image/initial/main"+i+".tif"));
            NumLayer init = new NumLayer("init", img, env);
            for (int y = 0; y < init.getHeight(); y++) {
                for (int x = 0; x < init.getWidth(); x++) {
                    Assert.assertEquals("Iter " + i + " step " + simulation.getStep(), init.getElement(x, y), simulation.getSimLayer().getElement(x, y), 0);
                }
            }
            i++;
        }
        
        Assert.assertEquals("Nb steps", 38, simulation.getStep());
    }
    
    
    public static final String RULES = 
            "repeat(1)\n"
                + "{\n"
                + "	noir -> rouge : ((((1+1)/2)*2)-1) + (5 mod 3) = 1 + round(2.2);//  2 = 2\n"
                + "}\n"
                + "\n"
                + "\n"
                + "/*int no bloc (s'execute qu'une fois)*/\n"
                + "	rouge -> jaune : int(1.8) = 1 ;\n"
                + "\n"
                + "\n"
                + "\n"
                + "/*cellule couche statique + etat */\n"
                + "repeat(1)\n"
                + "{\n"
                + "	jaune -> noir : coeur = noir; // coeur = 0\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "/*fonction sur couche statique + all*/\n"
                + "repeat(1)\n"
                + "{\n"
                + "	all -> rouge : coeur.north() = noir; // coeur = 0\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "/*remplissage du coeur*/\n"
                + "{\n"
                + "	blanc -> rose : nbCellCir(noir, 2)= 0;\n"
                + "	blanc -> rose : nbCellNeu(rose)>0;\n"
                + "}\n"
                + "\n"
                + "\n"
                + "//true + false + not + and + or + xor\n"
                + "{\n"
                + "	blanc -> rouge : not(false); //true\n"
                + "	rose -> jaune : true and false; //false\n"
                + "	rose -> bleu : (true or false) xor false; //true xor false = true\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "//nbCell + nbCellSq \n"
                + "while(nbCell(noir)<nbCell(bleu))\n"
                + "{\n"
                + "	bleu -> noir : nbCellSq(noir,2) > 0; \n"
                + "	noir -> bleu : nbCellSq(bleu,1) =1 ;\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "//all + nbCellCir + nbCellNeu\n"
                + "{\n"
                + "	bleu -> rose : pCellCir(noir,1)>0.25;	\n"
                + "	all -> bleu : pCellSq(rouge,1)>10%;\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "//test choix regle la plus forte \n"
                + "{ \n"
                + "	rose -> rouge : nbcellsq(3,1)>0;\n"
                + "	rose -> blanc : (nbcellsq(3,1)>0); // bleu = 3 \n"
                + "}\n"
                + " \n"
                + "\n"
                + "\n"
                + "//cellDuration\n"
                + "\n"
                + "	all -> vert : cellDuration()>8;\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "//north south east west northeast southwest\n"
                + "repeat(1){\n"
                + "	rose -> bleu : north() = noir and south() = noir ;\n"
                + "	noir -> jaune : east() = vert and west() = vert;\n"
                + "	rose -> rouge : northeast() = noir and southwest() = noir;\n"
                + "} \n"
                + "\n"
                + "\n"
                + "\n"
                + "repeat(5){\n"
                + "	bleu -> noir : nbCellNeu(bleu)< 4; //juste pour eleminer les cellule parasite ^^\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "// if + step() + % + m + ° + fonction()\n"
                + "if(step()>20){\n"
                + "\n"
                + "	noir -> vert : 0.2 = 20% ;\n"
                + "	jaune -> vert : 5m = 5;\n"
                + "	rouge -> vert : true; \n"
                + "	rose -> vert : nbCell(vert) > 110;\n"
                + "	blanc -> vert : true  ;\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "//  nbCellAng + pCellAng (fonction un peu lente) +  % \n"
                + "all -> violet :   nbCellAng(bleu,130,175,15) > 0 or nbCellAng(bleu,180,230,15)>0 or nbCellAng(bleu,310,0,15)>0 or nbCellAng(bleu,5,50,15)>0 ; \n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "//statique?etat\n"
                + "{\n"
                + "	all -> bleu : coeur?0;\n"
                + "}\n"
                + "\n"
                + "\n"
                + "\n"
                + "//fonction dans fonction\n"
                + "\n"
                + "repeat(1){\n"
                + "bleu -> jaune : round(5 mod (bleu-1)) = 1; // = 1\n"
                + "}\n"
                + "\n"
                + "repeat(1){\n"
                + "violet -> jaune :  nbCellSq(vert ,1) = 0;\n"
                + "}\n"
                + "\n"
                + "repeat(1){\n"
                + "noir,rose,blanc,bleu,rose2,jaune,rouge -> blanc : true;\n"
                + "}\n\n"
                + "all -> rouge : (south() = blanc) and (north() = vert) and ( West() = blanc or East() = blanc) ; \n"
                + "\n"
                + "repeat(3){\n"
                + "	vert -> rouge : south() = rouge  ; \n"
                + "}\n"
                + "vert -> bleu : south() = rouge and nbCellSq(rouge,1) <> 3 ;\n"
                + "vert -> bleu :nbCellSq(bleu,1) = 1 ;";
}
