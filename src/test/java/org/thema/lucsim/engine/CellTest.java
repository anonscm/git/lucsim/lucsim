/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.lucsim.engine;

import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import org.geotools.geometry.Envelope2D;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gvuidel
 */
public class CellTest {
    
    private static StateLayer layer, otherSameLayer, otherLayer;
    private static Envelope2D env;
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        WritableRaster img = Raster.createBandedRaster(DataBuffer.TYPE_BYTE, 100, 100, 1, null);
        img.setSample(50, 50, 0, 1);
        env = new Envelope2D(DefaultGeographicCRS.WGS84, 0, 0 - img.getHeight() * 1, img.getWidth() * 1, img.getHeight() * 1);
        layer = new StateLayer("main", img, env, new States());
        otherSameLayer = new StateLayer("other", img, env, new States());
        otherLayer = new StateLayer("other", img, new Envelope2D(DefaultGeographicCRS.WGS84, 0, 0 - img.getHeight() * 2, img.getWidth() * 2, img.getHeight() * 2), new States());
    }
    
    /**
     * Test of getStateImage method, of class Cell.
     */
    @Test
    public void testGetStateImage() {
        System.out.println("getStateImage");
        Cell cell = new Cell(layer);
        assertEquals(0, cell.getStateImage(layer), 0.0);
        cell.setPosition(50, 50);
        assertEquals(1, cell.getStateImage(otherSameLayer), 0.0);
        assertEquals(0, cell.getStateImage(otherLayer), 0.0);
        cell.setPosition(100, 100);
        assertEquals(1, cell.getStateImage(otherLayer), 0.0);
    }

    /**
     * Test of nbCellNeu method, of class Cell.
     */
    @Test
    public void testNbCellNeu() {
        System.out.println("nbCellNeu");
        Cell cell = new Cell(layer);
        assertEquals(2, cell.nbCellNeu(layer, 0));
        assertEquals(2, cell.nbCellNeu(otherSameLayer, 0));
        assertEquals(2, cell.nbCellNeu(otherLayer, 0));
        
        cell.setPosition(50, 50);
        assertEquals(4, cell.nbCellNeu(layer, 0));
        assertEquals(0, cell.nbCellNeu(layer, 1));
        assertEquals(4, cell.nbCellNeu(otherSameLayer, 0));
        assertEquals(0, cell.nbCellNeu(otherSameLayer, 1));
        
        cell.setPosition(49, 50);
        assertEquals(3, cell.nbCellNeu(layer, 0));
        assertEquals(1, cell.nbCellNeu(layer, 1));
        assertEquals(3, cell.nbCellNeu(otherSameLayer, 0));
        assertEquals(1, cell.nbCellNeu(otherSameLayer, 1));
    }

    /**
     * Test of nbCellMoo method, of class Cell.
     */
    @Test
    public void testNbCellMoo() {
        System.out.println("nbCellMoo");
        Cell cell = new Cell(layer);
        assertEquals(3, cell.nbCellMoo(layer, 0));
        
        cell.setPosition(50, 50);
        assertEquals(8, cell.nbCellMoo(layer, 0));
        assertEquals(0, cell.nbCellMoo(layer, 1));
        
        cell.setPosition(49, 49);
        assertEquals(7, cell.nbCellMoo(layer, 0));
        assertEquals(1, cell.nbCellMoo(layer, 1));
    }

    /**
     * Test of nbCellSq method, of class Cell.
     */
    @Test
    public void testNbCellSq() {
        System.out.println("nbCellSq");
        Cell cell = new Cell(layer);
        assertEquals(8, cell.nbCellSq(layer, 0, 2));
        
        cell.setPosition(50, 50);
        assertEquals(80, cell.nbCellSq(layer, 0, 4));
        assertEquals(0, cell.nbCellSq(layer, 1, 4));
        
        cell.setPosition(40, 40);
        assertEquals(439, cell.nbCellSq(layer, 0, 10));
        assertEquals(1, cell.nbCellSq(layer, 1, 10));
    }

    /**
     * Test of pCellSq method, of class Cell.
     */
    @Test
    public void testPCellSq() {
        System.out.println("pCellSq");
        Cell cell = new Cell(layer);
        assertEquals(1, cell.pCellSq(layer, 0, 2), 0);
        
        cell.setPosition(50, 50);
        assertEquals(1, cell.pCellSq(layer, 0, 4), 0);
        assertEquals(0, cell.pCellSq(layer, 1, 4), 0);
        
        cell.setPosition(40, 40);
        assertEquals(439.0/440, cell.pCellSq(layer, 0, 10), 0);
        assertEquals(1.0/440, cell.pCellSq(layer, 1, 10), 0);
    }

    /**
     * Test of nbCellCir method, of class Cell.
     */
    @Test
    public void testNbCellCir() {
        System.out.println("nbCellCir");
        Cell cell = new Cell(layer);
        assertEquals(5, cell.nbCellCir(layer, 0, 2));
        
        cell.setPosition(50, 50);
        assertEquals(48, cell.nbCellCir(layer, 0, 4));
        assertEquals(0, cell.nbCellCir(layer, 1, 4));
        
        cell.setPosition(43, 43);
        assertEquals(315, cell.nbCellCir(layer, 0, 10));
        assertEquals(1, cell.nbCellCir(layer, 1, 10));
    }

    /**
     * Test of pCellCir method, of class Cell.
     */
    @Test
    public void testPCellCir() {
        System.out.println("pCellCir");
        Cell cell = new Cell(layer);
        assertEquals(1, cell.pCellCir(layer, 0, 2), 0);
        
        cell.setPosition(50, 50);
        assertEquals(1, cell.pCellCir(layer, 0, 4), 0);
        assertEquals(0, cell.pCellCir(layer, 1, 4), 0);
        
        cell.setPosition(43, 43);
        assertEquals(315.0/316, cell.pCellCir(layer, 0, 10), 0);
        assertEquals(1.0/316, cell.pCellCir(layer, 1, 10), 0);
    }

    /**
     * Test of nbCellAng method, of class Cell.
     */
    @Test
    public void testNbCellAng() {
        System.out.println("nbCellAng");
        Cell cell = new Cell(layer);
        assertEquals(0, cell.nbCellAng(layer, 0, 0, 45, 10));
        assertEquals(0, cell.nbCellAng(layer, 0, 240, 270, 10));
        assertEquals(0, cell.nbCellAng(layer, 0, 300, 40, 10));
        assertEquals(5, cell.nbCellAng(layer, 0, 90, 180, 2));
        
        cell.setPosition(50, 50);
        assertEquals(316, cell.nbCellAng(layer, 0, 0, 360, 10));
        assertEquals(89, cell.nbCellAng(layer, 0, 90, 180, 10));
        assertEquals(48, cell.nbCellAng(layer, 0, 0, 45, 10));
        assertEquals(31, cell.nbCellAng(layer, 0, 240, 270, 10));
        assertEquals(86, cell.nbCellAng(layer, 0, 315, 45, 10));
        
        cell.setPosition(43, 43); // cell one is at south east (135°)
        assertEquals(315, cell.nbCellAng(layer, 0, 0, 360, 10));
        assertEquals(1, cell.nbCellAng(layer, 1, 90, 135, 10));
        assertEquals(1, cell.nbCellAng(layer, 1, 135, 180, 10));
        assertEquals(0, cell.nbCellAng(layer, 1, 136, 180, 10));
        
        cell.setPosition(60, 50); // cell one is at west (270°)
        assertEquals(315, cell.nbCellAng(layer, 0, 0, 360, 10));
        assertEquals(1, cell.nbCellAng(layer, 1, 270, 360, 10));
        assertEquals(1, cell.nbCellAng(layer, 1, 180, 270, 10));
        assertEquals(0, cell.nbCellAng(layer, 1, 180, 269, 10));
        
    }

    /**
     * Test of pCellAng method, of class Cell.
     */
    @Test
    public void testPCellAng() {
        System.out.println("pCellAng");
        Cell cell = new Cell(layer);
        assertEquals(0, cell.pCellAng(layer, 0, 0, 45, 10), 0);
        assertEquals(0, cell.pCellAng(layer, 0, 240, 270, 10), 0);
        assertEquals(0, cell.pCellAng(layer, 0, 300, 40, 10), 0);
        assertEquals(1, cell.pCellAng(layer, 0, 90, 180, 2), 0);
        
        cell.setPosition(50, 50);
        assertEquals(1, cell.pCellAng(layer, 0, 0, 360, 10), 0);
        assertEquals(1, cell.pCellAng(layer, 0, 90, 180, 10), 0);
        assertEquals(1, cell.pCellAng(layer, 0, 0, 45, 10), 0);
        assertEquals(1, cell.pCellAng(layer, 0, 240, 270, 10), 0);
        assertEquals(1, cell.pCellAng(layer, 0, 315, 45, 10), 0);
        
        cell.setPosition(43, 43); // cell one is at south east (135°)
        assertEquals(315.0/316, cell.pCellAng(layer, 0, 0, 360, 10), 0);
        assertEquals(1.0/48, cell.pCellAng(layer, 1, 90, 135, 10), 0);
        assertEquals(1.0/48, cell.pCellAng(layer, 1, 135, 180, 10), 0);
        assertEquals(0, cell.pCellAng(layer, 1, 136, 180, 10), 0);
        
        cell.setPosition(60, 50); // cell one is at west (270°)
        assertEquals(315.0/316, cell.pCellAng(layer, 0, 0, 360, 10), 0);
        assertEquals(1.0/89, cell.pCellAng(layer, 1, 270, 0, 10), 0);
        assertEquals(1.0/89, cell.pCellAng(layer, 1, 180, 270, 10), 0);
        assertEquals(0, cell.pCellAng(layer, 1, 180, 269, 10), 0);
    }

    /**
     * Test of north method, of class Cell.
     */
    @Test
    public void testNorth() {
        System.out.println("north");
        Cell cell = new Cell(layer);
        assertEquals(Double.NaN, cell.north(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.north(layer), 0);
        
        cell.setPosition(50, 51);
        assertEquals(1, cell.north(layer), 0);
    }

    /**
     * Test of south method, of class Cell.
     */
    @Test
    public void testSouth() {
        System.out.println("south");
        Cell cell = new Cell(layer);
        assertEquals(0, cell.south(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.south(layer), 0);
        
        cell.setPosition(50, 49);
        assertEquals(1, cell.south(layer), 0);
    }

    /**
     * Test of east method, of class Cell.
     */
    @Test
    public void testEast() {
        System.out.println("east");
        Cell cell = new Cell(layer);
        assertEquals(0, cell.east(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.east(layer), 0);
        
        cell.setPosition(49, 50);
        assertEquals(1, cell.east(layer), 0);
    }

    /**
     * Test of west method, of class Cell.
     */
    @Test
    public void testWest() {
        System.out.println("west");
        Cell cell = new Cell(layer);
        assertEquals(Double.NaN, cell.west(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.west(layer), 0);
        
        cell.setPosition(51, 50);
        assertEquals(1, cell.west(layer), 0);
    }

    /**
     * Test of northEast method, of class Cell.
     */
    @Test
    public void testNorthEast() {
        System.out.println("northEast");
        Cell cell = new Cell(layer);
        assertEquals(Double.NaN, cell.northEast(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.northEast(layer), 0);
        
        cell.setPosition(49, 51);
        assertEquals(1, cell.northEast(layer), 0);
    }

    /**
     * Test of northWest method, of class Cell.
     */
    @Test
    public void testNorthWest() {
        System.out.println("northWest");
        Cell cell = new Cell(layer);
        assertEquals(Double.NaN, cell.northWest(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.northWest(layer), 0);
        
        cell.setPosition(51, 51);
        assertEquals(1, cell.northWest(layer), 0);
    }

    /**
     * Test of southEast method, of class Cell.
     */
    @Test
    public void testSouthEast() {
        System.out.println("southEast");
        Cell cell = new Cell(layer);
        assertEquals(0, cell.southEast(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.southEast(layer), 0);
        
        cell.setPosition(49, 49);
        assertEquals(1, cell.southEast(layer), 0);
    }

    /**
     * Test of southWest method, of class Cell.
     */
    @Test
    public void testSouthWest() {
        System.out.println("southWest");
        Cell cell = new Cell(layer);
        assertEquals(Double.NaN, cell.southWest(layer), 0);
        
        cell.setPosition(50, 50);
        assertEquals(0, cell.southWest(layer), 0);
        
        cell.setPosition(51, 49);
        assertEquals(1, cell.southWest(layer), 0);
    }
    
}
