/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.analysis;

import org.thema.lucsim.generator.dt.IncrementalClassifierRunnable;
import org.thema.lucsim.generator.dt.BatchClassifierRunnable;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.imageio.ImageIO;
import org.geotools.geometry.Envelope2D;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.thema.common.ProgressBar;
import org.thema.common.swing.TaskMonitor;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.Project;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.engine.States;
import org.thema.lucsim.stat.CalcDataExtractor;
import org.thema.lucsim.stat.FileDataExtractor;

public class TestAnalysis {

    private static Envelope2D envelope2d;
    private static StateLayer formerLayer, modernLayer;
    private static Project project;

    @BeforeClass
    public static void init() throws IOException {
        
        RenderedImage formerImage = ImageIO.read(new File("src/test/java/image/initial/main11.tif"));
        envelope2d = new Envelope2D(DefaultGeographicCRS.WGS84, 0, 0 - formerImage.getHeight() * 1, formerImage.getWidth() * 1, formerImage.getHeight() * 1);
        formerLayer = new StateLayer("main11", formerImage, envelope2d, new States());
        project = new Project(formerLayer);
        
        RenderedImage modernImage = ImageIO.read(new File("src/test/java/image/initial/main12.tif"));
        envelope2d = new Envelope2D(DefaultGeographicCRS.WGS84, 0, 0 - modernImage.getHeight() * 1, formerImage.getWidth() * 1, formerImage.getHeight() * 1);
        modernLayer = new StateLayer("main12", modernImage, envelope2d, project.getStates());
        project.addLayer(modernLayer);
    }

    @Test
    public void j48TreeWithImportedDataSet() throws Exception {
        double trainingRatio = 0.75;
        List<Double> params = Collections.EMPTY_LIST;
        File inputFile = new File("src/test/java/csv/exported_data.csv");
        ProgressBar progressBar = new TaskMonitor.EmptyMonitor();

        BatchClassifierRunnable batchClassifier = new BatchClassifierRunnable(
                new FileDataExtractor(inputFile, project),
                trainingRatio,
                params,
                progressBar,
                1
        );

        batchClassifier.run();

        Assert.assertEquals("J48 tree structure (imported)", J48_TREE_STR, batchClassifier.getTreeStr().trim());
    }

    @Test
    public void J48TreeWithBuiltDataSet() throws Exception {
        List<Layer> layers = new ArrayList<>();
        State targetState = formerLayer.getStates().getState("state3");
        String functionStr = "nbCellSq";
        boolean ring = false;
        int minRadius = 1;
        int maxRadius = 10;
        int step = 1;
        double weight = 1.0;
        double trainingRatio = 0.75;
        List<Double> params = Collections.EMPTY_LIST;
        ProgressBar progressBar = new TaskMonitor.EmptyMonitor();

        BatchClassifierRunnable batchClassifier = new BatchClassifierRunnable(
                new CalcDataExtractor(formerLayer, modernLayer, layers, functionStr, ring, minRadius, maxRadius, step, targetState, weight),
                trainingRatio,
                params,
                progressBar,
                1
        );

        batchClassifier.run();

        Assert.assertEquals("J48 tree structure (built)", J48_TREE_STR, batchClassifier.getTreeStr().trim());
    }

    @Test
    public void hoeffdingTreeWithBuiltDataSet() throws Exception {
        List<Layer> layers = new ArrayList<>();
        State targetState = formerLayer.getStates().getState("state3");
        String functionStr = "nbCellSq";
        boolean ring = false;
        int minRadius = 1;
        int maxRadius = 10;
        int step = 1;
        double weight = 1.0;
        List<Double> params = Collections.EMPTY_LIST;
        ProgressBar progressBar = new TaskMonitor.EmptyMonitor();

        IncrementalClassifierRunnable incrementalClassifier = new IncrementalClassifierRunnable(
                new CalcDataExtractor(formerLayer, modernLayer, layers, functionStr, ring, minRadius, maxRadius, step, targetState, weight),
                params,
                progressBar
        );

        incrementalClassifier.run();

        Assert.assertEquals("Hoeffding tree structure (built)", HOEFFDING_TREE_STR, incrementalClassifier.getTreeStr().replace(",", ".").trim());
    }
    
    @Test
    public void j48TreeConfusionMatrixFromTrainingData() throws Exception {
        List<Layer> layers = new ArrayList<>();
        State targetState = formerLayer.getStates().getState("state3");
        String functionStr = "nbCellSq";
        boolean ring = false;
        int minRadius = 1;
        int maxRadius = 10;
        int step = 1;
        double weight = 1.0;
        double trainingRatio = 0.75;
        List<Double> params = Collections.EMPTY_LIST;
        ProgressBar progressBar = new TaskMonitor.EmptyMonitor();

        BatchClassifierRunnable batchClassifier = new BatchClassifierRunnable(
                new CalcDataExtractor(formerLayer, modernLayer, layers, functionStr, ring, minRadius, maxRadius, step, targetState, weight),
                trainingRatio,
                params,
                progressBar,
                1
        );

        batchClassifier.run();
        
        Assert.assertEquals("Batch classifier TP", 12, batchClassifier.getConfusionMatrixFromTrainingData().get("TP"), 0);
        Assert.assertEquals("Batch classifier TN", 3023, batchClassifier.getConfusionMatrixFromTrainingData().get("TN"), 0);
        Assert.assertEquals("Batch classifier FP", 0, batchClassifier.getConfusionMatrixFromTrainingData().get("FP"), 0);
        Assert.assertEquals("Batch classifier FN", 0, batchClassifier.getConfusionMatrixFromTrainingData().get("FN"), 0);
    }
    
    @Test
    public void j48TreeConfusionMatrixFromTestData() throws Exception {
        List<Layer> layers = new ArrayList<>();
        State targetState = formerLayer.getStates().getState("state3");
        String functionStr = "nbCellSq";
        boolean ring = false;
        int minRadius = 1;
        int maxRadius = 10;
        int step = 1;
        double weight = 1.0;
        double trainingRatio = 0.75;
        List<Double> params = Collections.EMPTY_LIST;
        ProgressBar progressBar = new TaskMonitor.EmptyMonitor();

        BatchClassifierRunnable batchClassifier = new BatchClassifierRunnable(
                new CalcDataExtractor(formerLayer, modernLayer, layers, functionStr, ring, minRadius, maxRadius, step, targetState, weight),
                trainingRatio,
                params,
                progressBar,
                1
        );

        batchClassifier.run();
        
        Assert.assertEquals("Batch classifier TP", 7, batchClassifier.getConfusionMatrixFromTestData().get("TP"), 0);
        Assert.assertEquals("Batch classifier TN", 1005, batchClassifier.getConfusionMatrixFromTestData().get("TN"), 0);
        Assert.assertEquals("Batch classifier FP", 0, batchClassifier.getConfusionMatrixFromTestData().get("FP"), 0);
        Assert.assertEquals("Batch classifier FN", 0, batchClassifier.getConfusionMatrixFromTestData().get("FN"), 0);
    }
    
    @Test
    public void hoeffdingTreeConfusionMatrix() throws Exception {
        List<Layer> layers = new ArrayList<>();
        State targetState = formerLayer.getStates().getState("state3");
        String functionStr = "nbCellSq";
        boolean ring = false;
        int minRadius = 1;
        int maxRadius = 10;
        int step = 1;
        double weight = 1.0;
        List<Double> params = Collections.EMPTY_LIST;
        ProgressBar progressBar = new TaskMonitor.EmptyMonitor();

        IncrementalClassifierRunnable incrementalClassifier = new IncrementalClassifierRunnable(
                new CalcDataExtractor(formerLayer, modernLayer, layers, functionStr, ring, minRadius, maxRadius, step, targetState, weight),
                params,
                progressBar
        );

        incrementalClassifier.run();
                
        Assert.assertEquals("Batch classifier TP", 0, incrementalClassifier.getConfusionMatrixFromTrainingData().get("TP"), 0);
        Assert.assertEquals("Batch classifier TN", 4028, incrementalClassifier.getConfusionMatrixFromTrainingData().get("TN"), 0);
        Assert.assertEquals("Batch classifier FP", 0, incrementalClassifier.getConfusionMatrixFromTrainingData().get("FP"), 0);
        Assert.assertEquals("Batch classifier FN", 19, incrementalClassifier.getConfusionMatrixFromTrainingData().get("FN"), 0);
    }

    public static final String J48_TREE_STR
            = "J48 pruned tree\n"
            + "------------------\n"
            + "\n"
            + "nbCellSq_state3_1px <= 1\n"
            + "|   nbCellSq_state3_1px <= 0: false (26.0)\n"
            + "|   nbCellSq_state3_1px > 0\n"
            + "|   |   nbCellSq_state0_10px <= 83: true (12.0)\n"
            + "|   |   nbCellSq_state0_10px > 83: false (15.0)\n"
            + "nbCellSq_state3_1px > 1: false (2982.0)\n"
            + "\n"
            + "Number of Leaves  : 	4\n"
            + "\n"
            + "Size of the tree : 	7";

    public static final String HOEFFDING_TREE_STR
            = "nbCellSq_state0_2px <= 6.545: false (4012.023) NB1 NB adaptive1\n"
            + "nbCellSq_state0_2px > 6.545: true (19.000) NB2 NB adaptive2";
}
