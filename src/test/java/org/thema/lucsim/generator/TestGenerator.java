/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.lucsim.generator;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.geotools.geometry.Envelope2D;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.thema.lucsim.engine.Layer;
import org.thema.lucsim.engine.RulesBlock;
import org.thema.lucsim.engine.RulesBlock.BlockType;
import org.thema.lucsim.engine.State;
import org.thema.lucsim.engine.StateLayer;
import org.thema.lucsim.engine.States;
import org.thema.lucsim.generator.dt.RulesGeneratorDT;

public class TestGenerator {

    private static Envelope2D envelope2d;
    private static StateLayer formerLayer;

    @BeforeClass
    public static void init() throws IOException {
        RenderedImage formerImage = ImageIO.read(new File("src/test/java/image/initial/main11.tif"));
        envelope2d = new Envelope2D(DefaultGeographicCRS.WGS84, 0, 0 - formerImage.getHeight() * 1, formerImage.getWidth() * 1, formerImage.getHeight() * 1);
        formerLayer = new StateLayer("main11", formerImage, envelope2d, new States());
    }

    @Test
    public void rulesGenerator() {
        List<Layer> layers = new ArrayList<>();
        State targetState = formerLayer.getStates().getState("state3");
        String function = "nbCellSq";

        RulesGeneratorDT rulesGenerator = new RulesGeneratorDT(J48_TREE,
                formerLayer,
                layers,
                targetState,
                function, 1);
        rulesGenerator.extractRulesFromTree();

        RulesBlock rulesBlock = new RulesBlock(BlockType.SIMPLE);
        rulesGenerator.getRules().stream().forEach((rule) -> {
            rulesBlock.add(rule.getRule());
        });

        Assert.assertEquals("Rules generator", EXPECTED_RULES, rulesBlock.toString().trim());
    }

    public static final String J48_TREE
            = "digraph J48Tree {\n"
            + "N0 [label=\"nbCellSq_state3_1px\" ]\n"
            + "N0->N1 [label=\"<= 1\"]\n"
            + "N1 [label=\"nbCellSq_state3_1px\" ]\n"
            + "N1->N2 [label=\"<= 0\"]\n"
            + "N2 [label=\"false (38.0)\" shape=box style=filled ]\n"
            + "N1->N3 [label=\"> 0\"]\n"
            + "N3 [label=\"nbCellSq_state0_10px\" ]\n"
            + "N3->N4 [label=\"<= 83\"]\n"
            + "N4 [label=\"true (12.0)\" shape=box style=filled ]\n"
            + "N3->N5 [label=\"> 83\"]\n"
            + "N5 [label=\"false (15.0)\" shape=box style=filled ]\n"
            + "N0->N6 [label=\"> 1\"]\n"
            + "N6 [label=\"false (3173.0)\" shape=box style=filled ]\n"
            + "}";

    public static final String EXPECTED_RULES 
            = "{\n"
            + "\tall -> state3 : nbCellSq(state3,1) <= 1.0 and nbCellSq(state3,1) > 0.0 and nbCellSq(state0,10) <= 83.0;"
            + "\n}";
}
